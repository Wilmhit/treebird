#ifndef EMOJOS_H
#define EMOJOS_H
#include "emoji_info.h"
#define emojos_size 4590
#define EMOJO_CAT_SMILEY 0
#define EMOJO_CAT_ANIMALS 3078
#define EMOJO_CAT_FOOD 3225
#define EMOJO_CAT_TRAVEL 3356
#define EMOJO_CAT_ACTIVITIES 3620
#define EMOJO_CAT_OBJECTS 3715
#define EMOJO_CAT_SYMBOLS 4014
#define EMOJO_CAT_FLAGS 4315
#define EMOJO_CAT_MAX 4590
static struct emoji_info emojos[] = {{"😀","grinning face" }, /*0 : Smileys & Emotion*/
{"😃","grinning face with big eyes" }, /*1 : Smileys & Emotion*/
{"😄","grinning face with smiling eyes" }, /*2 : Smileys & Emotion*/
{"😁","beaming face with smiling eyes" }, /*3 : Smileys & Emotion*/
{"😆","grinning squinting face" }, /*4 : Smileys & Emotion*/
{"😅","grinning face with sweat" }, /*5 : Smileys & Emotion*/
{"🤣","rolling on the floor laughing" }, /*6 : Smileys & Emotion*/
{"😂","face with tears of joy" }, /*7 : Smileys & Emotion*/
{"🙂","slightly smiling face" }, /*8 : Smileys & Emotion*/
{"🙃","upside-down face" }, /*9 : Smileys & Emotion*/
{"😉","winking face" }, /*10 : Smileys & Emotion*/
{"😊","smiling face with smiling eyes" }, /*11 : Smileys & Emotion*/
{"😇","smiling face with halo" }, /*12 : Smileys & Emotion*/
{"🥰","smiling face with hearts" }, /*13 : Smileys & Emotion*/
{"😍","smiling face with heart-eyes" }, /*14 : Smileys & Emotion*/
{"🤩","star-struck" }, /*15 : Smileys & Emotion*/
{"😘","face blowing a kiss" }, /*16 : Smileys & Emotion*/
{"😗","kissing face" }, /*17 : Smileys & Emotion*/
{"☺️","smiling face" }, /*18 : Smileys & Emotion*/
{"☺","smiling face" }, /*19 : Smileys & Emotion*/
{"😚","kissing face with closed eyes" }, /*20 : Smileys & Emotion*/
{"😙","kissing face with smiling eyes" }, /*21 : Smileys & Emotion*/
{"🥲","smiling face with tear" }, /*22 : Smileys & Emotion*/
{"😋","face savoring food" }, /*23 : Smileys & Emotion*/
{"😛","face with tongue" }, /*24 : Smileys & Emotion*/
{"😜","winking face with tongue" }, /*25 : Smileys & Emotion*/
{"🤪","zany face" }, /*26 : Smileys & Emotion*/
{"😝","squinting face with tongue" }, /*27 : Smileys & Emotion*/
{"🤑","money-mouth face" }, /*28 : Smileys & Emotion*/
{"🤗","hugging face" }, /*29 : Smileys & Emotion*/
{"🤭","face with hand over mouth" }, /*30 : Smileys & Emotion*/
{"🤫","shushing face" }, /*31 : Smileys & Emotion*/
{"🤔","thinking face" }, /*32 : Smileys & Emotion*/
{"🤐","zipper-mouth face" }, /*33 : Smileys & Emotion*/
{"🤨","face with raised eyebrow" }, /*34 : Smileys & Emotion*/
{"😐","neutral face" }, /*35 : Smileys & Emotion*/
{"😑","expressionless face" }, /*36 : Smileys & Emotion*/
{"😶","face without mouth" }, /*37 : Smileys & Emotion*/
{"😶‍🌫️","face in clouds" }, /*38 : Smileys & Emotion*/
{"😶‍🌫","face in clouds" }, /*39 : Smileys & Emotion*/
{"😏","smirking face" }, /*40 : Smileys & Emotion*/
{"😒","unamused face" }, /*41 : Smileys & Emotion*/
{"🙄","face with rolling eyes" }, /*42 : Smileys & Emotion*/
{"😬","grimacing face" }, /*43 : Smileys & Emotion*/
{"😮‍💨","face exhaling" }, /*44 : Smileys & Emotion*/
{"🤥","lying face" }, /*45 : Smileys & Emotion*/
{"😌","relieved face" }, /*46 : Smileys & Emotion*/
{"😔","pensive face" }, /*47 : Smileys & Emotion*/
{"😪","sleepy face" }, /*48 : Smileys & Emotion*/
{"🤤","drooling face" }, /*49 : Smileys & Emotion*/
{"😴","sleeping face" }, /*50 : Smileys & Emotion*/
{"😷","face with medical mask" }, /*51 : Smileys & Emotion*/
{"🤒","face with thermometer" }, /*52 : Smileys & Emotion*/
{"🤕","face with head-bandage" }, /*53 : Smileys & Emotion*/
{"🤢","nauseated face" }, /*54 : Smileys & Emotion*/
{"🤮","face vomiting" }, /*55 : Smileys & Emotion*/
{"🤧","sneezing face" }, /*56 : Smileys & Emotion*/
{"🥵","hot face" }, /*57 : Smileys & Emotion*/
{"🥶","cold face" }, /*58 : Smileys & Emotion*/
{"🥴","woozy face" }, /*59 : Smileys & Emotion*/
{"😵","knocked-out face" }, /*60 : Smileys & Emotion*/
{"😵‍💫","face with spiral eyes" }, /*61 : Smileys & Emotion*/
{"🤯","exploding head" }, /*62 : Smileys & Emotion*/
{"🤠","cowboy hat face" }, /*63 : Smileys & Emotion*/
{"🥳","partying face" }, /*64 : Smileys & Emotion*/
{"🥸","disguised face" }, /*65 : Smileys & Emotion*/
{"😎","smiling face with sunglasses" }, /*66 : Smileys & Emotion*/
{"🤓","nerd face" }, /*67 : Smileys & Emotion*/
{"🧐","face with monocle" }, /*68 : Smileys & Emotion*/
{"😕","confused face" }, /*69 : Smileys & Emotion*/
{"😟","worried face" }, /*70 : Smileys & Emotion*/
{"🙁","slightly frowning face" }, /*71 : Smileys & Emotion*/
{"☹️","frowning face" }, /*72 : Smileys & Emotion*/
{"☹","frowning face" }, /*73 : Smileys & Emotion*/
{"😮","face with open mouth" }, /*74 : Smileys & Emotion*/
{"😯","hushed face" }, /*75 : Smileys & Emotion*/
{"😲","astonished face" }, /*76 : Smileys & Emotion*/
{"😳","flushed face" }, /*77 : Smileys & Emotion*/
{"🥺","pleading face" }, /*78 : Smileys & Emotion*/
{"😦","frowning face with open mouth" }, /*79 : Smileys & Emotion*/
{"😧","anguished face" }, /*80 : Smileys & Emotion*/
{"😨","fearful face" }, /*81 : Smileys & Emotion*/
{"😰","anxious face with sweat" }, /*82 : Smileys & Emotion*/
{"😥","sad but relieved face" }, /*83 : Smileys & Emotion*/
{"😢","crying face" }, /*84 : Smileys & Emotion*/
{"😭","loudly crying face" }, /*85 : Smileys & Emotion*/
{"😱","face screaming in fear" }, /*86 : Smileys & Emotion*/
{"😖","confounded face" }, /*87 : Smileys & Emotion*/
{"😣","persevering face" }, /*88 : Smileys & Emotion*/
{"😞","disappointed face" }, /*89 : Smileys & Emotion*/
{"😓","downcast face with sweat" }, /*90 : Smileys & Emotion*/
{"😩","weary face" }, /*91 : Smileys & Emotion*/
{"😫","tired face" }, /*92 : Smileys & Emotion*/
{"🥱","yawning face" }, /*93 : Smileys & Emotion*/
{"😤","face with steam from nose" }, /*94 : Smileys & Emotion*/
{"😡","pouting face" }, /*95 : Smileys & Emotion*/
{"😠","angry face" }, /*96 : Smileys & Emotion*/
{"🤬","face with symbols on mouth" }, /*97 : Smileys & Emotion*/
{"😈","smiling face with horns" }, /*98 : Smileys & Emotion*/
{"👿","angry face with horns" }, /*99 : Smileys & Emotion*/
{"💀","skull" }, /*100 : Smileys & Emotion*/
{"☠️","skull and crossbones" }, /*101 : Smileys & Emotion*/
{"☠","skull and crossbones" }, /*102 : Smileys & Emotion*/
{"💩","pile of poo" }, /*103 : Smileys & Emotion*/
{"🤡","clown face" }, /*104 : Smileys & Emotion*/
{"👹","ogre" }, /*105 : Smileys & Emotion*/
{"👺","goblin" }, /*106 : Smileys & Emotion*/
{"👻","ghost" }, /*107 : Smileys & Emotion*/
{"👽","alien" }, /*108 : Smileys & Emotion*/
{"👾","alien monster" }, /*109 : Smileys & Emotion*/
{"🤖","robot" }, /*110 : Smileys & Emotion*/
{"😺","grinning cat" }, /*111 : Smileys & Emotion*/
{"😸","grinning cat with smiling eyes" }, /*112 : Smileys & Emotion*/
{"😹","cat with tears of joy" }, /*113 : Smileys & Emotion*/
{"😻","smiling cat with heart-eyes" }, /*114 : Smileys & Emotion*/
{"😼","cat with wry smile" }, /*115 : Smileys & Emotion*/
{"😽","kissing cat" }, /*116 : Smileys & Emotion*/
{"🙀","weary cat" }, /*117 : Smileys & Emotion*/
{"😿","crying cat" }, /*118 : Smileys & Emotion*/
{"😾","pouting cat" }, /*119 : Smileys & Emotion*/
{"🙈","see-no-evil monkey" }, /*120 : Smileys & Emotion*/
{"🙉","hear-no-evil monkey" }, /*121 : Smileys & Emotion*/
{"🙊","speak-no-evil monkey" }, /*122 : Smileys & Emotion*/
{"💋","kiss mark" }, /*123 : Smileys & Emotion*/
{"💌","love letter" }, /*124 : Smileys & Emotion*/
{"💘","heart with arrow" }, /*125 : Smileys & Emotion*/
{"💝","heart with ribbon" }, /*126 : Smileys & Emotion*/
{"💖","sparkling heart" }, /*127 : Smileys & Emotion*/
{"💗","growing heart" }, /*128 : Smileys & Emotion*/
{"💓","beating heart" }, /*129 : Smileys & Emotion*/
{"💞","revolving hearts" }, /*130 : Smileys & Emotion*/
{"💕","two hearts" }, /*131 : Smileys & Emotion*/
{"💟","heart decoration" }, /*132 : Smileys & Emotion*/
{"❣️","heart exclamation" }, /*133 : Smileys & Emotion*/
{"❣","heart exclamation" }, /*134 : Smileys & Emotion*/
{"💔","broken heart" }, /*135 : Smileys & Emotion*/
{"❤️‍🔥","heart on fire" }, /*136 : Smileys & Emotion*/
{"❤‍🔥","heart on fire" }, /*137 : Smileys & Emotion*/
{"❤️‍🩹","mending heart" }, /*138 : Smileys & Emotion*/
{"❤‍🩹","mending heart" }, /*139 : Smileys & Emotion*/
{"❤️","red heart" }, /*140 : Smileys & Emotion*/
{"❤","red heart" }, /*141 : Smileys & Emotion*/
{"🧡","orange heart" }, /*142 : Smileys & Emotion*/
{"💛","yellow heart" }, /*143 : Smileys & Emotion*/
{"💚","green heart" }, /*144 : Smileys & Emotion*/
{"💙","blue heart" }, /*145 : Smileys & Emotion*/
{"💜","purple heart" }, /*146 : Smileys & Emotion*/
{"🤎","brown heart" }, /*147 : Smileys & Emotion*/
{"🖤","black heart" }, /*148 : Smileys & Emotion*/
{"🤍","white heart" }, /*149 : Smileys & Emotion*/
{"💯","hundred points" }, /*150 : Smileys & Emotion*/
{"💢","anger symbol" }, /*151 : Smileys & Emotion*/
{"💥","collision" }, /*152 : Smileys & Emotion*/
{"💫","dizzy" }, /*153 : Smileys & Emotion*/
{"💦","sweat droplets" }, /*154 : Smileys & Emotion*/
{"💨","dashing away" }, /*155 : Smileys & Emotion*/
{"🕳️","hole" }, /*156 : Smileys & Emotion*/
{"🕳","hole" }, /*157 : Smileys & Emotion*/
{"💣","bomb" }, /*158 : Smileys & Emotion*/
{"💬","speech balloon" }, /*159 : Smileys & Emotion*/
{"👁️‍🗨️","eye in speech bubble" }, /*160 : Smileys & Emotion*/
{"👁‍🗨️","eye in speech bubble" }, /*161 : Smileys & Emotion*/
{"👁️‍🗨","eye in speech bubble" }, /*162 : Smileys & Emotion*/
{"👁‍🗨","eye in speech bubble" }, /*163 : Smileys & Emotion*/
{"🗨️","left speech bubble" }, /*164 : Smileys & Emotion*/
{"🗨","left speech bubble" }, /*165 : Smileys & Emotion*/
{"🗯️","right anger bubble" }, /*166 : Smileys & Emotion*/
{"🗯","right anger bubble" }, /*167 : Smileys & Emotion*/
{"💭","thought balloon" }, /*168 : Smileys & Emotion*/
{"💤","zzz" }, /*169 : Smileys & Emotion*/
{"👋","waving hand" }, /*170 : People & Body*/
{"👋🏻","waving hand: light skin tone" }, /*171 : People & Body*/
{"👋🏼","waving hand: medium-light skin tone" }, /*172 : People & Body*/
{"👋🏽","waving hand: medium skin tone" }, /*173 : People & Body*/
{"👋🏾","waving hand: medium-dark skin tone" }, /*174 : People & Body*/
{"👋🏿","waving hand: dark skin tone" }, /*175 : People & Body*/
{"🤚","raised back of hand" }, /*176 : People & Body*/
{"🤚🏻","raised back of hand: light skin tone" }, /*177 : People & Body*/
{"🤚🏼","raised back of hand: medium-light skin tone" }, /*178 : People & Body*/
{"🤚🏽","raised back of hand: medium skin tone" }, /*179 : People & Body*/
{"🤚🏾","raised back of hand: medium-dark skin tone" }, /*180 : People & Body*/
{"🤚🏿","raised back of hand: dark skin tone" }, /*181 : People & Body*/
{"🖐️","hand with fingers splayed" }, /*182 : People & Body*/
{"🖐","hand with fingers splayed" }, /*183 : People & Body*/
{"🖐🏻","hand with fingers splayed: light skin tone" }, /*184 : People & Body*/
{"🖐🏼","hand with fingers splayed: medium-light skin tone" }, /*185 : People & Body*/
{"🖐🏽","hand with fingers splayed: medium skin tone" }, /*186 : People & Body*/
{"🖐🏾","hand with fingers splayed: medium-dark skin tone" }, /*187 : People & Body*/
{"🖐🏿","hand with fingers splayed: dark skin tone" }, /*188 : People & Body*/
{"✋","raised hand" }, /*189 : People & Body*/
{"✋🏻","raised hand: light skin tone" }, /*190 : People & Body*/
{"✋🏼","raised hand: medium-light skin tone" }, /*191 : People & Body*/
{"✋🏽","raised hand: medium skin tone" }, /*192 : People & Body*/
{"✋🏾","raised hand: medium-dark skin tone" }, /*193 : People & Body*/
{"✋🏿","raised hand: dark skin tone" }, /*194 : People & Body*/
{"🖖","vulcan salute" }, /*195 : People & Body*/
{"🖖🏻","vulcan salute: light skin tone" }, /*196 : People & Body*/
{"🖖🏼","vulcan salute: medium-light skin tone" }, /*197 : People & Body*/
{"🖖🏽","vulcan salute: medium skin tone" }, /*198 : People & Body*/
{"🖖🏾","vulcan salute: medium-dark skin tone" }, /*199 : People & Body*/
{"🖖🏿","vulcan salute: dark skin tone" }, /*200 : People & Body*/
{"👌","OK hand" }, /*201 : People & Body*/
{"👌🏻","OK hand: light skin tone" }, /*202 : People & Body*/
{"👌🏼","OK hand: medium-light skin tone" }, /*203 : People & Body*/
{"👌🏽","OK hand: medium skin tone" }, /*204 : People & Body*/
{"👌🏾","OK hand: medium-dark skin tone" }, /*205 : People & Body*/
{"👌🏿","OK hand: dark skin tone" }, /*206 : People & Body*/
{"🤌","pinched fingers" }, /*207 : People & Body*/
{"🤌🏻","pinched fingers: light skin tone" }, /*208 : People & Body*/
{"🤌🏼","pinched fingers: medium-light skin tone" }, /*209 : People & Body*/
{"🤌🏽","pinched fingers: medium skin tone" }, /*210 : People & Body*/
{"🤌🏾","pinched fingers: medium-dark skin tone" }, /*211 : People & Body*/
{"🤌🏿","pinched fingers: dark skin tone" }, /*212 : People & Body*/
{"🤏","pinching hand" }, /*213 : People & Body*/
{"🤏🏻","pinching hand: light skin tone" }, /*214 : People & Body*/
{"🤏🏼","pinching hand: medium-light skin tone" }, /*215 : People & Body*/
{"🤏🏽","pinching hand: medium skin tone" }, /*216 : People & Body*/
{"🤏🏾","pinching hand: medium-dark skin tone" }, /*217 : People & Body*/
{"🤏🏿","pinching hand: dark skin tone" }, /*218 : People & Body*/
{"✌️","victory hand" }, /*219 : People & Body*/
{"✌","victory hand" }, /*220 : People & Body*/
{"✌🏻","victory hand: light skin tone" }, /*221 : People & Body*/
{"✌🏼","victory hand: medium-light skin tone" }, /*222 : People & Body*/
{"✌🏽","victory hand: medium skin tone" }, /*223 : People & Body*/
{"✌🏾","victory hand: medium-dark skin tone" }, /*224 : People & Body*/
{"✌🏿","victory hand: dark skin tone" }, /*225 : People & Body*/
{"🤞","crossed fingers" }, /*226 : People & Body*/
{"🤞🏻","crossed fingers: light skin tone" }, /*227 : People & Body*/
{"🤞🏼","crossed fingers: medium-light skin tone" }, /*228 : People & Body*/
{"🤞🏽","crossed fingers: medium skin tone" }, /*229 : People & Body*/
{"🤞🏾","crossed fingers: medium-dark skin tone" }, /*230 : People & Body*/
{"🤞🏿","crossed fingers: dark skin tone" }, /*231 : People & Body*/
{"🤟","love-you gesture" }, /*232 : People & Body*/
{"🤟🏻","love-you gesture: light skin tone" }, /*233 : People & Body*/
{"🤟🏼","love-you gesture: medium-light skin tone" }, /*234 : People & Body*/
{"🤟🏽","love-you gesture: medium skin tone" }, /*235 : People & Body*/
{"🤟🏾","love-you gesture: medium-dark skin tone" }, /*236 : People & Body*/
{"🤟🏿","love-you gesture: dark skin tone" }, /*237 : People & Body*/
{"🤘","sign of the horns" }, /*238 : People & Body*/
{"🤘🏻","sign of the horns: light skin tone" }, /*239 : People & Body*/
{"🤘🏼","sign of the horns: medium-light skin tone" }, /*240 : People & Body*/
{"🤘🏽","sign of the horns: medium skin tone" }, /*241 : People & Body*/
{"🤘🏾","sign of the horns: medium-dark skin tone" }, /*242 : People & Body*/
{"🤘🏿","sign of the horns: dark skin tone" }, /*243 : People & Body*/
{"🤙","call me hand" }, /*244 : People & Body*/
{"🤙🏻","call me hand: light skin tone" }, /*245 : People & Body*/
{"🤙🏼","call me hand: medium-light skin tone" }, /*246 : People & Body*/
{"🤙🏽","call me hand: medium skin tone" }, /*247 : People & Body*/
{"🤙🏾","call me hand: medium-dark skin tone" }, /*248 : People & Body*/
{"🤙🏿","call me hand: dark skin tone" }, /*249 : People & Body*/
{"👈","backhand index pointing left" }, /*250 : People & Body*/
{"👈🏻","backhand index pointing left: light skin tone" }, /*251 : People & Body*/
{"👈🏼","backhand index pointing left: medium-light skin tone" }, /*252 : People & Body*/
{"👈🏽","backhand index pointing left: medium skin tone" }, /*253 : People & Body*/
{"👈🏾","backhand index pointing left: medium-dark skin tone" }, /*254 : People & Body*/
{"👈🏿","backhand index pointing left: dark skin tone" }, /*255 : People & Body*/
{"👉","backhand index pointing right" }, /*256 : People & Body*/
{"👉🏻","backhand index pointing right: light skin tone" }, /*257 : People & Body*/
{"👉🏼","backhand index pointing right: medium-light skin tone" }, /*258 : People & Body*/
{"👉🏽","backhand index pointing right: medium skin tone" }, /*259 : People & Body*/
{"👉🏾","backhand index pointing right: medium-dark skin tone" }, /*260 : People & Body*/
{"👉🏿","backhand index pointing right: dark skin tone" }, /*261 : People & Body*/
{"👆","backhand index pointing up" }, /*262 : People & Body*/
{"👆🏻","backhand index pointing up: light skin tone" }, /*263 : People & Body*/
{"👆🏼","backhand index pointing up: medium-light skin tone" }, /*264 : People & Body*/
{"👆🏽","backhand index pointing up: medium skin tone" }, /*265 : People & Body*/
{"👆🏾","backhand index pointing up: medium-dark skin tone" }, /*266 : People & Body*/
{"👆🏿","backhand index pointing up: dark skin tone" }, /*267 : People & Body*/
{"🖕","middle finger" }, /*268 : People & Body*/
{"🖕🏻","middle finger: light skin tone" }, /*269 : People & Body*/
{"🖕🏼","middle finger: medium-light skin tone" }, /*270 : People & Body*/
{"🖕🏽","middle finger: medium skin tone" }, /*271 : People & Body*/
{"🖕🏾","middle finger: medium-dark skin tone" }, /*272 : People & Body*/
{"🖕🏿","middle finger: dark skin tone" }, /*273 : People & Body*/
{"👇","backhand index pointing down" }, /*274 : People & Body*/
{"👇🏻","backhand index pointing down: light skin tone" }, /*275 : People & Body*/
{"👇🏼","backhand index pointing down: medium-light skin tone" }, /*276 : People & Body*/
{"👇🏽","backhand index pointing down: medium skin tone" }, /*277 : People & Body*/
{"👇🏾","backhand index pointing down: medium-dark skin tone" }, /*278 : People & Body*/
{"👇🏿","backhand index pointing down: dark skin tone" }, /*279 : People & Body*/
{"☝️","index pointing up" }, /*280 : People & Body*/
{"☝","index pointing up" }, /*281 : People & Body*/
{"☝🏻","index pointing up: light skin tone" }, /*282 : People & Body*/
{"☝🏼","index pointing up: medium-light skin tone" }, /*283 : People & Body*/
{"☝🏽","index pointing up: medium skin tone" }, /*284 : People & Body*/
{"☝🏾","index pointing up: medium-dark skin tone" }, /*285 : People & Body*/
{"☝🏿","index pointing up: dark skin tone" }, /*286 : People & Body*/
{"👍","thumbs up" }, /*287 : People & Body*/
{"👍🏻","thumbs up: light skin tone" }, /*288 : People & Body*/
{"👍🏼","thumbs up: medium-light skin tone" }, /*289 : People & Body*/
{"👍🏽","thumbs up: medium skin tone" }, /*290 : People & Body*/
{"👍🏾","thumbs up: medium-dark skin tone" }, /*291 : People & Body*/
{"👍🏿","thumbs up: dark skin tone" }, /*292 : People & Body*/
{"👎","thumbs down" }, /*293 : People & Body*/
{"👎🏻","thumbs down: light skin tone" }, /*294 : People & Body*/
{"👎🏼","thumbs down: medium-light skin tone" }, /*295 : People & Body*/
{"👎🏽","thumbs down: medium skin tone" }, /*296 : People & Body*/
{"👎🏾","thumbs down: medium-dark skin tone" }, /*297 : People & Body*/
{"👎🏿","thumbs down: dark skin tone" }, /*298 : People & Body*/
{"✊","raised fist" }, /*299 : People & Body*/
{"✊🏻","raised fist: light skin tone" }, /*300 : People & Body*/
{"✊🏼","raised fist: medium-light skin tone" }, /*301 : People & Body*/
{"✊🏽","raised fist: medium skin tone" }, /*302 : People & Body*/
{"✊🏾","raised fist: medium-dark skin tone" }, /*303 : People & Body*/
{"✊🏿","raised fist: dark skin tone" }, /*304 : People & Body*/
{"👊","oncoming fist" }, /*305 : People & Body*/
{"👊🏻","oncoming fist: light skin tone" }, /*306 : People & Body*/
{"👊🏼","oncoming fist: medium-light skin tone" }, /*307 : People & Body*/
{"👊🏽","oncoming fist: medium skin tone" }, /*308 : People & Body*/
{"👊🏾","oncoming fist: medium-dark skin tone" }, /*309 : People & Body*/
{"👊🏿","oncoming fist: dark skin tone" }, /*310 : People & Body*/
{"🤛","left-facing fist" }, /*311 : People & Body*/
{"🤛🏻","left-facing fist: light skin tone" }, /*312 : People & Body*/
{"🤛🏼","left-facing fist: medium-light skin tone" }, /*313 : People & Body*/
{"🤛🏽","left-facing fist: medium skin tone" }, /*314 : People & Body*/
{"🤛🏾","left-facing fist: medium-dark skin tone" }, /*315 : People & Body*/
{"🤛🏿","left-facing fist: dark skin tone" }, /*316 : People & Body*/
{"🤜","right-facing fist" }, /*317 : People & Body*/
{"🤜🏻","right-facing fist: light skin tone" }, /*318 : People & Body*/
{"🤜🏼","right-facing fist: medium-light skin tone" }, /*319 : People & Body*/
{"🤜🏽","right-facing fist: medium skin tone" }, /*320 : People & Body*/
{"🤜🏾","right-facing fist: medium-dark skin tone" }, /*321 : People & Body*/
{"🤜🏿","right-facing fist: dark skin tone" }, /*322 : People & Body*/
{"👏","clapping hands" }, /*323 : People & Body*/
{"👏🏻","clapping hands: light skin tone" }, /*324 : People & Body*/
{"👏🏼","clapping hands: medium-light skin tone" }, /*325 : People & Body*/
{"👏🏽","clapping hands: medium skin tone" }, /*326 : People & Body*/
{"👏🏾","clapping hands: medium-dark skin tone" }, /*327 : People & Body*/
{"👏🏿","clapping hands: dark skin tone" }, /*328 : People & Body*/
{"🙌","raising hands" }, /*329 : People & Body*/
{"🙌🏻","raising hands: light skin tone" }, /*330 : People & Body*/
{"🙌🏼","raising hands: medium-light skin tone" }, /*331 : People & Body*/
{"🙌🏽","raising hands: medium skin tone" }, /*332 : People & Body*/
{"🙌🏾","raising hands: medium-dark skin tone" }, /*333 : People & Body*/
{"🙌🏿","raising hands: dark skin tone" }, /*334 : People & Body*/
{"👐","open hands" }, /*335 : People & Body*/
{"👐🏻","open hands: light skin tone" }, /*336 : People & Body*/
{"👐🏼","open hands: medium-light skin tone" }, /*337 : People & Body*/
{"👐🏽","open hands: medium skin tone" }, /*338 : People & Body*/
{"👐🏾","open hands: medium-dark skin tone" }, /*339 : People & Body*/
{"👐🏿","open hands: dark skin tone" }, /*340 : People & Body*/
{"🤲","palms up together" }, /*341 : People & Body*/
{"🤲🏻","palms up together: light skin tone" }, /*342 : People & Body*/
{"🤲🏼","palms up together: medium-light skin tone" }, /*343 : People & Body*/
{"🤲🏽","palms up together: medium skin tone" }, /*344 : People & Body*/
{"🤲🏾","palms up together: medium-dark skin tone" }, /*345 : People & Body*/
{"🤲🏿","palms up together: dark skin tone" }, /*346 : People & Body*/
{"🤝","handshake" }, /*347 : People & Body*/
{"🙏","folded hands" }, /*348 : People & Body*/
{"🙏🏻","folded hands: light skin tone" }, /*349 : People & Body*/
{"🙏🏼","folded hands: medium-light skin tone" }, /*350 : People & Body*/
{"🙏🏽","folded hands: medium skin tone" }, /*351 : People & Body*/
{"🙏🏾","folded hands: medium-dark skin tone" }, /*352 : People & Body*/
{"🙏🏿","folded hands: dark skin tone" }, /*353 : People & Body*/
{"✍️","writing hand" }, /*354 : People & Body*/
{"✍","writing hand" }, /*355 : People & Body*/
{"✍🏻","writing hand: light skin tone" }, /*356 : People & Body*/
{"✍🏼","writing hand: medium-light skin tone" }, /*357 : People & Body*/
{"✍🏽","writing hand: medium skin tone" }, /*358 : People & Body*/
{"✍🏾","writing hand: medium-dark skin tone" }, /*359 : People & Body*/
{"✍🏿","writing hand: dark skin tone" }, /*360 : People & Body*/
{"💅","nail polish" }, /*361 : People & Body*/
{"💅🏻","nail polish: light skin tone" }, /*362 : People & Body*/
{"💅🏼","nail polish: medium-light skin tone" }, /*363 : People & Body*/
{"💅🏽","nail polish: medium skin tone" }, /*364 : People & Body*/
{"💅🏾","nail polish: medium-dark skin tone" }, /*365 : People & Body*/
{"💅🏿","nail polish: dark skin tone" }, /*366 : People & Body*/
{"🤳","selfie" }, /*367 : People & Body*/
{"🤳🏻","selfie: light skin tone" }, /*368 : People & Body*/
{"🤳🏼","selfie: medium-light skin tone" }, /*369 : People & Body*/
{"🤳🏽","selfie: medium skin tone" }, /*370 : People & Body*/
{"🤳🏾","selfie: medium-dark skin tone" }, /*371 : People & Body*/
{"🤳🏿","selfie: dark skin tone" }, /*372 : People & Body*/
{"💪","flexed biceps" }, /*373 : People & Body*/
{"💪🏻","flexed biceps: light skin tone" }, /*374 : People & Body*/
{"💪🏼","flexed biceps: medium-light skin tone" }, /*375 : People & Body*/
{"💪🏽","flexed biceps: medium skin tone" }, /*376 : People & Body*/
{"💪🏾","flexed biceps: medium-dark skin tone" }, /*377 : People & Body*/
{"💪🏿","flexed biceps: dark skin tone" }, /*378 : People & Body*/
{"🦾","mechanical arm" }, /*379 : People & Body*/
{"🦿","mechanical leg" }, /*380 : People & Body*/
{"🦵","leg" }, /*381 : People & Body*/
{"🦵🏻","leg: light skin tone" }, /*382 : People & Body*/
{"🦵🏼","leg: medium-light skin tone" }, /*383 : People & Body*/
{"🦵🏽","leg: medium skin tone" }, /*384 : People & Body*/
{"🦵🏾","leg: medium-dark skin tone" }, /*385 : People & Body*/
{"🦵🏿","leg: dark skin tone" }, /*386 : People & Body*/
{"🦶","foot" }, /*387 : People & Body*/
{"🦶🏻","foot: light skin tone" }, /*388 : People & Body*/
{"🦶🏼","foot: medium-light skin tone" }, /*389 : People & Body*/
{"🦶🏽","foot: medium skin tone" }, /*390 : People & Body*/
{"🦶🏾","foot: medium-dark skin tone" }, /*391 : People & Body*/
{"🦶🏿","foot: dark skin tone" }, /*392 : People & Body*/
{"👂","ear" }, /*393 : People & Body*/
{"👂🏻","ear: light skin tone" }, /*394 : People & Body*/
{"👂🏼","ear: medium-light skin tone" }, /*395 : People & Body*/
{"👂🏽","ear: medium skin tone" }, /*396 : People & Body*/
{"👂🏾","ear: medium-dark skin tone" }, /*397 : People & Body*/
{"👂🏿","ear: dark skin tone" }, /*398 : People & Body*/
{"🦻","ear with hearing aid" }, /*399 : People & Body*/
{"🦻🏻","ear with hearing aid: light skin tone" }, /*400 : People & Body*/
{"🦻🏼","ear with hearing aid: medium-light skin tone" }, /*401 : People & Body*/
{"🦻🏽","ear with hearing aid: medium skin tone" }, /*402 : People & Body*/
{"🦻🏾","ear with hearing aid: medium-dark skin tone" }, /*403 : People & Body*/
{"🦻🏿","ear with hearing aid: dark skin tone" }, /*404 : People & Body*/
{"👃","nose" }, /*405 : People & Body*/
{"👃🏻","nose: light skin tone" }, /*406 : People & Body*/
{"👃🏼","nose: medium-light skin tone" }, /*407 : People & Body*/
{"👃🏽","nose: medium skin tone" }, /*408 : People & Body*/
{"👃🏾","nose: medium-dark skin tone" }, /*409 : People & Body*/
{"👃🏿","nose: dark skin tone" }, /*410 : People & Body*/
{"🧠","brain" }, /*411 : People & Body*/
{"🫀","anatomical heart" }, /*412 : People & Body*/
{"🫁","lungs" }, /*413 : People & Body*/
{"🦷","tooth" }, /*414 : People & Body*/
{"🦴","bone" }, /*415 : People & Body*/
{"👀","eyes" }, /*416 : People & Body*/
{"👁️","eye" }, /*417 : People & Body*/
{"👁","eye" }, /*418 : People & Body*/
{"👅","tongue" }, /*419 : People & Body*/
{"👄","mouth" }, /*420 : People & Body*/
{"👶","baby" }, /*421 : People & Body*/
{"👶🏻","baby: light skin tone" }, /*422 : People & Body*/
{"👶🏼","baby: medium-light skin tone" }, /*423 : People & Body*/
{"👶🏽","baby: medium skin tone" }, /*424 : People & Body*/
{"👶🏾","baby: medium-dark skin tone" }, /*425 : People & Body*/
{"👶🏿","baby: dark skin tone" }, /*426 : People & Body*/
{"🧒","child" }, /*427 : People & Body*/
{"🧒🏻","child: light skin tone" }, /*428 : People & Body*/
{"🧒🏼","child: medium-light skin tone" }, /*429 : People & Body*/
{"🧒🏽","child: medium skin tone" }, /*430 : People & Body*/
{"🧒🏾","child: medium-dark skin tone" }, /*431 : People & Body*/
{"🧒🏿","child: dark skin tone" }, /*432 : People & Body*/
{"👦","boy" }, /*433 : People & Body*/
{"👦🏻","boy: light skin tone" }, /*434 : People & Body*/
{"👦🏼","boy: medium-light skin tone" }, /*435 : People & Body*/
{"👦🏽","boy: medium skin tone" }, /*436 : People & Body*/
{"👦🏾","boy: medium-dark skin tone" }, /*437 : People & Body*/
{"👦🏿","boy: dark skin tone" }, /*438 : People & Body*/
{"👧","girl" }, /*439 : People & Body*/
{"👧🏻","girl: light skin tone" }, /*440 : People & Body*/
{"👧🏼","girl: medium-light skin tone" }, /*441 : People & Body*/
{"👧🏽","girl: medium skin tone" }, /*442 : People & Body*/
{"👧🏾","girl: medium-dark skin tone" }, /*443 : People & Body*/
{"👧🏿","girl: dark skin tone" }, /*444 : People & Body*/
{"🧑","person" }, /*445 : People & Body*/
{"🧑🏻","person: light skin tone" }, /*446 : People & Body*/
{"🧑🏼","person: medium-light skin tone" }, /*447 : People & Body*/
{"🧑🏽","person: medium skin tone" }, /*448 : People & Body*/
{"🧑🏾","person: medium-dark skin tone" }, /*449 : People & Body*/
{"🧑🏿","person: dark skin tone" }, /*450 : People & Body*/
{"👱","person: blond hair" }, /*451 : People & Body*/
{"👱🏻","person: light skin tone, blond hair" }, /*452 : People & Body*/
{"👱🏼","person: medium-light skin tone, blond hair" }, /*453 : People & Body*/
{"👱🏽","person: medium skin tone, blond hair" }, /*454 : People & Body*/
{"👱🏾","person: medium-dark skin tone, blond hair" }, /*455 : People & Body*/
{"👱🏿","person: dark skin tone, blond hair" }, /*456 : People & Body*/
{"👨","man" }, /*457 : People & Body*/
{"👨🏻","man: light skin tone" }, /*458 : People & Body*/
{"👨🏼","man: medium-light skin tone" }, /*459 : People & Body*/
{"👨🏽","man: medium skin tone" }, /*460 : People & Body*/
{"👨🏾","man: medium-dark skin tone" }, /*461 : People & Body*/
{"👨🏿","man: dark skin tone" }, /*462 : People & Body*/
{"🧔","person: beard" }, /*463 : People & Body*/
{"🧔🏻","person: light skin tone, beard" }, /*464 : People & Body*/
{"🧔🏼","person: medium-light skin tone, beard" }, /*465 : People & Body*/
{"🧔🏽","person: medium skin tone, beard" }, /*466 : People & Body*/
{"🧔🏾","person: medium-dark skin tone, beard" }, /*467 : People & Body*/
{"🧔🏿","person: dark skin tone, beard" }, /*468 : People & Body*/
{"🧔‍♂️","man: beard" }, /*469 : People & Body*/
{"🧔‍♂","man: beard" }, /*470 : People & Body*/
{"🧔🏻‍♂️","man: light skin tone, beard" }, /*471 : People & Body*/
{"🧔🏻‍♂","man: light skin tone, beard" }, /*472 : People & Body*/
{"🧔🏼‍♂️","man: medium-light skin tone, beard" }, /*473 : People & Body*/
{"🧔🏼‍♂","man: medium-light skin tone, beard" }, /*474 : People & Body*/
{"🧔🏽‍♂️","man: medium skin tone, beard" }, /*475 : People & Body*/
{"🧔🏽‍♂","man: medium skin tone, beard" }, /*476 : People & Body*/
{"🧔🏾‍♂️","man: medium-dark skin tone, beard" }, /*477 : People & Body*/
{"🧔🏾‍♂","man: medium-dark skin tone, beard" }, /*478 : People & Body*/
{"🧔🏿‍♂️","man: dark skin tone, beard" }, /*479 : People & Body*/
{"🧔🏿‍♂","man: dark skin tone, beard" }, /*480 : People & Body*/
{"🧔‍♀️","woman: beard" }, /*481 : People & Body*/
{"🧔‍♀","woman: beard" }, /*482 : People & Body*/
{"🧔🏻‍♀️","woman: light skin tone, beard" }, /*483 : People & Body*/
{"🧔🏻‍♀","woman: light skin tone, beard" }, /*484 : People & Body*/
{"🧔🏼‍♀️","woman: medium-light skin tone, beard" }, /*485 : People & Body*/
{"🧔🏼‍♀","woman: medium-light skin tone, beard" }, /*486 : People & Body*/
{"🧔🏽‍♀️","woman: medium skin tone, beard" }, /*487 : People & Body*/
{"🧔🏽‍♀","woman: medium skin tone, beard" }, /*488 : People & Body*/
{"🧔🏾‍♀️","woman: medium-dark skin tone, beard" }, /*489 : People & Body*/
{"🧔🏾‍♀","woman: medium-dark skin tone, beard" }, /*490 : People & Body*/
{"🧔🏿‍♀️","woman: dark skin tone, beard" }, /*491 : People & Body*/
{"🧔🏿‍♀","woman: dark skin tone, beard" }, /*492 : People & Body*/
{"👨‍🦰","man: red hair" }, /*493 : People & Body*/
{"👨🏻‍🦰","man: light skin tone, red hair" }, /*494 : People & Body*/
{"👨🏼‍🦰","man: medium-light skin tone, red hair" }, /*495 : People & Body*/
{"👨🏽‍🦰","man: medium skin tone, red hair" }, /*496 : People & Body*/
{"👨🏾‍🦰","man: medium-dark skin tone, red hair" }, /*497 : People & Body*/
{"👨🏿‍🦰","man: dark skin tone, red hair" }, /*498 : People & Body*/
{"👨‍🦱","man: curly hair" }, /*499 : People & Body*/
{"👨🏻‍🦱","man: light skin tone, curly hair" }, /*500 : People & Body*/
{"👨🏼‍🦱","man: medium-light skin tone, curly hair" }, /*501 : People & Body*/
{"👨🏽‍🦱","man: medium skin tone, curly hair" }, /*502 : People & Body*/
{"👨🏾‍🦱","man: medium-dark skin tone, curly hair" }, /*503 : People & Body*/
{"👨🏿‍🦱","man: dark skin tone, curly hair" }, /*504 : People & Body*/
{"👨‍🦳","man: white hair" }, /*505 : People & Body*/
{"👨🏻‍🦳","man: light skin tone, white hair" }, /*506 : People & Body*/
{"👨🏼‍🦳","man: medium-light skin tone, white hair" }, /*507 : People & Body*/
{"👨🏽‍🦳","man: medium skin tone, white hair" }, /*508 : People & Body*/
{"👨🏾‍🦳","man: medium-dark skin tone, white hair" }, /*509 : People & Body*/
{"👨🏿‍🦳","man: dark skin tone, white hair" }, /*510 : People & Body*/
{"👨‍🦲","man: bald" }, /*511 : People & Body*/
{"👨🏻‍🦲","man: light skin tone, bald" }, /*512 : People & Body*/
{"👨🏼‍🦲","man: medium-light skin tone, bald" }, /*513 : People & Body*/
{"👨🏽‍🦲","man: medium skin tone, bald" }, /*514 : People & Body*/
{"👨🏾‍🦲","man: medium-dark skin tone, bald" }, /*515 : People & Body*/
{"👨🏿‍🦲","man: dark skin tone, bald" }, /*516 : People & Body*/
{"👩","woman" }, /*517 : People & Body*/
{"👩🏻","woman: light skin tone" }, /*518 : People & Body*/
{"👩🏼","woman: medium-light skin tone" }, /*519 : People & Body*/
{"👩🏽","woman: medium skin tone" }, /*520 : People & Body*/
{"👩🏾","woman: medium-dark skin tone" }, /*521 : People & Body*/
{"👩🏿","woman: dark skin tone" }, /*522 : People & Body*/
{"👩‍🦰","woman: red hair" }, /*523 : People & Body*/
{"👩🏻‍🦰","woman: light skin tone, red hair" }, /*524 : People & Body*/
{"👩🏼‍🦰","woman: medium-light skin tone, red hair" }, /*525 : People & Body*/
{"👩🏽‍🦰","woman: medium skin tone, red hair" }, /*526 : People & Body*/
{"👩🏾‍🦰","woman: medium-dark skin tone, red hair" }, /*527 : People & Body*/
{"👩🏿‍🦰","woman: dark skin tone, red hair" }, /*528 : People & Body*/
{"🧑‍🦰","person: red hair" }, /*529 : People & Body*/
{"🧑🏻‍🦰","person: light skin tone, red hair" }, /*530 : People & Body*/
{"🧑🏼‍🦰","person: medium-light skin tone, red hair" }, /*531 : People & Body*/
{"🧑🏽‍🦰","person: medium skin tone, red hair" }, /*532 : People & Body*/
{"🧑🏾‍🦰","person: medium-dark skin tone, red hair" }, /*533 : People & Body*/
{"🧑🏿‍🦰","person: dark skin tone, red hair" }, /*534 : People & Body*/
{"👩‍🦱","woman: curly hair" }, /*535 : People & Body*/
{"👩🏻‍🦱","woman: light skin tone, curly hair" }, /*536 : People & Body*/
{"👩🏼‍🦱","woman: medium-light skin tone, curly hair" }, /*537 : People & Body*/
{"👩🏽‍🦱","woman: medium skin tone, curly hair" }, /*538 : People & Body*/
{"👩🏾‍🦱","woman: medium-dark skin tone, curly hair" }, /*539 : People & Body*/
{"👩🏿‍🦱","woman: dark skin tone, curly hair" }, /*540 : People & Body*/
{"🧑‍🦱","person: curly hair" }, /*541 : People & Body*/
{"🧑🏻‍🦱","person: light skin tone, curly hair" }, /*542 : People & Body*/
{"🧑🏼‍🦱","person: medium-light skin tone, curly hair" }, /*543 : People & Body*/
{"🧑🏽‍🦱","person: medium skin tone, curly hair" }, /*544 : People & Body*/
{"🧑🏾‍🦱","person: medium-dark skin tone, curly hair" }, /*545 : People & Body*/
{"🧑🏿‍🦱","person: dark skin tone, curly hair" }, /*546 : People & Body*/
{"👩‍🦳","woman: white hair" }, /*547 : People & Body*/
{"👩🏻‍🦳","woman: light skin tone, white hair" }, /*548 : People & Body*/
{"👩🏼‍🦳","woman: medium-light skin tone, white hair" }, /*549 : People & Body*/
{"👩🏽‍🦳","woman: medium skin tone, white hair" }, /*550 : People & Body*/
{"👩🏾‍🦳","woman: medium-dark skin tone, white hair" }, /*551 : People & Body*/
{"👩🏿‍🦳","woman: dark skin tone, white hair" }, /*552 : People & Body*/
{"🧑‍🦳","person: white hair" }, /*553 : People & Body*/
{"🧑🏻‍🦳","person: light skin tone, white hair" }, /*554 : People & Body*/
{"🧑🏼‍🦳","person: medium-light skin tone, white hair" }, /*555 : People & Body*/
{"🧑🏽‍🦳","person: medium skin tone, white hair" }, /*556 : People & Body*/
{"🧑🏾‍🦳","person: medium-dark skin tone, white hair" }, /*557 : People & Body*/
{"🧑🏿‍🦳","person: dark skin tone, white hair" }, /*558 : People & Body*/
{"👩‍🦲","woman: bald" }, /*559 : People & Body*/
{"👩🏻‍🦲","woman: light skin tone, bald" }, /*560 : People & Body*/
{"👩🏼‍🦲","woman: medium-light skin tone, bald" }, /*561 : People & Body*/
{"👩🏽‍🦲","woman: medium skin tone, bald" }, /*562 : People & Body*/
{"👩🏾‍🦲","woman: medium-dark skin tone, bald" }, /*563 : People & Body*/
{"👩🏿‍🦲","woman: dark skin tone, bald" }, /*564 : People & Body*/
{"🧑‍🦲","person: bald" }, /*565 : People & Body*/
{"🧑🏻‍🦲","person: light skin tone, bald" }, /*566 : People & Body*/
{"🧑🏼‍🦲","person: medium-light skin tone, bald" }, /*567 : People & Body*/
{"🧑🏽‍🦲","person: medium skin tone, bald" }, /*568 : People & Body*/
{"🧑🏾‍🦲","person: medium-dark skin tone, bald" }, /*569 : People & Body*/
{"🧑🏿‍🦲","person: dark skin tone, bald" }, /*570 : People & Body*/
{"👱‍♀️","woman: blond hair" }, /*571 : People & Body*/
{"👱‍♀","woman: blond hair" }, /*572 : People & Body*/
{"👱🏻‍♀️","woman: light skin tone, blond hair" }, /*573 : People & Body*/
{"👱🏻‍♀","woman: light skin tone, blond hair" }, /*574 : People & Body*/
{"👱🏼‍♀️","woman: medium-light skin tone, blond hair" }, /*575 : People & Body*/
{"👱🏼‍♀","woman: medium-light skin tone, blond hair" }, /*576 : People & Body*/
{"👱🏽‍♀️","woman: medium skin tone, blond hair" }, /*577 : People & Body*/
{"👱🏽‍♀","woman: medium skin tone, blond hair" }, /*578 : People & Body*/
{"👱🏾‍♀️","woman: medium-dark skin tone, blond hair" }, /*579 : People & Body*/
{"👱🏾‍♀","woman: medium-dark skin tone, blond hair" }, /*580 : People & Body*/
{"👱🏿‍♀️","woman: dark skin tone, blond hair" }, /*581 : People & Body*/
{"👱🏿‍♀","woman: dark skin tone, blond hair" }, /*582 : People & Body*/
{"👱‍♂️","man: blond hair" }, /*583 : People & Body*/
{"👱‍♂","man: blond hair" }, /*584 : People & Body*/
{"👱🏻‍♂️","man: light skin tone, blond hair" }, /*585 : People & Body*/
{"👱🏻‍♂","man: light skin tone, blond hair" }, /*586 : People & Body*/
{"👱🏼‍♂️","man: medium-light skin tone, blond hair" }, /*587 : People & Body*/
{"👱🏼‍♂","man: medium-light skin tone, blond hair" }, /*588 : People & Body*/
{"👱🏽‍♂️","man: medium skin tone, blond hair" }, /*589 : People & Body*/
{"👱🏽‍♂","man: medium skin tone, blond hair" }, /*590 : People & Body*/
{"👱🏾‍♂️","man: medium-dark skin tone, blond hair" }, /*591 : People & Body*/
{"👱🏾‍♂","man: medium-dark skin tone, blond hair" }, /*592 : People & Body*/
{"👱🏿‍♂️","man: dark skin tone, blond hair" }, /*593 : People & Body*/
{"👱🏿‍♂","man: dark skin tone, blond hair" }, /*594 : People & Body*/
{"🧓","older person" }, /*595 : People & Body*/
{"🧓🏻","older person: light skin tone" }, /*596 : People & Body*/
{"🧓🏼","older person: medium-light skin tone" }, /*597 : People & Body*/
{"🧓🏽","older person: medium skin tone" }, /*598 : People & Body*/
{"🧓🏾","older person: medium-dark skin tone" }, /*599 : People & Body*/
{"🧓🏿","older person: dark skin tone" }, /*600 : People & Body*/
{"👴","old man" }, /*601 : People & Body*/
{"👴🏻","old man: light skin tone" }, /*602 : People & Body*/
{"👴🏼","old man: medium-light skin tone" }, /*603 : People & Body*/
{"👴🏽","old man: medium skin tone" }, /*604 : People & Body*/
{"👴🏾","old man: medium-dark skin tone" }, /*605 : People & Body*/
{"👴🏿","old man: dark skin tone" }, /*606 : People & Body*/
{"👵","old woman" }, /*607 : People & Body*/
{"👵🏻","old woman: light skin tone" }, /*608 : People & Body*/
{"👵🏼","old woman: medium-light skin tone" }, /*609 : People & Body*/
{"👵🏽","old woman: medium skin tone" }, /*610 : People & Body*/
{"👵🏾","old woman: medium-dark skin tone" }, /*611 : People & Body*/
{"👵🏿","old woman: dark skin tone" }, /*612 : People & Body*/
{"🙍","person frowning" }, /*613 : People & Body*/
{"🙍🏻","person frowning: light skin tone" }, /*614 : People & Body*/
{"🙍🏼","person frowning: medium-light skin tone" }, /*615 : People & Body*/
{"🙍🏽","person frowning: medium skin tone" }, /*616 : People & Body*/
{"🙍🏾","person frowning: medium-dark skin tone" }, /*617 : People & Body*/
{"🙍🏿","person frowning: dark skin tone" }, /*618 : People & Body*/
{"🙍‍♂️","man frowning" }, /*619 : People & Body*/
{"🙍‍♂","man frowning" }, /*620 : People & Body*/
{"🙍🏻‍♂️","man frowning: light skin tone" }, /*621 : People & Body*/
{"🙍🏻‍♂","man frowning: light skin tone" }, /*622 : People & Body*/
{"🙍🏼‍♂️","man frowning: medium-light skin tone" }, /*623 : People & Body*/
{"🙍🏼‍♂","man frowning: medium-light skin tone" }, /*624 : People & Body*/
{"🙍🏽‍♂️","man frowning: medium skin tone" }, /*625 : People & Body*/
{"🙍🏽‍♂","man frowning: medium skin tone" }, /*626 : People & Body*/
{"🙍🏾‍♂️","man frowning: medium-dark skin tone" }, /*627 : People & Body*/
{"🙍🏾‍♂","man frowning: medium-dark skin tone" }, /*628 : People & Body*/
{"🙍🏿‍♂️","man frowning: dark skin tone" }, /*629 : People & Body*/
{"🙍🏿‍♂","man frowning: dark skin tone" }, /*630 : People & Body*/
{"🙍‍♀️","woman frowning" }, /*631 : People & Body*/
{"🙍‍♀","woman frowning" }, /*632 : People & Body*/
{"🙍🏻‍♀️","woman frowning: light skin tone" }, /*633 : People & Body*/
{"🙍🏻‍♀","woman frowning: light skin tone" }, /*634 : People & Body*/
{"🙍🏼‍♀️","woman frowning: medium-light skin tone" }, /*635 : People & Body*/
{"🙍🏼‍♀","woman frowning: medium-light skin tone" }, /*636 : People & Body*/
{"🙍🏽‍♀️","woman frowning: medium skin tone" }, /*637 : People & Body*/
{"🙍🏽‍♀","woman frowning: medium skin tone" }, /*638 : People & Body*/
{"🙍🏾‍♀️","woman frowning: medium-dark skin tone" }, /*639 : People & Body*/
{"🙍🏾‍♀","woman frowning: medium-dark skin tone" }, /*640 : People & Body*/
{"🙍🏿‍♀️","woman frowning: dark skin tone" }, /*641 : People & Body*/
{"🙍🏿‍♀","woman frowning: dark skin tone" }, /*642 : People & Body*/
{"🙎","person pouting" }, /*643 : People & Body*/
{"🙎🏻","person pouting: light skin tone" }, /*644 : People & Body*/
{"🙎🏼","person pouting: medium-light skin tone" }, /*645 : People & Body*/
{"🙎🏽","person pouting: medium skin tone" }, /*646 : People & Body*/
{"🙎🏾","person pouting: medium-dark skin tone" }, /*647 : People & Body*/
{"🙎🏿","person pouting: dark skin tone" }, /*648 : People & Body*/
{"🙎‍♂️","man pouting" }, /*649 : People & Body*/
{"🙎‍♂","man pouting" }, /*650 : People & Body*/
{"🙎🏻‍♂️","man pouting: light skin tone" }, /*651 : People & Body*/
{"🙎🏻‍♂","man pouting: light skin tone" }, /*652 : People & Body*/
{"🙎🏼‍♂️","man pouting: medium-light skin tone" }, /*653 : People & Body*/
{"🙎🏼‍♂","man pouting: medium-light skin tone" }, /*654 : People & Body*/
{"🙎🏽‍♂️","man pouting: medium skin tone" }, /*655 : People & Body*/
{"🙎🏽‍♂","man pouting: medium skin tone" }, /*656 : People & Body*/
{"🙎🏾‍♂️","man pouting: medium-dark skin tone" }, /*657 : People & Body*/
{"🙎🏾‍♂","man pouting: medium-dark skin tone" }, /*658 : People & Body*/
{"🙎🏿‍♂️","man pouting: dark skin tone" }, /*659 : People & Body*/
{"🙎🏿‍♂","man pouting: dark skin tone" }, /*660 : People & Body*/
{"🙎‍♀️","woman pouting" }, /*661 : People & Body*/
{"🙎‍♀","woman pouting" }, /*662 : People & Body*/
{"🙎🏻‍♀️","woman pouting: light skin tone" }, /*663 : People & Body*/
{"🙎🏻‍♀","woman pouting: light skin tone" }, /*664 : People & Body*/
{"🙎🏼‍♀️","woman pouting: medium-light skin tone" }, /*665 : People & Body*/
{"🙎🏼‍♀","woman pouting: medium-light skin tone" }, /*666 : People & Body*/
{"🙎🏽‍♀️","woman pouting: medium skin tone" }, /*667 : People & Body*/
{"🙎🏽‍♀","woman pouting: medium skin tone" }, /*668 : People & Body*/
{"🙎🏾‍♀️","woman pouting: medium-dark skin tone" }, /*669 : People & Body*/
{"🙎🏾‍♀","woman pouting: medium-dark skin tone" }, /*670 : People & Body*/
{"🙎🏿‍♀️","woman pouting: dark skin tone" }, /*671 : People & Body*/
{"🙎🏿‍♀","woman pouting: dark skin tone" }, /*672 : People & Body*/
{"🙅","person gesturing NO" }, /*673 : People & Body*/
{"🙅🏻","person gesturing NO: light skin tone" }, /*674 : People & Body*/
{"🙅🏼","person gesturing NO: medium-light skin tone" }, /*675 : People & Body*/
{"🙅🏽","person gesturing NO: medium skin tone" }, /*676 : People & Body*/
{"🙅🏾","person gesturing NO: medium-dark skin tone" }, /*677 : People & Body*/
{"🙅🏿","person gesturing NO: dark skin tone" }, /*678 : People & Body*/
{"🙅‍♂️","man gesturing NO" }, /*679 : People & Body*/
{"🙅‍♂","man gesturing NO" }, /*680 : People & Body*/
{"🙅🏻‍♂️","man gesturing NO: light skin tone" }, /*681 : People & Body*/
{"🙅🏻‍♂","man gesturing NO: light skin tone" }, /*682 : People & Body*/
{"🙅🏼‍♂️","man gesturing NO: medium-light skin tone" }, /*683 : People & Body*/
{"🙅🏼‍♂","man gesturing NO: medium-light skin tone" }, /*684 : People & Body*/
{"🙅🏽‍♂️","man gesturing NO: medium skin tone" }, /*685 : People & Body*/
{"🙅🏽‍♂","man gesturing NO: medium skin tone" }, /*686 : People & Body*/
{"🙅🏾‍♂️","man gesturing NO: medium-dark skin tone" }, /*687 : People & Body*/
{"🙅🏾‍♂","man gesturing NO: medium-dark skin tone" }, /*688 : People & Body*/
{"🙅🏿‍♂️","man gesturing NO: dark skin tone" }, /*689 : People & Body*/
{"🙅🏿‍♂","man gesturing NO: dark skin tone" }, /*690 : People & Body*/
{"🙅‍♀️","woman gesturing NO" }, /*691 : People & Body*/
{"🙅‍♀","woman gesturing NO" }, /*692 : People & Body*/
{"🙅🏻‍♀️","woman gesturing NO: light skin tone" }, /*693 : People & Body*/
{"🙅🏻‍♀","woman gesturing NO: light skin tone" }, /*694 : People & Body*/
{"🙅🏼‍♀️","woman gesturing NO: medium-light skin tone" }, /*695 : People & Body*/
{"🙅🏼‍♀","woman gesturing NO: medium-light skin tone" }, /*696 : People & Body*/
{"🙅🏽‍♀️","woman gesturing NO: medium skin tone" }, /*697 : People & Body*/
{"🙅🏽‍♀","woman gesturing NO: medium skin tone" }, /*698 : People & Body*/
{"🙅🏾‍♀️","woman gesturing NO: medium-dark skin tone" }, /*699 : People & Body*/
{"🙅🏾‍♀","woman gesturing NO: medium-dark skin tone" }, /*700 : People & Body*/
{"🙅🏿‍♀️","woman gesturing NO: dark skin tone" }, /*701 : People & Body*/
{"🙅🏿‍♀","woman gesturing NO: dark skin tone" }, /*702 : People & Body*/
{"🙆","person gesturing OK" }, /*703 : People & Body*/
{"🙆🏻","person gesturing OK: light skin tone" }, /*704 : People & Body*/
{"🙆🏼","person gesturing OK: medium-light skin tone" }, /*705 : People & Body*/
{"🙆🏽","person gesturing OK: medium skin tone" }, /*706 : People & Body*/
{"🙆🏾","person gesturing OK: medium-dark skin tone" }, /*707 : People & Body*/
{"🙆🏿","person gesturing OK: dark skin tone" }, /*708 : People & Body*/
{"🙆‍♂️","man gesturing OK" }, /*709 : People & Body*/
{"🙆‍♂","man gesturing OK" }, /*710 : People & Body*/
{"🙆🏻‍♂️","man gesturing OK: light skin tone" }, /*711 : People & Body*/
{"🙆🏻‍♂","man gesturing OK: light skin tone" }, /*712 : People & Body*/
{"🙆🏼‍♂️","man gesturing OK: medium-light skin tone" }, /*713 : People & Body*/
{"🙆🏼‍♂","man gesturing OK: medium-light skin tone" }, /*714 : People & Body*/
{"🙆🏽‍♂️","man gesturing OK: medium skin tone" }, /*715 : People & Body*/
{"🙆🏽‍♂","man gesturing OK: medium skin tone" }, /*716 : People & Body*/
{"🙆🏾‍♂️","man gesturing OK: medium-dark skin tone" }, /*717 : People & Body*/
{"🙆🏾‍♂","man gesturing OK: medium-dark skin tone" }, /*718 : People & Body*/
{"🙆🏿‍♂️","man gesturing OK: dark skin tone" }, /*719 : People & Body*/
{"🙆🏿‍♂","man gesturing OK: dark skin tone" }, /*720 : People & Body*/
{"🙆‍♀️","woman gesturing OK" }, /*721 : People & Body*/
{"🙆‍♀","woman gesturing OK" }, /*722 : People & Body*/
{"🙆🏻‍♀️","woman gesturing OK: light skin tone" }, /*723 : People & Body*/
{"🙆🏻‍♀","woman gesturing OK: light skin tone" }, /*724 : People & Body*/
{"🙆🏼‍♀️","woman gesturing OK: medium-light skin tone" }, /*725 : People & Body*/
{"🙆🏼‍♀","woman gesturing OK: medium-light skin tone" }, /*726 : People & Body*/
{"🙆🏽‍♀️","woman gesturing OK: medium skin tone" }, /*727 : People & Body*/
{"🙆🏽‍♀","woman gesturing OK: medium skin tone" }, /*728 : People & Body*/
{"🙆🏾‍♀️","woman gesturing OK: medium-dark skin tone" }, /*729 : People & Body*/
{"🙆🏾‍♀","woman gesturing OK: medium-dark skin tone" }, /*730 : People & Body*/
{"🙆🏿‍♀️","woman gesturing OK: dark skin tone" }, /*731 : People & Body*/
{"🙆🏿‍♀","woman gesturing OK: dark skin tone" }, /*732 : People & Body*/
{"💁","person tipping hand" }, /*733 : People & Body*/
{"💁🏻","person tipping hand: light skin tone" }, /*734 : People & Body*/
{"💁🏼","person tipping hand: medium-light skin tone" }, /*735 : People & Body*/
{"💁🏽","person tipping hand: medium skin tone" }, /*736 : People & Body*/
{"💁🏾","person tipping hand: medium-dark skin tone" }, /*737 : People & Body*/
{"💁🏿","person tipping hand: dark skin tone" }, /*738 : People & Body*/
{"💁‍♂️","man tipping hand" }, /*739 : People & Body*/
{"💁‍♂","man tipping hand" }, /*740 : People & Body*/
{"💁🏻‍♂️","man tipping hand: light skin tone" }, /*741 : People & Body*/
{"💁🏻‍♂","man tipping hand: light skin tone" }, /*742 : People & Body*/
{"💁🏼‍♂️","man tipping hand: medium-light skin tone" }, /*743 : People & Body*/
{"💁🏼‍♂","man tipping hand: medium-light skin tone" }, /*744 : People & Body*/
{"💁🏽‍♂️","man tipping hand: medium skin tone" }, /*745 : People & Body*/
{"💁🏽‍♂","man tipping hand: medium skin tone" }, /*746 : People & Body*/
{"💁🏾‍♂️","man tipping hand: medium-dark skin tone" }, /*747 : People & Body*/
{"💁🏾‍♂","man tipping hand: medium-dark skin tone" }, /*748 : People & Body*/
{"💁🏿‍♂️","man tipping hand: dark skin tone" }, /*749 : People & Body*/
{"💁🏿‍♂","man tipping hand: dark skin tone" }, /*750 : People & Body*/
{"💁‍♀️","woman tipping hand" }, /*751 : People & Body*/
{"💁‍♀","woman tipping hand" }, /*752 : People & Body*/
{"💁🏻‍♀️","woman tipping hand: light skin tone" }, /*753 : People & Body*/
{"💁🏻‍♀","woman tipping hand: light skin tone" }, /*754 : People & Body*/
{"💁🏼‍♀️","woman tipping hand: medium-light skin tone" }, /*755 : People & Body*/
{"💁🏼‍♀","woman tipping hand: medium-light skin tone" }, /*756 : People & Body*/
{"💁🏽‍♀️","woman tipping hand: medium skin tone" }, /*757 : People & Body*/
{"💁🏽‍♀","woman tipping hand: medium skin tone" }, /*758 : People & Body*/
{"💁🏾‍♀️","woman tipping hand: medium-dark skin tone" }, /*759 : People & Body*/
{"💁🏾‍♀","woman tipping hand: medium-dark skin tone" }, /*760 : People & Body*/
{"💁🏿‍♀️","woman tipping hand: dark skin tone" }, /*761 : People & Body*/
{"💁🏿‍♀","woman tipping hand: dark skin tone" }, /*762 : People & Body*/
{"🙋","person raising hand" }, /*763 : People & Body*/
{"🙋🏻","person raising hand: light skin tone" }, /*764 : People & Body*/
{"🙋🏼","person raising hand: medium-light skin tone" }, /*765 : People & Body*/
{"🙋🏽","person raising hand: medium skin tone" }, /*766 : People & Body*/
{"🙋🏾","person raising hand: medium-dark skin tone" }, /*767 : People & Body*/
{"🙋🏿","person raising hand: dark skin tone" }, /*768 : People & Body*/
{"🙋‍♂️","man raising hand" }, /*769 : People & Body*/
{"🙋‍♂","man raising hand" }, /*770 : People & Body*/
{"🙋🏻‍♂️","man raising hand: light skin tone" }, /*771 : People & Body*/
{"🙋🏻‍♂","man raising hand: light skin tone" }, /*772 : People & Body*/
{"🙋🏼‍♂️","man raising hand: medium-light skin tone" }, /*773 : People & Body*/
{"🙋🏼‍♂","man raising hand: medium-light skin tone" }, /*774 : People & Body*/
{"🙋🏽‍♂️","man raising hand: medium skin tone" }, /*775 : People & Body*/
{"🙋🏽‍♂","man raising hand: medium skin tone" }, /*776 : People & Body*/
{"🙋🏾‍♂️","man raising hand: medium-dark skin tone" }, /*777 : People & Body*/
{"🙋🏾‍♂","man raising hand: medium-dark skin tone" }, /*778 : People & Body*/
{"🙋🏿‍♂️","man raising hand: dark skin tone" }, /*779 : People & Body*/
{"🙋🏿‍♂","man raising hand: dark skin tone" }, /*780 : People & Body*/
{"🙋‍♀️","woman raising hand" }, /*781 : People & Body*/
{"🙋‍♀","woman raising hand" }, /*782 : People & Body*/
{"🙋🏻‍♀️","woman raising hand: light skin tone" }, /*783 : People & Body*/
{"🙋🏻‍♀","woman raising hand: light skin tone" }, /*784 : People & Body*/
{"🙋🏼‍♀️","woman raising hand: medium-light skin tone" }, /*785 : People & Body*/
{"🙋🏼‍♀","woman raising hand: medium-light skin tone" }, /*786 : People & Body*/
{"🙋🏽‍♀️","woman raising hand: medium skin tone" }, /*787 : People & Body*/
{"🙋🏽‍♀","woman raising hand: medium skin tone" }, /*788 : People & Body*/
{"🙋🏾‍♀️","woman raising hand: medium-dark skin tone" }, /*789 : People & Body*/
{"🙋🏾‍♀","woman raising hand: medium-dark skin tone" }, /*790 : People & Body*/
{"🙋🏿‍♀️","woman raising hand: dark skin tone" }, /*791 : People & Body*/
{"🙋🏿‍♀","woman raising hand: dark skin tone" }, /*792 : People & Body*/
{"🧏","deaf person" }, /*793 : People & Body*/
{"🧏🏻","deaf person: light skin tone" }, /*794 : People & Body*/
{"🧏🏼","deaf person: medium-light skin tone" }, /*795 : People & Body*/
{"🧏🏽","deaf person: medium skin tone" }, /*796 : People & Body*/
{"🧏🏾","deaf person: medium-dark skin tone" }, /*797 : People & Body*/
{"🧏🏿","deaf person: dark skin tone" }, /*798 : People & Body*/
{"🧏‍♂️","deaf man" }, /*799 : People & Body*/
{"🧏‍♂","deaf man" }, /*800 : People & Body*/
{"🧏🏻‍♂️","deaf man: light skin tone" }, /*801 : People & Body*/
{"🧏🏻‍♂","deaf man: light skin tone" }, /*802 : People & Body*/
{"🧏🏼‍♂️","deaf man: medium-light skin tone" }, /*803 : People & Body*/
{"🧏🏼‍♂","deaf man: medium-light skin tone" }, /*804 : People & Body*/
{"🧏🏽‍♂️","deaf man: medium skin tone" }, /*805 : People & Body*/
{"🧏🏽‍♂","deaf man: medium skin tone" }, /*806 : People & Body*/
{"🧏🏾‍♂️","deaf man: medium-dark skin tone" }, /*807 : People & Body*/
{"🧏🏾‍♂","deaf man: medium-dark skin tone" }, /*808 : People & Body*/
{"🧏🏿‍♂️","deaf man: dark skin tone" }, /*809 : People & Body*/
{"🧏🏿‍♂","deaf man: dark skin tone" }, /*810 : People & Body*/
{"🧏‍♀️","deaf woman" }, /*811 : People & Body*/
{"🧏‍♀","deaf woman" }, /*812 : People & Body*/
{"🧏🏻‍♀️","deaf woman: light skin tone" }, /*813 : People & Body*/
{"🧏🏻‍♀","deaf woman: light skin tone" }, /*814 : People & Body*/
{"🧏🏼‍♀️","deaf woman: medium-light skin tone" }, /*815 : People & Body*/
{"🧏🏼‍♀","deaf woman: medium-light skin tone" }, /*816 : People & Body*/
{"🧏🏽‍♀️","deaf woman: medium skin tone" }, /*817 : People & Body*/
{"🧏🏽‍♀","deaf woman: medium skin tone" }, /*818 : People & Body*/
{"🧏🏾‍♀️","deaf woman: medium-dark skin tone" }, /*819 : People & Body*/
{"🧏🏾‍♀","deaf woman: medium-dark skin tone" }, /*820 : People & Body*/
{"🧏🏿‍♀️","deaf woman: dark skin tone" }, /*821 : People & Body*/
{"🧏🏿‍♀","deaf woman: dark skin tone" }, /*822 : People & Body*/
{"🙇","person bowing" }, /*823 : People & Body*/
{"🙇🏻","person bowing: light skin tone" }, /*824 : People & Body*/
{"🙇🏼","person bowing: medium-light skin tone" }, /*825 : People & Body*/
{"🙇🏽","person bowing: medium skin tone" }, /*826 : People & Body*/
{"🙇🏾","person bowing: medium-dark skin tone" }, /*827 : People & Body*/
{"🙇🏿","person bowing: dark skin tone" }, /*828 : People & Body*/
{"🙇‍♂️","man bowing" }, /*829 : People & Body*/
{"🙇‍♂","man bowing" }, /*830 : People & Body*/
{"🙇🏻‍♂️","man bowing: light skin tone" }, /*831 : People & Body*/
{"🙇🏻‍♂","man bowing: light skin tone" }, /*832 : People & Body*/
{"🙇🏼‍♂️","man bowing: medium-light skin tone" }, /*833 : People & Body*/
{"🙇🏼‍♂","man bowing: medium-light skin tone" }, /*834 : People & Body*/
{"🙇🏽‍♂️","man bowing: medium skin tone" }, /*835 : People & Body*/
{"🙇🏽‍♂","man bowing: medium skin tone" }, /*836 : People & Body*/
{"🙇🏾‍♂️","man bowing: medium-dark skin tone" }, /*837 : People & Body*/
{"🙇🏾‍♂","man bowing: medium-dark skin tone" }, /*838 : People & Body*/
{"🙇🏿‍♂️","man bowing: dark skin tone" }, /*839 : People & Body*/
{"🙇🏿‍♂","man bowing: dark skin tone" }, /*840 : People & Body*/
{"🙇‍♀️","woman bowing" }, /*841 : People & Body*/
{"🙇‍♀","woman bowing" }, /*842 : People & Body*/
{"🙇🏻‍♀️","woman bowing: light skin tone" }, /*843 : People & Body*/
{"🙇🏻‍♀","woman bowing: light skin tone" }, /*844 : People & Body*/
{"🙇🏼‍♀️","woman bowing: medium-light skin tone" }, /*845 : People & Body*/
{"🙇🏼‍♀","woman bowing: medium-light skin tone" }, /*846 : People & Body*/
{"🙇🏽‍♀️","woman bowing: medium skin tone" }, /*847 : People & Body*/
{"🙇🏽‍♀","woman bowing: medium skin tone" }, /*848 : People & Body*/
{"🙇🏾‍♀️","woman bowing: medium-dark skin tone" }, /*849 : People & Body*/
{"🙇🏾‍♀","woman bowing: medium-dark skin tone" }, /*850 : People & Body*/
{"🙇🏿‍♀️","woman bowing: dark skin tone" }, /*851 : People & Body*/
{"🙇🏿‍♀","woman bowing: dark skin tone" }, /*852 : People & Body*/
{"🤦","person facepalming" }, /*853 : People & Body*/
{"🤦🏻","person facepalming: light skin tone" }, /*854 : People & Body*/
{"🤦🏼","person facepalming: medium-light skin tone" }, /*855 : People & Body*/
{"🤦🏽","person facepalming: medium skin tone" }, /*856 : People & Body*/
{"🤦🏾","person facepalming: medium-dark skin tone" }, /*857 : People & Body*/
{"🤦🏿","person facepalming: dark skin tone" }, /*858 : People & Body*/
{"🤦‍♂️","man facepalming" }, /*859 : People & Body*/
{"🤦‍♂","man facepalming" }, /*860 : People & Body*/
{"🤦🏻‍♂️","man facepalming: light skin tone" }, /*861 : People & Body*/
{"🤦🏻‍♂","man facepalming: light skin tone" }, /*862 : People & Body*/
{"🤦🏼‍♂️","man facepalming: medium-light skin tone" }, /*863 : People & Body*/
{"🤦🏼‍♂","man facepalming: medium-light skin tone" }, /*864 : People & Body*/
{"🤦🏽‍♂️","man facepalming: medium skin tone" }, /*865 : People & Body*/
{"🤦🏽‍♂","man facepalming: medium skin tone" }, /*866 : People & Body*/
{"🤦🏾‍♂️","man facepalming: medium-dark skin tone" }, /*867 : People & Body*/
{"🤦🏾‍♂","man facepalming: medium-dark skin tone" }, /*868 : People & Body*/
{"🤦🏿‍♂️","man facepalming: dark skin tone" }, /*869 : People & Body*/
{"🤦🏿‍♂","man facepalming: dark skin tone" }, /*870 : People & Body*/
{"🤦‍♀️","woman facepalming" }, /*871 : People & Body*/
{"🤦‍♀","woman facepalming" }, /*872 : People & Body*/
{"🤦🏻‍♀️","woman facepalming: light skin tone" }, /*873 : People & Body*/
{"🤦🏻‍♀","woman facepalming: light skin tone" }, /*874 : People & Body*/
{"🤦🏼‍♀️","woman facepalming: medium-light skin tone" }, /*875 : People & Body*/
{"🤦🏼‍♀","woman facepalming: medium-light skin tone" }, /*876 : People & Body*/
{"🤦🏽‍♀️","woman facepalming: medium skin tone" }, /*877 : People & Body*/
{"🤦🏽‍♀","woman facepalming: medium skin tone" }, /*878 : People & Body*/
{"🤦🏾‍♀️","woman facepalming: medium-dark skin tone" }, /*879 : People & Body*/
{"🤦🏾‍♀","woman facepalming: medium-dark skin tone" }, /*880 : People & Body*/
{"🤦🏿‍♀️","woman facepalming: dark skin tone" }, /*881 : People & Body*/
{"🤦🏿‍♀","woman facepalming: dark skin tone" }, /*882 : People & Body*/
{"🤷","person shrugging" }, /*883 : People & Body*/
{"🤷🏻","person shrugging: light skin tone" }, /*884 : People & Body*/
{"🤷🏼","person shrugging: medium-light skin tone" }, /*885 : People & Body*/
{"🤷🏽","person shrugging: medium skin tone" }, /*886 : People & Body*/
{"🤷🏾","person shrugging: medium-dark skin tone" }, /*887 : People & Body*/
{"🤷🏿","person shrugging: dark skin tone" }, /*888 : People & Body*/
{"🤷‍♂️","man shrugging" }, /*889 : People & Body*/
{"🤷‍♂","man shrugging" }, /*890 : People & Body*/
{"🤷🏻‍♂️","man shrugging: light skin tone" }, /*891 : People & Body*/
{"🤷🏻‍♂","man shrugging: light skin tone" }, /*892 : People & Body*/
{"🤷🏼‍♂️","man shrugging: medium-light skin tone" }, /*893 : People & Body*/
{"🤷🏼‍♂","man shrugging: medium-light skin tone" }, /*894 : People & Body*/
{"🤷🏽‍♂️","man shrugging: medium skin tone" }, /*895 : People & Body*/
{"🤷🏽‍♂","man shrugging: medium skin tone" }, /*896 : People & Body*/
{"🤷🏾‍♂️","man shrugging: medium-dark skin tone" }, /*897 : People & Body*/
{"🤷🏾‍♂","man shrugging: medium-dark skin tone" }, /*898 : People & Body*/
{"🤷🏿‍♂️","man shrugging: dark skin tone" }, /*899 : People & Body*/
{"🤷🏿‍♂","man shrugging: dark skin tone" }, /*900 : People & Body*/
{"🤷‍♀️","woman shrugging" }, /*901 : People & Body*/
{"🤷‍♀","woman shrugging" }, /*902 : People & Body*/
{"🤷🏻‍♀️","woman shrugging: light skin tone" }, /*903 : People & Body*/
{"🤷🏻‍♀","woman shrugging: light skin tone" }, /*904 : People & Body*/
{"🤷🏼‍♀️","woman shrugging: medium-light skin tone" }, /*905 : People & Body*/
{"🤷🏼‍♀","woman shrugging: medium-light skin tone" }, /*906 : People & Body*/
{"🤷🏽‍♀️","woman shrugging: medium skin tone" }, /*907 : People & Body*/
{"🤷🏽‍♀","woman shrugging: medium skin tone" }, /*908 : People & Body*/
{"🤷🏾‍♀️","woman shrugging: medium-dark skin tone" }, /*909 : People & Body*/
{"🤷🏾‍♀","woman shrugging: medium-dark skin tone" }, /*910 : People & Body*/
{"🤷🏿‍♀️","woman shrugging: dark skin tone" }, /*911 : People & Body*/
{"🤷🏿‍♀","woman shrugging: dark skin tone" }, /*912 : People & Body*/
{"🧑‍⚕️","health worker" }, /*913 : People & Body*/
{"🧑‍⚕","health worker" }, /*914 : People & Body*/
{"🧑🏻‍⚕️","health worker: light skin tone" }, /*915 : People & Body*/
{"🧑🏻‍⚕","health worker: light skin tone" }, /*916 : People & Body*/
{"🧑🏼‍⚕️","health worker: medium-light skin tone" }, /*917 : People & Body*/
{"🧑🏼‍⚕","health worker: medium-light skin tone" }, /*918 : People & Body*/
{"🧑🏽‍⚕️","health worker: medium skin tone" }, /*919 : People & Body*/
{"🧑🏽‍⚕","health worker: medium skin tone" }, /*920 : People & Body*/
{"🧑🏾‍⚕️","health worker: medium-dark skin tone" }, /*921 : People & Body*/
{"🧑🏾‍⚕","health worker: medium-dark skin tone" }, /*922 : People & Body*/
{"🧑🏿‍⚕️","health worker: dark skin tone" }, /*923 : People & Body*/
{"🧑🏿‍⚕","health worker: dark skin tone" }, /*924 : People & Body*/
{"👨‍⚕️","man health worker" }, /*925 : People & Body*/
{"👨‍⚕","man health worker" }, /*926 : People & Body*/
{"👨🏻‍⚕️","man health worker: light skin tone" }, /*927 : People & Body*/
{"👨🏻‍⚕","man health worker: light skin tone" }, /*928 : People & Body*/
{"👨🏼‍⚕️","man health worker: medium-light skin tone" }, /*929 : People & Body*/
{"👨🏼‍⚕","man health worker: medium-light skin tone" }, /*930 : People & Body*/
{"👨🏽‍⚕️","man health worker: medium skin tone" }, /*931 : People & Body*/
{"👨🏽‍⚕","man health worker: medium skin tone" }, /*932 : People & Body*/
{"👨🏾‍⚕️","man health worker: medium-dark skin tone" }, /*933 : People & Body*/
{"👨🏾‍⚕","man health worker: medium-dark skin tone" }, /*934 : People & Body*/
{"👨🏿‍⚕️","man health worker: dark skin tone" }, /*935 : People & Body*/
{"👨🏿‍⚕","man health worker: dark skin tone" }, /*936 : People & Body*/
{"👩‍⚕️","woman health worker" }, /*937 : People & Body*/
{"👩‍⚕","woman health worker" }, /*938 : People & Body*/
{"👩🏻‍⚕️","woman health worker: light skin tone" }, /*939 : People & Body*/
{"👩🏻‍⚕","woman health worker: light skin tone" }, /*940 : People & Body*/
{"👩🏼‍⚕️","woman health worker: medium-light skin tone" }, /*941 : People & Body*/
{"👩🏼‍⚕","woman health worker: medium-light skin tone" }, /*942 : People & Body*/
{"👩🏽‍⚕️","woman health worker: medium skin tone" }, /*943 : People & Body*/
{"👩🏽‍⚕","woman health worker: medium skin tone" }, /*944 : People & Body*/
{"👩🏾‍⚕️","woman health worker: medium-dark skin tone" }, /*945 : People & Body*/
{"👩🏾‍⚕","woman health worker: medium-dark skin tone" }, /*946 : People & Body*/
{"👩🏿‍⚕️","woman health worker: dark skin tone" }, /*947 : People & Body*/
{"👩🏿‍⚕","woman health worker: dark skin tone" }, /*948 : People & Body*/
{"🧑‍🎓","student" }, /*949 : People & Body*/
{"🧑🏻‍🎓","student: light skin tone" }, /*950 : People & Body*/
{"🧑🏼‍🎓","student: medium-light skin tone" }, /*951 : People & Body*/
{"🧑🏽‍🎓","student: medium skin tone" }, /*952 : People & Body*/
{"🧑🏾‍🎓","student: medium-dark skin tone" }, /*953 : People & Body*/
{"🧑🏿‍🎓","student: dark skin tone" }, /*954 : People & Body*/
{"👨‍🎓","man student" }, /*955 : People & Body*/
{"👨🏻‍🎓","man student: light skin tone" }, /*956 : People & Body*/
{"👨🏼‍🎓","man student: medium-light skin tone" }, /*957 : People & Body*/
{"👨🏽‍🎓","man student: medium skin tone" }, /*958 : People & Body*/
{"👨🏾‍🎓","man student: medium-dark skin tone" }, /*959 : People & Body*/
{"👨🏿‍🎓","man student: dark skin tone" }, /*960 : People & Body*/
{"👩‍🎓","woman student" }, /*961 : People & Body*/
{"👩🏻‍🎓","woman student: light skin tone" }, /*962 : People & Body*/
{"👩🏼‍🎓","woman student: medium-light skin tone" }, /*963 : People & Body*/
{"👩🏽‍🎓","woman student: medium skin tone" }, /*964 : People & Body*/
{"👩🏾‍🎓","woman student: medium-dark skin tone" }, /*965 : People & Body*/
{"👩🏿‍🎓","woman student: dark skin tone" }, /*966 : People & Body*/
{"🧑‍🏫","teacher" }, /*967 : People & Body*/
{"🧑🏻‍🏫","teacher: light skin tone" }, /*968 : People & Body*/
{"🧑🏼‍🏫","teacher: medium-light skin tone" }, /*969 : People & Body*/
{"🧑🏽‍🏫","teacher: medium skin tone" }, /*970 : People & Body*/
{"🧑🏾‍🏫","teacher: medium-dark skin tone" }, /*971 : People & Body*/
{"🧑🏿‍🏫","teacher: dark skin tone" }, /*972 : People & Body*/
{"👨‍🏫","man teacher" }, /*973 : People & Body*/
{"👨🏻‍🏫","man teacher: light skin tone" }, /*974 : People & Body*/
{"👨🏼‍🏫","man teacher: medium-light skin tone" }, /*975 : People & Body*/
{"👨🏽‍🏫","man teacher: medium skin tone" }, /*976 : People & Body*/
{"👨🏾‍🏫","man teacher: medium-dark skin tone" }, /*977 : People & Body*/
{"👨🏿‍🏫","man teacher: dark skin tone" }, /*978 : People & Body*/
{"👩‍🏫","woman teacher" }, /*979 : People & Body*/
{"👩🏻‍🏫","woman teacher: light skin tone" }, /*980 : People & Body*/
{"👩🏼‍🏫","woman teacher: medium-light skin tone" }, /*981 : People & Body*/
{"👩🏽‍🏫","woman teacher: medium skin tone" }, /*982 : People & Body*/
{"👩🏾‍🏫","woman teacher: medium-dark skin tone" }, /*983 : People & Body*/
{"👩🏿‍🏫","woman teacher: dark skin tone" }, /*984 : People & Body*/
{"🧑‍⚖️","judge" }, /*985 : People & Body*/
{"🧑‍⚖","judge" }, /*986 : People & Body*/
{"🧑🏻‍⚖️","judge: light skin tone" }, /*987 : People & Body*/
{"🧑🏻‍⚖","judge: light skin tone" }, /*988 : People & Body*/
{"🧑🏼‍⚖️","judge: medium-light skin tone" }, /*989 : People & Body*/
{"🧑🏼‍⚖","judge: medium-light skin tone" }, /*990 : People & Body*/
{"🧑🏽‍⚖️","judge: medium skin tone" }, /*991 : People & Body*/
{"🧑🏽‍⚖","judge: medium skin tone" }, /*992 : People & Body*/
{"🧑🏾‍⚖️","judge: medium-dark skin tone" }, /*993 : People & Body*/
{"🧑🏾‍⚖","judge: medium-dark skin tone" }, /*994 : People & Body*/
{"🧑🏿‍⚖️","judge: dark skin tone" }, /*995 : People & Body*/
{"🧑🏿‍⚖","judge: dark skin tone" }, /*996 : People & Body*/
{"👨‍⚖️","man judge" }, /*997 : People & Body*/
{"👨‍⚖","man judge" }, /*998 : People & Body*/
{"👨🏻‍⚖️","man judge: light skin tone" }, /*999 : People & Body*/
{"👨🏻‍⚖","man judge: light skin tone" }, /*1000 : People & Body*/
{"👨🏼‍⚖️","man judge: medium-light skin tone" }, /*1001 : People & Body*/
{"👨🏼‍⚖","man judge: medium-light skin tone" }, /*1002 : People & Body*/
{"👨🏽‍⚖️","man judge: medium skin tone" }, /*1003 : People & Body*/
{"👨🏽‍⚖","man judge: medium skin tone" }, /*1004 : People & Body*/
{"👨🏾‍⚖️","man judge: medium-dark skin tone" }, /*1005 : People & Body*/
{"👨🏾‍⚖","man judge: medium-dark skin tone" }, /*1006 : People & Body*/
{"👨🏿‍⚖️","man judge: dark skin tone" }, /*1007 : People & Body*/
{"👨🏿‍⚖","man judge: dark skin tone" }, /*1008 : People & Body*/
{"👩‍⚖️","woman judge" }, /*1009 : People & Body*/
{"👩‍⚖","woman judge" }, /*1010 : People & Body*/
{"👩🏻‍⚖️","woman judge: light skin tone" }, /*1011 : People & Body*/
{"👩🏻‍⚖","woman judge: light skin tone" }, /*1012 : People & Body*/
{"👩🏼‍⚖️","woman judge: medium-light skin tone" }, /*1013 : People & Body*/
{"👩🏼‍⚖","woman judge: medium-light skin tone" }, /*1014 : People & Body*/
{"👩🏽‍⚖️","woman judge: medium skin tone" }, /*1015 : People & Body*/
{"👩🏽‍⚖","woman judge: medium skin tone" }, /*1016 : People & Body*/
{"👩🏾‍⚖️","woman judge: medium-dark skin tone" }, /*1017 : People & Body*/
{"👩🏾‍⚖","woman judge: medium-dark skin tone" }, /*1018 : People & Body*/
{"👩🏿‍⚖️","woman judge: dark skin tone" }, /*1019 : People & Body*/
{"👩🏿‍⚖","woman judge: dark skin tone" }, /*1020 : People & Body*/
{"🧑‍🌾","farmer" }, /*1021 : People & Body*/
{"🧑🏻‍🌾","farmer: light skin tone" }, /*1022 : People & Body*/
{"🧑🏼‍🌾","farmer: medium-light skin tone" }, /*1023 : People & Body*/
{"🧑🏽‍🌾","farmer: medium skin tone" }, /*1024 : People & Body*/
{"🧑🏾‍🌾","farmer: medium-dark skin tone" }, /*1025 : People & Body*/
{"🧑🏿‍🌾","farmer: dark skin tone" }, /*1026 : People & Body*/
{"👨‍🌾","man farmer" }, /*1027 : People & Body*/
{"👨🏻‍🌾","man farmer: light skin tone" }, /*1028 : People & Body*/
{"👨🏼‍🌾","man farmer: medium-light skin tone" }, /*1029 : People & Body*/
{"👨🏽‍🌾","man farmer: medium skin tone" }, /*1030 : People & Body*/
{"👨🏾‍🌾","man farmer: medium-dark skin tone" }, /*1031 : People & Body*/
{"👨🏿‍🌾","man farmer: dark skin tone" }, /*1032 : People & Body*/
{"👩‍🌾","woman farmer" }, /*1033 : People & Body*/
{"👩🏻‍🌾","woman farmer: light skin tone" }, /*1034 : People & Body*/
{"👩🏼‍🌾","woman farmer: medium-light skin tone" }, /*1035 : People & Body*/
{"👩🏽‍🌾","woman farmer: medium skin tone" }, /*1036 : People & Body*/
{"👩🏾‍🌾","woman farmer: medium-dark skin tone" }, /*1037 : People & Body*/
{"👩🏿‍🌾","woman farmer: dark skin tone" }, /*1038 : People & Body*/
{"🧑‍🍳","cook" }, /*1039 : People & Body*/
{"🧑🏻‍🍳","cook: light skin tone" }, /*1040 : People & Body*/
{"🧑🏼‍🍳","cook: medium-light skin tone" }, /*1041 : People & Body*/
{"🧑🏽‍🍳","cook: medium skin tone" }, /*1042 : People & Body*/
{"🧑🏾‍🍳","cook: medium-dark skin tone" }, /*1043 : People & Body*/
{"🧑🏿‍🍳","cook: dark skin tone" }, /*1044 : People & Body*/
{"👨‍🍳","man cook" }, /*1045 : People & Body*/
{"👨🏻‍🍳","man cook: light skin tone" }, /*1046 : People & Body*/
{"👨🏼‍🍳","man cook: medium-light skin tone" }, /*1047 : People & Body*/
{"👨🏽‍🍳","man cook: medium skin tone" }, /*1048 : People & Body*/
{"👨🏾‍🍳","man cook: medium-dark skin tone" }, /*1049 : People & Body*/
{"👨🏿‍🍳","man cook: dark skin tone" }, /*1050 : People & Body*/
{"👩‍🍳","woman cook" }, /*1051 : People & Body*/
{"👩🏻‍🍳","woman cook: light skin tone" }, /*1052 : People & Body*/
{"👩🏼‍🍳","woman cook: medium-light skin tone" }, /*1053 : People & Body*/
{"👩🏽‍🍳","woman cook: medium skin tone" }, /*1054 : People & Body*/
{"👩🏾‍🍳","woman cook: medium-dark skin tone" }, /*1055 : People & Body*/
{"👩🏿‍🍳","woman cook: dark skin tone" }, /*1056 : People & Body*/
{"🧑‍🔧","mechanic" }, /*1057 : People & Body*/
{"🧑🏻‍🔧","mechanic: light skin tone" }, /*1058 : People & Body*/
{"🧑🏼‍🔧","mechanic: medium-light skin tone" }, /*1059 : People & Body*/
{"🧑🏽‍🔧","mechanic: medium skin tone" }, /*1060 : People & Body*/
{"🧑🏾‍🔧","mechanic: medium-dark skin tone" }, /*1061 : People & Body*/
{"🧑🏿‍🔧","mechanic: dark skin tone" }, /*1062 : People & Body*/
{"👨‍🔧","man mechanic" }, /*1063 : People & Body*/
{"👨🏻‍🔧","man mechanic: light skin tone" }, /*1064 : People & Body*/
{"👨🏼‍🔧","man mechanic: medium-light skin tone" }, /*1065 : People & Body*/
{"👨🏽‍🔧","man mechanic: medium skin tone" }, /*1066 : People & Body*/
{"👨🏾‍🔧","man mechanic: medium-dark skin tone" }, /*1067 : People & Body*/
{"👨🏿‍🔧","man mechanic: dark skin tone" }, /*1068 : People & Body*/
{"👩‍🔧","woman mechanic" }, /*1069 : People & Body*/
{"👩🏻‍🔧","woman mechanic: light skin tone" }, /*1070 : People & Body*/
{"👩🏼‍🔧","woman mechanic: medium-light skin tone" }, /*1071 : People & Body*/
{"👩🏽‍🔧","woman mechanic: medium skin tone" }, /*1072 : People & Body*/
{"👩🏾‍🔧","woman mechanic: medium-dark skin tone" }, /*1073 : People & Body*/
{"👩🏿‍🔧","woman mechanic: dark skin tone" }, /*1074 : People & Body*/
{"🧑‍🏭","factory worker" }, /*1075 : People & Body*/
{"🧑🏻‍🏭","factory worker: light skin tone" }, /*1076 : People & Body*/
{"🧑🏼‍🏭","factory worker: medium-light skin tone" }, /*1077 : People & Body*/
{"🧑🏽‍🏭","factory worker: medium skin tone" }, /*1078 : People & Body*/
{"🧑🏾‍🏭","factory worker: medium-dark skin tone" }, /*1079 : People & Body*/
{"🧑🏿‍🏭","factory worker: dark skin tone" }, /*1080 : People & Body*/
{"👨‍🏭","man factory worker" }, /*1081 : People & Body*/
{"👨🏻‍🏭","man factory worker: light skin tone" }, /*1082 : People & Body*/
{"👨🏼‍🏭","man factory worker: medium-light skin tone" }, /*1083 : People & Body*/
{"👨🏽‍🏭","man factory worker: medium skin tone" }, /*1084 : People & Body*/
{"👨🏾‍🏭","man factory worker: medium-dark skin tone" }, /*1085 : People & Body*/
{"👨🏿‍🏭","man factory worker: dark skin tone" }, /*1086 : People & Body*/
{"👩‍🏭","woman factory worker" }, /*1087 : People & Body*/
{"👩🏻‍🏭","woman factory worker: light skin tone" }, /*1088 : People & Body*/
{"👩🏼‍🏭","woman factory worker: medium-light skin tone" }, /*1089 : People & Body*/
{"👩🏽‍🏭","woman factory worker: medium skin tone" }, /*1090 : People & Body*/
{"👩🏾‍🏭","woman factory worker: medium-dark skin tone" }, /*1091 : People & Body*/
{"👩🏿‍🏭","woman factory worker: dark skin tone" }, /*1092 : People & Body*/
{"🧑‍💼","office worker" }, /*1093 : People & Body*/
{"🧑🏻‍💼","office worker: light skin tone" }, /*1094 : People & Body*/
{"🧑🏼‍💼","office worker: medium-light skin tone" }, /*1095 : People & Body*/
{"🧑🏽‍💼","office worker: medium skin tone" }, /*1096 : People & Body*/
{"🧑🏾‍💼","office worker: medium-dark skin tone" }, /*1097 : People & Body*/
{"🧑🏿‍💼","office worker: dark skin tone" }, /*1098 : People & Body*/
{"👨‍💼","man office worker" }, /*1099 : People & Body*/
{"👨🏻‍💼","man office worker: light skin tone" }, /*1100 : People & Body*/
{"👨🏼‍💼","man office worker: medium-light skin tone" }, /*1101 : People & Body*/
{"👨🏽‍💼","man office worker: medium skin tone" }, /*1102 : People & Body*/
{"👨🏾‍💼","man office worker: medium-dark skin tone" }, /*1103 : People & Body*/
{"👨🏿‍💼","man office worker: dark skin tone" }, /*1104 : People & Body*/
{"👩‍💼","woman office worker" }, /*1105 : People & Body*/
{"👩🏻‍💼","woman office worker: light skin tone" }, /*1106 : People & Body*/
{"👩🏼‍💼","woman office worker: medium-light skin tone" }, /*1107 : People & Body*/
{"👩🏽‍💼","woman office worker: medium skin tone" }, /*1108 : People & Body*/
{"👩🏾‍💼","woman office worker: medium-dark skin tone" }, /*1109 : People & Body*/
{"👩🏿‍💼","woman office worker: dark skin tone" }, /*1110 : People & Body*/
{"🧑‍🔬","scientist" }, /*1111 : People & Body*/
{"🧑🏻‍🔬","scientist: light skin tone" }, /*1112 : People & Body*/
{"🧑🏼‍🔬","scientist: medium-light skin tone" }, /*1113 : People & Body*/
{"🧑🏽‍🔬","scientist: medium skin tone" }, /*1114 : People & Body*/
{"🧑🏾‍🔬","scientist: medium-dark skin tone" }, /*1115 : People & Body*/
{"🧑🏿‍🔬","scientist: dark skin tone" }, /*1116 : People & Body*/
{"👨‍🔬","man scientist" }, /*1117 : People & Body*/
{"👨🏻‍🔬","man scientist: light skin tone" }, /*1118 : People & Body*/
{"👨🏼‍🔬","man scientist: medium-light skin tone" }, /*1119 : People & Body*/
{"👨🏽‍🔬","man scientist: medium skin tone" }, /*1120 : People & Body*/
{"👨🏾‍🔬","man scientist: medium-dark skin tone" }, /*1121 : People & Body*/
{"👨🏿‍🔬","man scientist: dark skin tone" }, /*1122 : People & Body*/
{"👩‍🔬","woman scientist" }, /*1123 : People & Body*/
{"👩🏻‍🔬","woman scientist: light skin tone" }, /*1124 : People & Body*/
{"👩🏼‍🔬","woman scientist: medium-light skin tone" }, /*1125 : People & Body*/
{"👩🏽‍🔬","woman scientist: medium skin tone" }, /*1126 : People & Body*/
{"👩🏾‍🔬","woman scientist: medium-dark skin tone" }, /*1127 : People & Body*/
{"👩🏿‍🔬","woman scientist: dark skin tone" }, /*1128 : People & Body*/
{"🧑‍💻","technologist" }, /*1129 : People & Body*/
{"🧑🏻‍💻","technologist: light skin tone" }, /*1130 : People & Body*/
{"🧑🏼‍💻","technologist: medium-light skin tone" }, /*1131 : People & Body*/
{"🧑🏽‍💻","technologist: medium skin tone" }, /*1132 : People & Body*/
{"🧑🏾‍💻","technologist: medium-dark skin tone" }, /*1133 : People & Body*/
{"🧑🏿‍💻","technologist: dark skin tone" }, /*1134 : People & Body*/
{"👨‍💻","man technologist" }, /*1135 : People & Body*/
{"👨🏻‍💻","man technologist: light skin tone" }, /*1136 : People & Body*/
{"👨🏼‍💻","man technologist: medium-light skin tone" }, /*1137 : People & Body*/
{"👨🏽‍💻","man technologist: medium skin tone" }, /*1138 : People & Body*/
{"👨🏾‍💻","man technologist: medium-dark skin tone" }, /*1139 : People & Body*/
{"👨🏿‍💻","man technologist: dark skin tone" }, /*1140 : People & Body*/
{"👩‍💻","woman technologist" }, /*1141 : People & Body*/
{"👩🏻‍💻","woman technologist: light skin tone" }, /*1142 : People & Body*/
{"👩🏼‍💻","woman technologist: medium-light skin tone" }, /*1143 : People & Body*/
{"👩🏽‍💻","woman technologist: medium skin tone" }, /*1144 : People & Body*/
{"👩🏾‍💻","woman technologist: medium-dark skin tone" }, /*1145 : People & Body*/
{"👩🏿‍💻","woman technologist: dark skin tone" }, /*1146 : People & Body*/
{"🧑‍🎤","singer" }, /*1147 : People & Body*/
{"🧑🏻‍🎤","singer: light skin tone" }, /*1148 : People & Body*/
{"🧑🏼‍🎤","singer: medium-light skin tone" }, /*1149 : People & Body*/
{"🧑🏽‍🎤","singer: medium skin tone" }, /*1150 : People & Body*/
{"🧑🏾‍🎤","singer: medium-dark skin tone" }, /*1151 : People & Body*/
{"🧑🏿‍🎤","singer: dark skin tone" }, /*1152 : People & Body*/
{"👨‍🎤","man singer" }, /*1153 : People & Body*/
{"👨🏻‍🎤","man singer: light skin tone" }, /*1154 : People & Body*/
{"👨🏼‍🎤","man singer: medium-light skin tone" }, /*1155 : People & Body*/
{"👨🏽‍🎤","man singer: medium skin tone" }, /*1156 : People & Body*/
{"👨🏾‍🎤","man singer: medium-dark skin tone" }, /*1157 : People & Body*/
{"👨🏿‍🎤","man singer: dark skin tone" }, /*1158 : People & Body*/
{"👩‍🎤","woman singer" }, /*1159 : People & Body*/
{"👩🏻‍🎤","woman singer: light skin tone" }, /*1160 : People & Body*/
{"👩🏼‍🎤","woman singer: medium-light skin tone" }, /*1161 : People & Body*/
{"👩🏽‍🎤","woman singer: medium skin tone" }, /*1162 : People & Body*/
{"👩🏾‍🎤","woman singer: medium-dark skin tone" }, /*1163 : People & Body*/
{"👩🏿‍🎤","woman singer: dark skin tone" }, /*1164 : People & Body*/
{"🧑‍🎨","artist" }, /*1165 : People & Body*/
{"🧑🏻‍🎨","artist: light skin tone" }, /*1166 : People & Body*/
{"🧑🏼‍🎨","artist: medium-light skin tone" }, /*1167 : People & Body*/
{"🧑🏽‍🎨","artist: medium skin tone" }, /*1168 : People & Body*/
{"🧑🏾‍🎨","artist: medium-dark skin tone" }, /*1169 : People & Body*/
{"🧑🏿‍🎨","artist: dark skin tone" }, /*1170 : People & Body*/
{"👨‍🎨","man artist" }, /*1171 : People & Body*/
{"👨🏻‍🎨","man artist: light skin tone" }, /*1172 : People & Body*/
{"👨🏼‍🎨","man artist: medium-light skin tone" }, /*1173 : People & Body*/
{"👨🏽‍🎨","man artist: medium skin tone" }, /*1174 : People & Body*/
{"👨🏾‍🎨","man artist: medium-dark skin tone" }, /*1175 : People & Body*/
{"👨🏿‍🎨","man artist: dark skin tone" }, /*1176 : People & Body*/
{"👩‍🎨","woman artist" }, /*1177 : People & Body*/
{"👩🏻‍🎨","woman artist: light skin tone" }, /*1178 : People & Body*/
{"👩🏼‍🎨","woman artist: medium-light skin tone" }, /*1179 : People & Body*/
{"👩🏽‍🎨","woman artist: medium skin tone" }, /*1180 : People & Body*/
{"👩🏾‍🎨","woman artist: medium-dark skin tone" }, /*1181 : People & Body*/
{"👩🏿‍🎨","woman artist: dark skin tone" }, /*1182 : People & Body*/
{"🧑‍✈️","pilot" }, /*1183 : People & Body*/
{"🧑‍✈","pilot" }, /*1184 : People & Body*/
{"🧑🏻‍✈️","pilot: light skin tone" }, /*1185 : People & Body*/
{"🧑🏻‍✈","pilot: light skin tone" }, /*1186 : People & Body*/
{"🧑🏼‍✈️","pilot: medium-light skin tone" }, /*1187 : People & Body*/
{"🧑🏼‍✈","pilot: medium-light skin tone" }, /*1188 : People & Body*/
{"🧑🏽‍✈️","pilot: medium skin tone" }, /*1189 : People & Body*/
{"🧑🏽‍✈","pilot: medium skin tone" }, /*1190 : People & Body*/
{"🧑🏾‍✈️","pilot: medium-dark skin tone" }, /*1191 : People & Body*/
{"🧑🏾‍✈","pilot: medium-dark skin tone" }, /*1192 : People & Body*/
{"🧑🏿‍✈️","pilot: dark skin tone" }, /*1193 : People & Body*/
{"🧑🏿‍✈","pilot: dark skin tone" }, /*1194 : People & Body*/
{"👨‍✈️","man pilot" }, /*1195 : People & Body*/
{"👨‍✈","man pilot" }, /*1196 : People & Body*/
{"👨🏻‍✈️","man pilot: light skin tone" }, /*1197 : People & Body*/
{"👨🏻‍✈","man pilot: light skin tone" }, /*1198 : People & Body*/
{"👨🏼‍✈️","man pilot: medium-light skin tone" }, /*1199 : People & Body*/
{"👨🏼‍✈","man pilot: medium-light skin tone" }, /*1200 : People & Body*/
{"👨🏽‍✈️","man pilot: medium skin tone" }, /*1201 : People & Body*/
{"👨🏽‍✈","man pilot: medium skin tone" }, /*1202 : People & Body*/
{"👨🏾‍✈️","man pilot: medium-dark skin tone" }, /*1203 : People & Body*/
{"👨🏾‍✈","man pilot: medium-dark skin tone" }, /*1204 : People & Body*/
{"👨🏿‍✈️","man pilot: dark skin tone" }, /*1205 : People & Body*/
{"👨🏿‍✈","man pilot: dark skin tone" }, /*1206 : People & Body*/
{"👩‍✈️","woman pilot" }, /*1207 : People & Body*/
{"👩‍✈","woman pilot" }, /*1208 : People & Body*/
{"👩🏻‍✈️","woman pilot: light skin tone" }, /*1209 : People & Body*/
{"👩🏻‍✈","woman pilot: light skin tone" }, /*1210 : People & Body*/
{"👩🏼‍✈️","woman pilot: medium-light skin tone" }, /*1211 : People & Body*/
{"👩🏼‍✈","woman pilot: medium-light skin tone" }, /*1212 : People & Body*/
{"👩🏽‍✈️","woman pilot: medium skin tone" }, /*1213 : People & Body*/
{"👩🏽‍✈","woman pilot: medium skin tone" }, /*1214 : People & Body*/
{"👩🏾‍✈️","woman pilot: medium-dark skin tone" }, /*1215 : People & Body*/
{"👩🏾‍✈","woman pilot: medium-dark skin tone" }, /*1216 : People & Body*/
{"👩🏿‍✈️","woman pilot: dark skin tone" }, /*1217 : People & Body*/
{"👩🏿‍✈","woman pilot: dark skin tone" }, /*1218 : People & Body*/
{"🧑‍🚀","astronaut" }, /*1219 : People & Body*/
{"🧑🏻‍🚀","astronaut: light skin tone" }, /*1220 : People & Body*/
{"🧑🏼‍🚀","astronaut: medium-light skin tone" }, /*1221 : People & Body*/
{"🧑🏽‍🚀","astronaut: medium skin tone" }, /*1222 : People & Body*/
{"🧑🏾‍🚀","astronaut: medium-dark skin tone" }, /*1223 : People & Body*/
{"🧑🏿‍🚀","astronaut: dark skin tone" }, /*1224 : People & Body*/
{"👨‍🚀","man astronaut" }, /*1225 : People & Body*/
{"👨🏻‍🚀","man astronaut: light skin tone" }, /*1226 : People & Body*/
{"👨🏼‍🚀","man astronaut: medium-light skin tone" }, /*1227 : People & Body*/
{"👨🏽‍🚀","man astronaut: medium skin tone" }, /*1228 : People & Body*/
{"👨🏾‍🚀","man astronaut: medium-dark skin tone" }, /*1229 : People & Body*/
{"👨🏿‍🚀","man astronaut: dark skin tone" }, /*1230 : People & Body*/
{"👩‍🚀","woman astronaut" }, /*1231 : People & Body*/
{"👩🏻‍🚀","woman astronaut: light skin tone" }, /*1232 : People & Body*/
{"👩🏼‍🚀","woman astronaut: medium-light skin tone" }, /*1233 : People & Body*/
{"👩🏽‍🚀","woman astronaut: medium skin tone" }, /*1234 : People & Body*/
{"👩🏾‍🚀","woman astronaut: medium-dark skin tone" }, /*1235 : People & Body*/
{"👩🏿‍🚀","woman astronaut: dark skin tone" }, /*1236 : People & Body*/
{"🧑‍🚒","firefighter" }, /*1237 : People & Body*/
{"🧑🏻‍🚒","firefighter: light skin tone" }, /*1238 : People & Body*/
{"🧑🏼‍🚒","firefighter: medium-light skin tone" }, /*1239 : People & Body*/
{"🧑🏽‍🚒","firefighter: medium skin tone" }, /*1240 : People & Body*/
{"🧑🏾‍🚒","firefighter: medium-dark skin tone" }, /*1241 : People & Body*/
{"🧑🏿‍🚒","firefighter: dark skin tone" }, /*1242 : People & Body*/
{"👨‍🚒","man firefighter" }, /*1243 : People & Body*/
{"👨🏻‍🚒","man firefighter: light skin tone" }, /*1244 : People & Body*/
{"👨🏼‍🚒","man firefighter: medium-light skin tone" }, /*1245 : People & Body*/
{"👨🏽‍🚒","man firefighter: medium skin tone" }, /*1246 : People & Body*/
{"👨🏾‍🚒","man firefighter: medium-dark skin tone" }, /*1247 : People & Body*/
{"👨🏿‍🚒","man firefighter: dark skin tone" }, /*1248 : People & Body*/
{"👩‍🚒","woman firefighter" }, /*1249 : People & Body*/
{"👩🏻‍🚒","woman firefighter: light skin tone" }, /*1250 : People & Body*/
{"👩🏼‍🚒","woman firefighter: medium-light skin tone" }, /*1251 : People & Body*/
{"👩🏽‍🚒","woman firefighter: medium skin tone" }, /*1252 : People & Body*/
{"👩🏾‍🚒","woman firefighter: medium-dark skin tone" }, /*1253 : People & Body*/
{"👩🏿‍🚒","woman firefighter: dark skin tone" }, /*1254 : People & Body*/
{"👮","police officer" }, /*1255 : People & Body*/
{"👮🏻","police officer: light skin tone" }, /*1256 : People & Body*/
{"👮🏼","police officer: medium-light skin tone" }, /*1257 : People & Body*/
{"👮🏽","police officer: medium skin tone" }, /*1258 : People & Body*/
{"👮🏾","police officer: medium-dark skin tone" }, /*1259 : People & Body*/
{"👮🏿","police officer: dark skin tone" }, /*1260 : People & Body*/
{"👮‍♂️","man police officer" }, /*1261 : People & Body*/
{"👮‍♂","man police officer" }, /*1262 : People & Body*/
{"👮🏻‍♂️","man police officer: light skin tone" }, /*1263 : People & Body*/
{"👮🏻‍♂","man police officer: light skin tone" }, /*1264 : People & Body*/
{"👮🏼‍♂️","man police officer: medium-light skin tone" }, /*1265 : People & Body*/
{"👮🏼‍♂","man police officer: medium-light skin tone" }, /*1266 : People & Body*/
{"👮🏽‍♂️","man police officer: medium skin tone" }, /*1267 : People & Body*/
{"👮🏽‍♂","man police officer: medium skin tone" }, /*1268 : People & Body*/
{"👮🏾‍♂️","man police officer: medium-dark skin tone" }, /*1269 : People & Body*/
{"👮🏾‍♂","man police officer: medium-dark skin tone" }, /*1270 : People & Body*/
{"👮🏿‍♂️","man police officer: dark skin tone" }, /*1271 : People & Body*/
{"👮🏿‍♂","man police officer: dark skin tone" }, /*1272 : People & Body*/
{"👮‍♀️","woman police officer" }, /*1273 : People & Body*/
{"👮‍♀","woman police officer" }, /*1274 : People & Body*/
{"👮🏻‍♀️","woman police officer: light skin tone" }, /*1275 : People & Body*/
{"👮🏻‍♀","woman police officer: light skin tone" }, /*1276 : People & Body*/
{"👮🏼‍♀️","woman police officer: medium-light skin tone" }, /*1277 : People & Body*/
{"👮🏼‍♀","woman police officer: medium-light skin tone" }, /*1278 : People & Body*/
{"👮🏽‍♀️","woman police officer: medium skin tone" }, /*1279 : People & Body*/
{"👮🏽‍♀","woman police officer: medium skin tone" }, /*1280 : People & Body*/
{"👮🏾‍♀️","woman police officer: medium-dark skin tone" }, /*1281 : People & Body*/
{"👮🏾‍♀","woman police officer: medium-dark skin tone" }, /*1282 : People & Body*/
{"👮🏿‍♀️","woman police officer: dark skin tone" }, /*1283 : People & Body*/
{"👮🏿‍♀","woman police officer: dark skin tone" }, /*1284 : People & Body*/
{"🕵️","detective" }, /*1285 : People & Body*/
{"🕵","detective" }, /*1286 : People & Body*/
{"🕵🏻","detective: light skin tone" }, /*1287 : People & Body*/
{"🕵🏼","detective: medium-light skin tone" }, /*1288 : People & Body*/
{"🕵🏽","detective: medium skin tone" }, /*1289 : People & Body*/
{"🕵🏾","detective: medium-dark skin tone" }, /*1290 : People & Body*/
{"🕵🏿","detective: dark skin tone" }, /*1291 : People & Body*/
{"🕵️‍♂️","man detective" }, /*1292 : People & Body*/
{"🕵‍♂️","man detective" }, /*1293 : People & Body*/
{"🕵️‍♂","man detective" }, /*1294 : People & Body*/
{"🕵‍♂","man detective" }, /*1295 : People & Body*/
{"🕵🏻‍♂️","man detective: light skin tone" }, /*1296 : People & Body*/
{"🕵🏻‍♂","man detective: light skin tone" }, /*1297 : People & Body*/
{"🕵🏼‍♂️","man detective: medium-light skin tone" }, /*1298 : People & Body*/
{"🕵🏼‍♂","man detective: medium-light skin tone" }, /*1299 : People & Body*/
{"🕵🏽‍♂️","man detective: medium skin tone" }, /*1300 : People & Body*/
{"🕵🏽‍♂","man detective: medium skin tone" }, /*1301 : People & Body*/
{"🕵🏾‍♂️","man detective: medium-dark skin tone" }, /*1302 : People & Body*/
{"🕵🏾‍♂","man detective: medium-dark skin tone" }, /*1303 : People & Body*/
{"🕵🏿‍♂️","man detective: dark skin tone" }, /*1304 : People & Body*/
{"🕵🏿‍♂","man detective: dark skin tone" }, /*1305 : People & Body*/
{"🕵️‍♀️","woman detective" }, /*1306 : People & Body*/
{"🕵‍♀️","woman detective" }, /*1307 : People & Body*/
{"🕵️‍♀","woman detective" }, /*1308 : People & Body*/
{"🕵‍♀","woman detective" }, /*1309 : People & Body*/
{"🕵🏻‍♀️","woman detective: light skin tone" }, /*1310 : People & Body*/
{"🕵🏻‍♀","woman detective: light skin tone" }, /*1311 : People & Body*/
{"🕵🏼‍♀️","woman detective: medium-light skin tone" }, /*1312 : People & Body*/
{"🕵🏼‍♀","woman detective: medium-light skin tone" }, /*1313 : People & Body*/
{"🕵🏽‍♀️","woman detective: medium skin tone" }, /*1314 : People & Body*/
{"🕵🏽‍♀","woman detective: medium skin tone" }, /*1315 : People & Body*/
{"🕵🏾‍♀️","woman detective: medium-dark skin tone" }, /*1316 : People & Body*/
{"🕵🏾‍♀","woman detective: medium-dark skin tone" }, /*1317 : People & Body*/
{"🕵🏿‍♀️","woman detective: dark skin tone" }, /*1318 : People & Body*/
{"🕵🏿‍♀","woman detective: dark skin tone" }, /*1319 : People & Body*/
{"💂","guard" }, /*1320 : People & Body*/
{"💂🏻","guard: light skin tone" }, /*1321 : People & Body*/
{"💂🏼","guard: medium-light skin tone" }, /*1322 : People & Body*/
{"💂🏽","guard: medium skin tone" }, /*1323 : People & Body*/
{"💂🏾","guard: medium-dark skin tone" }, /*1324 : People & Body*/
{"💂🏿","guard: dark skin tone" }, /*1325 : People & Body*/
{"💂‍♂️","man guard" }, /*1326 : People & Body*/
{"💂‍♂","man guard" }, /*1327 : People & Body*/
{"💂🏻‍♂️","man guard: light skin tone" }, /*1328 : People & Body*/
{"💂🏻‍♂","man guard: light skin tone" }, /*1329 : People & Body*/
{"💂🏼‍♂️","man guard: medium-light skin tone" }, /*1330 : People & Body*/
{"💂🏼‍♂","man guard: medium-light skin tone" }, /*1331 : People & Body*/
{"💂🏽‍♂️","man guard: medium skin tone" }, /*1332 : People & Body*/
{"💂🏽‍♂","man guard: medium skin tone" }, /*1333 : People & Body*/
{"💂🏾‍♂️","man guard: medium-dark skin tone" }, /*1334 : People & Body*/
{"💂🏾‍♂","man guard: medium-dark skin tone" }, /*1335 : People & Body*/
{"💂🏿‍♂️","man guard: dark skin tone" }, /*1336 : People & Body*/
{"💂🏿‍♂","man guard: dark skin tone" }, /*1337 : People & Body*/
{"💂‍♀️","woman guard" }, /*1338 : People & Body*/
{"💂‍♀","woman guard" }, /*1339 : People & Body*/
{"💂🏻‍♀️","woman guard: light skin tone" }, /*1340 : People & Body*/
{"💂🏻‍♀","woman guard: light skin tone" }, /*1341 : People & Body*/
{"💂🏼‍♀️","woman guard: medium-light skin tone" }, /*1342 : People & Body*/
{"💂🏼‍♀","woman guard: medium-light skin tone" }, /*1343 : People & Body*/
{"💂🏽‍♀️","woman guard: medium skin tone" }, /*1344 : People & Body*/
{"💂🏽‍♀","woman guard: medium skin tone" }, /*1345 : People & Body*/
{"💂🏾‍♀️","woman guard: medium-dark skin tone" }, /*1346 : People & Body*/
{"💂🏾‍♀","woman guard: medium-dark skin tone" }, /*1347 : People & Body*/
{"💂🏿‍♀️","woman guard: dark skin tone" }, /*1348 : People & Body*/
{"💂🏿‍♀","woman guard: dark skin tone" }, /*1349 : People & Body*/
{"🥷","ninja" }, /*1350 : People & Body*/
{"🥷🏻","ninja: light skin tone" }, /*1351 : People & Body*/
{"🥷🏼","ninja: medium-light skin tone" }, /*1352 : People & Body*/
{"🥷🏽","ninja: medium skin tone" }, /*1353 : People & Body*/
{"🥷🏾","ninja: medium-dark skin tone" }, /*1354 : People & Body*/
{"🥷🏿","ninja: dark skin tone" }, /*1355 : People & Body*/
{"👷","construction worker" }, /*1356 : People & Body*/
{"👷🏻","construction worker: light skin tone" }, /*1357 : People & Body*/
{"👷🏼","construction worker: medium-light skin tone" }, /*1358 : People & Body*/
{"👷🏽","construction worker: medium skin tone" }, /*1359 : People & Body*/
{"👷🏾","construction worker: medium-dark skin tone" }, /*1360 : People & Body*/
{"👷🏿","construction worker: dark skin tone" }, /*1361 : People & Body*/
{"👷‍♂️","man construction worker" }, /*1362 : People & Body*/
{"👷‍♂","man construction worker" }, /*1363 : People & Body*/
{"👷🏻‍♂️","man construction worker: light skin tone" }, /*1364 : People & Body*/
{"👷🏻‍♂","man construction worker: light skin tone" }, /*1365 : People & Body*/
{"👷🏼‍♂️","man construction worker: medium-light skin tone" }, /*1366 : People & Body*/
{"👷🏼‍♂","man construction worker: medium-light skin tone" }, /*1367 : People & Body*/
{"👷🏽‍♂️","man construction worker: medium skin tone" }, /*1368 : People & Body*/
{"👷🏽‍♂","man construction worker: medium skin tone" }, /*1369 : People & Body*/
{"👷🏾‍♂️","man construction worker: medium-dark skin tone" }, /*1370 : People & Body*/
{"👷🏾‍♂","man construction worker: medium-dark skin tone" }, /*1371 : People & Body*/
{"👷🏿‍♂️","man construction worker: dark skin tone" }, /*1372 : People & Body*/
{"👷🏿‍♂","man construction worker: dark skin tone" }, /*1373 : People & Body*/
{"👷‍♀️","woman construction worker" }, /*1374 : People & Body*/
{"👷‍♀","woman construction worker" }, /*1375 : People & Body*/
{"👷🏻‍♀️","woman construction worker: light skin tone" }, /*1376 : People & Body*/
{"👷🏻‍♀","woman construction worker: light skin tone" }, /*1377 : People & Body*/
{"👷🏼‍♀️","woman construction worker: medium-light skin tone" }, /*1378 : People & Body*/
{"👷🏼‍♀","woman construction worker: medium-light skin tone" }, /*1379 : People & Body*/
{"👷🏽‍♀️","woman construction worker: medium skin tone" }, /*1380 : People & Body*/
{"👷🏽‍♀","woman construction worker: medium skin tone" }, /*1381 : People & Body*/
{"👷🏾‍♀️","woman construction worker: medium-dark skin tone" }, /*1382 : People & Body*/
{"👷🏾‍♀","woman construction worker: medium-dark skin tone" }, /*1383 : People & Body*/
{"👷🏿‍♀️","woman construction worker: dark skin tone" }, /*1384 : People & Body*/
{"👷🏿‍♀","woman construction worker: dark skin tone" }, /*1385 : People & Body*/
{"🤴","prince" }, /*1386 : People & Body*/
{"🤴🏻","prince: light skin tone" }, /*1387 : People & Body*/
{"🤴🏼","prince: medium-light skin tone" }, /*1388 : People & Body*/
{"🤴🏽","prince: medium skin tone" }, /*1389 : People & Body*/
{"🤴🏾","prince: medium-dark skin tone" }, /*1390 : People & Body*/
{"🤴🏿","prince: dark skin tone" }, /*1391 : People & Body*/
{"👸","princess" }, /*1392 : People & Body*/
{"👸🏻","princess: light skin tone" }, /*1393 : People & Body*/
{"👸🏼","princess: medium-light skin tone" }, /*1394 : People & Body*/
{"👸🏽","princess: medium skin tone" }, /*1395 : People & Body*/
{"👸🏾","princess: medium-dark skin tone" }, /*1396 : People & Body*/
{"👸🏿","princess: dark skin tone" }, /*1397 : People & Body*/
{"👳","person wearing turban" }, /*1398 : People & Body*/
{"👳🏻","person wearing turban: light skin tone" }, /*1399 : People & Body*/
{"👳🏼","person wearing turban: medium-light skin tone" }, /*1400 : People & Body*/
{"👳🏽","person wearing turban: medium skin tone" }, /*1401 : People & Body*/
{"👳🏾","person wearing turban: medium-dark skin tone" }, /*1402 : People & Body*/
{"👳🏿","person wearing turban: dark skin tone" }, /*1403 : People & Body*/
{"👳‍♂️","man wearing turban" }, /*1404 : People & Body*/
{"👳‍♂","man wearing turban" }, /*1405 : People & Body*/
{"👳🏻‍♂️","man wearing turban: light skin tone" }, /*1406 : People & Body*/
{"👳🏻‍♂","man wearing turban: light skin tone" }, /*1407 : People & Body*/
{"👳🏼‍♂️","man wearing turban: medium-light skin tone" }, /*1408 : People & Body*/
{"👳🏼‍♂","man wearing turban: medium-light skin tone" }, /*1409 : People & Body*/
{"👳🏽‍♂️","man wearing turban: medium skin tone" }, /*1410 : People & Body*/
{"👳🏽‍♂","man wearing turban: medium skin tone" }, /*1411 : People & Body*/
{"👳🏾‍♂️","man wearing turban: medium-dark skin tone" }, /*1412 : People & Body*/
{"👳🏾‍♂","man wearing turban: medium-dark skin tone" }, /*1413 : People & Body*/
{"👳🏿‍♂️","man wearing turban: dark skin tone" }, /*1414 : People & Body*/
{"👳🏿‍♂","man wearing turban: dark skin tone" }, /*1415 : People & Body*/
{"👳‍♀️","woman wearing turban" }, /*1416 : People & Body*/
{"👳‍♀","woman wearing turban" }, /*1417 : People & Body*/
{"👳🏻‍♀️","woman wearing turban: light skin tone" }, /*1418 : People & Body*/
{"👳🏻‍♀","woman wearing turban: light skin tone" }, /*1419 : People & Body*/
{"👳🏼‍♀️","woman wearing turban: medium-light skin tone" }, /*1420 : People & Body*/
{"👳🏼‍♀","woman wearing turban: medium-light skin tone" }, /*1421 : People & Body*/
{"👳🏽‍♀️","woman wearing turban: medium skin tone" }, /*1422 : People & Body*/
{"👳🏽‍♀","woman wearing turban: medium skin tone" }, /*1423 : People & Body*/
{"👳🏾‍♀️","woman wearing turban: medium-dark skin tone" }, /*1424 : People & Body*/
{"👳🏾‍♀","woman wearing turban: medium-dark skin tone" }, /*1425 : People & Body*/
{"👳🏿‍♀️","woman wearing turban: dark skin tone" }, /*1426 : People & Body*/
{"👳🏿‍♀","woman wearing turban: dark skin tone" }, /*1427 : People & Body*/
{"👲","person with skullcap" }, /*1428 : People & Body*/
{"👲🏻","person with skullcap: light skin tone" }, /*1429 : People & Body*/
{"👲🏼","person with skullcap: medium-light skin tone" }, /*1430 : People & Body*/
{"👲🏽","person with skullcap: medium skin tone" }, /*1431 : People & Body*/
{"👲🏾","person with skullcap: medium-dark skin tone" }, /*1432 : People & Body*/
{"👲🏿","person with skullcap: dark skin tone" }, /*1433 : People & Body*/
{"🧕","woman with headscarf" }, /*1434 : People & Body*/
{"🧕🏻","woman with headscarf: light skin tone" }, /*1435 : People & Body*/
{"🧕🏼","woman with headscarf: medium-light skin tone" }, /*1436 : People & Body*/
{"🧕🏽","woman with headscarf: medium skin tone" }, /*1437 : People & Body*/
{"🧕🏾","woman with headscarf: medium-dark skin tone" }, /*1438 : People & Body*/
{"🧕🏿","woman with headscarf: dark skin tone" }, /*1439 : People & Body*/
{"🤵","person in tuxedo" }, /*1440 : People & Body*/
{"🤵🏻","person in tuxedo: light skin tone" }, /*1441 : People & Body*/
{"🤵🏼","person in tuxedo: medium-light skin tone" }, /*1442 : People & Body*/
{"🤵🏽","person in tuxedo: medium skin tone" }, /*1443 : People & Body*/
{"🤵🏾","person in tuxedo: medium-dark skin tone" }, /*1444 : People & Body*/
{"🤵🏿","person in tuxedo: dark skin tone" }, /*1445 : People & Body*/
{"🤵‍♂️","man in tuxedo" }, /*1446 : People & Body*/
{"🤵‍♂","man in tuxedo" }, /*1447 : People & Body*/
{"🤵🏻‍♂️","man in tuxedo: light skin tone" }, /*1448 : People & Body*/
{"🤵🏻‍♂","man in tuxedo: light skin tone" }, /*1449 : People & Body*/
{"🤵🏼‍♂️","man in tuxedo: medium-light skin tone" }, /*1450 : People & Body*/
{"🤵🏼‍♂","man in tuxedo: medium-light skin tone" }, /*1451 : People & Body*/
{"🤵🏽‍♂️","man in tuxedo: medium skin tone" }, /*1452 : People & Body*/
{"🤵🏽‍♂","man in tuxedo: medium skin tone" }, /*1453 : People & Body*/
{"🤵🏾‍♂️","man in tuxedo: medium-dark skin tone" }, /*1454 : People & Body*/
{"🤵🏾‍♂","man in tuxedo: medium-dark skin tone" }, /*1455 : People & Body*/
{"🤵🏿‍♂️","man in tuxedo: dark skin tone" }, /*1456 : People & Body*/
{"🤵🏿‍♂","man in tuxedo: dark skin tone" }, /*1457 : People & Body*/
{"🤵‍♀️","woman in tuxedo" }, /*1458 : People & Body*/
{"🤵‍♀","woman in tuxedo" }, /*1459 : People & Body*/
{"🤵🏻‍♀️","woman in tuxedo: light skin tone" }, /*1460 : People & Body*/
{"🤵🏻‍♀","woman in tuxedo: light skin tone" }, /*1461 : People & Body*/
{"🤵🏼‍♀️","woman in tuxedo: medium-light skin tone" }, /*1462 : People & Body*/
{"🤵🏼‍♀","woman in tuxedo: medium-light skin tone" }, /*1463 : People & Body*/
{"🤵🏽‍♀️","woman in tuxedo: medium skin tone" }, /*1464 : People & Body*/
{"🤵🏽‍♀","woman in tuxedo: medium skin tone" }, /*1465 : People & Body*/
{"🤵🏾‍♀️","woman in tuxedo: medium-dark skin tone" }, /*1466 : People & Body*/
{"🤵🏾‍♀","woman in tuxedo: medium-dark skin tone" }, /*1467 : People & Body*/
{"🤵🏿‍♀️","woman in tuxedo: dark skin tone" }, /*1468 : People & Body*/
{"🤵🏿‍♀","woman in tuxedo: dark skin tone" }, /*1469 : People & Body*/
{"👰","person with veil" }, /*1470 : People & Body*/
{"👰🏻","person with veil: light skin tone" }, /*1471 : People & Body*/
{"👰🏼","person with veil: medium-light skin tone" }, /*1472 : People & Body*/
{"👰🏽","person with veil: medium skin tone" }, /*1473 : People & Body*/
{"👰🏾","person with veil: medium-dark skin tone" }, /*1474 : People & Body*/
{"👰🏿","person with veil: dark skin tone" }, /*1475 : People & Body*/
{"👰‍♂️","man with veil" }, /*1476 : People & Body*/
{"👰‍♂","man with veil" }, /*1477 : People & Body*/
{"👰🏻‍♂️","man with veil: light skin tone" }, /*1478 : People & Body*/
{"👰🏻‍♂","man with veil: light skin tone" }, /*1479 : People & Body*/
{"👰🏼‍♂️","man with veil: medium-light skin tone" }, /*1480 : People & Body*/
{"👰🏼‍♂","man with veil: medium-light skin tone" }, /*1481 : People & Body*/
{"👰🏽‍♂️","man with veil: medium skin tone" }, /*1482 : People & Body*/
{"👰🏽‍♂","man with veil: medium skin tone" }, /*1483 : People & Body*/
{"👰🏾‍♂️","man with veil: medium-dark skin tone" }, /*1484 : People & Body*/
{"👰🏾‍♂","man with veil: medium-dark skin tone" }, /*1485 : People & Body*/
{"👰🏿‍♂️","man with veil: dark skin tone" }, /*1486 : People & Body*/
{"👰🏿‍♂","man with veil: dark skin tone" }, /*1487 : People & Body*/
{"👰‍♀️","woman with veil" }, /*1488 : People & Body*/
{"👰‍♀","woman with veil" }, /*1489 : People & Body*/
{"👰🏻‍♀️","woman with veil: light skin tone" }, /*1490 : People & Body*/
{"👰🏻‍♀","woman with veil: light skin tone" }, /*1491 : People & Body*/
{"👰🏼‍♀️","woman with veil: medium-light skin tone" }, /*1492 : People & Body*/
{"👰🏼‍♀","woman with veil: medium-light skin tone" }, /*1493 : People & Body*/
{"👰🏽‍♀️","woman with veil: medium skin tone" }, /*1494 : People & Body*/
{"👰🏽‍♀","woman with veil: medium skin tone" }, /*1495 : People & Body*/
{"👰🏾‍♀️","woman with veil: medium-dark skin tone" }, /*1496 : People & Body*/
{"👰🏾‍♀","woman with veil: medium-dark skin tone" }, /*1497 : People & Body*/
{"👰🏿‍♀️","woman with veil: dark skin tone" }, /*1498 : People & Body*/
{"👰🏿‍♀","woman with veil: dark skin tone" }, /*1499 : People & Body*/
{"🤰","pregnant woman" }, /*1500 : People & Body*/
{"🤰🏻","pregnant woman: light skin tone" }, /*1501 : People & Body*/
{"🤰🏼","pregnant woman: medium-light skin tone" }, /*1502 : People & Body*/
{"🤰🏽","pregnant woman: medium skin tone" }, /*1503 : People & Body*/
{"🤰🏾","pregnant woman: medium-dark skin tone" }, /*1504 : People & Body*/
{"🤰🏿","pregnant woman: dark skin tone" }, /*1505 : People & Body*/
{"🤱","breast-feeding" }, /*1506 : People & Body*/
{"🤱🏻","breast-feeding: light skin tone" }, /*1507 : People & Body*/
{"🤱🏼","breast-feeding: medium-light skin tone" }, /*1508 : People & Body*/
{"🤱🏽","breast-feeding: medium skin tone" }, /*1509 : People & Body*/
{"🤱🏾","breast-feeding: medium-dark skin tone" }, /*1510 : People & Body*/
{"🤱🏿","breast-feeding: dark skin tone" }, /*1511 : People & Body*/
{"👩‍🍼","woman feeding baby" }, /*1512 : People & Body*/
{"👩🏻‍🍼","woman feeding baby: light skin tone" }, /*1513 : People & Body*/
{"👩🏼‍🍼","woman feeding baby: medium-light skin tone" }, /*1514 : People & Body*/
{"👩🏽‍🍼","woman feeding baby: medium skin tone" }, /*1515 : People & Body*/
{"👩🏾‍🍼","woman feeding baby: medium-dark skin tone" }, /*1516 : People & Body*/
{"👩🏿‍🍼","woman feeding baby: dark skin tone" }, /*1517 : People & Body*/
{"👨‍🍼","man feeding baby" }, /*1518 : People & Body*/
{"👨🏻‍🍼","man feeding baby: light skin tone" }, /*1519 : People & Body*/
{"👨🏼‍🍼","man feeding baby: medium-light skin tone" }, /*1520 : People & Body*/
{"👨🏽‍🍼","man feeding baby: medium skin tone" }, /*1521 : People & Body*/
{"👨🏾‍🍼","man feeding baby: medium-dark skin tone" }, /*1522 : People & Body*/
{"👨🏿‍🍼","man feeding baby: dark skin tone" }, /*1523 : People & Body*/
{"🧑‍🍼","person feeding baby" }, /*1524 : People & Body*/
{"🧑🏻‍🍼","person feeding baby: light skin tone" }, /*1525 : People & Body*/
{"🧑🏼‍🍼","person feeding baby: medium-light skin tone" }, /*1526 : People & Body*/
{"🧑🏽‍🍼","person feeding baby: medium skin tone" }, /*1527 : People & Body*/
{"🧑🏾‍🍼","person feeding baby: medium-dark skin tone" }, /*1528 : People & Body*/
{"🧑🏿‍🍼","person feeding baby: dark skin tone" }, /*1529 : People & Body*/
{"👼","baby angel" }, /*1530 : People & Body*/
{"👼🏻","baby angel: light skin tone" }, /*1531 : People & Body*/
{"👼🏼","baby angel: medium-light skin tone" }, /*1532 : People & Body*/
{"👼🏽","baby angel: medium skin tone" }, /*1533 : People & Body*/
{"👼🏾","baby angel: medium-dark skin tone" }, /*1534 : People & Body*/
{"👼🏿","baby angel: dark skin tone" }, /*1535 : People & Body*/
{"🎅","Santa Claus" }, /*1536 : People & Body*/
{"🎅🏻","Santa Claus: light skin tone" }, /*1537 : People & Body*/
{"🎅🏼","Santa Claus: medium-light skin tone" }, /*1538 : People & Body*/
{"🎅🏽","Santa Claus: medium skin tone" }, /*1539 : People & Body*/
{"🎅🏾","Santa Claus: medium-dark skin tone" }, /*1540 : People & Body*/
{"🎅🏿","Santa Claus: dark skin tone" }, /*1541 : People & Body*/
{"🤶","Mrs. Claus" }, /*1542 : People & Body*/
{"🤶🏻","Mrs. Claus: light skin tone" }, /*1543 : People & Body*/
{"🤶🏼","Mrs. Claus: medium-light skin tone" }, /*1544 : People & Body*/
{"🤶🏽","Mrs. Claus: medium skin tone" }, /*1545 : People & Body*/
{"🤶🏾","Mrs. Claus: medium-dark skin tone" }, /*1546 : People & Body*/
{"🤶🏿","Mrs. Claus: dark skin tone" }, /*1547 : People & Body*/
{"🧑‍🎄","mx claus" }, /*1548 : People & Body*/
{"🧑🏻‍🎄","mx claus: light skin tone" }, /*1549 : People & Body*/
{"🧑🏼‍🎄","mx claus: medium-light skin tone" }, /*1550 : People & Body*/
{"🧑🏽‍🎄","mx claus: medium skin tone" }, /*1551 : People & Body*/
{"🧑🏾‍🎄","mx claus: medium-dark skin tone" }, /*1552 : People & Body*/
{"🧑🏿‍🎄","mx claus: dark skin tone" }, /*1553 : People & Body*/
{"🦸","superhero" }, /*1554 : People & Body*/
{"🦸🏻","superhero: light skin tone" }, /*1555 : People & Body*/
{"🦸🏼","superhero: medium-light skin tone" }, /*1556 : People & Body*/
{"🦸🏽","superhero: medium skin tone" }, /*1557 : People & Body*/
{"🦸🏾","superhero: medium-dark skin tone" }, /*1558 : People & Body*/
{"🦸🏿","superhero: dark skin tone" }, /*1559 : People & Body*/
{"🦸‍♂️","man superhero" }, /*1560 : People & Body*/
{"🦸‍♂","man superhero" }, /*1561 : People & Body*/
{"🦸🏻‍♂️","man superhero: light skin tone" }, /*1562 : People & Body*/
{"🦸🏻‍♂","man superhero: light skin tone" }, /*1563 : People & Body*/
{"🦸🏼‍♂️","man superhero: medium-light skin tone" }, /*1564 : People & Body*/
{"🦸🏼‍♂","man superhero: medium-light skin tone" }, /*1565 : People & Body*/
{"🦸🏽‍♂️","man superhero: medium skin tone" }, /*1566 : People & Body*/
{"🦸🏽‍♂","man superhero: medium skin tone" }, /*1567 : People & Body*/
{"🦸🏾‍♂️","man superhero: medium-dark skin tone" }, /*1568 : People & Body*/
{"🦸🏾‍♂","man superhero: medium-dark skin tone" }, /*1569 : People & Body*/
{"🦸🏿‍♂️","man superhero: dark skin tone" }, /*1570 : People & Body*/
{"🦸🏿‍♂","man superhero: dark skin tone" }, /*1571 : People & Body*/
{"🦸‍♀️","woman superhero" }, /*1572 : People & Body*/
{"🦸‍♀","woman superhero" }, /*1573 : People & Body*/
{"🦸🏻‍♀️","woman superhero: light skin tone" }, /*1574 : People & Body*/
{"🦸🏻‍♀","woman superhero: light skin tone" }, /*1575 : People & Body*/
{"🦸🏼‍♀️","woman superhero: medium-light skin tone" }, /*1576 : People & Body*/
{"🦸🏼‍♀","woman superhero: medium-light skin tone" }, /*1577 : People & Body*/
{"🦸🏽‍♀️","woman superhero: medium skin tone" }, /*1578 : People & Body*/
{"🦸🏽‍♀","woman superhero: medium skin tone" }, /*1579 : People & Body*/
{"🦸🏾‍♀️","woman superhero: medium-dark skin tone" }, /*1580 : People & Body*/
{"🦸🏾‍♀","woman superhero: medium-dark skin tone" }, /*1581 : People & Body*/
{"🦸🏿‍♀️","woman superhero: dark skin tone" }, /*1582 : People & Body*/
{"🦸🏿‍♀","woman superhero: dark skin tone" }, /*1583 : People & Body*/
{"🦹","supervillain" }, /*1584 : People & Body*/
{"🦹🏻","supervillain: light skin tone" }, /*1585 : People & Body*/
{"🦹🏼","supervillain: medium-light skin tone" }, /*1586 : People & Body*/
{"🦹🏽","supervillain: medium skin tone" }, /*1587 : People & Body*/
{"🦹🏾","supervillain: medium-dark skin tone" }, /*1588 : People & Body*/
{"🦹🏿","supervillain: dark skin tone" }, /*1589 : People & Body*/
{"🦹‍♂️","man supervillain" }, /*1590 : People & Body*/
{"🦹‍♂","man supervillain" }, /*1591 : People & Body*/
{"🦹🏻‍♂️","man supervillain: light skin tone" }, /*1592 : People & Body*/
{"🦹🏻‍♂","man supervillain: light skin tone" }, /*1593 : People & Body*/
{"🦹🏼‍♂️","man supervillain: medium-light skin tone" }, /*1594 : People & Body*/
{"🦹🏼‍♂","man supervillain: medium-light skin tone" }, /*1595 : People & Body*/
{"🦹🏽‍♂️","man supervillain: medium skin tone" }, /*1596 : People & Body*/
{"🦹🏽‍♂","man supervillain: medium skin tone" }, /*1597 : People & Body*/
{"🦹🏾‍♂️","man supervillain: medium-dark skin tone" }, /*1598 : People & Body*/
{"🦹🏾‍♂","man supervillain: medium-dark skin tone" }, /*1599 : People & Body*/
{"🦹🏿‍♂️","man supervillain: dark skin tone" }, /*1600 : People & Body*/
{"🦹🏿‍♂","man supervillain: dark skin tone" }, /*1601 : People & Body*/
{"🦹‍♀️","woman supervillain" }, /*1602 : People & Body*/
{"🦹‍♀","woman supervillain" }, /*1603 : People & Body*/
{"🦹🏻‍♀️","woman supervillain: light skin tone" }, /*1604 : People & Body*/
{"🦹🏻‍♀","woman supervillain: light skin tone" }, /*1605 : People & Body*/
{"🦹🏼‍♀️","woman supervillain: medium-light skin tone" }, /*1606 : People & Body*/
{"🦹🏼‍♀","woman supervillain: medium-light skin tone" }, /*1607 : People & Body*/
{"🦹🏽‍♀️","woman supervillain: medium skin tone" }, /*1608 : People & Body*/
{"🦹🏽‍♀","woman supervillain: medium skin tone" }, /*1609 : People & Body*/
{"🦹🏾‍♀️","woman supervillain: medium-dark skin tone" }, /*1610 : People & Body*/
{"🦹🏾‍♀","woman supervillain: medium-dark skin tone" }, /*1611 : People & Body*/
{"🦹🏿‍♀️","woman supervillain: dark skin tone" }, /*1612 : People & Body*/
{"🦹🏿‍♀","woman supervillain: dark skin tone" }, /*1613 : People & Body*/
{"🧙","mage" }, /*1614 : People & Body*/
{"🧙🏻","mage: light skin tone" }, /*1615 : People & Body*/
{"🧙🏼","mage: medium-light skin tone" }, /*1616 : People & Body*/
{"🧙🏽","mage: medium skin tone" }, /*1617 : People & Body*/
{"🧙🏾","mage: medium-dark skin tone" }, /*1618 : People & Body*/
{"🧙🏿","mage: dark skin tone" }, /*1619 : People & Body*/
{"🧙‍♂️","man mage" }, /*1620 : People & Body*/
{"🧙‍♂","man mage" }, /*1621 : People & Body*/
{"🧙🏻‍♂️","man mage: light skin tone" }, /*1622 : People & Body*/
{"🧙🏻‍♂","man mage: light skin tone" }, /*1623 : People & Body*/
{"🧙🏼‍♂️","man mage: medium-light skin tone" }, /*1624 : People & Body*/
{"🧙🏼‍♂","man mage: medium-light skin tone" }, /*1625 : People & Body*/
{"🧙🏽‍♂️","man mage: medium skin tone" }, /*1626 : People & Body*/
{"🧙🏽‍♂","man mage: medium skin tone" }, /*1627 : People & Body*/
{"🧙🏾‍♂️","man mage: medium-dark skin tone" }, /*1628 : People & Body*/
{"🧙🏾‍♂","man mage: medium-dark skin tone" }, /*1629 : People & Body*/
{"🧙🏿‍♂️","man mage: dark skin tone" }, /*1630 : People & Body*/
{"🧙🏿‍♂","man mage: dark skin tone" }, /*1631 : People & Body*/
{"🧙‍♀️","woman mage" }, /*1632 : People & Body*/
{"🧙‍♀","woman mage" }, /*1633 : People & Body*/
{"🧙🏻‍♀️","woman mage: light skin tone" }, /*1634 : People & Body*/
{"🧙🏻‍♀","woman mage: light skin tone" }, /*1635 : People & Body*/
{"🧙🏼‍♀️","woman mage: medium-light skin tone" }, /*1636 : People & Body*/
{"🧙🏼‍♀","woman mage: medium-light skin tone" }, /*1637 : People & Body*/
{"🧙🏽‍♀️","woman mage: medium skin tone" }, /*1638 : People & Body*/
{"🧙🏽‍♀","woman mage: medium skin tone" }, /*1639 : People & Body*/
{"🧙🏾‍♀️","woman mage: medium-dark skin tone" }, /*1640 : People & Body*/
{"🧙🏾‍♀","woman mage: medium-dark skin tone" }, /*1641 : People & Body*/
{"🧙🏿‍♀️","woman mage: dark skin tone" }, /*1642 : People & Body*/
{"🧙🏿‍♀","woman mage: dark skin tone" }, /*1643 : People & Body*/
{"🧚","fairy" }, /*1644 : People & Body*/
{"🧚🏻","fairy: light skin tone" }, /*1645 : People & Body*/
{"🧚🏼","fairy: medium-light skin tone" }, /*1646 : People & Body*/
{"🧚🏽","fairy: medium skin tone" }, /*1647 : People & Body*/
{"🧚🏾","fairy: medium-dark skin tone" }, /*1648 : People & Body*/
{"🧚🏿","fairy: dark skin tone" }, /*1649 : People & Body*/
{"🧚‍♂️","man fairy" }, /*1650 : People & Body*/
{"🧚‍♂","man fairy" }, /*1651 : People & Body*/
{"🧚🏻‍♂️","man fairy: light skin tone" }, /*1652 : People & Body*/
{"🧚🏻‍♂","man fairy: light skin tone" }, /*1653 : People & Body*/
{"🧚🏼‍♂️","man fairy: medium-light skin tone" }, /*1654 : People & Body*/
{"🧚🏼‍♂","man fairy: medium-light skin tone" }, /*1655 : People & Body*/
{"🧚🏽‍♂️","man fairy: medium skin tone" }, /*1656 : People & Body*/
{"🧚🏽‍♂","man fairy: medium skin tone" }, /*1657 : People & Body*/
{"🧚🏾‍♂️","man fairy: medium-dark skin tone" }, /*1658 : People & Body*/
{"🧚🏾‍♂","man fairy: medium-dark skin tone" }, /*1659 : People & Body*/
{"🧚🏿‍♂️","man fairy: dark skin tone" }, /*1660 : People & Body*/
{"🧚🏿‍♂","man fairy: dark skin tone" }, /*1661 : People & Body*/
{"🧚‍♀️","woman fairy" }, /*1662 : People & Body*/
{"🧚‍♀","woman fairy" }, /*1663 : People & Body*/
{"🧚🏻‍♀️","woman fairy: light skin tone" }, /*1664 : People & Body*/
{"🧚🏻‍♀","woman fairy: light skin tone" }, /*1665 : People & Body*/
{"🧚🏼‍♀️","woman fairy: medium-light skin tone" }, /*1666 : People & Body*/
{"🧚🏼‍♀","woman fairy: medium-light skin tone" }, /*1667 : People & Body*/
{"🧚🏽‍♀️","woman fairy: medium skin tone" }, /*1668 : People & Body*/
{"🧚🏽‍♀","woman fairy: medium skin tone" }, /*1669 : People & Body*/
{"🧚🏾‍♀️","woman fairy: medium-dark skin tone" }, /*1670 : People & Body*/
{"🧚🏾‍♀","woman fairy: medium-dark skin tone" }, /*1671 : People & Body*/
{"🧚🏿‍♀️","woman fairy: dark skin tone" }, /*1672 : People & Body*/
{"🧚🏿‍♀","woman fairy: dark skin tone" }, /*1673 : People & Body*/
{"🧛","vampire" }, /*1674 : People & Body*/
{"🧛🏻","vampire: light skin tone" }, /*1675 : People & Body*/
{"🧛🏼","vampire: medium-light skin tone" }, /*1676 : People & Body*/
{"🧛🏽","vampire: medium skin tone" }, /*1677 : People & Body*/
{"🧛🏾","vampire: medium-dark skin tone" }, /*1678 : People & Body*/
{"🧛🏿","vampire: dark skin tone" }, /*1679 : People & Body*/
{"🧛‍♂️","man vampire" }, /*1680 : People & Body*/
{"🧛‍♂","man vampire" }, /*1681 : People & Body*/
{"🧛🏻‍♂️","man vampire: light skin tone" }, /*1682 : People & Body*/
{"🧛🏻‍♂","man vampire: light skin tone" }, /*1683 : People & Body*/
{"🧛🏼‍♂️","man vampire: medium-light skin tone" }, /*1684 : People & Body*/
{"🧛🏼‍♂","man vampire: medium-light skin tone" }, /*1685 : People & Body*/
{"🧛🏽‍♂️","man vampire: medium skin tone" }, /*1686 : People & Body*/
{"🧛🏽‍♂","man vampire: medium skin tone" }, /*1687 : People & Body*/
{"🧛🏾‍♂️","man vampire: medium-dark skin tone" }, /*1688 : People & Body*/
{"🧛🏾‍♂","man vampire: medium-dark skin tone" }, /*1689 : People & Body*/
{"🧛🏿‍♂️","man vampire: dark skin tone" }, /*1690 : People & Body*/
{"🧛🏿‍♂","man vampire: dark skin tone" }, /*1691 : People & Body*/
{"🧛‍♀️","woman vampire" }, /*1692 : People & Body*/
{"🧛‍♀","woman vampire" }, /*1693 : People & Body*/
{"🧛🏻‍♀️","woman vampire: light skin tone" }, /*1694 : People & Body*/
{"🧛🏻‍♀","woman vampire: light skin tone" }, /*1695 : People & Body*/
{"🧛🏼‍♀️","woman vampire: medium-light skin tone" }, /*1696 : People & Body*/
{"🧛🏼‍♀","woman vampire: medium-light skin tone" }, /*1697 : People & Body*/
{"🧛🏽‍♀️","woman vampire: medium skin tone" }, /*1698 : People & Body*/
{"🧛🏽‍♀","woman vampire: medium skin tone" }, /*1699 : People & Body*/
{"🧛🏾‍♀️","woman vampire: medium-dark skin tone" }, /*1700 : People & Body*/
{"🧛🏾‍♀","woman vampire: medium-dark skin tone" }, /*1701 : People & Body*/
{"🧛🏿‍♀️","woman vampire: dark skin tone" }, /*1702 : People & Body*/
{"🧛🏿‍♀","woman vampire: dark skin tone" }, /*1703 : People & Body*/
{"🧜","merperson" }, /*1704 : People & Body*/
{"🧜🏻","merperson: light skin tone" }, /*1705 : People & Body*/
{"🧜🏼","merperson: medium-light skin tone" }, /*1706 : People & Body*/
{"🧜🏽","merperson: medium skin tone" }, /*1707 : People & Body*/
{"🧜🏾","merperson: medium-dark skin tone" }, /*1708 : People & Body*/
{"🧜🏿","merperson: dark skin tone" }, /*1709 : People & Body*/
{"🧜‍♂️","merman" }, /*1710 : People & Body*/
{"🧜‍♂","merman" }, /*1711 : People & Body*/
{"🧜🏻‍♂️","merman: light skin tone" }, /*1712 : People & Body*/
{"🧜🏻‍♂","merman: light skin tone" }, /*1713 : People & Body*/
{"🧜🏼‍♂️","merman: medium-light skin tone" }, /*1714 : People & Body*/
{"🧜🏼‍♂","merman: medium-light skin tone" }, /*1715 : People & Body*/
{"🧜🏽‍♂️","merman: medium skin tone" }, /*1716 : People & Body*/
{"🧜🏽‍♂","merman: medium skin tone" }, /*1717 : People & Body*/
{"🧜🏾‍♂️","merman: medium-dark skin tone" }, /*1718 : People & Body*/
{"🧜🏾‍♂","merman: medium-dark skin tone" }, /*1719 : People & Body*/
{"🧜🏿‍♂️","merman: dark skin tone" }, /*1720 : People & Body*/
{"🧜🏿‍♂","merman: dark skin tone" }, /*1721 : People & Body*/
{"🧜‍♀️","mermaid" }, /*1722 : People & Body*/
{"🧜‍♀","mermaid" }, /*1723 : People & Body*/
{"🧜🏻‍♀️","mermaid: light skin tone" }, /*1724 : People & Body*/
{"🧜🏻‍♀","mermaid: light skin tone" }, /*1725 : People & Body*/
{"🧜🏼‍♀️","mermaid: medium-light skin tone" }, /*1726 : People & Body*/
{"🧜🏼‍♀","mermaid: medium-light skin tone" }, /*1727 : People & Body*/
{"🧜🏽‍♀️","mermaid: medium skin tone" }, /*1728 : People & Body*/
{"🧜🏽‍♀","mermaid: medium skin tone" }, /*1729 : People & Body*/
{"🧜🏾‍♀️","mermaid: medium-dark skin tone" }, /*1730 : People & Body*/
{"🧜🏾‍♀","mermaid: medium-dark skin tone" }, /*1731 : People & Body*/
{"🧜🏿‍♀️","mermaid: dark skin tone" }, /*1732 : People & Body*/
{"🧜🏿‍♀","mermaid: dark skin tone" }, /*1733 : People & Body*/
{"🧝","elf" }, /*1734 : People & Body*/
{"🧝🏻","elf: light skin tone" }, /*1735 : People & Body*/
{"🧝🏼","elf: medium-light skin tone" }, /*1736 : People & Body*/
{"🧝🏽","elf: medium skin tone" }, /*1737 : People & Body*/
{"🧝🏾","elf: medium-dark skin tone" }, /*1738 : People & Body*/
{"🧝🏿","elf: dark skin tone" }, /*1739 : People & Body*/
{"🧝‍♂️","man elf" }, /*1740 : People & Body*/
{"🧝‍♂","man elf" }, /*1741 : People & Body*/
{"🧝🏻‍♂️","man elf: light skin tone" }, /*1742 : People & Body*/
{"🧝🏻‍♂","man elf: light skin tone" }, /*1743 : People & Body*/
{"🧝🏼‍♂️","man elf: medium-light skin tone" }, /*1744 : People & Body*/
{"🧝🏼‍♂","man elf: medium-light skin tone" }, /*1745 : People & Body*/
{"🧝🏽‍♂️","man elf: medium skin tone" }, /*1746 : People & Body*/
{"🧝🏽‍♂","man elf: medium skin tone" }, /*1747 : People & Body*/
{"🧝🏾‍♂️","man elf: medium-dark skin tone" }, /*1748 : People & Body*/
{"🧝🏾‍♂","man elf: medium-dark skin tone" }, /*1749 : People & Body*/
{"🧝🏿‍♂️","man elf: dark skin tone" }, /*1750 : People & Body*/
{"🧝🏿‍♂","man elf: dark skin tone" }, /*1751 : People & Body*/
{"🧝‍♀️","woman elf" }, /*1752 : People & Body*/
{"🧝‍♀","woman elf" }, /*1753 : People & Body*/
{"🧝🏻‍♀️","woman elf: light skin tone" }, /*1754 : People & Body*/
{"🧝🏻‍♀","woman elf: light skin tone" }, /*1755 : People & Body*/
{"🧝🏼‍♀️","woman elf: medium-light skin tone" }, /*1756 : People & Body*/
{"🧝🏼‍♀","woman elf: medium-light skin tone" }, /*1757 : People & Body*/
{"🧝🏽‍♀️","woman elf: medium skin tone" }, /*1758 : People & Body*/
{"🧝🏽‍♀","woman elf: medium skin tone" }, /*1759 : People & Body*/
{"🧝🏾‍♀️","woman elf: medium-dark skin tone" }, /*1760 : People & Body*/
{"🧝🏾‍♀","woman elf: medium-dark skin tone" }, /*1761 : People & Body*/
{"🧝🏿‍♀️","woman elf: dark skin tone" }, /*1762 : People & Body*/
{"🧝🏿‍♀","woman elf: dark skin tone" }, /*1763 : People & Body*/
{"🧞","genie" }, /*1764 : People & Body*/
{"🧞‍♂️","man genie" }, /*1765 : People & Body*/
{"🧞‍♂","man genie" }, /*1766 : People & Body*/
{"🧞‍♀️","woman genie" }, /*1767 : People & Body*/
{"🧞‍♀","woman genie" }, /*1768 : People & Body*/
{"🧟","zombie" }, /*1769 : People & Body*/
{"🧟‍♂️","man zombie" }, /*1770 : People & Body*/
{"🧟‍♂","man zombie" }, /*1771 : People & Body*/
{"🧟‍♀️","woman zombie" }, /*1772 : People & Body*/
{"🧟‍♀","woman zombie" }, /*1773 : People & Body*/
{"💆","person getting massage" }, /*1774 : People & Body*/
{"💆🏻","person getting massage: light skin tone" }, /*1775 : People & Body*/
{"💆🏼","person getting massage: medium-light skin tone" }, /*1776 : People & Body*/
{"💆🏽","person getting massage: medium skin tone" }, /*1777 : People & Body*/
{"💆🏾","person getting massage: medium-dark skin tone" }, /*1778 : People & Body*/
{"💆🏿","person getting massage: dark skin tone" }, /*1779 : People & Body*/
{"💆‍♂️","man getting massage" }, /*1780 : People & Body*/
{"💆‍♂","man getting massage" }, /*1781 : People & Body*/
{"💆🏻‍♂️","man getting massage: light skin tone" }, /*1782 : People & Body*/
{"💆🏻‍♂","man getting massage: light skin tone" }, /*1783 : People & Body*/
{"💆🏼‍♂️","man getting massage: medium-light skin tone" }, /*1784 : People & Body*/
{"💆🏼‍♂","man getting massage: medium-light skin tone" }, /*1785 : People & Body*/
{"💆🏽‍♂️","man getting massage: medium skin tone" }, /*1786 : People & Body*/
{"💆🏽‍♂","man getting massage: medium skin tone" }, /*1787 : People & Body*/
{"💆🏾‍♂️","man getting massage: medium-dark skin tone" }, /*1788 : People & Body*/
{"💆🏾‍♂","man getting massage: medium-dark skin tone" }, /*1789 : People & Body*/
{"💆🏿‍♂️","man getting massage: dark skin tone" }, /*1790 : People & Body*/
{"💆🏿‍♂","man getting massage: dark skin tone" }, /*1791 : People & Body*/
{"💆‍♀️","woman getting massage" }, /*1792 : People & Body*/
{"💆‍♀","woman getting massage" }, /*1793 : People & Body*/
{"💆🏻‍♀️","woman getting massage: light skin tone" }, /*1794 : People & Body*/
{"💆🏻‍♀","woman getting massage: light skin tone" }, /*1795 : People & Body*/
{"💆🏼‍♀️","woman getting massage: medium-light skin tone" }, /*1796 : People & Body*/
{"💆🏼‍♀","woman getting massage: medium-light skin tone" }, /*1797 : People & Body*/
{"💆🏽‍♀️","woman getting massage: medium skin tone" }, /*1798 : People & Body*/
{"💆🏽‍♀","woman getting massage: medium skin tone" }, /*1799 : People & Body*/
{"💆🏾‍♀️","woman getting massage: medium-dark skin tone" }, /*1800 : People & Body*/
{"💆🏾‍♀","woman getting massage: medium-dark skin tone" }, /*1801 : People & Body*/
{"💆🏿‍♀️","woman getting massage: dark skin tone" }, /*1802 : People & Body*/
{"💆🏿‍♀","woman getting massage: dark skin tone" }, /*1803 : People & Body*/
{"💇","person getting haircut" }, /*1804 : People & Body*/
{"💇🏻","person getting haircut: light skin tone" }, /*1805 : People & Body*/
{"💇🏼","person getting haircut: medium-light skin tone" }, /*1806 : People & Body*/
{"💇🏽","person getting haircut: medium skin tone" }, /*1807 : People & Body*/
{"💇🏾","person getting haircut: medium-dark skin tone" }, /*1808 : People & Body*/
{"💇🏿","person getting haircut: dark skin tone" }, /*1809 : People & Body*/
{"💇‍♂️","man getting haircut" }, /*1810 : People & Body*/
{"💇‍♂","man getting haircut" }, /*1811 : People & Body*/
{"💇🏻‍♂️","man getting haircut: light skin tone" }, /*1812 : People & Body*/
{"💇🏻‍♂","man getting haircut: light skin tone" }, /*1813 : People & Body*/
{"💇🏼‍♂️","man getting haircut: medium-light skin tone" }, /*1814 : People & Body*/
{"💇🏼‍♂","man getting haircut: medium-light skin tone" }, /*1815 : People & Body*/
{"💇🏽‍♂️","man getting haircut: medium skin tone" }, /*1816 : People & Body*/
{"💇🏽‍♂","man getting haircut: medium skin tone" }, /*1817 : People & Body*/
{"💇🏾‍♂️","man getting haircut: medium-dark skin tone" }, /*1818 : People & Body*/
{"💇🏾‍♂","man getting haircut: medium-dark skin tone" }, /*1819 : People & Body*/
{"💇🏿‍♂️","man getting haircut: dark skin tone" }, /*1820 : People & Body*/
{"💇🏿‍♂","man getting haircut: dark skin tone" }, /*1821 : People & Body*/
{"💇‍♀️","woman getting haircut" }, /*1822 : People & Body*/
{"💇‍♀","woman getting haircut" }, /*1823 : People & Body*/
{"💇🏻‍♀️","woman getting haircut: light skin tone" }, /*1824 : People & Body*/
{"💇🏻‍♀","woman getting haircut: light skin tone" }, /*1825 : People & Body*/
{"💇🏼‍♀️","woman getting haircut: medium-light skin tone" }, /*1826 : People & Body*/
{"💇🏼‍♀","woman getting haircut: medium-light skin tone" }, /*1827 : People & Body*/
{"💇🏽‍♀️","woman getting haircut: medium skin tone" }, /*1828 : People & Body*/
{"💇🏽‍♀","woman getting haircut: medium skin tone" }, /*1829 : People & Body*/
{"💇🏾‍♀️","woman getting haircut: medium-dark skin tone" }, /*1830 : People & Body*/
{"💇🏾‍♀","woman getting haircut: medium-dark skin tone" }, /*1831 : People & Body*/
{"💇🏿‍♀️","woman getting haircut: dark skin tone" }, /*1832 : People & Body*/
{"💇🏿‍♀","woman getting haircut: dark skin tone" }, /*1833 : People & Body*/
{"🚶","person walking" }, /*1834 : People & Body*/
{"🚶🏻","person walking: light skin tone" }, /*1835 : People & Body*/
{"🚶🏼","person walking: medium-light skin tone" }, /*1836 : People & Body*/
{"🚶🏽","person walking: medium skin tone" }, /*1837 : People & Body*/
{"🚶🏾","person walking: medium-dark skin tone" }, /*1838 : People & Body*/
{"🚶🏿","person walking: dark skin tone" }, /*1839 : People & Body*/
{"🚶‍♂️","man walking" }, /*1840 : People & Body*/
{"🚶‍♂","man walking" }, /*1841 : People & Body*/
{"🚶🏻‍♂️","man walking: light skin tone" }, /*1842 : People & Body*/
{"🚶🏻‍♂","man walking: light skin tone" }, /*1843 : People & Body*/
{"🚶🏼‍♂️","man walking: medium-light skin tone" }, /*1844 : People & Body*/
{"🚶🏼‍♂","man walking: medium-light skin tone" }, /*1845 : People & Body*/
{"🚶🏽‍♂️","man walking: medium skin tone" }, /*1846 : People & Body*/
{"🚶🏽‍♂","man walking: medium skin tone" }, /*1847 : People & Body*/
{"🚶🏾‍♂️","man walking: medium-dark skin tone" }, /*1848 : People & Body*/
{"🚶🏾‍♂","man walking: medium-dark skin tone" }, /*1849 : People & Body*/
{"🚶🏿‍♂️","man walking: dark skin tone" }, /*1850 : People & Body*/
{"🚶🏿‍♂","man walking: dark skin tone" }, /*1851 : People & Body*/
{"🚶‍♀️","woman walking" }, /*1852 : People & Body*/
{"🚶‍♀","woman walking" }, /*1853 : People & Body*/
{"🚶🏻‍♀️","woman walking: light skin tone" }, /*1854 : People & Body*/
{"🚶🏻‍♀","woman walking: light skin tone" }, /*1855 : People & Body*/
{"🚶🏼‍♀️","woman walking: medium-light skin tone" }, /*1856 : People & Body*/
{"🚶🏼‍♀","woman walking: medium-light skin tone" }, /*1857 : People & Body*/
{"🚶🏽‍♀️","woman walking: medium skin tone" }, /*1858 : People & Body*/
{"🚶🏽‍♀","woman walking: medium skin tone" }, /*1859 : People & Body*/
{"🚶🏾‍♀️","woman walking: medium-dark skin tone" }, /*1860 : People & Body*/
{"🚶🏾‍♀","woman walking: medium-dark skin tone" }, /*1861 : People & Body*/
{"🚶🏿‍♀️","woman walking: dark skin tone" }, /*1862 : People & Body*/
{"🚶🏿‍♀","woman walking: dark skin tone" }, /*1863 : People & Body*/
{"🧍","person standing" }, /*1864 : People & Body*/
{"🧍🏻","person standing: light skin tone" }, /*1865 : People & Body*/
{"🧍🏼","person standing: medium-light skin tone" }, /*1866 : People & Body*/
{"🧍🏽","person standing: medium skin tone" }, /*1867 : People & Body*/
{"🧍🏾","person standing: medium-dark skin tone" }, /*1868 : People & Body*/
{"🧍🏿","person standing: dark skin tone" }, /*1869 : People & Body*/
{"🧍‍♂️","man standing" }, /*1870 : People & Body*/
{"🧍‍♂","man standing" }, /*1871 : People & Body*/
{"🧍🏻‍♂️","man standing: light skin tone" }, /*1872 : People & Body*/
{"🧍🏻‍♂","man standing: light skin tone" }, /*1873 : People & Body*/
{"🧍🏼‍♂️","man standing: medium-light skin tone" }, /*1874 : People & Body*/
{"🧍🏼‍♂","man standing: medium-light skin tone" }, /*1875 : People & Body*/
{"🧍🏽‍♂️","man standing: medium skin tone" }, /*1876 : People & Body*/
{"🧍🏽‍♂","man standing: medium skin tone" }, /*1877 : People & Body*/
{"🧍🏾‍♂️","man standing: medium-dark skin tone" }, /*1878 : People & Body*/
{"🧍🏾‍♂","man standing: medium-dark skin tone" }, /*1879 : People & Body*/
{"🧍🏿‍♂️","man standing: dark skin tone" }, /*1880 : People & Body*/
{"🧍🏿‍♂","man standing: dark skin tone" }, /*1881 : People & Body*/
{"🧍‍♀️","woman standing" }, /*1882 : People & Body*/
{"🧍‍♀","woman standing" }, /*1883 : People & Body*/
{"🧍🏻‍♀️","woman standing: light skin tone" }, /*1884 : People & Body*/
{"🧍🏻‍♀","woman standing: light skin tone" }, /*1885 : People & Body*/
{"🧍🏼‍♀️","woman standing: medium-light skin tone" }, /*1886 : People & Body*/
{"🧍🏼‍♀","woman standing: medium-light skin tone" }, /*1887 : People & Body*/
{"🧍🏽‍♀️","woman standing: medium skin tone" }, /*1888 : People & Body*/
{"🧍🏽‍♀","woman standing: medium skin tone" }, /*1889 : People & Body*/
{"🧍🏾‍♀️","woman standing: medium-dark skin tone" }, /*1890 : People & Body*/
{"🧍🏾‍♀","woman standing: medium-dark skin tone" }, /*1891 : People & Body*/
{"🧍🏿‍♀️","woman standing: dark skin tone" }, /*1892 : People & Body*/
{"🧍🏿‍♀","woman standing: dark skin tone" }, /*1893 : People & Body*/
{"🧎","person kneeling" }, /*1894 : People & Body*/
{"🧎🏻","person kneeling: light skin tone" }, /*1895 : People & Body*/
{"🧎🏼","person kneeling: medium-light skin tone" }, /*1896 : People & Body*/
{"🧎🏽","person kneeling: medium skin tone" }, /*1897 : People & Body*/
{"🧎🏾","person kneeling: medium-dark skin tone" }, /*1898 : People & Body*/
{"🧎🏿","person kneeling: dark skin tone" }, /*1899 : People & Body*/
{"🧎‍♂️","man kneeling" }, /*1900 : People & Body*/
{"🧎‍♂","man kneeling" }, /*1901 : People & Body*/
{"🧎🏻‍♂️","man kneeling: light skin tone" }, /*1902 : People & Body*/
{"🧎🏻‍♂","man kneeling: light skin tone" }, /*1903 : People & Body*/
{"🧎🏼‍♂️","man kneeling: medium-light skin tone" }, /*1904 : People & Body*/
{"🧎🏼‍♂","man kneeling: medium-light skin tone" }, /*1905 : People & Body*/
{"🧎🏽‍♂️","man kneeling: medium skin tone" }, /*1906 : People & Body*/
{"🧎🏽‍♂","man kneeling: medium skin tone" }, /*1907 : People & Body*/
{"🧎🏾‍♂️","man kneeling: medium-dark skin tone" }, /*1908 : People & Body*/
{"🧎🏾‍♂","man kneeling: medium-dark skin tone" }, /*1909 : People & Body*/
{"🧎🏿‍♂️","man kneeling: dark skin tone" }, /*1910 : People & Body*/
{"🧎🏿‍♂","man kneeling: dark skin tone" }, /*1911 : People & Body*/
{"🧎‍♀️","woman kneeling" }, /*1912 : People & Body*/
{"🧎‍♀","woman kneeling" }, /*1913 : People & Body*/
{"🧎🏻‍♀️","woman kneeling: light skin tone" }, /*1914 : People & Body*/
{"🧎🏻‍♀","woman kneeling: light skin tone" }, /*1915 : People & Body*/
{"🧎🏼‍♀️","woman kneeling: medium-light skin tone" }, /*1916 : People & Body*/
{"🧎🏼‍♀","woman kneeling: medium-light skin tone" }, /*1917 : People & Body*/
{"🧎🏽‍♀️","woman kneeling: medium skin tone" }, /*1918 : People & Body*/
{"🧎🏽‍♀","woman kneeling: medium skin tone" }, /*1919 : People & Body*/
{"🧎🏾‍♀️","woman kneeling: medium-dark skin tone" }, /*1920 : People & Body*/
{"🧎🏾‍♀","woman kneeling: medium-dark skin tone" }, /*1921 : People & Body*/
{"🧎🏿‍♀️","woman kneeling: dark skin tone" }, /*1922 : People & Body*/
{"🧎🏿‍♀","woman kneeling: dark skin tone" }, /*1923 : People & Body*/
{"🧑‍🦯","person with white cane" }, /*1924 : People & Body*/
{"🧑🏻‍🦯","person with white cane: light skin tone" }, /*1925 : People & Body*/
{"🧑🏼‍🦯","person with white cane: medium-light skin tone" }, /*1926 : People & Body*/
{"🧑🏽‍🦯","person with white cane: medium skin tone" }, /*1927 : People & Body*/
{"🧑🏾‍🦯","person with white cane: medium-dark skin tone" }, /*1928 : People & Body*/
{"🧑🏿‍🦯","person with white cane: dark skin tone" }, /*1929 : People & Body*/
{"👨‍🦯","man with white cane" }, /*1930 : People & Body*/
{"👨🏻‍🦯","man with white cane: light skin tone" }, /*1931 : People & Body*/
{"👨🏼‍🦯","man with white cane: medium-light skin tone" }, /*1932 : People & Body*/
{"👨🏽‍🦯","man with white cane: medium skin tone" }, /*1933 : People & Body*/
{"👨🏾‍🦯","man with white cane: medium-dark skin tone" }, /*1934 : People & Body*/
{"👨🏿‍🦯","man with white cane: dark skin tone" }, /*1935 : People & Body*/
{"👩‍🦯","woman with white cane" }, /*1936 : People & Body*/
{"👩🏻‍🦯","woman with white cane: light skin tone" }, /*1937 : People & Body*/
{"👩🏼‍🦯","woman with white cane: medium-light skin tone" }, /*1938 : People & Body*/
{"👩🏽‍🦯","woman with white cane: medium skin tone" }, /*1939 : People & Body*/
{"👩🏾‍🦯","woman with white cane: medium-dark skin tone" }, /*1940 : People & Body*/
{"👩🏿‍🦯","woman with white cane: dark skin tone" }, /*1941 : People & Body*/
{"🧑‍🦼","person in motorized wheelchair" }, /*1942 : People & Body*/
{"🧑🏻‍🦼","person in motorized wheelchair: light skin tone" }, /*1943 : People & Body*/
{"🧑🏼‍🦼","person in motorized wheelchair: medium-light skin tone" }, /*1944 : People & Body*/
{"🧑🏽‍🦼","person in motorized wheelchair: medium skin tone" }, /*1945 : People & Body*/
{"🧑🏾‍🦼","person in motorized wheelchair: medium-dark skin tone" }, /*1946 : People & Body*/
{"🧑🏿‍🦼","person in motorized wheelchair: dark skin tone" }, /*1947 : People & Body*/
{"👨‍🦼","man in motorized wheelchair" }, /*1948 : People & Body*/
{"👨🏻‍🦼","man in motorized wheelchair: light skin tone" }, /*1949 : People & Body*/
{"👨🏼‍🦼","man in motorized wheelchair: medium-light skin tone" }, /*1950 : People & Body*/
{"👨🏽‍🦼","man in motorized wheelchair: medium skin tone" }, /*1951 : People & Body*/
{"👨🏾‍🦼","man in motorized wheelchair: medium-dark skin tone" }, /*1952 : People & Body*/
{"👨🏿‍🦼","man in motorized wheelchair: dark skin tone" }, /*1953 : People & Body*/
{"👩‍🦼","woman in motorized wheelchair" }, /*1954 : People & Body*/
{"👩🏻‍🦼","woman in motorized wheelchair: light skin tone" }, /*1955 : People & Body*/
{"👩🏼‍🦼","woman in motorized wheelchair: medium-light skin tone" }, /*1956 : People & Body*/
{"👩🏽‍🦼","woman in motorized wheelchair: medium skin tone" }, /*1957 : People & Body*/
{"👩🏾‍🦼","woman in motorized wheelchair: medium-dark skin tone" }, /*1958 : People & Body*/
{"👩🏿‍🦼","woman in motorized wheelchair: dark skin tone" }, /*1959 : People & Body*/
{"🧑‍🦽","person in manual wheelchair" }, /*1960 : People & Body*/
{"🧑🏻‍🦽","person in manual wheelchair: light skin tone" }, /*1961 : People & Body*/
{"🧑🏼‍🦽","person in manual wheelchair: medium-light skin tone" }, /*1962 : People & Body*/
{"🧑🏽‍🦽","person in manual wheelchair: medium skin tone" }, /*1963 : People & Body*/
{"🧑🏾‍🦽","person in manual wheelchair: medium-dark skin tone" }, /*1964 : People & Body*/
{"🧑🏿‍🦽","person in manual wheelchair: dark skin tone" }, /*1965 : People & Body*/
{"👨‍🦽","man in manual wheelchair" }, /*1966 : People & Body*/
{"👨🏻‍🦽","man in manual wheelchair: light skin tone" }, /*1967 : People & Body*/
{"👨🏼‍🦽","man in manual wheelchair: medium-light skin tone" }, /*1968 : People & Body*/
{"👨🏽‍🦽","man in manual wheelchair: medium skin tone" }, /*1969 : People & Body*/
{"👨🏾‍🦽","man in manual wheelchair: medium-dark skin tone" }, /*1970 : People & Body*/
{"👨🏿‍🦽","man in manual wheelchair: dark skin tone" }, /*1971 : People & Body*/
{"👩‍🦽","woman in manual wheelchair" }, /*1972 : People & Body*/
{"👩🏻‍🦽","woman in manual wheelchair: light skin tone" }, /*1973 : People & Body*/
{"👩🏼‍🦽","woman in manual wheelchair: medium-light skin tone" }, /*1974 : People & Body*/
{"👩🏽‍🦽","woman in manual wheelchair: medium skin tone" }, /*1975 : People & Body*/
{"👩🏾‍🦽","woman in manual wheelchair: medium-dark skin tone" }, /*1976 : People & Body*/
{"👩🏿‍🦽","woman in manual wheelchair: dark skin tone" }, /*1977 : People & Body*/
{"🏃","person running" }, /*1978 : People & Body*/
{"🏃🏻","person running: light skin tone" }, /*1979 : People & Body*/
{"🏃🏼","person running: medium-light skin tone" }, /*1980 : People & Body*/
{"🏃🏽","person running: medium skin tone" }, /*1981 : People & Body*/
{"🏃🏾","person running: medium-dark skin tone" }, /*1982 : People & Body*/
{"🏃🏿","person running: dark skin tone" }, /*1983 : People & Body*/
{"🏃‍♂️","man running" }, /*1984 : People & Body*/
{"🏃‍♂","man running" }, /*1985 : People & Body*/
{"🏃🏻‍♂️","man running: light skin tone" }, /*1986 : People & Body*/
{"🏃🏻‍♂","man running: light skin tone" }, /*1987 : People & Body*/
{"🏃🏼‍♂️","man running: medium-light skin tone" }, /*1988 : People & Body*/
{"🏃🏼‍♂","man running: medium-light skin tone" }, /*1989 : People & Body*/
{"🏃🏽‍♂️","man running: medium skin tone" }, /*1990 : People & Body*/
{"🏃🏽‍♂","man running: medium skin tone" }, /*1991 : People & Body*/
{"🏃🏾‍♂️","man running: medium-dark skin tone" }, /*1992 : People & Body*/
{"🏃🏾‍♂","man running: medium-dark skin tone" }, /*1993 : People & Body*/
{"🏃🏿‍♂️","man running: dark skin tone" }, /*1994 : People & Body*/
{"🏃🏿‍♂","man running: dark skin tone" }, /*1995 : People & Body*/
{"🏃‍♀️","woman running" }, /*1996 : People & Body*/
{"🏃‍♀","woman running" }, /*1997 : People & Body*/
{"🏃🏻‍♀️","woman running: light skin tone" }, /*1998 : People & Body*/
{"🏃🏻‍♀","woman running: light skin tone" }, /*1999 : People & Body*/
{"🏃🏼‍♀️","woman running: medium-light skin tone" }, /*2000 : People & Body*/
{"🏃🏼‍♀","woman running: medium-light skin tone" }, /*2001 : People & Body*/
{"🏃🏽‍♀️","woman running: medium skin tone" }, /*2002 : People & Body*/
{"🏃🏽‍♀","woman running: medium skin tone" }, /*2003 : People & Body*/
{"🏃🏾‍♀️","woman running: medium-dark skin tone" }, /*2004 : People & Body*/
{"🏃🏾‍♀","woman running: medium-dark skin tone" }, /*2005 : People & Body*/
{"🏃🏿‍♀️","woman running: dark skin tone" }, /*2006 : People & Body*/
{"🏃🏿‍♀","woman running: dark skin tone" }, /*2007 : People & Body*/
{"💃","woman dancing" }, /*2008 : People & Body*/
{"💃🏻","woman dancing: light skin tone" }, /*2009 : People & Body*/
{"💃🏼","woman dancing: medium-light skin tone" }, /*2010 : People & Body*/
{"💃🏽","woman dancing: medium skin tone" }, /*2011 : People & Body*/
{"💃🏾","woman dancing: medium-dark skin tone" }, /*2012 : People & Body*/
{"💃🏿","woman dancing: dark skin tone" }, /*2013 : People & Body*/
{"🕺","man dancing" }, /*2014 : People & Body*/
{"🕺🏻","man dancing: light skin tone" }, /*2015 : People & Body*/
{"🕺🏼","man dancing: medium-light skin tone" }, /*2016 : People & Body*/
{"🕺🏽","man dancing: medium skin tone" }, /*2017 : People & Body*/
{"🕺🏾","man dancing: medium-dark skin tone" }, /*2018 : People & Body*/
{"🕺🏿","man dancing: dark skin tone" }, /*2019 : People & Body*/
{"🕴️","person in suit levitating" }, /*2020 : People & Body*/
{"🕴","person in suit levitating" }, /*2021 : People & Body*/
{"🕴🏻","person in suit levitating: light skin tone" }, /*2022 : People & Body*/
{"🕴🏼","person in suit levitating: medium-light skin tone" }, /*2023 : People & Body*/
{"🕴🏽","person in suit levitating: medium skin tone" }, /*2024 : People & Body*/
{"🕴🏾","person in suit levitating: medium-dark skin tone" }, /*2025 : People & Body*/
{"🕴🏿","person in suit levitating: dark skin tone" }, /*2026 : People & Body*/
{"👯","people with bunny ears" }, /*2027 : People & Body*/
{"👯‍♂️","men with bunny ears" }, /*2028 : People & Body*/
{"👯‍♂","men with bunny ears" }, /*2029 : People & Body*/
{"👯‍♀️","women with bunny ears" }, /*2030 : People & Body*/
{"👯‍♀","women with bunny ears" }, /*2031 : People & Body*/
{"🧖","person in steamy room" }, /*2032 : People & Body*/
{"🧖🏻","person in steamy room: light skin tone" }, /*2033 : People & Body*/
{"🧖🏼","person in steamy room: medium-light skin tone" }, /*2034 : People & Body*/
{"🧖🏽","person in steamy room: medium skin tone" }, /*2035 : People & Body*/
{"🧖🏾","person in steamy room: medium-dark skin tone" }, /*2036 : People & Body*/
{"🧖🏿","person in steamy room: dark skin tone" }, /*2037 : People & Body*/
{"🧖‍♂️","man in steamy room" }, /*2038 : People & Body*/
{"🧖‍♂","man in steamy room" }, /*2039 : People & Body*/
{"🧖🏻‍♂️","man in steamy room: light skin tone" }, /*2040 : People & Body*/
{"🧖🏻‍♂","man in steamy room: light skin tone" }, /*2041 : People & Body*/
{"🧖🏼‍♂️","man in steamy room: medium-light skin tone" }, /*2042 : People & Body*/
{"🧖🏼‍♂","man in steamy room: medium-light skin tone" }, /*2043 : People & Body*/
{"🧖🏽‍♂️","man in steamy room: medium skin tone" }, /*2044 : People & Body*/
{"🧖🏽‍♂","man in steamy room: medium skin tone" }, /*2045 : People & Body*/
{"🧖🏾‍♂️","man in steamy room: medium-dark skin tone" }, /*2046 : People & Body*/
{"🧖🏾‍♂","man in steamy room: medium-dark skin tone" }, /*2047 : People & Body*/
{"🧖🏿‍♂️","man in steamy room: dark skin tone" }, /*2048 : People & Body*/
{"🧖🏿‍♂","man in steamy room: dark skin tone" }, /*2049 : People & Body*/
{"🧖‍♀️","woman in steamy room" }, /*2050 : People & Body*/
{"🧖‍♀","woman in steamy room" }, /*2051 : People & Body*/
{"🧖🏻‍♀️","woman in steamy room: light skin tone" }, /*2052 : People & Body*/
{"🧖🏻‍♀","woman in steamy room: light skin tone" }, /*2053 : People & Body*/
{"🧖🏼‍♀️","woman in steamy room: medium-light skin tone" }, /*2054 : People & Body*/
{"🧖🏼‍♀","woman in steamy room: medium-light skin tone" }, /*2055 : People & Body*/
{"🧖🏽‍♀️","woman in steamy room: medium skin tone" }, /*2056 : People & Body*/
{"🧖🏽‍♀","woman in steamy room: medium skin tone" }, /*2057 : People & Body*/
{"🧖🏾‍♀️","woman in steamy room: medium-dark skin tone" }, /*2058 : People & Body*/
{"🧖🏾‍♀","woman in steamy room: medium-dark skin tone" }, /*2059 : People & Body*/
{"🧖🏿‍♀️","woman in steamy room: dark skin tone" }, /*2060 : People & Body*/
{"🧖🏿‍♀","woman in steamy room: dark skin tone" }, /*2061 : People & Body*/
{"🧗","person climbing" }, /*2062 : People & Body*/
{"🧗🏻","person climbing: light skin tone" }, /*2063 : People & Body*/
{"🧗🏼","person climbing: medium-light skin tone" }, /*2064 : People & Body*/
{"🧗🏽","person climbing: medium skin tone" }, /*2065 : People & Body*/
{"🧗🏾","person climbing: medium-dark skin tone" }, /*2066 : People & Body*/
{"🧗🏿","person climbing: dark skin tone" }, /*2067 : People & Body*/
{"🧗‍♂️","man climbing" }, /*2068 : People & Body*/
{"🧗‍♂","man climbing" }, /*2069 : People & Body*/
{"🧗🏻‍♂️","man climbing: light skin tone" }, /*2070 : People & Body*/
{"🧗🏻‍♂","man climbing: light skin tone" }, /*2071 : People & Body*/
{"🧗🏼‍♂️","man climbing: medium-light skin tone" }, /*2072 : People & Body*/
{"🧗🏼‍♂","man climbing: medium-light skin tone" }, /*2073 : People & Body*/
{"🧗🏽‍♂️","man climbing: medium skin tone" }, /*2074 : People & Body*/
{"🧗🏽‍♂","man climbing: medium skin tone" }, /*2075 : People & Body*/
{"🧗🏾‍♂️","man climbing: medium-dark skin tone" }, /*2076 : People & Body*/
{"🧗🏾‍♂","man climbing: medium-dark skin tone" }, /*2077 : People & Body*/
{"🧗🏿‍♂️","man climbing: dark skin tone" }, /*2078 : People & Body*/
{"🧗🏿‍♂","man climbing: dark skin tone" }, /*2079 : People & Body*/
{"🧗‍♀️","woman climbing" }, /*2080 : People & Body*/
{"🧗‍♀","woman climbing" }, /*2081 : People & Body*/
{"🧗🏻‍♀️","woman climbing: light skin tone" }, /*2082 : People & Body*/
{"🧗🏻‍♀","woman climbing: light skin tone" }, /*2083 : People & Body*/
{"🧗🏼‍♀️","woman climbing: medium-light skin tone" }, /*2084 : People & Body*/
{"🧗🏼‍♀","woman climbing: medium-light skin tone" }, /*2085 : People & Body*/
{"🧗🏽‍♀️","woman climbing: medium skin tone" }, /*2086 : People & Body*/
{"🧗🏽‍♀","woman climbing: medium skin tone" }, /*2087 : People & Body*/
{"🧗🏾‍♀️","woman climbing: medium-dark skin tone" }, /*2088 : People & Body*/
{"🧗🏾‍♀","woman climbing: medium-dark skin tone" }, /*2089 : People & Body*/
{"🧗🏿‍♀️","woman climbing: dark skin tone" }, /*2090 : People & Body*/
{"🧗🏿‍♀","woman climbing: dark skin tone" }, /*2091 : People & Body*/
{"🤺","person fencing" }, /*2092 : People & Body*/
{"🏇","horse racing" }, /*2093 : People & Body*/
{"🏇🏻","horse racing: light skin tone" }, /*2094 : People & Body*/
{"🏇🏼","horse racing: medium-light skin tone" }, /*2095 : People & Body*/
{"🏇🏽","horse racing: medium skin tone" }, /*2096 : People & Body*/
{"🏇🏾","horse racing: medium-dark skin tone" }, /*2097 : People & Body*/
{"🏇🏿","horse racing: dark skin tone" }, /*2098 : People & Body*/
{"⛷️","skier" }, /*2099 : People & Body*/
{"⛷","skier" }, /*2100 : People & Body*/
{"🏂","snowboarder" }, /*2101 : People & Body*/
{"🏂🏻","snowboarder: light skin tone" }, /*2102 : People & Body*/
{"🏂🏼","snowboarder: medium-light skin tone" }, /*2103 : People & Body*/
{"🏂🏽","snowboarder: medium skin tone" }, /*2104 : People & Body*/
{"🏂🏾","snowboarder: medium-dark skin tone" }, /*2105 : People & Body*/
{"🏂🏿","snowboarder: dark skin tone" }, /*2106 : People & Body*/
{"🏌️","person golfing" }, /*2107 : People & Body*/
{"🏌","person golfing" }, /*2108 : People & Body*/
{"🏌🏻","person golfing: light skin tone" }, /*2109 : People & Body*/
{"🏌🏼","person golfing: medium-light skin tone" }, /*2110 : People & Body*/
{"🏌🏽","person golfing: medium skin tone" }, /*2111 : People & Body*/
{"🏌🏾","person golfing: medium-dark skin tone" }, /*2112 : People & Body*/
{"🏌🏿","person golfing: dark skin tone" }, /*2113 : People & Body*/
{"🏌️‍♂️","man golfing" }, /*2114 : People & Body*/
{"🏌‍♂️","man golfing" }, /*2115 : People & Body*/
{"🏌️‍♂","man golfing" }, /*2116 : People & Body*/
{"🏌‍♂","man golfing" }, /*2117 : People & Body*/
{"🏌🏻‍♂️","man golfing: light skin tone" }, /*2118 : People & Body*/
{"🏌🏻‍♂","man golfing: light skin tone" }, /*2119 : People & Body*/
{"🏌🏼‍♂️","man golfing: medium-light skin tone" }, /*2120 : People & Body*/
{"🏌🏼‍♂","man golfing: medium-light skin tone" }, /*2121 : People & Body*/
{"🏌🏽‍♂️","man golfing: medium skin tone" }, /*2122 : People & Body*/
{"🏌🏽‍♂","man golfing: medium skin tone" }, /*2123 : People & Body*/
{"🏌🏾‍♂️","man golfing: medium-dark skin tone" }, /*2124 : People & Body*/
{"🏌🏾‍♂","man golfing: medium-dark skin tone" }, /*2125 : People & Body*/
{"🏌🏿‍♂️","man golfing: dark skin tone" }, /*2126 : People & Body*/
{"🏌🏿‍♂","man golfing: dark skin tone" }, /*2127 : People & Body*/
{"🏌️‍♀️","woman golfing" }, /*2128 : People & Body*/
{"🏌‍♀️","woman golfing" }, /*2129 : People & Body*/
{"🏌️‍♀","woman golfing" }, /*2130 : People & Body*/
{"🏌‍♀","woman golfing" }, /*2131 : People & Body*/
{"🏌🏻‍♀️","woman golfing: light skin tone" }, /*2132 : People & Body*/
{"🏌🏻‍♀","woman golfing: light skin tone" }, /*2133 : People & Body*/
{"🏌🏼‍♀️","woman golfing: medium-light skin tone" }, /*2134 : People & Body*/
{"🏌🏼‍♀","woman golfing: medium-light skin tone" }, /*2135 : People & Body*/
{"🏌🏽‍♀️","woman golfing: medium skin tone" }, /*2136 : People & Body*/
{"🏌🏽‍♀","woman golfing: medium skin tone" }, /*2137 : People & Body*/
{"🏌🏾‍♀️","woman golfing: medium-dark skin tone" }, /*2138 : People & Body*/
{"🏌🏾‍♀","woman golfing: medium-dark skin tone" }, /*2139 : People & Body*/
{"🏌🏿‍♀️","woman golfing: dark skin tone" }, /*2140 : People & Body*/
{"🏌🏿‍♀","woman golfing: dark skin tone" }, /*2141 : People & Body*/
{"🏄","person surfing" }, /*2142 : People & Body*/
{"🏄🏻","person surfing: light skin tone" }, /*2143 : People & Body*/
{"🏄🏼","person surfing: medium-light skin tone" }, /*2144 : People & Body*/
{"🏄🏽","person surfing: medium skin tone" }, /*2145 : People & Body*/
{"🏄🏾","person surfing: medium-dark skin tone" }, /*2146 : People & Body*/
{"🏄🏿","person surfing: dark skin tone" }, /*2147 : People & Body*/
{"🏄‍♂️","man surfing" }, /*2148 : People & Body*/
{"🏄‍♂","man surfing" }, /*2149 : People & Body*/
{"🏄🏻‍♂️","man surfing: light skin tone" }, /*2150 : People & Body*/
{"🏄🏻‍♂","man surfing: light skin tone" }, /*2151 : People & Body*/
{"🏄🏼‍♂️","man surfing: medium-light skin tone" }, /*2152 : People & Body*/
{"🏄🏼‍♂","man surfing: medium-light skin tone" }, /*2153 : People & Body*/
{"🏄🏽‍♂️","man surfing: medium skin tone" }, /*2154 : People & Body*/
{"🏄🏽‍♂","man surfing: medium skin tone" }, /*2155 : People & Body*/
{"🏄🏾‍♂️","man surfing: medium-dark skin tone" }, /*2156 : People & Body*/
{"🏄🏾‍♂","man surfing: medium-dark skin tone" }, /*2157 : People & Body*/
{"🏄🏿‍♂️","man surfing: dark skin tone" }, /*2158 : People & Body*/
{"🏄🏿‍♂","man surfing: dark skin tone" }, /*2159 : People & Body*/
{"🏄‍♀️","woman surfing" }, /*2160 : People & Body*/
{"🏄‍♀","woman surfing" }, /*2161 : People & Body*/
{"🏄🏻‍♀️","woman surfing: light skin tone" }, /*2162 : People & Body*/
{"🏄🏻‍♀","woman surfing: light skin tone" }, /*2163 : People & Body*/
{"🏄🏼‍♀️","woman surfing: medium-light skin tone" }, /*2164 : People & Body*/
{"🏄🏼‍♀","woman surfing: medium-light skin tone" }, /*2165 : People & Body*/
{"🏄🏽‍♀️","woman surfing: medium skin tone" }, /*2166 : People & Body*/
{"🏄🏽‍♀","woman surfing: medium skin tone" }, /*2167 : People & Body*/
{"🏄🏾‍♀️","woman surfing: medium-dark skin tone" }, /*2168 : People & Body*/
{"🏄🏾‍♀","woman surfing: medium-dark skin tone" }, /*2169 : People & Body*/
{"🏄🏿‍♀️","woman surfing: dark skin tone" }, /*2170 : People & Body*/
{"🏄🏿‍♀","woman surfing: dark skin tone" }, /*2171 : People & Body*/
{"🚣","person rowing boat" }, /*2172 : People & Body*/
{"🚣🏻","person rowing boat: light skin tone" }, /*2173 : People & Body*/
{"🚣🏼","person rowing boat: medium-light skin tone" }, /*2174 : People & Body*/
{"🚣🏽","person rowing boat: medium skin tone" }, /*2175 : People & Body*/
{"🚣🏾","person rowing boat: medium-dark skin tone" }, /*2176 : People & Body*/
{"🚣🏿","person rowing boat: dark skin tone" }, /*2177 : People & Body*/
{"🚣‍♂️","man rowing boat" }, /*2178 : People & Body*/
{"🚣‍♂","man rowing boat" }, /*2179 : People & Body*/
{"🚣🏻‍♂️","man rowing boat: light skin tone" }, /*2180 : People & Body*/
{"🚣🏻‍♂","man rowing boat: light skin tone" }, /*2181 : People & Body*/
{"🚣🏼‍♂️","man rowing boat: medium-light skin tone" }, /*2182 : People & Body*/
{"🚣🏼‍♂","man rowing boat: medium-light skin tone" }, /*2183 : People & Body*/
{"🚣🏽‍♂️","man rowing boat: medium skin tone" }, /*2184 : People & Body*/
{"🚣🏽‍♂","man rowing boat: medium skin tone" }, /*2185 : People & Body*/
{"🚣🏾‍♂️","man rowing boat: medium-dark skin tone" }, /*2186 : People & Body*/
{"🚣🏾‍♂","man rowing boat: medium-dark skin tone" }, /*2187 : People & Body*/
{"🚣🏿‍♂️","man rowing boat: dark skin tone" }, /*2188 : People & Body*/
{"🚣🏿‍♂","man rowing boat: dark skin tone" }, /*2189 : People & Body*/
{"🚣‍♀️","woman rowing boat" }, /*2190 : People & Body*/
{"🚣‍♀","woman rowing boat" }, /*2191 : People & Body*/
{"🚣🏻‍♀️","woman rowing boat: light skin tone" }, /*2192 : People & Body*/
{"🚣🏻‍♀","woman rowing boat: light skin tone" }, /*2193 : People & Body*/
{"🚣🏼‍♀️","woman rowing boat: medium-light skin tone" }, /*2194 : People & Body*/
{"🚣🏼‍♀","woman rowing boat: medium-light skin tone" }, /*2195 : People & Body*/
{"🚣🏽‍♀️","woman rowing boat: medium skin tone" }, /*2196 : People & Body*/
{"🚣🏽‍♀","woman rowing boat: medium skin tone" }, /*2197 : People & Body*/
{"🚣🏾‍♀️","woman rowing boat: medium-dark skin tone" }, /*2198 : People & Body*/
{"🚣🏾‍♀","woman rowing boat: medium-dark skin tone" }, /*2199 : People & Body*/
{"🚣🏿‍♀️","woman rowing boat: dark skin tone" }, /*2200 : People & Body*/
{"🚣🏿‍♀","woman rowing boat: dark skin tone" }, /*2201 : People & Body*/
{"🏊","person swimming" }, /*2202 : People & Body*/
{"🏊🏻","person swimming: light skin tone" }, /*2203 : People & Body*/
{"🏊🏼","person swimming: medium-light skin tone" }, /*2204 : People & Body*/
{"🏊🏽","person swimming: medium skin tone" }, /*2205 : People & Body*/
{"🏊🏾","person swimming: medium-dark skin tone" }, /*2206 : People & Body*/
{"🏊🏿","person swimming: dark skin tone" }, /*2207 : People & Body*/
{"🏊‍♂️","man swimming" }, /*2208 : People & Body*/
{"🏊‍♂","man swimming" }, /*2209 : People & Body*/
{"🏊🏻‍♂️","man swimming: light skin tone" }, /*2210 : People & Body*/
{"🏊🏻‍♂","man swimming: light skin tone" }, /*2211 : People & Body*/
{"🏊🏼‍♂️","man swimming: medium-light skin tone" }, /*2212 : People & Body*/
{"🏊🏼‍♂","man swimming: medium-light skin tone" }, /*2213 : People & Body*/
{"🏊🏽‍♂️","man swimming: medium skin tone" }, /*2214 : People & Body*/
{"🏊🏽‍♂","man swimming: medium skin tone" }, /*2215 : People & Body*/
{"🏊🏾‍♂️","man swimming: medium-dark skin tone" }, /*2216 : People & Body*/
{"🏊🏾‍♂","man swimming: medium-dark skin tone" }, /*2217 : People & Body*/
{"🏊🏿‍♂️","man swimming: dark skin tone" }, /*2218 : People & Body*/
{"🏊🏿‍♂","man swimming: dark skin tone" }, /*2219 : People & Body*/
{"🏊‍♀️","woman swimming" }, /*2220 : People & Body*/
{"🏊‍♀","woman swimming" }, /*2221 : People & Body*/
{"🏊🏻‍♀️","woman swimming: light skin tone" }, /*2222 : People & Body*/
{"🏊🏻‍♀","woman swimming: light skin tone" }, /*2223 : People & Body*/
{"🏊🏼‍♀️","woman swimming: medium-light skin tone" }, /*2224 : People & Body*/
{"🏊🏼‍♀","woman swimming: medium-light skin tone" }, /*2225 : People & Body*/
{"🏊🏽‍♀️","woman swimming: medium skin tone" }, /*2226 : People & Body*/
{"🏊🏽‍♀","woman swimming: medium skin tone" }, /*2227 : People & Body*/
{"🏊🏾‍♀️","woman swimming: medium-dark skin tone" }, /*2228 : People & Body*/
{"🏊🏾‍♀","woman swimming: medium-dark skin tone" }, /*2229 : People & Body*/
{"🏊🏿‍♀️","woman swimming: dark skin tone" }, /*2230 : People & Body*/
{"🏊🏿‍♀","woman swimming: dark skin tone" }, /*2231 : People & Body*/
{"⛹️","person bouncing ball" }, /*2232 : People & Body*/
{"⛹","person bouncing ball" }, /*2233 : People & Body*/
{"⛹🏻","person bouncing ball: light skin tone" }, /*2234 : People & Body*/
{"⛹🏼","person bouncing ball: medium-light skin tone" }, /*2235 : People & Body*/
{"⛹🏽","person bouncing ball: medium skin tone" }, /*2236 : People & Body*/
{"⛹🏾","person bouncing ball: medium-dark skin tone" }, /*2237 : People & Body*/
{"⛹🏿","person bouncing ball: dark skin tone" }, /*2238 : People & Body*/
{"⛹️‍♂️","man bouncing ball" }, /*2239 : People & Body*/
{"⛹‍♂️","man bouncing ball" }, /*2240 : People & Body*/
{"⛹️‍♂","man bouncing ball" }, /*2241 : People & Body*/
{"⛹‍♂","man bouncing ball" }, /*2242 : People & Body*/
{"⛹🏻‍♂️","man bouncing ball: light skin tone" }, /*2243 : People & Body*/
{"⛹🏻‍♂","man bouncing ball: light skin tone" }, /*2244 : People & Body*/
{"⛹🏼‍♂️","man bouncing ball: medium-light skin tone" }, /*2245 : People & Body*/
{"⛹🏼‍♂","man bouncing ball: medium-light skin tone" }, /*2246 : People & Body*/
{"⛹🏽‍♂️","man bouncing ball: medium skin tone" }, /*2247 : People & Body*/
{"⛹🏽‍♂","man bouncing ball: medium skin tone" }, /*2248 : People & Body*/
{"⛹🏾‍♂️","man bouncing ball: medium-dark skin tone" }, /*2249 : People & Body*/
{"⛹🏾‍♂","man bouncing ball: medium-dark skin tone" }, /*2250 : People & Body*/
{"⛹🏿‍♂️","man bouncing ball: dark skin tone" }, /*2251 : People & Body*/
{"⛹🏿‍♂","man bouncing ball: dark skin tone" }, /*2252 : People & Body*/
{"⛹️‍♀️","woman bouncing ball" }, /*2253 : People & Body*/
{"⛹‍♀️","woman bouncing ball" }, /*2254 : People & Body*/
{"⛹️‍♀","woman bouncing ball" }, /*2255 : People & Body*/
{"⛹‍♀","woman bouncing ball" }, /*2256 : People & Body*/
{"⛹🏻‍♀️","woman bouncing ball: light skin tone" }, /*2257 : People & Body*/
{"⛹🏻‍♀","woman bouncing ball: light skin tone" }, /*2258 : People & Body*/
{"⛹🏼‍♀️","woman bouncing ball: medium-light skin tone" }, /*2259 : People & Body*/
{"⛹🏼‍♀","woman bouncing ball: medium-light skin tone" }, /*2260 : People & Body*/
{"⛹🏽‍♀️","woman bouncing ball: medium skin tone" }, /*2261 : People & Body*/
{"⛹🏽‍♀","woman bouncing ball: medium skin tone" }, /*2262 : People & Body*/
{"⛹🏾‍♀️","woman bouncing ball: medium-dark skin tone" }, /*2263 : People & Body*/
{"⛹🏾‍♀","woman bouncing ball: medium-dark skin tone" }, /*2264 : People & Body*/
{"⛹🏿‍♀️","woman bouncing ball: dark skin tone" }, /*2265 : People & Body*/
{"⛹🏿‍♀","woman bouncing ball: dark skin tone" }, /*2266 : People & Body*/
{"🏋️","person lifting weights" }, /*2267 : People & Body*/
{"🏋","person lifting weights" }, /*2268 : People & Body*/
{"🏋🏻","person lifting weights: light skin tone" }, /*2269 : People & Body*/
{"🏋🏼","person lifting weights: medium-light skin tone" }, /*2270 : People & Body*/
{"🏋🏽","person lifting weights: medium skin tone" }, /*2271 : People & Body*/
{"🏋🏾","person lifting weights: medium-dark skin tone" }, /*2272 : People & Body*/
{"🏋🏿","person lifting weights: dark skin tone" }, /*2273 : People & Body*/
{"🏋️‍♂️","man lifting weights" }, /*2274 : People & Body*/
{"🏋‍♂️","man lifting weights" }, /*2275 : People & Body*/
{"🏋️‍♂","man lifting weights" }, /*2276 : People & Body*/
{"🏋‍♂","man lifting weights" }, /*2277 : People & Body*/
{"🏋🏻‍♂️","man lifting weights: light skin tone" }, /*2278 : People & Body*/
{"🏋🏻‍♂","man lifting weights: light skin tone" }, /*2279 : People & Body*/
{"🏋🏼‍♂️","man lifting weights: medium-light skin tone" }, /*2280 : People & Body*/
{"🏋🏼‍♂","man lifting weights: medium-light skin tone" }, /*2281 : People & Body*/
{"🏋🏽‍♂️","man lifting weights: medium skin tone" }, /*2282 : People & Body*/
{"🏋🏽‍♂","man lifting weights: medium skin tone" }, /*2283 : People & Body*/
{"🏋🏾‍♂️","man lifting weights: medium-dark skin tone" }, /*2284 : People & Body*/
{"🏋🏾‍♂","man lifting weights: medium-dark skin tone" }, /*2285 : People & Body*/
{"🏋🏿‍♂️","man lifting weights: dark skin tone" }, /*2286 : People & Body*/
{"🏋🏿‍♂","man lifting weights: dark skin tone" }, /*2287 : People & Body*/
{"🏋️‍♀️","woman lifting weights" }, /*2288 : People & Body*/
{"🏋‍♀️","woman lifting weights" }, /*2289 : People & Body*/
{"🏋️‍♀","woman lifting weights" }, /*2290 : People & Body*/
{"🏋‍♀","woman lifting weights" }, /*2291 : People & Body*/
{"🏋🏻‍♀️","woman lifting weights: light skin tone" }, /*2292 : People & Body*/
{"🏋🏻‍♀","woman lifting weights: light skin tone" }, /*2293 : People & Body*/
{"🏋🏼‍♀️","woman lifting weights: medium-light skin tone" }, /*2294 : People & Body*/
{"🏋🏼‍♀","woman lifting weights: medium-light skin tone" }, /*2295 : People & Body*/
{"🏋🏽‍♀️","woman lifting weights: medium skin tone" }, /*2296 : People & Body*/
{"🏋🏽‍♀","woman lifting weights: medium skin tone" }, /*2297 : People & Body*/
{"🏋🏾‍♀️","woman lifting weights: medium-dark skin tone" }, /*2298 : People & Body*/
{"🏋🏾‍♀","woman lifting weights: medium-dark skin tone" }, /*2299 : People & Body*/
{"🏋🏿‍♀️","woman lifting weights: dark skin tone" }, /*2300 : People & Body*/
{"🏋🏿‍♀","woman lifting weights: dark skin tone" }, /*2301 : People & Body*/
{"🚴","person biking" }, /*2302 : People & Body*/
{"🚴🏻","person biking: light skin tone" }, /*2303 : People & Body*/
{"🚴🏼","person biking: medium-light skin tone" }, /*2304 : People & Body*/
{"🚴🏽","person biking: medium skin tone" }, /*2305 : People & Body*/
{"🚴🏾","person biking: medium-dark skin tone" }, /*2306 : People & Body*/
{"🚴🏿","person biking: dark skin tone" }, /*2307 : People & Body*/
{"🚴‍♂️","man biking" }, /*2308 : People & Body*/
{"🚴‍♂","man biking" }, /*2309 : People & Body*/
{"🚴🏻‍♂️","man biking: light skin tone" }, /*2310 : People & Body*/
{"🚴🏻‍♂","man biking: light skin tone" }, /*2311 : People & Body*/
{"🚴🏼‍♂️","man biking: medium-light skin tone" }, /*2312 : People & Body*/
{"🚴🏼‍♂","man biking: medium-light skin tone" }, /*2313 : People & Body*/
{"🚴🏽‍♂️","man biking: medium skin tone" }, /*2314 : People & Body*/
{"🚴🏽‍♂","man biking: medium skin tone" }, /*2315 : People & Body*/
{"🚴🏾‍♂️","man biking: medium-dark skin tone" }, /*2316 : People & Body*/
{"🚴🏾‍♂","man biking: medium-dark skin tone" }, /*2317 : People & Body*/
{"🚴🏿‍♂️","man biking: dark skin tone" }, /*2318 : People & Body*/
{"🚴🏿‍♂","man biking: dark skin tone" }, /*2319 : People & Body*/
{"🚴‍♀️","woman biking" }, /*2320 : People & Body*/
{"🚴‍♀","woman biking" }, /*2321 : People & Body*/
{"🚴🏻‍♀️","woman biking: light skin tone" }, /*2322 : People & Body*/
{"🚴🏻‍♀","woman biking: light skin tone" }, /*2323 : People & Body*/
{"🚴🏼‍♀️","woman biking: medium-light skin tone" }, /*2324 : People & Body*/
{"🚴🏼‍♀","woman biking: medium-light skin tone" }, /*2325 : People & Body*/
{"🚴🏽‍♀️","woman biking: medium skin tone" }, /*2326 : People & Body*/
{"🚴🏽‍♀","woman biking: medium skin tone" }, /*2327 : People & Body*/
{"🚴🏾‍♀️","woman biking: medium-dark skin tone" }, /*2328 : People & Body*/
{"🚴🏾‍♀","woman biking: medium-dark skin tone" }, /*2329 : People & Body*/
{"🚴🏿‍♀️","woman biking: dark skin tone" }, /*2330 : People & Body*/
{"🚴🏿‍♀","woman biking: dark skin tone" }, /*2331 : People & Body*/
{"🚵","person mountain biking" }, /*2332 : People & Body*/
{"🚵🏻","person mountain biking: light skin tone" }, /*2333 : People & Body*/
{"🚵🏼","person mountain biking: medium-light skin tone" }, /*2334 : People & Body*/
{"🚵🏽","person mountain biking: medium skin tone" }, /*2335 : People & Body*/
{"🚵🏾","person mountain biking: medium-dark skin tone" }, /*2336 : People & Body*/
{"🚵🏿","person mountain biking: dark skin tone" }, /*2337 : People & Body*/
{"🚵‍♂️","man mountain biking" }, /*2338 : People & Body*/
{"🚵‍♂","man mountain biking" }, /*2339 : People & Body*/
{"🚵🏻‍♂️","man mountain biking: light skin tone" }, /*2340 : People & Body*/
{"🚵🏻‍♂","man mountain biking: light skin tone" }, /*2341 : People & Body*/
{"🚵🏼‍♂️","man mountain biking: medium-light skin tone" }, /*2342 : People & Body*/
{"🚵🏼‍♂","man mountain biking: medium-light skin tone" }, /*2343 : People & Body*/
{"🚵🏽‍♂️","man mountain biking: medium skin tone" }, /*2344 : People & Body*/
{"🚵🏽‍♂","man mountain biking: medium skin tone" }, /*2345 : People & Body*/
{"🚵🏾‍♂️","man mountain biking: medium-dark skin tone" }, /*2346 : People & Body*/
{"🚵🏾‍♂","man mountain biking: medium-dark skin tone" }, /*2347 : People & Body*/
{"🚵🏿‍♂️","man mountain biking: dark skin tone" }, /*2348 : People & Body*/
{"🚵🏿‍♂","man mountain biking: dark skin tone" }, /*2349 : People & Body*/
{"🚵‍♀️","woman mountain biking" }, /*2350 : People & Body*/
{"🚵‍♀","woman mountain biking" }, /*2351 : People & Body*/
{"🚵🏻‍♀️","woman mountain biking: light skin tone" }, /*2352 : People & Body*/
{"🚵🏻‍♀","woman mountain biking: light skin tone" }, /*2353 : People & Body*/
{"🚵🏼‍♀️","woman mountain biking: medium-light skin tone" }, /*2354 : People & Body*/
{"🚵🏼‍♀","woman mountain biking: medium-light skin tone" }, /*2355 : People & Body*/
{"🚵🏽‍♀️","woman mountain biking: medium skin tone" }, /*2356 : People & Body*/
{"🚵🏽‍♀","woman mountain biking: medium skin tone" }, /*2357 : People & Body*/
{"🚵🏾‍♀️","woman mountain biking: medium-dark skin tone" }, /*2358 : People & Body*/
{"🚵🏾‍♀","woman mountain biking: medium-dark skin tone" }, /*2359 : People & Body*/
{"🚵🏿‍♀️","woman mountain biking: dark skin tone" }, /*2360 : People & Body*/
{"🚵🏿‍♀","woman mountain biking: dark skin tone" }, /*2361 : People & Body*/
{"🤸","person cartwheeling" }, /*2362 : People & Body*/
{"🤸🏻","person cartwheeling: light skin tone" }, /*2363 : People & Body*/
{"🤸🏼","person cartwheeling: medium-light skin tone" }, /*2364 : People & Body*/
{"🤸🏽","person cartwheeling: medium skin tone" }, /*2365 : People & Body*/
{"🤸🏾","person cartwheeling: medium-dark skin tone" }, /*2366 : People & Body*/
{"🤸🏿","person cartwheeling: dark skin tone" }, /*2367 : People & Body*/
{"🤸‍♂️","man cartwheeling" }, /*2368 : People & Body*/
{"🤸‍♂","man cartwheeling" }, /*2369 : People & Body*/
{"🤸🏻‍♂️","man cartwheeling: light skin tone" }, /*2370 : People & Body*/
{"🤸🏻‍♂","man cartwheeling: light skin tone" }, /*2371 : People & Body*/
{"🤸🏼‍♂️","man cartwheeling: medium-light skin tone" }, /*2372 : People & Body*/
{"🤸🏼‍♂","man cartwheeling: medium-light skin tone" }, /*2373 : People & Body*/
{"🤸🏽‍♂️","man cartwheeling: medium skin tone" }, /*2374 : People & Body*/
{"🤸🏽‍♂","man cartwheeling: medium skin tone" }, /*2375 : People & Body*/
{"🤸🏾‍♂️","man cartwheeling: medium-dark skin tone" }, /*2376 : People & Body*/
{"🤸🏾‍♂","man cartwheeling: medium-dark skin tone" }, /*2377 : People & Body*/
{"🤸🏿‍♂️","man cartwheeling: dark skin tone" }, /*2378 : People & Body*/
{"🤸🏿‍♂","man cartwheeling: dark skin tone" }, /*2379 : People & Body*/
{"🤸‍♀️","woman cartwheeling" }, /*2380 : People & Body*/
{"🤸‍♀","woman cartwheeling" }, /*2381 : People & Body*/
{"🤸🏻‍♀️","woman cartwheeling: light skin tone" }, /*2382 : People & Body*/
{"🤸🏻‍♀","woman cartwheeling: light skin tone" }, /*2383 : People & Body*/
{"🤸🏼‍♀️","woman cartwheeling: medium-light skin tone" }, /*2384 : People & Body*/
{"🤸🏼‍♀","woman cartwheeling: medium-light skin tone" }, /*2385 : People & Body*/
{"🤸🏽‍♀️","woman cartwheeling: medium skin tone" }, /*2386 : People & Body*/
{"🤸🏽‍♀","woman cartwheeling: medium skin tone" }, /*2387 : People & Body*/
{"🤸🏾‍♀️","woman cartwheeling: medium-dark skin tone" }, /*2388 : People & Body*/
{"🤸🏾‍♀","woman cartwheeling: medium-dark skin tone" }, /*2389 : People & Body*/
{"🤸🏿‍♀️","woman cartwheeling: dark skin tone" }, /*2390 : People & Body*/
{"🤸🏿‍♀","woman cartwheeling: dark skin tone" }, /*2391 : People & Body*/
{"🤼","people wrestling" }, /*2392 : People & Body*/
{"🤼‍♂️","men wrestling" }, /*2393 : People & Body*/
{"🤼‍♂","men wrestling" }, /*2394 : People & Body*/
{"🤼‍♀️","women wrestling" }, /*2395 : People & Body*/
{"🤼‍♀","women wrestling" }, /*2396 : People & Body*/
{"🤽","person playing water polo" }, /*2397 : People & Body*/
{"🤽🏻","person playing water polo: light skin tone" }, /*2398 : People & Body*/
{"🤽🏼","person playing water polo: medium-light skin tone" }, /*2399 : People & Body*/
{"🤽🏽","person playing water polo: medium skin tone" }, /*2400 : People & Body*/
{"🤽🏾","person playing water polo: medium-dark skin tone" }, /*2401 : People & Body*/
{"🤽🏿","person playing water polo: dark skin tone" }, /*2402 : People & Body*/
{"🤽‍♂️","man playing water polo" }, /*2403 : People & Body*/
{"🤽‍♂","man playing water polo" }, /*2404 : People & Body*/
{"🤽🏻‍♂️","man playing water polo: light skin tone" }, /*2405 : People & Body*/
{"🤽🏻‍♂","man playing water polo: light skin tone" }, /*2406 : People & Body*/
{"🤽🏼‍♂️","man playing water polo: medium-light skin tone" }, /*2407 : People & Body*/
{"🤽🏼‍♂","man playing water polo: medium-light skin tone" }, /*2408 : People & Body*/
{"🤽🏽‍♂️","man playing water polo: medium skin tone" }, /*2409 : People & Body*/
{"🤽🏽‍♂","man playing water polo: medium skin tone" }, /*2410 : People & Body*/
{"🤽🏾‍♂️","man playing water polo: medium-dark skin tone" }, /*2411 : People & Body*/
{"🤽🏾‍♂","man playing water polo: medium-dark skin tone" }, /*2412 : People & Body*/
{"🤽🏿‍♂️","man playing water polo: dark skin tone" }, /*2413 : People & Body*/
{"🤽🏿‍♂","man playing water polo: dark skin tone" }, /*2414 : People & Body*/
{"🤽‍♀️","woman playing water polo" }, /*2415 : People & Body*/
{"🤽‍♀","woman playing water polo" }, /*2416 : People & Body*/
{"🤽🏻‍♀️","woman playing water polo: light skin tone" }, /*2417 : People & Body*/
{"🤽🏻‍♀","woman playing water polo: light skin tone" }, /*2418 : People & Body*/
{"🤽🏼‍♀️","woman playing water polo: medium-light skin tone" }, /*2419 : People & Body*/
{"🤽🏼‍♀","woman playing water polo: medium-light skin tone" }, /*2420 : People & Body*/
{"🤽🏽‍♀️","woman playing water polo: medium skin tone" }, /*2421 : People & Body*/
{"🤽🏽‍♀","woman playing water polo: medium skin tone" }, /*2422 : People & Body*/
{"🤽🏾‍♀️","woman playing water polo: medium-dark skin tone" }, /*2423 : People & Body*/
{"🤽🏾‍♀","woman playing water polo: medium-dark skin tone" }, /*2424 : People & Body*/
{"🤽🏿‍♀️","woman playing water polo: dark skin tone" }, /*2425 : People & Body*/
{"🤽🏿‍♀","woman playing water polo: dark skin tone" }, /*2426 : People & Body*/
{"🤾","person playing handball" }, /*2427 : People & Body*/
{"🤾🏻","person playing handball: light skin tone" }, /*2428 : People & Body*/
{"🤾🏼","person playing handball: medium-light skin tone" }, /*2429 : People & Body*/
{"🤾🏽","person playing handball: medium skin tone" }, /*2430 : People & Body*/
{"🤾🏾","person playing handball: medium-dark skin tone" }, /*2431 : People & Body*/
{"🤾🏿","person playing handball: dark skin tone" }, /*2432 : People & Body*/
{"🤾‍♂️","man playing handball" }, /*2433 : People & Body*/
{"🤾‍♂","man playing handball" }, /*2434 : People & Body*/
{"🤾🏻‍♂️","man playing handball: light skin tone" }, /*2435 : People & Body*/
{"🤾🏻‍♂","man playing handball: light skin tone" }, /*2436 : People & Body*/
{"🤾🏼‍♂️","man playing handball: medium-light skin tone" }, /*2437 : People & Body*/
{"🤾🏼‍♂","man playing handball: medium-light skin tone" }, /*2438 : People & Body*/
{"🤾🏽‍♂️","man playing handball: medium skin tone" }, /*2439 : People & Body*/
{"🤾🏽‍♂","man playing handball: medium skin tone" }, /*2440 : People & Body*/
{"🤾🏾‍♂️","man playing handball: medium-dark skin tone" }, /*2441 : People & Body*/
{"🤾🏾‍♂","man playing handball: medium-dark skin tone" }, /*2442 : People & Body*/
{"🤾🏿‍♂️","man playing handball: dark skin tone" }, /*2443 : People & Body*/
{"🤾🏿‍♂","man playing handball: dark skin tone" }, /*2444 : People & Body*/
{"🤾‍♀️","woman playing handball" }, /*2445 : People & Body*/
{"🤾‍♀","woman playing handball" }, /*2446 : People & Body*/
{"🤾🏻‍♀️","woman playing handball: light skin tone" }, /*2447 : People & Body*/
{"🤾🏻‍♀","woman playing handball: light skin tone" }, /*2448 : People & Body*/
{"🤾🏼‍♀️","woman playing handball: medium-light skin tone" }, /*2449 : People & Body*/
{"🤾🏼‍♀","woman playing handball: medium-light skin tone" }, /*2450 : People & Body*/
{"🤾🏽‍♀️","woman playing handball: medium skin tone" }, /*2451 : People & Body*/
{"🤾🏽‍♀","woman playing handball: medium skin tone" }, /*2452 : People & Body*/
{"🤾🏾‍♀️","woman playing handball: medium-dark skin tone" }, /*2453 : People & Body*/
{"🤾🏾‍♀","woman playing handball: medium-dark skin tone" }, /*2454 : People & Body*/
{"🤾🏿‍♀️","woman playing handball: dark skin tone" }, /*2455 : People & Body*/
{"🤾🏿‍♀","woman playing handball: dark skin tone" }, /*2456 : People & Body*/
{"🤹","person juggling" }, /*2457 : People & Body*/
{"🤹🏻","person juggling: light skin tone" }, /*2458 : People & Body*/
{"🤹🏼","person juggling: medium-light skin tone" }, /*2459 : People & Body*/
{"🤹🏽","person juggling: medium skin tone" }, /*2460 : People & Body*/
{"🤹🏾","person juggling: medium-dark skin tone" }, /*2461 : People & Body*/
{"🤹🏿","person juggling: dark skin tone" }, /*2462 : People & Body*/
{"🤹‍♂️","man juggling" }, /*2463 : People & Body*/
{"🤹‍♂","man juggling" }, /*2464 : People & Body*/
{"🤹🏻‍♂️","man juggling: light skin tone" }, /*2465 : People & Body*/
{"🤹🏻‍♂","man juggling: light skin tone" }, /*2466 : People & Body*/
{"🤹🏼‍♂️","man juggling: medium-light skin tone" }, /*2467 : People & Body*/
{"🤹🏼‍♂","man juggling: medium-light skin tone" }, /*2468 : People & Body*/
{"🤹🏽‍♂️","man juggling: medium skin tone" }, /*2469 : People & Body*/
{"🤹🏽‍♂","man juggling: medium skin tone" }, /*2470 : People & Body*/
{"🤹🏾‍♂️","man juggling: medium-dark skin tone" }, /*2471 : People & Body*/
{"🤹🏾‍♂","man juggling: medium-dark skin tone" }, /*2472 : People & Body*/
{"🤹🏿‍♂️","man juggling: dark skin tone" }, /*2473 : People & Body*/
{"🤹🏿‍♂","man juggling: dark skin tone" }, /*2474 : People & Body*/
{"🤹‍♀️","woman juggling" }, /*2475 : People & Body*/
{"🤹‍♀","woman juggling" }, /*2476 : People & Body*/
{"🤹🏻‍♀️","woman juggling: light skin tone" }, /*2477 : People & Body*/
{"🤹🏻‍♀","woman juggling: light skin tone" }, /*2478 : People & Body*/
{"🤹🏼‍♀️","woman juggling: medium-light skin tone" }, /*2479 : People & Body*/
{"🤹🏼‍♀","woman juggling: medium-light skin tone" }, /*2480 : People & Body*/
{"🤹🏽‍♀️","woman juggling: medium skin tone" }, /*2481 : People & Body*/
{"🤹🏽‍♀","woman juggling: medium skin tone" }, /*2482 : People & Body*/
{"🤹🏾‍♀️","woman juggling: medium-dark skin tone" }, /*2483 : People & Body*/
{"🤹🏾‍♀","woman juggling: medium-dark skin tone" }, /*2484 : People & Body*/
{"🤹🏿‍♀️","woman juggling: dark skin tone" }, /*2485 : People & Body*/
{"🤹🏿‍♀","woman juggling: dark skin tone" }, /*2486 : People & Body*/
{"🧘","person in lotus position" }, /*2487 : People & Body*/
{"🧘🏻","person in lotus position: light skin tone" }, /*2488 : People & Body*/
{"🧘🏼","person in lotus position: medium-light skin tone" }, /*2489 : People & Body*/
{"🧘🏽","person in lotus position: medium skin tone" }, /*2490 : People & Body*/
{"🧘🏾","person in lotus position: medium-dark skin tone" }, /*2491 : People & Body*/
{"🧘🏿","person in lotus position: dark skin tone" }, /*2492 : People & Body*/
{"🧘‍♂️","man in lotus position" }, /*2493 : People & Body*/
{"🧘‍♂","man in lotus position" }, /*2494 : People & Body*/
{"🧘🏻‍♂️","man in lotus position: light skin tone" }, /*2495 : People & Body*/
{"🧘🏻‍♂","man in lotus position: light skin tone" }, /*2496 : People & Body*/
{"🧘🏼‍♂️","man in lotus position: medium-light skin tone" }, /*2497 : People & Body*/
{"🧘🏼‍♂","man in lotus position: medium-light skin tone" }, /*2498 : People & Body*/
{"🧘🏽‍♂️","man in lotus position: medium skin tone" }, /*2499 : People & Body*/
{"🧘🏽‍♂","man in lotus position: medium skin tone" }, /*2500 : People & Body*/
{"🧘🏾‍♂️","man in lotus position: medium-dark skin tone" }, /*2501 : People & Body*/
{"🧘🏾‍♂","man in lotus position: medium-dark skin tone" }, /*2502 : People & Body*/
{"🧘🏿‍♂️","man in lotus position: dark skin tone" }, /*2503 : People & Body*/
{"🧘🏿‍♂","man in lotus position: dark skin tone" }, /*2504 : People & Body*/
{"🧘‍♀️","woman in lotus position" }, /*2505 : People & Body*/
{"🧘‍♀","woman in lotus position" }, /*2506 : People & Body*/
{"🧘🏻‍♀️","woman in lotus position: light skin tone" }, /*2507 : People & Body*/
{"🧘🏻‍♀","woman in lotus position: light skin tone" }, /*2508 : People & Body*/
{"🧘🏼‍♀️","woman in lotus position: medium-light skin tone" }, /*2509 : People & Body*/
{"🧘🏼‍♀","woman in lotus position: medium-light skin tone" }, /*2510 : People & Body*/
{"🧘🏽‍♀️","woman in lotus position: medium skin tone" }, /*2511 : People & Body*/
{"🧘🏽‍♀","woman in lotus position: medium skin tone" }, /*2512 : People & Body*/
{"🧘🏾‍♀️","woman in lotus position: medium-dark skin tone" }, /*2513 : People & Body*/
{"🧘🏾‍♀","woman in lotus position: medium-dark skin tone" }, /*2514 : People & Body*/
{"🧘🏿‍♀️","woman in lotus position: dark skin tone" }, /*2515 : People & Body*/
{"🧘🏿‍♀","woman in lotus position: dark skin tone" }, /*2516 : People & Body*/
{"🛀","person taking bath" }, /*2517 : People & Body*/
{"🛀🏻","person taking bath: light skin tone" }, /*2518 : People & Body*/
{"🛀🏼","person taking bath: medium-light skin tone" }, /*2519 : People & Body*/
{"🛀🏽","person taking bath: medium skin tone" }, /*2520 : People & Body*/
{"🛀🏾","person taking bath: medium-dark skin tone" }, /*2521 : People & Body*/
{"🛀🏿","person taking bath: dark skin tone" }, /*2522 : People & Body*/
{"🛌","person in bed" }, /*2523 : People & Body*/
{"🛌🏻","person in bed: light skin tone" }, /*2524 : People & Body*/
{"🛌🏼","person in bed: medium-light skin tone" }, /*2525 : People & Body*/
{"🛌🏽","person in bed: medium skin tone" }, /*2526 : People & Body*/
{"🛌🏾","person in bed: medium-dark skin tone" }, /*2527 : People & Body*/
{"🛌🏿","person in bed: dark skin tone" }, /*2528 : People & Body*/
{"🧑‍🤝‍🧑","people holding hands" }, /*2529 : People & Body*/
{"🧑🏻‍🤝‍🧑🏻","people holding hands: light skin tone" }, /*2530 : People & Body*/
{"🧑🏻‍🤝‍🧑🏼","people holding hands: light skin tone, medium-light skin tone" }, /*2531 : People & Body*/
{"🧑🏻‍🤝‍🧑🏽","people holding hands: light skin tone, medium skin tone" }, /*2532 : People & Body*/
{"🧑🏻‍🤝‍🧑🏾","people holding hands: light skin tone, medium-dark skin tone" }, /*2533 : People & Body*/
{"🧑🏻‍🤝‍🧑🏿","people holding hands: light skin tone, dark skin tone" }, /*2534 : People & Body*/
{"🧑🏼‍🤝‍🧑🏻","people holding hands: medium-light skin tone, light skin tone" }, /*2535 : People & Body*/
{"🧑🏼‍🤝‍🧑🏼","people holding hands: medium-light skin tone" }, /*2536 : People & Body*/
{"🧑🏼‍🤝‍🧑🏽","people holding hands: medium-light skin tone, medium skin tone" }, /*2537 : People & Body*/
{"🧑🏼‍🤝‍🧑🏾","people holding hands: medium-light skin tone, medium-dark skin tone" }, /*2538 : People & Body*/
{"🧑🏼‍🤝‍🧑🏿","people holding hands: medium-light skin tone, dark skin tone" }, /*2539 : People & Body*/
{"🧑🏽‍🤝‍🧑🏻","people holding hands: medium skin tone, light skin tone" }, /*2540 : People & Body*/
{"🧑🏽‍🤝‍🧑🏼","people holding hands: medium skin tone, medium-light skin tone" }, /*2541 : People & Body*/
{"🧑🏽‍🤝‍🧑🏽","people holding hands: medium skin tone" }, /*2542 : People & Body*/
{"🧑🏽‍🤝‍🧑🏾","people holding hands: medium skin tone, medium-dark skin tone" }, /*2543 : People & Body*/
{"🧑🏽‍🤝‍🧑🏿","people holding hands: medium skin tone, dark skin tone" }, /*2544 : People & Body*/
{"🧑🏾‍🤝‍🧑🏻","people holding hands: medium-dark skin tone, light skin tone" }, /*2545 : People & Body*/
{"🧑🏾‍🤝‍🧑🏼","people holding hands: medium-dark skin tone, medium-light skin tone" }, /*2546 : People & Body*/
{"🧑🏾‍🤝‍🧑🏽","people holding hands: medium-dark skin tone, medium skin tone" }, /*2547 : People & Body*/
{"🧑🏾‍🤝‍🧑🏾","people holding hands: medium-dark skin tone" }, /*2548 : People & Body*/
{"🧑🏾‍🤝‍🧑🏿","people holding hands: medium-dark skin tone, dark skin tone" }, /*2549 : People & Body*/
{"🧑🏿‍🤝‍🧑🏻","people holding hands: dark skin tone, light skin tone" }, /*2550 : People & Body*/
{"🧑🏿‍🤝‍🧑🏼","people holding hands: dark skin tone, medium-light skin tone" }, /*2551 : People & Body*/
{"🧑🏿‍🤝‍🧑🏽","people holding hands: dark skin tone, medium skin tone" }, /*2552 : People & Body*/
{"🧑🏿‍🤝‍🧑🏾","people holding hands: dark skin tone, medium-dark skin tone" }, /*2553 : People & Body*/
{"🧑🏿‍🤝‍🧑🏿","people holding hands: dark skin tone" }, /*2554 : People & Body*/
{"👭","women holding hands" }, /*2555 : People & Body*/
{"👭🏻","women holding hands: light skin tone" }, /*2556 : People & Body*/
{"👩🏻‍🤝‍👩🏼","women holding hands: light skin tone, medium-light skin tone" }, /*2557 : People & Body*/
{"👩🏻‍🤝‍👩🏽","women holding hands: light skin tone, medium skin tone" }, /*2558 : People & Body*/
{"👩🏻‍🤝‍👩🏾","women holding hands: light skin tone, medium-dark skin tone" }, /*2559 : People & Body*/
{"👩🏻‍🤝‍👩🏿","women holding hands: light skin tone, dark skin tone" }, /*2560 : People & Body*/
{"👩🏼‍🤝‍👩🏻","women holding hands: medium-light skin tone, light skin tone" }, /*2561 : People & Body*/
{"👭🏼","women holding hands: medium-light skin tone" }, /*2562 : People & Body*/
{"👩🏼‍🤝‍👩🏽","women holding hands: medium-light skin tone, medium skin tone" }, /*2563 : People & Body*/
{"👩🏼‍🤝‍👩🏾","women holding hands: medium-light skin tone, medium-dark skin tone" }, /*2564 : People & Body*/
{"👩🏼‍🤝‍👩🏿","women holding hands: medium-light skin tone, dark skin tone" }, /*2565 : People & Body*/
{"👩🏽‍🤝‍👩🏻","women holding hands: medium skin tone, light skin tone" }, /*2566 : People & Body*/
{"👩🏽‍🤝‍👩🏼","women holding hands: medium skin tone, medium-light skin tone" }, /*2567 : People & Body*/
{"👭🏽","women holding hands: medium skin tone" }, /*2568 : People & Body*/
{"👩🏽‍🤝‍👩🏾","women holding hands: medium skin tone, medium-dark skin tone" }, /*2569 : People & Body*/
{"👩🏽‍🤝‍👩🏿","women holding hands: medium skin tone, dark skin tone" }, /*2570 : People & Body*/
{"👩🏾‍🤝‍👩🏻","women holding hands: medium-dark skin tone, light skin tone" }, /*2571 : People & Body*/
{"👩🏾‍🤝‍👩🏼","women holding hands: medium-dark skin tone, medium-light skin tone" }, /*2572 : People & Body*/
{"👩🏾‍🤝‍👩🏽","women holding hands: medium-dark skin tone, medium skin tone" }, /*2573 : People & Body*/
{"👭🏾","women holding hands: medium-dark skin tone" }, /*2574 : People & Body*/
{"👩🏾‍🤝‍👩🏿","women holding hands: medium-dark skin tone, dark skin tone" }, /*2575 : People & Body*/
{"👩🏿‍🤝‍👩🏻","women holding hands: dark skin tone, light skin tone" }, /*2576 : People & Body*/
{"👩🏿‍🤝‍👩🏼","women holding hands: dark skin tone, medium-light skin tone" }, /*2577 : People & Body*/
{"👩🏿‍🤝‍👩🏽","women holding hands: dark skin tone, medium skin tone" }, /*2578 : People & Body*/
{"👩🏿‍🤝‍👩🏾","women holding hands: dark skin tone, medium-dark skin tone" }, /*2579 : People & Body*/
{"👭🏿","women holding hands: dark skin tone" }, /*2580 : People & Body*/
{"👫","woman and man holding hands" }, /*2581 : People & Body*/
{"👫🏻","woman and man holding hands: light skin tone" }, /*2582 : People & Body*/
{"👩🏻‍🤝‍👨🏼","woman and man holding hands: light skin tone, medium-light skin tone" }, /*2583 : People & Body*/
{"👩🏻‍🤝‍👨🏽","woman and man holding hands: light skin tone, medium skin tone" }, /*2584 : People & Body*/
{"👩🏻‍🤝‍👨🏾","woman and man holding hands: light skin tone, medium-dark skin tone" }, /*2585 : People & Body*/
{"👩🏻‍🤝‍👨🏿","woman and man holding hands: light skin tone, dark skin tone" }, /*2586 : People & Body*/
{"👩🏼‍🤝‍👨🏻","woman and man holding hands: medium-light skin tone, light skin tone" }, /*2587 : People & Body*/
{"👫🏼","woman and man holding hands: medium-light skin tone" }, /*2588 : People & Body*/
{"👩🏼‍🤝‍👨🏽","woman and man holding hands: medium-light skin tone, medium skin tone" }, /*2589 : People & Body*/
{"👩🏼‍🤝‍👨🏾","woman and man holding hands: medium-light skin tone, medium-dark skin tone" }, /*2590 : People & Body*/
{"👩🏼‍🤝‍👨🏿","woman and man holding hands: medium-light skin tone, dark skin tone" }, /*2591 : People & Body*/
{"👩🏽‍🤝‍👨🏻","woman and man holding hands: medium skin tone, light skin tone" }, /*2592 : People & Body*/
{"👩🏽‍🤝‍👨🏼","woman and man holding hands: medium skin tone, medium-light skin tone" }, /*2593 : People & Body*/
{"👫🏽","woman and man holding hands: medium skin tone" }, /*2594 : People & Body*/
{"👩🏽‍🤝‍👨🏾","woman and man holding hands: medium skin tone, medium-dark skin tone" }, /*2595 : People & Body*/
{"👩🏽‍🤝‍👨🏿","woman and man holding hands: medium skin tone, dark skin tone" }, /*2596 : People & Body*/
{"👩🏾‍🤝‍👨🏻","woman and man holding hands: medium-dark skin tone, light skin tone" }, /*2597 : People & Body*/
{"👩🏾‍🤝‍👨🏼","woman and man holding hands: medium-dark skin tone, medium-light skin tone" }, /*2598 : People & Body*/
{"👩🏾‍🤝‍👨🏽","woman and man holding hands: medium-dark skin tone, medium skin tone" }, /*2599 : People & Body*/
{"👫🏾","woman and man holding hands: medium-dark skin tone" }, /*2600 : People & Body*/
{"👩🏾‍🤝‍👨🏿","woman and man holding hands: medium-dark skin tone, dark skin tone" }, /*2601 : People & Body*/
{"👩🏿‍🤝‍👨🏻","woman and man holding hands: dark skin tone, light skin tone" }, /*2602 : People & Body*/
{"👩🏿‍🤝‍👨🏼","woman and man holding hands: dark skin tone, medium-light skin tone" }, /*2603 : People & Body*/
{"👩🏿‍🤝‍👨🏽","woman and man holding hands: dark skin tone, medium skin tone" }, /*2604 : People & Body*/
{"👩🏿‍🤝‍👨🏾","woman and man holding hands: dark skin tone, medium-dark skin tone" }, /*2605 : People & Body*/
{"👫🏿","woman and man holding hands: dark skin tone" }, /*2606 : People & Body*/
{"👬","men holding hands" }, /*2607 : People & Body*/
{"👬🏻","men holding hands: light skin tone" }, /*2608 : People & Body*/
{"👨🏻‍🤝‍👨🏼","men holding hands: light skin tone, medium-light skin tone" }, /*2609 : People & Body*/
{"👨🏻‍🤝‍👨🏽","men holding hands: light skin tone, medium skin tone" }, /*2610 : People & Body*/
{"👨🏻‍🤝‍👨🏾","men holding hands: light skin tone, medium-dark skin tone" }, /*2611 : People & Body*/
{"👨🏻‍🤝‍👨🏿","men holding hands: light skin tone, dark skin tone" }, /*2612 : People & Body*/
{"👨🏼‍🤝‍👨🏻","men holding hands: medium-light skin tone, light skin tone" }, /*2613 : People & Body*/
{"👬🏼","men holding hands: medium-light skin tone" }, /*2614 : People & Body*/
{"👨🏼‍🤝‍👨🏽","men holding hands: medium-light skin tone, medium skin tone" }, /*2615 : People & Body*/
{"👨🏼‍🤝‍👨🏾","men holding hands: medium-light skin tone, medium-dark skin tone" }, /*2616 : People & Body*/
{"👨🏼‍🤝‍👨🏿","men holding hands: medium-light skin tone, dark skin tone" }, /*2617 : People & Body*/
{"👨🏽‍🤝‍👨🏻","men holding hands: medium skin tone, light skin tone" }, /*2618 : People & Body*/
{"👨🏽‍🤝‍👨🏼","men holding hands: medium skin tone, medium-light skin tone" }, /*2619 : People & Body*/
{"👬🏽","men holding hands: medium skin tone" }, /*2620 : People & Body*/
{"👨🏽‍🤝‍👨🏾","men holding hands: medium skin tone, medium-dark skin tone" }, /*2621 : People & Body*/
{"👨🏽‍🤝‍👨🏿","men holding hands: medium skin tone, dark skin tone" }, /*2622 : People & Body*/
{"👨🏾‍🤝‍👨🏻","men holding hands: medium-dark skin tone, light skin tone" }, /*2623 : People & Body*/
{"👨🏾‍🤝‍👨🏼","men holding hands: medium-dark skin tone, medium-light skin tone" }, /*2624 : People & Body*/
{"👨🏾‍🤝‍👨🏽","men holding hands: medium-dark skin tone, medium skin tone" }, /*2625 : People & Body*/
{"👬🏾","men holding hands: medium-dark skin tone" }, /*2626 : People & Body*/
{"👨🏾‍🤝‍👨🏿","men holding hands: medium-dark skin tone, dark skin tone" }, /*2627 : People & Body*/
{"👨🏿‍🤝‍👨🏻","men holding hands: dark skin tone, light skin tone" }, /*2628 : People & Body*/
{"👨🏿‍🤝‍👨🏼","men holding hands: dark skin tone, medium-light skin tone" }, /*2629 : People & Body*/
{"👨🏿‍🤝‍👨🏽","men holding hands: dark skin tone, medium skin tone" }, /*2630 : People & Body*/
{"👨🏿‍🤝‍👨🏾","men holding hands: dark skin tone, medium-dark skin tone" }, /*2631 : People & Body*/
{"👬🏿","men holding hands: dark skin tone" }, /*2632 : People & Body*/
{"💏","kiss" }, /*2633 : People & Body*/
{"💏🏻","kiss: light skin tone" }, /*2634 : People & Body*/
{"💏🏼","kiss: medium-light skin tone" }, /*2635 : People & Body*/
{"💏🏽","kiss: medium skin tone" }, /*2636 : People & Body*/
{"💏🏾","kiss: medium-dark skin tone" }, /*2637 : People & Body*/
{"💏🏿","kiss: dark skin tone" }, /*2638 : People & Body*/
{"🧑🏻‍❤️‍💋‍🧑🏼","kiss: person, person, light skin tone, medium-light skin tone" }, /*2639 : People & Body*/
{"🧑🏻‍❤‍💋‍🧑🏼","kiss: person, person, light skin tone, medium-light skin tone" }, /*2640 : People & Body*/
{"🧑🏻‍❤️‍💋‍🧑🏽","kiss: person, person, light skin tone, medium skin tone" }, /*2641 : People & Body*/
{"🧑🏻‍❤‍💋‍🧑🏽","kiss: person, person, light skin tone, medium skin tone" }, /*2642 : People & Body*/
{"🧑🏻‍❤️‍💋‍🧑🏾","kiss: person, person, light skin tone, medium-dark skin tone" }, /*2643 : People & Body*/
{"🧑🏻‍❤‍💋‍🧑🏾","kiss: person, person, light skin tone, medium-dark skin tone" }, /*2644 : People & Body*/
{"🧑🏻‍❤️‍💋‍🧑🏿","kiss: person, person, light skin tone, dark skin tone" }, /*2645 : People & Body*/
{"🧑🏻‍❤‍💋‍🧑🏿","kiss: person, person, light skin tone, dark skin tone" }, /*2646 : People & Body*/
{"🧑🏼‍❤️‍💋‍🧑🏻","kiss: person, person, medium-light skin tone, light skin tone" }, /*2647 : People & Body*/
{"🧑🏼‍❤‍💋‍🧑🏻","kiss: person, person, medium-light skin tone, light skin tone" }, /*2648 : People & Body*/
{"🧑🏼‍❤️‍💋‍🧑🏽","kiss: person, person, medium-light skin tone, medium skin tone" }, /*2649 : People & Body*/
{"🧑🏼‍❤‍💋‍🧑🏽","kiss: person, person, medium-light skin tone, medium skin tone" }, /*2650 : People & Body*/
{"🧑🏼‍❤️‍💋‍🧑🏾","kiss: person, person, medium-light skin tone, medium-dark skin tone" }, /*2651 : People & Body*/
{"🧑🏼‍❤‍💋‍🧑🏾","kiss: person, person, medium-light skin tone, medium-dark skin tone" }, /*2652 : People & Body*/
{"🧑🏼‍❤️‍💋‍🧑🏿","kiss: person, person, medium-light skin tone, dark skin tone" }, /*2653 : People & Body*/
{"🧑🏼‍❤‍💋‍🧑🏿","kiss: person, person, medium-light skin tone, dark skin tone" }, /*2654 : People & Body*/
{"🧑🏽‍❤️‍💋‍🧑🏻","kiss: person, person, medium skin tone, light skin tone" }, /*2655 : People & Body*/
{"🧑🏽‍❤‍💋‍🧑🏻","kiss: person, person, medium skin tone, light skin tone" }, /*2656 : People & Body*/
{"🧑🏽‍❤️‍💋‍🧑🏼","kiss: person, person, medium skin tone, medium-light skin tone" }, /*2657 : People & Body*/
{"🧑🏽‍❤‍💋‍🧑🏼","kiss: person, person, medium skin tone, medium-light skin tone" }, /*2658 : People & Body*/
{"🧑🏽‍❤️‍💋‍🧑🏾","kiss: person, person, medium skin tone, medium-dark skin tone" }, /*2659 : People & Body*/
{"🧑🏽‍❤‍💋‍🧑🏾","kiss: person, person, medium skin tone, medium-dark skin tone" }, /*2660 : People & Body*/
{"🧑🏽‍❤️‍💋‍🧑🏿","kiss: person, person, medium skin tone, dark skin tone" }, /*2661 : People & Body*/
{"🧑🏽‍❤‍💋‍🧑🏿","kiss: person, person, medium skin tone, dark skin tone" }, /*2662 : People & Body*/
{"🧑🏾‍❤️‍💋‍🧑🏻","kiss: person, person, medium-dark skin tone, light skin tone" }, /*2663 : People & Body*/
{"🧑🏾‍❤‍💋‍🧑🏻","kiss: person, person, medium-dark skin tone, light skin tone" }, /*2664 : People & Body*/
{"🧑🏾‍❤️‍💋‍🧑🏼","kiss: person, person, medium-dark skin tone, medium-light skin tone" }, /*2665 : People & Body*/
{"🧑🏾‍❤‍💋‍🧑🏼","kiss: person, person, medium-dark skin tone, medium-light skin tone" }, /*2666 : People & Body*/
{"🧑🏾‍❤️‍💋‍🧑🏽","kiss: person, person, medium-dark skin tone, medium skin tone" }, /*2667 : People & Body*/
{"🧑🏾‍❤‍💋‍🧑🏽","kiss: person, person, medium-dark skin tone, medium skin tone" }, /*2668 : People & Body*/
{"🧑🏾‍❤️‍💋‍🧑🏿","kiss: person, person, medium-dark skin tone, dark skin tone" }, /*2669 : People & Body*/
{"🧑🏾‍❤‍💋‍🧑🏿","kiss: person, person, medium-dark skin tone, dark skin tone" }, /*2670 : People & Body*/
{"🧑🏿‍❤️‍💋‍🧑🏻","kiss: person, person, dark skin tone, light skin tone" }, /*2671 : People & Body*/
{"🧑🏿‍❤‍💋‍🧑🏻","kiss: person, person, dark skin tone, light skin tone" }, /*2672 : People & Body*/
{"🧑🏿‍❤️‍💋‍🧑🏼","kiss: person, person, dark skin tone, medium-light skin tone" }, /*2673 : People & Body*/
{"🧑🏿‍❤‍💋‍🧑🏼","kiss: person, person, dark skin tone, medium-light skin tone" }, /*2674 : People & Body*/
{"🧑🏿‍❤️‍💋‍🧑🏽","kiss: person, person, dark skin tone, medium skin tone" }, /*2675 : People & Body*/
{"🧑🏿‍❤‍💋‍🧑🏽","kiss: person, person, dark skin tone, medium skin tone" }, /*2676 : People & Body*/
{"🧑🏿‍❤️‍💋‍🧑🏾","kiss: person, person, dark skin tone, medium-dark skin tone" }, /*2677 : People & Body*/
{"🧑🏿‍❤‍💋‍🧑🏾","kiss: person, person, dark skin tone, medium-dark skin tone" }, /*2678 : People & Body*/
{"👩‍❤️‍💋‍👨","kiss: woman, man" }, /*2679 : People & Body*/
{"👩‍❤‍💋‍👨","kiss: woman, man" }, /*2680 : People & Body*/
{"👩🏻‍❤️‍💋‍👨🏻","kiss: woman, man, light skin tone" }, /*2681 : People & Body*/
{"👩🏻‍❤‍💋‍👨🏻","kiss: woman, man, light skin tone" }, /*2682 : People & Body*/
{"👩🏻‍❤️‍💋‍👨🏼","kiss: woman, man, light skin tone, medium-light skin tone" }, /*2683 : People & Body*/
{"👩🏻‍❤‍💋‍👨🏼","kiss: woman, man, light skin tone, medium-light skin tone" }, /*2684 : People & Body*/
{"👩🏻‍❤️‍💋‍👨🏽","kiss: woman, man, light skin tone, medium skin tone" }, /*2685 : People & Body*/
{"👩🏻‍❤‍💋‍👨🏽","kiss: woman, man, light skin tone, medium skin tone" }, /*2686 : People & Body*/
{"👩🏻‍❤️‍💋‍👨🏾","kiss: woman, man, light skin tone, medium-dark skin tone" }, /*2687 : People & Body*/
{"👩🏻‍❤‍💋‍👨🏾","kiss: woman, man, light skin tone, medium-dark skin tone" }, /*2688 : People & Body*/
{"👩🏻‍❤️‍💋‍👨🏿","kiss: woman, man, light skin tone, dark skin tone" }, /*2689 : People & Body*/
{"👩🏻‍❤‍💋‍👨🏿","kiss: woman, man, light skin tone, dark skin tone" }, /*2690 : People & Body*/
{"👩🏼‍❤️‍💋‍👨🏻","kiss: woman, man, medium-light skin tone, light skin tone" }, /*2691 : People & Body*/
{"👩🏼‍❤‍💋‍👨🏻","kiss: woman, man, medium-light skin tone, light skin tone" }, /*2692 : People & Body*/
{"👩🏼‍❤️‍💋‍👨🏼","kiss: woman, man, medium-light skin tone" }, /*2693 : People & Body*/
{"👩🏼‍❤‍💋‍👨🏼","kiss: woman, man, medium-light skin tone" }, /*2694 : People & Body*/
{"👩🏼‍❤️‍💋‍👨🏽","kiss: woman, man, medium-light skin tone, medium skin tone" }, /*2695 : People & Body*/
{"👩🏼‍❤‍💋‍👨🏽","kiss: woman, man, medium-light skin tone, medium skin tone" }, /*2696 : People & Body*/
{"👩🏼‍❤️‍💋‍👨🏾","kiss: woman, man, medium-light skin tone, medium-dark skin tone" }, /*2697 : People & Body*/
{"👩🏼‍❤‍💋‍👨🏾","kiss: woman, man, medium-light skin tone, medium-dark skin tone" }, /*2698 : People & Body*/
{"👩🏼‍❤️‍💋‍👨🏿","kiss: woman, man, medium-light skin tone, dark skin tone" }, /*2699 : People & Body*/
{"👩🏼‍❤‍💋‍👨🏿","kiss: woman, man, medium-light skin tone, dark skin tone" }, /*2700 : People & Body*/
{"👩🏽‍❤️‍💋‍👨🏻","kiss: woman, man, medium skin tone, light skin tone" }, /*2701 : People & Body*/
{"👩🏽‍❤‍💋‍👨🏻","kiss: woman, man, medium skin tone, light skin tone" }, /*2702 : People & Body*/
{"👩🏽‍❤️‍💋‍👨🏼","kiss: woman, man, medium skin tone, medium-light skin tone" }, /*2703 : People & Body*/
{"👩🏽‍❤‍💋‍👨🏼","kiss: woman, man, medium skin tone, medium-light skin tone" }, /*2704 : People & Body*/
{"👩🏽‍❤️‍💋‍👨🏽","kiss: woman, man, medium skin tone" }, /*2705 : People & Body*/
{"👩🏽‍❤‍💋‍👨🏽","kiss: woman, man, medium skin tone" }, /*2706 : People & Body*/
{"👩🏽‍❤️‍💋‍👨🏾","kiss: woman, man, medium skin tone, medium-dark skin tone" }, /*2707 : People & Body*/
{"👩🏽‍❤‍💋‍👨🏾","kiss: woman, man, medium skin tone, medium-dark skin tone" }, /*2708 : People & Body*/
{"👩🏽‍❤️‍💋‍👨🏿","kiss: woman, man, medium skin tone, dark skin tone" }, /*2709 : People & Body*/
{"👩🏽‍❤‍💋‍👨🏿","kiss: woman, man, medium skin tone, dark skin tone" }, /*2710 : People & Body*/
{"👩🏾‍❤️‍💋‍👨🏻","kiss: woman, man, medium-dark skin tone, light skin tone" }, /*2711 : People & Body*/
{"👩🏾‍❤‍💋‍👨🏻","kiss: woman, man, medium-dark skin tone, light skin tone" }, /*2712 : People & Body*/
{"👩🏾‍❤️‍💋‍👨🏼","kiss: woman, man, medium-dark skin tone, medium-light skin tone" }, /*2713 : People & Body*/
{"👩🏾‍❤‍💋‍👨🏼","kiss: woman, man, medium-dark skin tone, medium-light skin tone" }, /*2714 : People & Body*/
{"👩🏾‍❤️‍💋‍👨🏽","kiss: woman, man, medium-dark skin tone, medium skin tone" }, /*2715 : People & Body*/
{"👩🏾‍❤‍💋‍👨🏽","kiss: woman, man, medium-dark skin tone, medium skin tone" }, /*2716 : People & Body*/
{"👩🏾‍❤️‍💋‍👨🏾","kiss: woman, man, medium-dark skin tone" }, /*2717 : People & Body*/
{"👩🏾‍❤‍💋‍👨🏾","kiss: woman, man, medium-dark skin tone" }, /*2718 : People & Body*/
{"👩🏾‍❤️‍💋‍👨🏿","kiss: woman, man, medium-dark skin tone, dark skin tone" }, /*2719 : People & Body*/
{"👩🏾‍❤‍💋‍👨🏿","kiss: woman, man, medium-dark skin tone, dark skin tone" }, /*2720 : People & Body*/
{"👩🏿‍❤️‍💋‍👨🏻","kiss: woman, man, dark skin tone, light skin tone" }, /*2721 : People & Body*/
{"👩🏿‍❤‍💋‍👨🏻","kiss: woman, man, dark skin tone, light skin tone" }, /*2722 : People & Body*/
{"👩🏿‍❤️‍💋‍👨🏼","kiss: woman, man, dark skin tone, medium-light skin tone" }, /*2723 : People & Body*/
{"👩🏿‍❤‍💋‍👨🏼","kiss: woman, man, dark skin tone, medium-light skin tone" }, /*2724 : People & Body*/
{"👩🏿‍❤️‍💋‍👨🏽","kiss: woman, man, dark skin tone, medium skin tone" }, /*2725 : People & Body*/
{"👩🏿‍❤‍💋‍👨🏽","kiss: woman, man, dark skin tone, medium skin tone" }, /*2726 : People & Body*/
{"👩🏿‍❤️‍💋‍👨🏾","kiss: woman, man, dark skin tone, medium-dark skin tone" }, /*2727 : People & Body*/
{"👩🏿‍❤‍💋‍👨🏾","kiss: woman, man, dark skin tone, medium-dark skin tone" }, /*2728 : People & Body*/
{"👩🏿‍❤️‍💋‍👨🏿","kiss: woman, man, dark skin tone" }, /*2729 : People & Body*/
{"👩🏿‍❤‍💋‍👨🏿","kiss: woman, man, dark skin tone" }, /*2730 : People & Body*/
{"👨‍❤️‍💋‍👨","kiss: man, man" }, /*2731 : People & Body*/
{"👨‍❤‍💋‍👨","kiss: man, man" }, /*2732 : People & Body*/
{"👨🏻‍❤️‍💋‍👨🏻","kiss: man, man, light skin tone" }, /*2733 : People & Body*/
{"👨🏻‍❤‍💋‍👨🏻","kiss: man, man, light skin tone" }, /*2734 : People & Body*/
{"👨🏻‍❤️‍💋‍👨🏼","kiss: man, man, light skin tone, medium-light skin tone" }, /*2735 : People & Body*/
{"👨🏻‍❤‍💋‍👨🏼","kiss: man, man, light skin tone, medium-light skin tone" }, /*2736 : People & Body*/
{"👨🏻‍❤️‍💋‍👨🏽","kiss: man, man, light skin tone, medium skin tone" }, /*2737 : People & Body*/
{"👨🏻‍❤‍💋‍👨🏽","kiss: man, man, light skin tone, medium skin tone" }, /*2738 : People & Body*/
{"👨🏻‍❤️‍💋‍👨🏾","kiss: man, man, light skin tone, medium-dark skin tone" }, /*2739 : People & Body*/
{"👨🏻‍❤‍💋‍👨🏾","kiss: man, man, light skin tone, medium-dark skin tone" }, /*2740 : People & Body*/
{"👨🏻‍❤️‍💋‍👨🏿","kiss: man, man, light skin tone, dark skin tone" }, /*2741 : People & Body*/
{"👨🏻‍❤‍💋‍👨🏿","kiss: man, man, light skin tone, dark skin tone" }, /*2742 : People & Body*/
{"👨🏼‍❤️‍💋‍👨🏻","kiss: man, man, medium-light skin tone, light skin tone" }, /*2743 : People & Body*/
{"👨🏼‍❤‍💋‍👨🏻","kiss: man, man, medium-light skin tone, light skin tone" }, /*2744 : People & Body*/
{"👨🏼‍❤️‍💋‍👨🏼","kiss: man, man, medium-light skin tone" }, /*2745 : People & Body*/
{"👨🏼‍❤‍💋‍👨🏼","kiss: man, man, medium-light skin tone" }, /*2746 : People & Body*/
{"👨🏼‍❤️‍💋‍👨🏽","kiss: man, man, medium-light skin tone, medium skin tone" }, /*2747 : People & Body*/
{"👨🏼‍❤‍💋‍👨🏽","kiss: man, man, medium-light skin tone, medium skin tone" }, /*2748 : People & Body*/
{"👨🏼‍❤️‍💋‍👨🏾","kiss: man, man, medium-light skin tone, medium-dark skin tone" }, /*2749 : People & Body*/
{"👨🏼‍❤‍💋‍👨🏾","kiss: man, man, medium-light skin tone, medium-dark skin tone" }, /*2750 : People & Body*/
{"👨🏼‍❤️‍💋‍👨🏿","kiss: man, man, medium-light skin tone, dark skin tone" }, /*2751 : People & Body*/
{"👨🏼‍❤‍💋‍👨🏿","kiss: man, man, medium-light skin tone, dark skin tone" }, /*2752 : People & Body*/
{"👨🏽‍❤️‍💋‍👨🏻","kiss: man, man, medium skin tone, light skin tone" }, /*2753 : People & Body*/
{"👨🏽‍❤‍💋‍👨🏻","kiss: man, man, medium skin tone, light skin tone" }, /*2754 : People & Body*/
{"👨🏽‍❤️‍💋‍👨🏼","kiss: man, man, medium skin tone, medium-light skin tone" }, /*2755 : People & Body*/
{"👨🏽‍❤‍💋‍👨🏼","kiss: man, man, medium skin tone, medium-light skin tone" }, /*2756 : People & Body*/
{"👨🏽‍❤️‍💋‍👨🏽","kiss: man, man, medium skin tone" }, /*2757 : People & Body*/
{"👨🏽‍❤‍💋‍👨🏽","kiss: man, man, medium skin tone" }, /*2758 : People & Body*/
{"👨🏽‍❤️‍💋‍👨🏾","kiss: man, man, medium skin tone, medium-dark skin tone" }, /*2759 : People & Body*/
{"👨🏽‍❤‍💋‍👨🏾","kiss: man, man, medium skin tone, medium-dark skin tone" }, /*2760 : People & Body*/
{"👨🏽‍❤️‍💋‍👨🏿","kiss: man, man, medium skin tone, dark skin tone" }, /*2761 : People & Body*/
{"👨🏽‍❤‍💋‍👨🏿","kiss: man, man, medium skin tone, dark skin tone" }, /*2762 : People & Body*/
{"👨🏾‍❤️‍💋‍👨🏻","kiss: man, man, medium-dark skin tone, light skin tone" }, /*2763 : People & Body*/
{"👨🏾‍❤‍💋‍👨🏻","kiss: man, man, medium-dark skin tone, light skin tone" }, /*2764 : People & Body*/
{"👨🏾‍❤️‍💋‍👨🏼","kiss: man, man, medium-dark skin tone, medium-light skin tone" }, /*2765 : People & Body*/
{"👨🏾‍❤‍💋‍👨🏼","kiss: man, man, medium-dark skin tone, medium-light skin tone" }, /*2766 : People & Body*/
{"👨🏾‍❤️‍💋‍👨🏽","kiss: man, man, medium-dark skin tone, medium skin tone" }, /*2767 : People & Body*/
{"👨🏾‍❤‍💋‍👨🏽","kiss: man, man, medium-dark skin tone, medium skin tone" }, /*2768 : People & Body*/
{"👨🏾‍❤️‍💋‍👨🏾","kiss: man, man, medium-dark skin tone" }, /*2769 : People & Body*/
{"👨🏾‍❤‍💋‍👨🏾","kiss: man, man, medium-dark skin tone" }, /*2770 : People & Body*/
{"👨🏾‍❤️‍💋‍👨🏿","kiss: man, man, medium-dark skin tone, dark skin tone" }, /*2771 : People & Body*/
{"👨🏾‍❤‍💋‍👨🏿","kiss: man, man, medium-dark skin tone, dark skin tone" }, /*2772 : People & Body*/
{"👨🏿‍❤️‍💋‍👨🏻","kiss: man, man, dark skin tone, light skin tone" }, /*2773 : People & Body*/
{"👨🏿‍❤‍💋‍👨🏻","kiss: man, man, dark skin tone, light skin tone" }, /*2774 : People & Body*/
{"👨🏿‍❤️‍💋‍👨🏼","kiss: man, man, dark skin tone, medium-light skin tone" }, /*2775 : People & Body*/
{"👨🏿‍❤‍💋‍👨🏼","kiss: man, man, dark skin tone, medium-light skin tone" }, /*2776 : People & Body*/
{"👨🏿‍❤️‍💋‍👨🏽","kiss: man, man, dark skin tone, medium skin tone" }, /*2777 : People & Body*/
{"👨🏿‍❤‍💋‍👨🏽","kiss: man, man, dark skin tone, medium skin tone" }, /*2778 : People & Body*/
{"👨🏿‍❤️‍💋‍👨🏾","kiss: man, man, dark skin tone, medium-dark skin tone" }, /*2779 : People & Body*/
{"👨🏿‍❤‍💋‍👨🏾","kiss: man, man, dark skin tone, medium-dark skin tone" }, /*2780 : People & Body*/
{"👨🏿‍❤️‍💋‍👨🏿","kiss: man, man, dark skin tone" }, /*2781 : People & Body*/
{"👨🏿‍❤‍💋‍👨🏿","kiss: man, man, dark skin tone" }, /*2782 : People & Body*/
{"👩‍❤️‍💋‍👩","kiss: woman, woman" }, /*2783 : People & Body*/
{"👩‍❤‍💋‍👩","kiss: woman, woman" }, /*2784 : People & Body*/
{"👩🏻‍❤️‍💋‍👩🏻","kiss: woman, woman, light skin tone" }, /*2785 : People & Body*/
{"👩🏻‍❤‍💋‍👩🏻","kiss: woman, woman, light skin tone" }, /*2786 : People & Body*/
{"👩🏻‍❤️‍💋‍👩🏼","kiss: woman, woman, light skin tone, medium-light skin tone" }, /*2787 : People & Body*/
{"👩🏻‍❤‍💋‍👩🏼","kiss: woman, woman, light skin tone, medium-light skin tone" }, /*2788 : People & Body*/
{"👩🏻‍❤️‍💋‍👩🏽","kiss: woman, woman, light skin tone, medium skin tone" }, /*2789 : People & Body*/
{"👩🏻‍❤‍💋‍👩🏽","kiss: woman, woman, light skin tone, medium skin tone" }, /*2790 : People & Body*/
{"👩🏻‍❤️‍💋‍👩🏾","kiss: woman, woman, light skin tone, medium-dark skin tone" }, /*2791 : People & Body*/
{"👩🏻‍❤‍💋‍👩🏾","kiss: woman, woman, light skin tone, medium-dark skin tone" }, /*2792 : People & Body*/
{"👩🏻‍❤️‍💋‍👩🏿","kiss: woman, woman, light skin tone, dark skin tone" }, /*2793 : People & Body*/
{"👩🏻‍❤‍💋‍👩🏿","kiss: woman, woman, light skin tone, dark skin tone" }, /*2794 : People & Body*/
{"👩🏼‍❤️‍💋‍👩🏻","kiss: woman, woman, medium-light skin tone, light skin tone" }, /*2795 : People & Body*/
{"👩🏼‍❤‍💋‍👩🏻","kiss: woman, woman, medium-light skin tone, light skin tone" }, /*2796 : People & Body*/
{"👩🏼‍❤️‍💋‍👩🏼","kiss: woman, woman, medium-light skin tone" }, /*2797 : People & Body*/
{"👩🏼‍❤‍💋‍👩🏼","kiss: woman, woman, medium-light skin tone" }, /*2798 : People & Body*/
{"👩🏼‍❤️‍💋‍👩🏽","kiss: woman, woman, medium-light skin tone, medium skin tone" }, /*2799 : People & Body*/
{"👩🏼‍❤‍💋‍👩🏽","kiss: woman, woman, medium-light skin tone, medium skin tone" }, /*2800 : People & Body*/
{"👩🏼‍❤️‍💋‍👩🏾","kiss: woman, woman, medium-light skin tone, medium-dark skin tone" }, /*2801 : People & Body*/
{"👩🏼‍❤‍💋‍👩🏾","kiss: woman, woman, medium-light skin tone, medium-dark skin tone" }, /*2802 : People & Body*/
{"👩🏼‍❤️‍💋‍👩🏿","kiss: woman, woman, medium-light skin tone, dark skin tone" }, /*2803 : People & Body*/
{"👩🏼‍❤‍💋‍👩🏿","kiss: woman, woman, medium-light skin tone, dark skin tone" }, /*2804 : People & Body*/
{"👩🏽‍❤️‍💋‍👩🏻","kiss: woman, woman, medium skin tone, light skin tone" }, /*2805 : People & Body*/
{"👩🏽‍❤‍💋‍👩🏻","kiss: woman, woman, medium skin tone, light skin tone" }, /*2806 : People & Body*/
{"👩🏽‍❤️‍💋‍👩🏼","kiss: woman, woman, medium skin tone, medium-light skin tone" }, /*2807 : People & Body*/
{"👩🏽‍❤‍💋‍👩🏼","kiss: woman, woman, medium skin tone, medium-light skin tone" }, /*2808 : People & Body*/
{"👩🏽‍❤️‍💋‍👩🏽","kiss: woman, woman, medium skin tone" }, /*2809 : People & Body*/
{"👩🏽‍❤‍💋‍👩🏽","kiss: woman, woman, medium skin tone" }, /*2810 : People & Body*/
{"👩🏽‍❤️‍💋‍👩🏾","kiss: woman, woman, medium skin tone, medium-dark skin tone" }, /*2811 : People & Body*/
{"👩🏽‍❤‍💋‍👩🏾","kiss: woman, woman, medium skin tone, medium-dark skin tone" }, /*2812 : People & Body*/
{"👩🏽‍❤️‍💋‍👩🏿","kiss: woman, woman, medium skin tone, dark skin tone" }, /*2813 : People & Body*/
{"👩🏽‍❤‍💋‍👩🏿","kiss: woman, woman, medium skin tone, dark skin tone" }, /*2814 : People & Body*/
{"👩🏾‍❤️‍💋‍👩🏻","kiss: woman, woman, medium-dark skin tone, light skin tone" }, /*2815 : People & Body*/
{"👩🏾‍❤‍💋‍👩🏻","kiss: woman, woman, medium-dark skin tone, light skin tone" }, /*2816 : People & Body*/
{"👩🏾‍❤️‍💋‍👩🏼","kiss: woman, woman, medium-dark skin tone, medium-light skin tone" }, /*2817 : People & Body*/
{"👩🏾‍❤‍💋‍👩🏼","kiss: woman, woman, medium-dark skin tone, medium-light skin tone" }, /*2818 : People & Body*/
{"👩🏾‍❤️‍💋‍👩🏽","kiss: woman, woman, medium-dark skin tone, medium skin tone" }, /*2819 : People & Body*/
{"👩🏾‍❤‍💋‍👩🏽","kiss: woman, woman, medium-dark skin tone, medium skin tone" }, /*2820 : People & Body*/
{"👩🏾‍❤️‍💋‍👩🏾","kiss: woman, woman, medium-dark skin tone" }, /*2821 : People & Body*/
{"👩🏾‍❤‍💋‍👩🏾","kiss: woman, woman, medium-dark skin tone" }, /*2822 : People & Body*/
{"👩🏾‍❤️‍💋‍👩🏿","kiss: woman, woman, medium-dark skin tone, dark skin tone" }, /*2823 : People & Body*/
{"👩🏾‍❤‍💋‍👩🏿","kiss: woman, woman, medium-dark skin tone, dark skin tone" }, /*2824 : People & Body*/
{"👩🏿‍❤️‍💋‍👩🏻","kiss: woman, woman, dark skin tone, light skin tone" }, /*2825 : People & Body*/
{"👩🏿‍❤‍💋‍👩🏻","kiss: woman, woman, dark skin tone, light skin tone" }, /*2826 : People & Body*/
{"👩🏿‍❤️‍💋‍👩🏼","kiss: woman, woman, dark skin tone, medium-light skin tone" }, /*2827 : People & Body*/
{"👩🏿‍❤‍💋‍👩🏼","kiss: woman, woman, dark skin tone, medium-light skin tone" }, /*2828 : People & Body*/
{"👩🏿‍❤️‍💋‍👩🏽","kiss: woman, woman, dark skin tone, medium skin tone" }, /*2829 : People & Body*/
{"👩🏿‍❤‍💋‍👩🏽","kiss: woman, woman, dark skin tone, medium skin tone" }, /*2830 : People & Body*/
{"👩🏿‍❤️‍💋‍👩🏾","kiss: woman, woman, dark skin tone, medium-dark skin tone" }, /*2831 : People & Body*/
{"👩🏿‍❤‍💋‍👩🏾","kiss: woman, woman, dark skin tone, medium-dark skin tone" }, /*2832 : People & Body*/
{"👩🏿‍❤️‍💋‍👩🏿","kiss: woman, woman, dark skin tone" }, /*2833 : People & Body*/
{"👩🏿‍❤‍💋‍👩🏿","kiss: woman, woman, dark skin tone" }, /*2834 : People & Body*/
{"💑","couple with heart" }, /*2835 : People & Body*/
{"💑🏻","couple with heart: light skin tone" }, /*2836 : People & Body*/
{"💑🏼","couple with heart: medium-light skin tone" }, /*2837 : People & Body*/
{"💑🏽","couple with heart: medium skin tone" }, /*2838 : People & Body*/
{"💑🏾","couple with heart: medium-dark skin tone" }, /*2839 : People & Body*/
{"💑🏿","couple with heart: dark skin tone" }, /*2840 : People & Body*/
{"🧑🏻‍❤️‍🧑🏼","couple with heart: person, person, light skin tone, medium-light skin tone" }, /*2841 : People & Body*/
{"🧑🏻‍❤‍🧑🏼","couple with heart: person, person, light skin tone, medium-light skin tone" }, /*2842 : People & Body*/
{"🧑🏻‍❤️‍🧑🏽","couple with heart: person, person, light skin tone, medium skin tone" }, /*2843 : People & Body*/
{"🧑🏻‍❤‍🧑🏽","couple with heart: person, person, light skin tone, medium skin tone" }, /*2844 : People & Body*/
{"🧑🏻‍❤️‍🧑🏾","couple with heart: person, person, light skin tone, medium-dark skin tone" }, /*2845 : People & Body*/
{"🧑🏻‍❤‍🧑🏾","couple with heart: person, person, light skin tone, medium-dark skin tone" }, /*2846 : People & Body*/
{"🧑🏻‍❤️‍🧑🏿","couple with heart: person, person, light skin tone, dark skin tone" }, /*2847 : People & Body*/
{"🧑🏻‍❤‍🧑🏿","couple with heart: person, person, light skin tone, dark skin tone" }, /*2848 : People & Body*/
{"🧑🏼‍❤️‍🧑🏻","couple with heart: person, person, medium-light skin tone, light skin tone" }, /*2849 : People & Body*/
{"🧑🏼‍❤‍🧑🏻","couple with heart: person, person, medium-light skin tone, light skin tone" }, /*2850 : People & Body*/
{"🧑🏼‍❤️‍🧑🏽","couple with heart: person, person, medium-light skin tone, medium skin tone" }, /*2851 : People & Body*/
{"🧑🏼‍❤‍🧑🏽","couple with heart: person, person, medium-light skin tone, medium skin tone" }, /*2852 : People & Body*/
{"🧑🏼‍❤️‍🧑🏾","couple with heart: person, person, medium-light skin tone, medium-dark skin tone" }, /*2853 : People & Body*/
{"🧑🏼‍❤‍🧑🏾","couple with heart: person, person, medium-light skin tone, medium-dark skin tone" }, /*2854 : People & Body*/
{"🧑🏼‍❤️‍🧑🏿","couple with heart: person, person, medium-light skin tone, dark skin tone" }, /*2855 : People & Body*/
{"🧑🏼‍❤‍🧑🏿","couple with heart: person, person, medium-light skin tone, dark skin tone" }, /*2856 : People & Body*/
{"🧑🏽‍❤️‍🧑🏻","couple with heart: person, person, medium skin tone, light skin tone" }, /*2857 : People & Body*/
{"🧑🏽‍❤‍🧑🏻","couple with heart: person, person, medium skin tone, light skin tone" }, /*2858 : People & Body*/
{"🧑🏽‍❤️‍🧑🏼","couple with heart: person, person, medium skin tone, medium-light skin tone" }, /*2859 : People & Body*/
{"🧑🏽‍❤‍🧑🏼","couple with heart: person, person, medium skin tone, medium-light skin tone" }, /*2860 : People & Body*/
{"🧑🏽‍❤️‍🧑🏾","couple with heart: person, person, medium skin tone, medium-dark skin tone" }, /*2861 : People & Body*/
{"🧑🏽‍❤‍🧑🏾","couple with heart: person, person, medium skin tone, medium-dark skin tone" }, /*2862 : People & Body*/
{"🧑🏽‍❤️‍🧑🏿","couple with heart: person, person, medium skin tone, dark skin tone" }, /*2863 : People & Body*/
{"🧑🏽‍❤‍🧑🏿","couple with heart: person, person, medium skin tone, dark skin tone" }, /*2864 : People & Body*/
{"🧑🏾‍❤️‍🧑🏻","couple with heart: person, person, medium-dark skin tone, light skin tone" }, /*2865 : People & Body*/
{"🧑🏾‍❤‍🧑🏻","couple with heart: person, person, medium-dark skin tone, light skin tone" }, /*2866 : People & Body*/
{"🧑🏾‍❤️‍🧑🏼","couple with heart: person, person, medium-dark skin tone, medium-light skin tone" }, /*2867 : People & Body*/
{"🧑🏾‍❤‍🧑🏼","couple with heart: person, person, medium-dark skin tone, medium-light skin tone" }, /*2868 : People & Body*/
{"🧑🏾‍❤️‍🧑🏽","couple with heart: person, person, medium-dark skin tone, medium skin tone" }, /*2869 : People & Body*/
{"🧑🏾‍❤‍🧑🏽","couple with heart: person, person, medium-dark skin tone, medium skin tone" }, /*2870 : People & Body*/
{"🧑🏾‍❤️‍🧑🏿","couple with heart: person, person, medium-dark skin tone, dark skin tone" }, /*2871 : People & Body*/
{"🧑🏾‍❤‍🧑🏿","couple with heart: person, person, medium-dark skin tone, dark skin tone" }, /*2872 : People & Body*/
{"🧑🏿‍❤️‍🧑🏻","couple with heart: person, person, dark skin tone, light skin tone" }, /*2873 : People & Body*/
{"🧑🏿‍❤‍🧑🏻","couple with heart: person, person, dark skin tone, light skin tone" }, /*2874 : People & Body*/
{"🧑🏿‍❤️‍🧑🏼","couple with heart: person, person, dark skin tone, medium-light skin tone" }, /*2875 : People & Body*/
{"🧑🏿‍❤‍🧑🏼","couple with heart: person, person, dark skin tone, medium-light skin tone" }, /*2876 : People & Body*/
{"🧑🏿‍❤️‍🧑🏽","couple with heart: person, person, dark skin tone, medium skin tone" }, /*2877 : People & Body*/
{"🧑🏿‍❤‍🧑🏽","couple with heart: person, person, dark skin tone, medium skin tone" }, /*2878 : People & Body*/
{"🧑🏿‍❤️‍🧑🏾","couple with heart: person, person, dark skin tone, medium-dark skin tone" }, /*2879 : People & Body*/
{"🧑🏿‍❤‍🧑🏾","couple with heart: person, person, dark skin tone, medium-dark skin tone" }, /*2880 : People & Body*/
{"👩‍❤️‍👨","couple with heart: woman, man" }, /*2881 : People & Body*/
{"👩‍❤‍👨","couple with heart: woman, man" }, /*2882 : People & Body*/
{"👩🏻‍❤️‍👨🏻","couple with heart: woman, man, light skin tone" }, /*2883 : People & Body*/
{"👩🏻‍❤‍👨🏻","couple with heart: woman, man, light skin tone" }, /*2884 : People & Body*/
{"👩🏻‍❤️‍👨🏼","couple with heart: woman, man, light skin tone, medium-light skin tone" }, /*2885 : People & Body*/
{"👩🏻‍❤‍👨🏼","couple with heart: woman, man, light skin tone, medium-light skin tone" }, /*2886 : People & Body*/
{"👩🏻‍❤️‍👨🏽","couple with heart: woman, man, light skin tone, medium skin tone" }, /*2887 : People & Body*/
{"👩🏻‍❤‍👨🏽","couple with heart: woman, man, light skin tone, medium skin tone" }, /*2888 : People & Body*/
{"👩🏻‍❤️‍👨🏾","couple with heart: woman, man, light skin tone, medium-dark skin tone" }, /*2889 : People & Body*/
{"👩🏻‍❤‍👨🏾","couple with heart: woman, man, light skin tone, medium-dark skin tone" }, /*2890 : People & Body*/
{"👩🏻‍❤️‍👨🏿","couple with heart: woman, man, light skin tone, dark skin tone" }, /*2891 : People & Body*/
{"👩🏻‍❤‍👨🏿","couple with heart: woman, man, light skin tone, dark skin tone" }, /*2892 : People & Body*/
{"👩🏼‍❤️‍👨🏻","couple with heart: woman, man, medium-light skin tone, light skin tone" }, /*2893 : People & Body*/
{"👩🏼‍❤‍👨🏻","couple with heart: woman, man, medium-light skin tone, light skin tone" }, /*2894 : People & Body*/
{"👩🏼‍❤️‍👨🏼","couple with heart: woman, man, medium-light skin tone" }, /*2895 : People & Body*/
{"👩🏼‍❤‍👨🏼","couple with heart: woman, man, medium-light skin tone" }, /*2896 : People & Body*/
{"👩🏼‍❤️‍👨🏽","couple with heart: woman, man, medium-light skin tone, medium skin tone" }, /*2897 : People & Body*/
{"👩🏼‍❤‍👨🏽","couple with heart: woman, man, medium-light skin tone, medium skin tone" }, /*2898 : People & Body*/
{"👩🏼‍❤️‍👨🏾","couple with heart: woman, man, medium-light skin tone, medium-dark skin tone" }, /*2899 : People & Body*/
{"👩🏼‍❤‍👨🏾","couple with heart: woman, man, medium-light skin tone, medium-dark skin tone" }, /*2900 : People & Body*/
{"👩🏼‍❤️‍👨🏿","couple with heart: woman, man, medium-light skin tone, dark skin tone" }, /*2901 : People & Body*/
{"👩🏼‍❤‍👨🏿","couple with heart: woman, man, medium-light skin tone, dark skin tone" }, /*2902 : People & Body*/
{"👩🏽‍❤️‍👨🏻","couple with heart: woman, man, medium skin tone, light skin tone" }, /*2903 : People & Body*/
{"👩🏽‍❤‍👨🏻","couple with heart: woman, man, medium skin tone, light skin tone" }, /*2904 : People & Body*/
{"👩🏽‍❤️‍👨🏼","couple with heart: woman, man, medium skin tone, medium-light skin tone" }, /*2905 : People & Body*/
{"👩🏽‍❤‍👨🏼","couple with heart: woman, man, medium skin tone, medium-light skin tone" }, /*2906 : People & Body*/
{"👩🏽‍❤️‍👨🏽","couple with heart: woman, man, medium skin tone" }, /*2907 : People & Body*/
{"👩🏽‍❤‍👨🏽","couple with heart: woman, man, medium skin tone" }, /*2908 : People & Body*/
{"👩🏽‍❤️‍👨🏾","couple with heart: woman, man, medium skin tone, medium-dark skin tone" }, /*2909 : People & Body*/
{"👩🏽‍❤‍👨🏾","couple with heart: woman, man, medium skin tone, medium-dark skin tone" }, /*2910 : People & Body*/
{"👩🏽‍❤️‍👨🏿","couple with heart: woman, man, medium skin tone, dark skin tone" }, /*2911 : People & Body*/
{"👩🏽‍❤‍👨🏿","couple with heart: woman, man, medium skin tone, dark skin tone" }, /*2912 : People & Body*/
{"👩🏾‍❤️‍👨🏻","couple with heart: woman, man, medium-dark skin tone, light skin tone" }, /*2913 : People & Body*/
{"👩🏾‍❤‍👨🏻","couple with heart: woman, man, medium-dark skin tone, light skin tone" }, /*2914 : People & Body*/
{"👩🏾‍❤️‍👨🏼","couple with heart: woman, man, medium-dark skin tone, medium-light skin tone" }, /*2915 : People & Body*/
{"👩🏾‍❤‍👨🏼","couple with heart: woman, man, medium-dark skin tone, medium-light skin tone" }, /*2916 : People & Body*/
{"👩🏾‍❤️‍👨🏽","couple with heart: woman, man, medium-dark skin tone, medium skin tone" }, /*2917 : People & Body*/
{"👩🏾‍❤‍👨🏽","couple with heart: woman, man, medium-dark skin tone, medium skin tone" }, /*2918 : People & Body*/
{"👩🏾‍❤️‍👨🏾","couple with heart: woman, man, medium-dark skin tone" }, /*2919 : People & Body*/
{"👩🏾‍❤‍👨🏾","couple with heart: woman, man, medium-dark skin tone" }, /*2920 : People & Body*/
{"👩🏾‍❤️‍👨🏿","couple with heart: woman, man, medium-dark skin tone, dark skin tone" }, /*2921 : People & Body*/
{"👩🏾‍❤‍👨🏿","couple with heart: woman, man, medium-dark skin tone, dark skin tone" }, /*2922 : People & Body*/
{"👩🏿‍❤️‍👨🏻","couple with heart: woman, man, dark skin tone, light skin tone" }, /*2923 : People & Body*/
{"👩🏿‍❤‍👨🏻","couple with heart: woman, man, dark skin tone, light skin tone" }, /*2924 : People & Body*/
{"👩🏿‍❤️‍👨🏼","couple with heart: woman, man, dark skin tone, medium-light skin tone" }, /*2925 : People & Body*/
{"👩🏿‍❤‍👨🏼","couple with heart: woman, man, dark skin tone, medium-light skin tone" }, /*2926 : People & Body*/
{"👩🏿‍❤️‍👨🏽","couple with heart: woman, man, dark skin tone, medium skin tone" }, /*2927 : People & Body*/
{"👩🏿‍❤‍👨🏽","couple with heart: woman, man, dark skin tone, medium skin tone" }, /*2928 : People & Body*/
{"👩🏿‍❤️‍👨🏾","couple with heart: woman, man, dark skin tone, medium-dark skin tone" }, /*2929 : People & Body*/
{"👩🏿‍❤‍👨🏾","couple with heart: woman, man, dark skin tone, medium-dark skin tone" }, /*2930 : People & Body*/
{"👩🏿‍❤️‍👨🏿","couple with heart: woman, man, dark skin tone" }, /*2931 : People & Body*/
{"👩🏿‍❤‍👨🏿","couple with heart: woman, man, dark skin tone" }, /*2932 : People & Body*/
{"👨‍❤️‍👨","couple with heart: man, man" }, /*2933 : People & Body*/
{"👨‍❤‍👨","couple with heart: man, man" }, /*2934 : People & Body*/
{"👨🏻‍❤️‍👨🏻","couple with heart: man, man, light skin tone" }, /*2935 : People & Body*/
{"👨🏻‍❤‍👨🏻","couple with heart: man, man, light skin tone" }, /*2936 : People & Body*/
{"👨🏻‍❤️‍👨🏼","couple with heart: man, man, light skin tone, medium-light skin tone" }, /*2937 : People & Body*/
{"👨🏻‍❤‍👨🏼","couple with heart: man, man, light skin tone, medium-light skin tone" }, /*2938 : People & Body*/
{"👨🏻‍❤️‍👨🏽","couple with heart: man, man, light skin tone, medium skin tone" }, /*2939 : People & Body*/
{"👨🏻‍❤‍👨🏽","couple with heart: man, man, light skin tone, medium skin tone" }, /*2940 : People & Body*/
{"👨🏻‍❤️‍👨🏾","couple with heart: man, man, light skin tone, medium-dark skin tone" }, /*2941 : People & Body*/
{"👨🏻‍❤‍👨🏾","couple with heart: man, man, light skin tone, medium-dark skin tone" }, /*2942 : People & Body*/
{"👨🏻‍❤️‍👨🏿","couple with heart: man, man, light skin tone, dark skin tone" }, /*2943 : People & Body*/
{"👨🏻‍❤‍👨🏿","couple with heart: man, man, light skin tone, dark skin tone" }, /*2944 : People & Body*/
{"👨🏼‍❤️‍👨🏻","couple with heart: man, man, medium-light skin tone, light skin tone" }, /*2945 : People & Body*/
{"👨🏼‍❤‍👨🏻","couple with heart: man, man, medium-light skin tone, light skin tone" }, /*2946 : People & Body*/
{"👨🏼‍❤️‍👨🏼","couple with heart: man, man, medium-light skin tone" }, /*2947 : People & Body*/
{"👨🏼‍❤‍👨🏼","couple with heart: man, man, medium-light skin tone" }, /*2948 : People & Body*/
{"👨🏼‍❤️‍👨🏽","couple with heart: man, man, medium-light skin tone, medium skin tone" }, /*2949 : People & Body*/
{"👨🏼‍❤‍👨🏽","couple with heart: man, man, medium-light skin tone, medium skin tone" }, /*2950 : People & Body*/
{"👨🏼‍❤️‍👨🏾","couple with heart: man, man, medium-light skin tone, medium-dark skin tone" }, /*2951 : People & Body*/
{"👨🏼‍❤‍👨🏾","couple with heart: man, man, medium-light skin tone, medium-dark skin tone" }, /*2952 : People & Body*/
{"👨🏼‍❤️‍👨🏿","couple with heart: man, man, medium-light skin tone, dark skin tone" }, /*2953 : People & Body*/
{"👨🏼‍❤‍👨🏿","couple with heart: man, man, medium-light skin tone, dark skin tone" }, /*2954 : People & Body*/
{"👨🏽‍❤️‍👨🏻","couple with heart: man, man, medium skin tone, light skin tone" }, /*2955 : People & Body*/
{"👨🏽‍❤‍👨🏻","couple with heart: man, man, medium skin tone, light skin tone" }, /*2956 : People & Body*/
{"👨🏽‍❤️‍👨🏼","couple with heart: man, man, medium skin tone, medium-light skin tone" }, /*2957 : People & Body*/
{"👨🏽‍❤‍👨🏼","couple with heart: man, man, medium skin tone, medium-light skin tone" }, /*2958 : People & Body*/
{"👨🏽‍❤️‍👨🏽","couple with heart: man, man, medium skin tone" }, /*2959 : People & Body*/
{"👨🏽‍❤‍👨🏽","couple with heart: man, man, medium skin tone" }, /*2960 : People & Body*/
{"👨🏽‍❤️‍👨🏾","couple with heart: man, man, medium skin tone, medium-dark skin tone" }, /*2961 : People & Body*/
{"👨🏽‍❤‍👨🏾","couple with heart: man, man, medium skin tone, medium-dark skin tone" }, /*2962 : People & Body*/
{"👨🏽‍❤️‍👨🏿","couple with heart: man, man, medium skin tone, dark skin tone" }, /*2963 : People & Body*/
{"👨🏽‍❤‍👨🏿","couple with heart: man, man, medium skin tone, dark skin tone" }, /*2964 : People & Body*/
{"👨🏾‍❤️‍👨🏻","couple with heart: man, man, medium-dark skin tone, light skin tone" }, /*2965 : People & Body*/
{"👨🏾‍❤‍👨🏻","couple with heart: man, man, medium-dark skin tone, light skin tone" }, /*2966 : People & Body*/
{"👨🏾‍❤️‍👨🏼","couple with heart: man, man, medium-dark skin tone, medium-light skin tone" }, /*2967 : People & Body*/
{"👨🏾‍❤‍👨🏼","couple with heart: man, man, medium-dark skin tone, medium-light skin tone" }, /*2968 : People & Body*/
{"👨🏾‍❤️‍👨🏽","couple with heart: man, man, medium-dark skin tone, medium skin tone" }, /*2969 : People & Body*/
{"👨🏾‍❤‍👨🏽","couple with heart: man, man, medium-dark skin tone, medium skin tone" }, /*2970 : People & Body*/
{"👨🏾‍❤️‍👨🏾","couple with heart: man, man, medium-dark skin tone" }, /*2971 : People & Body*/
{"👨🏾‍❤‍👨🏾","couple with heart: man, man, medium-dark skin tone" }, /*2972 : People & Body*/
{"👨🏾‍❤️‍👨🏿","couple with heart: man, man, medium-dark skin tone, dark skin tone" }, /*2973 : People & Body*/
{"👨🏾‍❤‍👨🏿","couple with heart: man, man, medium-dark skin tone, dark skin tone" }, /*2974 : People & Body*/
{"👨🏿‍❤️‍👨🏻","couple with heart: man, man, dark skin tone, light skin tone" }, /*2975 : People & Body*/
{"👨🏿‍❤‍👨🏻","couple with heart: man, man, dark skin tone, light skin tone" }, /*2976 : People & Body*/
{"👨🏿‍❤️‍👨🏼","couple with heart: man, man, dark skin tone, medium-light skin tone" }, /*2977 : People & Body*/
{"👨🏿‍❤‍👨🏼","couple with heart: man, man, dark skin tone, medium-light skin tone" }, /*2978 : People & Body*/
{"👨🏿‍❤️‍👨🏽","couple with heart: man, man, dark skin tone, medium skin tone" }, /*2979 : People & Body*/
{"👨🏿‍❤‍👨🏽","couple with heart: man, man, dark skin tone, medium skin tone" }, /*2980 : People & Body*/
{"👨🏿‍❤️‍👨🏾","couple with heart: man, man, dark skin tone, medium-dark skin tone" }, /*2981 : People & Body*/
{"👨🏿‍❤‍👨🏾","couple with heart: man, man, dark skin tone, medium-dark skin tone" }, /*2982 : People & Body*/
{"👨🏿‍❤️‍👨🏿","couple with heart: man, man, dark skin tone" }, /*2983 : People & Body*/
{"👨🏿‍❤‍👨🏿","couple with heart: man, man, dark skin tone" }, /*2984 : People & Body*/
{"👩‍❤️‍👩","couple with heart: woman, woman" }, /*2985 : People & Body*/
{"👩‍❤‍👩","couple with heart: woman, woman" }, /*2986 : People & Body*/
{"👩🏻‍❤️‍👩🏻","couple with heart: woman, woman, light skin tone" }, /*2987 : People & Body*/
{"👩🏻‍❤‍👩🏻","couple with heart: woman, woman, light skin tone" }, /*2988 : People & Body*/
{"👩🏻‍❤️‍👩🏼","couple with heart: woman, woman, light skin tone, medium-light skin tone" }, /*2989 : People & Body*/
{"👩🏻‍❤‍👩🏼","couple with heart: woman, woman, light skin tone, medium-light skin tone" }, /*2990 : People & Body*/
{"👩🏻‍❤️‍👩🏽","couple with heart: woman, woman, light skin tone, medium skin tone" }, /*2991 : People & Body*/
{"👩🏻‍❤‍👩🏽","couple with heart: woman, woman, light skin tone, medium skin tone" }, /*2992 : People & Body*/
{"👩🏻‍❤️‍👩🏾","couple with heart: woman, woman, light skin tone, medium-dark skin tone" }, /*2993 : People & Body*/
{"👩🏻‍❤‍👩🏾","couple with heart: woman, woman, light skin tone, medium-dark skin tone" }, /*2994 : People & Body*/
{"👩🏻‍❤️‍👩🏿","couple with heart: woman, woman, light skin tone, dark skin tone" }, /*2995 : People & Body*/
{"👩🏻‍❤‍👩🏿","couple with heart: woman, woman, light skin tone, dark skin tone" }, /*2996 : People & Body*/
{"👩🏼‍❤️‍👩🏻","couple with heart: woman, woman, medium-light skin tone, light skin tone" }, /*2997 : People & Body*/
{"👩🏼‍❤‍👩🏻","couple with heart: woman, woman, medium-light skin tone, light skin tone" }, /*2998 : People & Body*/
{"👩🏼‍❤️‍👩🏼","couple with heart: woman, woman, medium-light skin tone" }, /*2999 : People & Body*/
{"👩🏼‍❤‍👩🏼","couple with heart: woman, woman, medium-light skin tone" }, /*3000 : People & Body*/
{"👩🏼‍❤️‍👩🏽","couple with heart: woman, woman, medium-light skin tone, medium skin tone" }, /*3001 : People & Body*/
{"👩🏼‍❤‍👩🏽","couple with heart: woman, woman, medium-light skin tone, medium skin tone" }, /*3002 : People & Body*/
{"👩🏼‍❤️‍👩🏾","couple with heart: woman, woman, medium-light skin tone, medium-dark skin tone" }, /*3003 : People & Body*/
{"👩🏼‍❤‍👩🏾","couple with heart: woman, woman, medium-light skin tone, medium-dark skin tone" }, /*3004 : People & Body*/
{"👩🏼‍❤️‍👩🏿","couple with heart: woman, woman, medium-light skin tone, dark skin tone" }, /*3005 : People & Body*/
{"👩🏼‍❤‍👩🏿","couple with heart: woman, woman, medium-light skin tone, dark skin tone" }, /*3006 : People & Body*/
{"👩🏽‍❤️‍👩🏻","couple with heart: woman, woman, medium skin tone, light skin tone" }, /*3007 : People & Body*/
{"👩🏽‍❤‍👩🏻","couple with heart: woman, woman, medium skin tone, light skin tone" }, /*3008 : People & Body*/
{"👩🏽‍❤️‍👩🏼","couple with heart: woman, woman, medium skin tone, medium-light skin tone" }, /*3009 : People & Body*/
{"👩🏽‍❤‍👩🏼","couple with heart: woman, woman, medium skin tone, medium-light skin tone" }, /*3010 : People & Body*/
{"👩🏽‍❤️‍👩🏽","couple with heart: woman, woman, medium skin tone" }, /*3011 : People & Body*/
{"👩🏽‍❤‍👩🏽","couple with heart: woman, woman, medium skin tone" }, /*3012 : People & Body*/
{"👩🏽‍❤️‍👩🏾","couple with heart: woman, woman, medium skin tone, medium-dark skin tone" }, /*3013 : People & Body*/
{"👩🏽‍❤‍👩🏾","couple with heart: woman, woman, medium skin tone, medium-dark skin tone" }, /*3014 : People & Body*/
{"👩🏽‍❤️‍👩🏿","couple with heart: woman, woman, medium skin tone, dark skin tone" }, /*3015 : People & Body*/
{"👩🏽‍❤‍👩🏿","couple with heart: woman, woman, medium skin tone, dark skin tone" }, /*3016 : People & Body*/
{"👩🏾‍❤️‍👩🏻","couple with heart: woman, woman, medium-dark skin tone, light skin tone" }, /*3017 : People & Body*/
{"👩🏾‍❤‍👩🏻","couple with heart: woman, woman, medium-dark skin tone, light skin tone" }, /*3018 : People & Body*/
{"👩🏾‍❤️‍👩🏼","couple with heart: woman, woman, medium-dark skin tone, medium-light skin tone" }, /*3019 : People & Body*/
{"👩🏾‍❤‍👩🏼","couple with heart: woman, woman, medium-dark skin tone, medium-light skin tone" }, /*3020 : People & Body*/
{"👩🏾‍❤️‍👩🏽","couple with heart: woman, woman, medium-dark skin tone, medium skin tone" }, /*3021 : People & Body*/
{"👩🏾‍❤‍👩🏽","couple with heart: woman, woman, medium-dark skin tone, medium skin tone" }, /*3022 : People & Body*/
{"👩🏾‍❤️‍👩🏾","couple with heart: woman, woman, medium-dark skin tone" }, /*3023 : People & Body*/
{"👩🏾‍❤‍👩🏾","couple with heart: woman, woman, medium-dark skin tone" }, /*3024 : People & Body*/
{"👩🏾‍❤️‍👩🏿","couple with heart: woman, woman, medium-dark skin tone, dark skin tone" }, /*3025 : People & Body*/
{"👩🏾‍❤‍👩🏿","couple with heart: woman, woman, medium-dark skin tone, dark skin tone" }, /*3026 : People & Body*/
{"👩🏿‍❤️‍👩🏻","couple with heart: woman, woman, dark skin tone, light skin tone" }, /*3027 : People & Body*/
{"👩🏿‍❤‍👩🏻","couple with heart: woman, woman, dark skin tone, light skin tone" }, /*3028 : People & Body*/
{"👩🏿‍❤️‍👩🏼","couple with heart: woman, woman, dark skin tone, medium-light skin tone" }, /*3029 : People & Body*/
{"👩🏿‍❤‍👩🏼","couple with heart: woman, woman, dark skin tone, medium-light skin tone" }, /*3030 : People & Body*/
{"👩🏿‍❤️‍👩🏽","couple with heart: woman, woman, dark skin tone, medium skin tone" }, /*3031 : People & Body*/
{"👩🏿‍❤‍👩🏽","couple with heart: woman, woman, dark skin tone, medium skin tone" }, /*3032 : People & Body*/
{"👩🏿‍❤️‍👩🏾","couple with heart: woman, woman, dark skin tone, medium-dark skin tone" }, /*3033 : People & Body*/
{"👩🏿‍❤‍👩🏾","couple with heart: woman, woman, dark skin tone, medium-dark skin tone" }, /*3034 : People & Body*/
{"👩🏿‍❤️‍👩🏿","couple with heart: woman, woman, dark skin tone" }, /*3035 : People & Body*/
{"👩🏿‍❤‍👩🏿","couple with heart: woman, woman, dark skin tone" }, /*3036 : People & Body*/
{"👪","family" }, /*3037 : People & Body*/
{"👨‍👩‍👦","family: man, woman, boy" }, /*3038 : People & Body*/
{"👨‍👩‍👧","family: man, woman, girl" }, /*3039 : People & Body*/
{"👨‍👩‍👧‍👦","family: man, woman, girl, boy" }, /*3040 : People & Body*/
{"👨‍👩‍👦‍👦","family: man, woman, boy, boy" }, /*3041 : People & Body*/
{"👨‍👩‍👧‍👧","family: man, woman, girl, girl" }, /*3042 : People & Body*/
{"👨‍👨‍👦","family: man, man, boy" }, /*3043 : People & Body*/
{"👨‍👨‍👧","family: man, man, girl" }, /*3044 : People & Body*/
{"👨‍👨‍👧‍👦","family: man, man, girl, boy" }, /*3045 : People & Body*/
{"👨‍👨‍👦‍👦","family: man, man, boy, boy" }, /*3046 : People & Body*/
{"👨‍👨‍👧‍👧","family: man, man, girl, girl" }, /*3047 : People & Body*/
{"👩‍👩‍👦","family: woman, woman, boy" }, /*3048 : People & Body*/
{"👩‍👩‍👧","family: woman, woman, girl" }, /*3049 : People & Body*/
{"👩‍👩‍👧‍👦","family: woman, woman, girl, boy" }, /*3050 : People & Body*/
{"👩‍👩‍👦‍👦","family: woman, woman, boy, boy" }, /*3051 : People & Body*/
{"👩‍👩‍👧‍👧","family: woman, woman, girl, girl" }, /*3052 : People & Body*/
{"👨‍👦","family: man, boy" }, /*3053 : People & Body*/
{"👨‍👦‍👦","family: man, boy, boy" }, /*3054 : People & Body*/
{"👨‍👧","family: man, girl" }, /*3055 : People & Body*/
{"👨‍👧‍👦","family: man, girl, boy" }, /*3056 : People & Body*/
{"👨‍👧‍👧","family: man, girl, girl" }, /*3057 : People & Body*/
{"👩‍👦","family: woman, boy" }, /*3058 : People & Body*/
{"👩‍👦‍👦","family: woman, boy, boy" }, /*3059 : People & Body*/
{"👩‍👧","family: woman, girl" }, /*3060 : People & Body*/
{"👩‍👧‍👦","family: woman, girl, boy" }, /*3061 : People & Body*/
{"👩‍👧‍👧","family: woman, girl, girl" }, /*3062 : People & Body*/
{"🗣️","speaking head" }, /*3063 : People & Body*/
{"🗣","speaking head" }, /*3064 : People & Body*/
{"👤","bust in silhouette" }, /*3065 : People & Body*/
{"👥","busts in silhouette" }, /*3066 : People & Body*/
{"🫂","people hugging" }, /*3067 : People & Body*/
{"👣","footprints" }, /*3068 : People & Body*/
{"🏻","light skin tone" }, /*3069 : Component*/
{"🏼","medium-light skin tone" }, /*3070 : Component*/
{"🏽","medium skin tone" }, /*3071 : Component*/
{"🏾","medium-dark skin tone" }, /*3072 : Component*/
{"🏿","dark skin tone" }, /*3073 : Component*/
{"🦰","red hair" }, /*3074 : Component*/
{"🦱","curly hair" }, /*3075 : Component*/
{"🦳","white hair" }, /*3076 : Component*/
{"🦲","bald" }, /*3077 : Component*/
{"🐵","monkey face" }, /*3078 : Animals & Nature*/
{"🐒","monkey" }, /*3079 : Animals & Nature*/
{"🦍","gorilla" }, /*3080 : Animals & Nature*/
{"🦧","orangutan" }, /*3081 : Animals & Nature*/
{"🐶","dog face" }, /*3082 : Animals & Nature*/
{"🐕","dog" }, /*3083 : Animals & Nature*/
{"🦮","guide dog" }, /*3084 : Animals & Nature*/
{"🐕‍🦺","service dog" }, /*3085 : Animals & Nature*/
{"🐩","poodle" }, /*3086 : Animals & Nature*/
{"🐺","wolf" }, /*3087 : Animals & Nature*/
{"🦊","fox" }, /*3088 : Animals & Nature*/
{"🦝","raccoon" }, /*3089 : Animals & Nature*/
{"🐱","cat face" }, /*3090 : Animals & Nature*/
{"🐈","cat" }, /*3091 : Animals & Nature*/
{"🐈‍⬛","black cat" }, /*3092 : Animals & Nature*/
{"🦁","lion" }, /*3093 : Animals & Nature*/
{"🐯","tiger face" }, /*3094 : Animals & Nature*/
{"🐅","tiger" }, /*3095 : Animals & Nature*/
{"🐆","leopard" }, /*3096 : Animals & Nature*/
{"🐴","horse face" }, /*3097 : Animals & Nature*/
{"🐎","horse" }, /*3098 : Animals & Nature*/
{"🦄","unicorn" }, /*3099 : Animals & Nature*/
{"🦓","zebra" }, /*3100 : Animals & Nature*/
{"🦌","deer" }, /*3101 : Animals & Nature*/
{"🦬","bison" }, /*3102 : Animals & Nature*/
{"🐮","cow face" }, /*3103 : Animals & Nature*/
{"🐂","ox" }, /*3104 : Animals & Nature*/
{"🐃","water buffalo" }, /*3105 : Animals & Nature*/
{"🐄","cow" }, /*3106 : Animals & Nature*/
{"🐷","pig face" }, /*3107 : Animals & Nature*/
{"🐖","pig" }, /*3108 : Animals & Nature*/
{"🐗","boar" }, /*3109 : Animals & Nature*/
{"🐽","pig nose" }, /*3110 : Animals & Nature*/
{"🐏","ram" }, /*3111 : Animals & Nature*/
{"🐑","ewe" }, /*3112 : Animals & Nature*/
{"🐐","goat" }, /*3113 : Animals & Nature*/
{"🐪","camel" }, /*3114 : Animals & Nature*/
{"🐫","two-hump camel" }, /*3115 : Animals & Nature*/
{"🦙","llama" }, /*3116 : Animals & Nature*/
{"🦒","giraffe" }, /*3117 : Animals & Nature*/
{"🐘","elephant" }, /*3118 : Animals & Nature*/
{"🦣","mammoth" }, /*3119 : Animals & Nature*/
{"🦏","rhinoceros" }, /*3120 : Animals & Nature*/
{"🦛","hippopotamus" }, /*3121 : Animals & Nature*/
{"🐭","mouse face" }, /*3122 : Animals & Nature*/
{"🐁","mouse" }, /*3123 : Animals & Nature*/
{"🐀","rat" }, /*3124 : Animals & Nature*/
{"🐹","hamster" }, /*3125 : Animals & Nature*/
{"🐰","rabbit face" }, /*3126 : Animals & Nature*/
{"🐇","rabbit" }, /*3127 : Animals & Nature*/
{"🐿️","chipmunk" }, /*3128 : Animals & Nature*/
{"🐿","chipmunk" }, /*3129 : Animals & Nature*/
{"🦫","beaver" }, /*3130 : Animals & Nature*/
{"🦔","hedgehog" }, /*3131 : Animals & Nature*/
{"🦇","bat" }, /*3132 : Animals & Nature*/
{"🐻","bear" }, /*3133 : Animals & Nature*/
{"🐻‍❄️","polar bear" }, /*3134 : Animals & Nature*/
{"🐻‍❄","polar bear" }, /*3135 : Animals & Nature*/
{"🐨","koala" }, /*3136 : Animals & Nature*/
{"🐼","panda" }, /*3137 : Animals & Nature*/
{"🦥","sloth" }, /*3138 : Animals & Nature*/
{"🦦","otter" }, /*3139 : Animals & Nature*/
{"🦨","skunk" }, /*3140 : Animals & Nature*/
{"🦘","kangaroo" }, /*3141 : Animals & Nature*/
{"🦡","badger" }, /*3142 : Animals & Nature*/
{"🐾","paw prints" }, /*3143 : Animals & Nature*/
{"🦃","turkey" }, /*3144 : Animals & Nature*/
{"🐔","chicken" }, /*3145 : Animals & Nature*/
{"🐓","rooster" }, /*3146 : Animals & Nature*/
{"🐣","hatching chick" }, /*3147 : Animals & Nature*/
{"🐤","baby chick" }, /*3148 : Animals & Nature*/
{"🐥","front-facing baby chick" }, /*3149 : Animals & Nature*/
{"🐦","bird" }, /*3150 : Animals & Nature*/
{"🐧","penguin" }, /*3151 : Animals & Nature*/
{"🕊️","dove" }, /*3152 : Animals & Nature*/
{"🕊","dove" }, /*3153 : Animals & Nature*/
{"🦅","eagle" }, /*3154 : Animals & Nature*/
{"🦆","duck" }, /*3155 : Animals & Nature*/
{"🦢","swan" }, /*3156 : Animals & Nature*/
{"🦉","owl" }, /*3157 : Animals & Nature*/
{"🦤","dodo" }, /*3158 : Animals & Nature*/
{"🪶","feather" }, /*3159 : Animals & Nature*/
{"🦩","flamingo" }, /*3160 : Animals & Nature*/
{"🦚","peacock" }, /*3161 : Animals & Nature*/
{"🦜","parrot" }, /*3162 : Animals & Nature*/
{"🐸","frog" }, /*3163 : Animals & Nature*/
{"🐊","crocodile" }, /*3164 : Animals & Nature*/
{"🐢","turtle" }, /*3165 : Animals & Nature*/
{"🦎","lizard" }, /*3166 : Animals & Nature*/
{"🐍","snake" }, /*3167 : Animals & Nature*/
{"🐲","dragon face" }, /*3168 : Animals & Nature*/
{"🐉","dragon" }, /*3169 : Animals & Nature*/
{"🦕","sauropod" }, /*3170 : Animals & Nature*/
{"🦖","T-Rex" }, /*3171 : Animals & Nature*/
{"🐳","spouting whale" }, /*3172 : Animals & Nature*/
{"🐋","whale" }, /*3173 : Animals & Nature*/
{"🐬","dolphin" }, /*3174 : Animals & Nature*/
{"🦭","seal" }, /*3175 : Animals & Nature*/
{"🐟","fish" }, /*3176 : Animals & Nature*/
{"🐠","tropical fish" }, /*3177 : Animals & Nature*/
{"🐡","blowfish" }, /*3178 : Animals & Nature*/
{"🦈","shark" }, /*3179 : Animals & Nature*/
{"🐙","octopus" }, /*3180 : Animals & Nature*/
{"🐚","spiral shell" }, /*3181 : Animals & Nature*/
{"🐌","snail" }, /*3182 : Animals & Nature*/
{"🦋","butterfly" }, /*3183 : Animals & Nature*/
{"🐛","bug" }, /*3184 : Animals & Nature*/
{"🐜","ant" }, /*3185 : Animals & Nature*/
{"🐝","honeybee" }, /*3186 : Animals & Nature*/
{"🪲","beetle" }, /*3187 : Animals & Nature*/
{"🐞","lady beetle" }, /*3188 : Animals & Nature*/
{"🦗","cricket" }, /*3189 : Animals & Nature*/
{"🪳","cockroach" }, /*3190 : Animals & Nature*/
{"🕷️","spider" }, /*3191 : Animals & Nature*/
{"🕷","spider" }, /*3192 : Animals & Nature*/
{"🕸️","spider web" }, /*3193 : Animals & Nature*/
{"🕸","spider web" }, /*3194 : Animals & Nature*/
{"🦂","scorpion" }, /*3195 : Animals & Nature*/
{"🦟","mosquito" }, /*3196 : Animals & Nature*/
{"🪰","fly" }, /*3197 : Animals & Nature*/
{"🪱","worm" }, /*3198 : Animals & Nature*/
{"🦠","microbe" }, /*3199 : Animals & Nature*/
{"💐","bouquet" }, /*3200 : Animals & Nature*/
{"🌸","cherry blossom" }, /*3201 : Animals & Nature*/
{"💮","white flower" }, /*3202 : Animals & Nature*/
{"🏵️","rosette" }, /*3203 : Animals & Nature*/
{"🏵","rosette" }, /*3204 : Animals & Nature*/
{"🌹","rose" }, /*3205 : Animals & Nature*/
{"🥀","wilted flower" }, /*3206 : Animals & Nature*/
{"🌺","hibiscus" }, /*3207 : Animals & Nature*/
{"🌻","sunflower" }, /*3208 : Animals & Nature*/
{"🌼","blossom" }, /*3209 : Animals & Nature*/
{"🌷","tulip" }, /*3210 : Animals & Nature*/
{"🌱","seedling" }, /*3211 : Animals & Nature*/
{"🪴","potted plant" }, /*3212 : Animals & Nature*/
{"🌲","evergreen tree" }, /*3213 : Animals & Nature*/
{"🌳","deciduous tree" }, /*3214 : Animals & Nature*/
{"🌴","palm tree" }, /*3215 : Animals & Nature*/
{"🌵","cactus" }, /*3216 : Animals & Nature*/
{"🌾","sheaf of rice" }, /*3217 : Animals & Nature*/
{"🌿","herb" }, /*3218 : Animals & Nature*/
{"☘️","shamrock" }, /*3219 : Animals & Nature*/
{"☘","shamrock" }, /*3220 : Animals & Nature*/
{"🍀","four leaf clover" }, /*3221 : Animals & Nature*/
{"🍁","maple leaf" }, /*3222 : Animals & Nature*/
{"🍂","fallen leaf" }, /*3223 : Animals & Nature*/
{"🍃","leaf fluttering in wind" }, /*3224 : Animals & Nature*/
{"🍇","grapes" }, /*3225 : Food & Drink*/
{"🍈","melon" }, /*3226 : Food & Drink*/
{"🍉","watermelon" }, /*3227 : Food & Drink*/
{"🍊","tangerine" }, /*3228 : Food & Drink*/
{"🍋","lemon" }, /*3229 : Food & Drink*/
{"🍌","banana" }, /*3230 : Food & Drink*/
{"🍍","pineapple" }, /*3231 : Food & Drink*/
{"🥭","mango" }, /*3232 : Food & Drink*/
{"🍎","red apple" }, /*3233 : Food & Drink*/
{"🍏","green apple" }, /*3234 : Food & Drink*/
{"🍐","pear" }, /*3235 : Food & Drink*/
{"🍑","peach" }, /*3236 : Food & Drink*/
{"🍒","cherries" }, /*3237 : Food & Drink*/
{"🍓","strawberry" }, /*3238 : Food & Drink*/
{"🫐","blueberries" }, /*3239 : Food & Drink*/
{"🥝","kiwi fruit" }, /*3240 : Food & Drink*/
{"🍅","tomato" }, /*3241 : Food & Drink*/
{"🫒","olive" }, /*3242 : Food & Drink*/
{"🥥","coconut" }, /*3243 : Food & Drink*/
{"🥑","avocado" }, /*3244 : Food & Drink*/
{"🍆","eggplant" }, /*3245 : Food & Drink*/
{"🥔","potato" }, /*3246 : Food & Drink*/
{"🥕","carrot" }, /*3247 : Food & Drink*/
{"🌽","ear of corn" }, /*3248 : Food & Drink*/
{"🌶️","hot pepper" }, /*3249 : Food & Drink*/
{"🌶","hot pepper" }, /*3250 : Food & Drink*/
{"🫑","bell pepper" }, /*3251 : Food & Drink*/
{"🥒","cucumber" }, /*3252 : Food & Drink*/
{"🥬","leafy green" }, /*3253 : Food & Drink*/
{"🥦","broccoli" }, /*3254 : Food & Drink*/
{"🧄","garlic" }, /*3255 : Food & Drink*/
{"🧅","onion" }, /*3256 : Food & Drink*/
{"🍄","mushroom" }, /*3257 : Food & Drink*/
{"🥜","peanuts" }, /*3258 : Food & Drink*/
{"🌰","chestnut" }, /*3259 : Food & Drink*/
{"🍞","bread" }, /*3260 : Food & Drink*/
{"🥐","croissant" }, /*3261 : Food & Drink*/
{"🥖","baguette bread" }, /*3262 : Food & Drink*/
{"🫓","flatbread" }, /*3263 : Food & Drink*/
{"🥨","pretzel" }, /*3264 : Food & Drink*/
{"🥯","bagel" }, /*3265 : Food & Drink*/
{"🥞","pancakes" }, /*3266 : Food & Drink*/
{"🧇","waffle" }, /*3267 : Food & Drink*/
{"🧀","cheese wedge" }, /*3268 : Food & Drink*/
{"🍖","meat on bone" }, /*3269 : Food & Drink*/
{"🍗","poultry leg" }, /*3270 : Food & Drink*/
{"🥩","cut of meat" }, /*3271 : Food & Drink*/
{"🥓","bacon" }, /*3272 : Food & Drink*/
{"🍔","hamburger" }, /*3273 : Food & Drink*/
{"🍟","french fries" }, /*3274 : Food & Drink*/
{"🍕","pizza" }, /*3275 : Food & Drink*/
{"🌭","hot dog" }, /*3276 : Food & Drink*/
{"🥪","sandwich" }, /*3277 : Food & Drink*/
{"🌮","taco" }, /*3278 : Food & Drink*/
{"🌯","burrito" }, /*3279 : Food & Drink*/
{"🫔","tamale" }, /*3280 : Food & Drink*/
{"🥙","stuffed flatbread" }, /*3281 : Food & Drink*/
{"🧆","falafel" }, /*3282 : Food & Drink*/
{"🥚","egg" }, /*3283 : Food & Drink*/
{"🍳","cooking" }, /*3284 : Food & Drink*/
{"🥘","shallow pan of food" }, /*3285 : Food & Drink*/
{"🍲","pot of food" }, /*3286 : Food & Drink*/
{"🫕","fondue" }, /*3287 : Food & Drink*/
{"🥣","bowl with spoon" }, /*3288 : Food & Drink*/
{"🥗","green salad" }, /*3289 : Food & Drink*/
{"🍿","popcorn" }, /*3290 : Food & Drink*/
{"🧈","butter" }, /*3291 : Food & Drink*/
{"🧂","salt" }, /*3292 : Food & Drink*/
{"🥫","canned food" }, /*3293 : Food & Drink*/
{"🍱","bento box" }, /*3294 : Food & Drink*/
{"🍘","rice cracker" }, /*3295 : Food & Drink*/
{"🍙","rice ball" }, /*3296 : Food & Drink*/
{"🍚","cooked rice" }, /*3297 : Food & Drink*/
{"🍛","curry rice" }, /*3298 : Food & Drink*/
{"🍜","steaming bowl" }, /*3299 : Food & Drink*/
{"🍝","spaghetti" }, /*3300 : Food & Drink*/
{"🍠","roasted sweet potato" }, /*3301 : Food & Drink*/
{"🍢","oden" }, /*3302 : Food & Drink*/
{"🍣","sushi" }, /*3303 : Food & Drink*/
{"🍤","fried shrimp" }, /*3304 : Food & Drink*/
{"🍥","fish cake with swirl" }, /*3305 : Food & Drink*/
{"🥮","moon cake" }, /*3306 : Food & Drink*/
{"🍡","dango" }, /*3307 : Food & Drink*/
{"🥟","dumpling" }, /*3308 : Food & Drink*/
{"🥠","fortune cookie" }, /*3309 : Food & Drink*/
{"🥡","takeout box" }, /*3310 : Food & Drink*/
{"🦀","crab" }, /*3311 : Food & Drink*/
{"🦞","lobster" }, /*3312 : Food & Drink*/
{"🦐","shrimp" }, /*3313 : Food & Drink*/
{"🦑","squid" }, /*3314 : Food & Drink*/
{"🦪","oyster" }, /*3315 : Food & Drink*/
{"🍦","soft ice cream" }, /*3316 : Food & Drink*/
{"🍧","shaved ice" }, /*3317 : Food & Drink*/
{"🍨","ice cream" }, /*3318 : Food & Drink*/
{"🍩","doughnut" }, /*3319 : Food & Drink*/
{"🍪","cookie" }, /*3320 : Food & Drink*/
{"🎂","birthday cake" }, /*3321 : Food & Drink*/
{"🍰","shortcake" }, /*3322 : Food & Drink*/
{"🧁","cupcake" }, /*3323 : Food & Drink*/
{"🥧","pie" }, /*3324 : Food & Drink*/
{"🍫","chocolate bar" }, /*3325 : Food & Drink*/
{"🍬","candy" }, /*3326 : Food & Drink*/
{"🍭","lollipop" }, /*3327 : Food & Drink*/
{"🍮","custard" }, /*3328 : Food & Drink*/
{"🍯","honey pot" }, /*3329 : Food & Drink*/
{"🍼","baby bottle" }, /*3330 : Food & Drink*/
{"🥛","glass of milk" }, /*3331 : Food & Drink*/
{"☕","hot beverage" }, /*3332 : Food & Drink*/
{"🫖","teapot" }, /*3333 : Food & Drink*/
{"🍵","teacup without handle" }, /*3334 : Food & Drink*/
{"🍶","sake" }, /*3335 : Food & Drink*/
{"🍾","bottle with popping cork" }, /*3336 : Food & Drink*/
{"🍷","wine glass" }, /*3337 : Food & Drink*/
{"🍸","cocktail glass" }, /*3338 : Food & Drink*/
{"🍹","tropical drink" }, /*3339 : Food & Drink*/
{"🍺","beer mug" }, /*3340 : Food & Drink*/
{"🍻","clinking beer mugs" }, /*3341 : Food & Drink*/
{"🥂","clinking glasses" }, /*3342 : Food & Drink*/
{"🥃","tumbler glass" }, /*3343 : Food & Drink*/
{"🥤","cup with straw" }, /*3344 : Food & Drink*/
{"🧋","bubble tea" }, /*3345 : Food & Drink*/
{"🧃","beverage box" }, /*3346 : Food & Drink*/
{"🧉","mate" }, /*3347 : Food & Drink*/
{"🧊","ice" }, /*3348 : Food & Drink*/
{"🥢","chopsticks" }, /*3349 : Food & Drink*/
{"🍽️","fork and knife with plate" }, /*3350 : Food & Drink*/
{"🍽","fork and knife with plate" }, /*3351 : Food & Drink*/
{"🍴","fork and knife" }, /*3352 : Food & Drink*/
{"🥄","spoon" }, /*3353 : Food & Drink*/
{"🔪","kitchen knife" }, /*3354 : Food & Drink*/
{"🏺","amphora" }, /*3355 : Food & Drink*/
{"🌍","globe showing Europe-Africa" }, /*3356 : Travel & Places*/
{"🌎","globe showing Americas" }, /*3357 : Travel & Places*/
{"🌏","globe showing Asia-Australia" }, /*3358 : Travel & Places*/
{"🌐","globe with meridians" }, /*3359 : Travel & Places*/
{"🗺️","world map" }, /*3360 : Travel & Places*/
{"🗺","world map" }, /*3361 : Travel & Places*/
{"🗾","map of Japan" }, /*3362 : Travel & Places*/
{"🧭","compass" }, /*3363 : Travel & Places*/
{"🏔️","snow-capped mountain" }, /*3364 : Travel & Places*/
{"🏔","snow-capped mountain" }, /*3365 : Travel & Places*/
{"⛰️","mountain" }, /*3366 : Travel & Places*/
{"⛰","mountain" }, /*3367 : Travel & Places*/
{"🌋","volcano" }, /*3368 : Travel & Places*/
{"🗻","mount fuji" }, /*3369 : Travel & Places*/
{"🏕️","camping" }, /*3370 : Travel & Places*/
{"🏕","camping" }, /*3371 : Travel & Places*/
{"🏖️","beach with umbrella" }, /*3372 : Travel & Places*/
{"🏖","beach with umbrella" }, /*3373 : Travel & Places*/
{"🏜️","desert" }, /*3374 : Travel & Places*/
{"🏜","desert" }, /*3375 : Travel & Places*/
{"🏝️","desert island" }, /*3376 : Travel & Places*/
{"🏝","desert island" }, /*3377 : Travel & Places*/
{"🏞️","national park" }, /*3378 : Travel & Places*/
{"🏞","national park" }, /*3379 : Travel & Places*/
{"🏟️","stadium" }, /*3380 : Travel & Places*/
{"🏟","stadium" }, /*3381 : Travel & Places*/
{"🏛️","classical building" }, /*3382 : Travel & Places*/
{"🏛","classical building" }, /*3383 : Travel & Places*/
{"🏗️","building construction" }, /*3384 : Travel & Places*/
{"🏗","building construction" }, /*3385 : Travel & Places*/
{"🧱","brick" }, /*3386 : Travel & Places*/
{"🪨","rock" }, /*3387 : Travel & Places*/
{"🪵","wood" }, /*3388 : Travel & Places*/
{"🛖","hut" }, /*3389 : Travel & Places*/
{"🏘️","houses" }, /*3390 : Travel & Places*/
{"🏘","houses" }, /*3391 : Travel & Places*/
{"🏚️","derelict house" }, /*3392 : Travel & Places*/
{"🏚","derelict house" }, /*3393 : Travel & Places*/
{"🏠","house" }, /*3394 : Travel & Places*/
{"🏡","house with garden" }, /*3395 : Travel & Places*/
{"🏢","office building" }, /*3396 : Travel & Places*/
{"🏣","Japanese post office" }, /*3397 : Travel & Places*/
{"🏤","post office" }, /*3398 : Travel & Places*/
{"🏥","hospital" }, /*3399 : Travel & Places*/
{"🏦","bank" }, /*3400 : Travel & Places*/
{"🏨","hotel" }, /*3401 : Travel & Places*/
{"🏩","love hotel" }, /*3402 : Travel & Places*/
{"🏪","convenience store" }, /*3403 : Travel & Places*/
{"🏫","school" }, /*3404 : Travel & Places*/
{"🏬","department store" }, /*3405 : Travel & Places*/
{"🏭","factory" }, /*3406 : Travel & Places*/
{"🏯","Japanese castle" }, /*3407 : Travel & Places*/
{"🏰","castle" }, /*3408 : Travel & Places*/
{"💒","wedding" }, /*3409 : Travel & Places*/
{"🗼","Tokyo tower" }, /*3410 : Travel & Places*/
{"🗽","Statue of Liberty" }, /*3411 : Travel & Places*/
{"⛪","church" }, /*3412 : Travel & Places*/
{"🕌","mosque" }, /*3413 : Travel & Places*/
{"🛕","hindu temple" }, /*3414 : Travel & Places*/
{"🕍","synagogue" }, /*3415 : Travel & Places*/
{"⛩️","shinto shrine" }, /*3416 : Travel & Places*/
{"⛩","shinto shrine" }, /*3417 : Travel & Places*/
{"🕋","kaaba" }, /*3418 : Travel & Places*/
{"⛲","fountain" }, /*3419 : Travel & Places*/
{"⛺","tent" }, /*3420 : Travel & Places*/
{"🌁","foggy" }, /*3421 : Travel & Places*/
{"🌃","night with stars" }, /*3422 : Travel & Places*/
{"🏙️","cityscape" }, /*3423 : Travel & Places*/
{"🏙","cityscape" }, /*3424 : Travel & Places*/
{"🌄","sunrise over mountains" }, /*3425 : Travel & Places*/
{"🌅","sunrise" }, /*3426 : Travel & Places*/
{"🌆","cityscape at dusk" }, /*3427 : Travel & Places*/
{"🌇","sunset" }, /*3428 : Travel & Places*/
{"🌉","bridge at night" }, /*3429 : Travel & Places*/
{"♨️","hot springs" }, /*3430 : Travel & Places*/
{"♨","hot springs" }, /*3431 : Travel & Places*/
{"🎠","carousel horse" }, /*3432 : Travel & Places*/
{"🎡","ferris wheel" }, /*3433 : Travel & Places*/
{"🎢","roller coaster" }, /*3434 : Travel & Places*/
{"💈","barber pole" }, /*3435 : Travel & Places*/
{"🎪","circus tent" }, /*3436 : Travel & Places*/
{"🚂","locomotive" }, /*3437 : Travel & Places*/
{"🚃","railway car" }, /*3438 : Travel & Places*/
{"🚄","high-speed train" }, /*3439 : Travel & Places*/
{"🚅","bullet train" }, /*3440 : Travel & Places*/
{"🚆","train" }, /*3441 : Travel & Places*/
{"🚇","metro" }, /*3442 : Travel & Places*/
{"🚈","light rail" }, /*3443 : Travel & Places*/
{"🚉","station" }, /*3444 : Travel & Places*/
{"🚊","tram" }, /*3445 : Travel & Places*/
{"🚝","monorail" }, /*3446 : Travel & Places*/
{"🚞","mountain railway" }, /*3447 : Travel & Places*/
{"🚋","tram car" }, /*3448 : Travel & Places*/
{"🚌","bus" }, /*3449 : Travel & Places*/
{"🚍","oncoming bus" }, /*3450 : Travel & Places*/
{"🚎","trolleybus" }, /*3451 : Travel & Places*/
{"🚐","minibus" }, /*3452 : Travel & Places*/
{"🚑","ambulance" }, /*3453 : Travel & Places*/
{"🚒","fire engine" }, /*3454 : Travel & Places*/
{"🚓","police car" }, /*3455 : Travel & Places*/
{"🚔","oncoming police car" }, /*3456 : Travel & Places*/
{"🚕","taxi" }, /*3457 : Travel & Places*/
{"🚖","oncoming taxi" }, /*3458 : Travel & Places*/
{"🚗","automobile" }, /*3459 : Travel & Places*/
{"🚘","oncoming automobile" }, /*3460 : Travel & Places*/
{"🚙","sport utility vehicle" }, /*3461 : Travel & Places*/
{"🛻","pickup truck" }, /*3462 : Travel & Places*/
{"🚚","delivery truck" }, /*3463 : Travel & Places*/
{"🚛","articulated lorry" }, /*3464 : Travel & Places*/
{"🚜","tractor" }, /*3465 : Travel & Places*/
{"🏎️","racing car" }, /*3466 : Travel & Places*/
{"🏎","racing car" }, /*3467 : Travel & Places*/
{"🏍️","motorcycle" }, /*3468 : Travel & Places*/
{"🏍","motorcycle" }, /*3469 : Travel & Places*/
{"🛵","motor scooter" }, /*3470 : Travel & Places*/
{"🦽","manual wheelchair" }, /*3471 : Travel & Places*/
{"🦼","motorized wheelchair" }, /*3472 : Travel & Places*/
{"🛺","auto rickshaw" }, /*3473 : Travel & Places*/
{"🚲","bicycle" }, /*3474 : Travel & Places*/
{"🛴","kick scooter" }, /*3475 : Travel & Places*/
{"🛹","skateboard" }, /*3476 : Travel & Places*/
{"🛼","roller skate" }, /*3477 : Travel & Places*/
{"🚏","bus stop" }, /*3478 : Travel & Places*/
{"🛣️","motorway" }, /*3479 : Travel & Places*/
{"🛣","motorway" }, /*3480 : Travel & Places*/
{"🛤️","railway track" }, /*3481 : Travel & Places*/
{"🛤","railway track" }, /*3482 : Travel & Places*/
{"🛢️","oil drum" }, /*3483 : Travel & Places*/
{"🛢","oil drum" }, /*3484 : Travel & Places*/
{"⛽","fuel pump" }, /*3485 : Travel & Places*/
{"🚨","police car light" }, /*3486 : Travel & Places*/
{"🚥","horizontal traffic light" }, /*3487 : Travel & Places*/
{"🚦","vertical traffic light" }, /*3488 : Travel & Places*/
{"🛑","stop sign" }, /*3489 : Travel & Places*/
{"🚧","construction" }, /*3490 : Travel & Places*/
{"⚓","anchor" }, /*3491 : Travel & Places*/
{"⛵","sailboat" }, /*3492 : Travel & Places*/
{"🛶","canoe" }, /*3493 : Travel & Places*/
{"🚤","speedboat" }, /*3494 : Travel & Places*/
{"🛳️","passenger ship" }, /*3495 : Travel & Places*/
{"🛳","passenger ship" }, /*3496 : Travel & Places*/
{"⛴️","ferry" }, /*3497 : Travel & Places*/
{"⛴","ferry" }, /*3498 : Travel & Places*/
{"🛥️","motor boat" }, /*3499 : Travel & Places*/
{"🛥","motor boat" }, /*3500 : Travel & Places*/
{"🚢","ship" }, /*3501 : Travel & Places*/
{"✈️","airplane" }, /*3502 : Travel & Places*/
{"✈","airplane" }, /*3503 : Travel & Places*/
{"🛩️","small airplane" }, /*3504 : Travel & Places*/
{"🛩","small airplane" }, /*3505 : Travel & Places*/
{"🛫","airplane departure" }, /*3506 : Travel & Places*/
{"🛬","airplane arrival" }, /*3507 : Travel & Places*/
{"🪂","parachute" }, /*3508 : Travel & Places*/
{"💺","seat" }, /*3509 : Travel & Places*/
{"🚁","helicopter" }, /*3510 : Travel & Places*/
{"🚟","suspension railway" }, /*3511 : Travel & Places*/
{"🚠","mountain cableway" }, /*3512 : Travel & Places*/
{"🚡","aerial tramway" }, /*3513 : Travel & Places*/
{"🛰️","satellite" }, /*3514 : Travel & Places*/
{"🛰","satellite" }, /*3515 : Travel & Places*/
{"🚀","rocket" }, /*3516 : Travel & Places*/
{"🛸","flying saucer" }, /*3517 : Travel & Places*/
{"🛎️","bellhop bell" }, /*3518 : Travel & Places*/
{"🛎","bellhop bell" }, /*3519 : Travel & Places*/
{"🧳","luggage" }, /*3520 : Travel & Places*/
{"⌛","hourglass done" }, /*3521 : Travel & Places*/
{"⏳","hourglass not done" }, /*3522 : Travel & Places*/
{"⌚","watch" }, /*3523 : Travel & Places*/
{"⏰","alarm clock" }, /*3524 : Travel & Places*/
{"⏱️","stopwatch" }, /*3525 : Travel & Places*/
{"⏱","stopwatch" }, /*3526 : Travel & Places*/
{"⏲️","timer clock" }, /*3527 : Travel & Places*/
{"⏲","timer clock" }, /*3528 : Travel & Places*/
{"🕰️","mantelpiece clock" }, /*3529 : Travel & Places*/
{"🕰","mantelpiece clock" }, /*3530 : Travel & Places*/
{"🕛","twelve o’clock" }, /*3531 : Travel & Places*/
{"🕧","twelve-thirty" }, /*3532 : Travel & Places*/
{"🕐","one o’clock" }, /*3533 : Travel & Places*/
{"🕜","one-thirty" }, /*3534 : Travel & Places*/
{"🕑","two o’clock" }, /*3535 : Travel & Places*/
{"🕝","two-thirty" }, /*3536 : Travel & Places*/
{"🕒","three o’clock" }, /*3537 : Travel & Places*/
{"🕞","three-thirty" }, /*3538 : Travel & Places*/
{"🕓","four o’clock" }, /*3539 : Travel & Places*/
{"🕟","four-thirty" }, /*3540 : Travel & Places*/
{"🕔","five o’clock" }, /*3541 : Travel & Places*/
{"🕠","five-thirty" }, /*3542 : Travel & Places*/
{"🕕","six o’clock" }, /*3543 : Travel & Places*/
{"🕡","six-thirty" }, /*3544 : Travel & Places*/
{"🕖","seven o’clock" }, /*3545 : Travel & Places*/
{"🕢","seven-thirty" }, /*3546 : Travel & Places*/
{"🕗","eight o’clock" }, /*3547 : Travel & Places*/
{"🕣","eight-thirty" }, /*3548 : Travel & Places*/
{"🕘","nine o’clock" }, /*3549 : Travel & Places*/
{"🕤","nine-thirty" }, /*3550 : Travel & Places*/
{"🕙","ten o’clock" }, /*3551 : Travel & Places*/
{"🕥","ten-thirty" }, /*3552 : Travel & Places*/
{"🕚","eleven o’clock" }, /*3553 : Travel & Places*/
{"🕦","eleven-thirty" }, /*3554 : Travel & Places*/
{"🌑","new moon" }, /*3555 : Travel & Places*/
{"🌒","waxing crescent moon" }, /*3556 : Travel & Places*/
{"🌓","first quarter moon" }, /*3557 : Travel & Places*/
{"🌔","waxing gibbous moon" }, /*3558 : Travel & Places*/
{"🌕","full moon" }, /*3559 : Travel & Places*/
{"🌖","waning gibbous moon" }, /*3560 : Travel & Places*/
{"🌗","last quarter moon" }, /*3561 : Travel & Places*/
{"🌘","waning crescent moon" }, /*3562 : Travel & Places*/
{"🌙","crescent moon" }, /*3563 : Travel & Places*/
{"🌚","new moon face" }, /*3564 : Travel & Places*/
{"🌛","first quarter moon face" }, /*3565 : Travel & Places*/
{"🌜","last quarter moon face" }, /*3566 : Travel & Places*/
{"🌡️","thermometer" }, /*3567 : Travel & Places*/
{"🌡","thermometer" }, /*3568 : Travel & Places*/
{"☀️","sun" }, /*3569 : Travel & Places*/
{"☀","sun" }, /*3570 : Travel & Places*/
{"🌝","full moon face" }, /*3571 : Travel & Places*/
{"🌞","sun with face" }, /*3572 : Travel & Places*/
{"🪐","ringed planet" }, /*3573 : Travel & Places*/
{"⭐","star" }, /*3574 : Travel & Places*/
{"🌟","glowing star" }, /*3575 : Travel & Places*/
{"🌠","shooting star" }, /*3576 : Travel & Places*/
{"🌌","milky way" }, /*3577 : Travel & Places*/
{"☁️","cloud" }, /*3578 : Travel & Places*/
{"☁","cloud" }, /*3579 : Travel & Places*/
{"⛅","sun behind cloud" }, /*3580 : Travel & Places*/
{"⛈️","cloud with lightning and rain" }, /*3581 : Travel & Places*/
{"⛈","cloud with lightning and rain" }, /*3582 : Travel & Places*/
{"🌤️","sun behind small cloud" }, /*3583 : Travel & Places*/
{"🌤","sun behind small cloud" }, /*3584 : Travel & Places*/
{"🌥️","sun behind large cloud" }, /*3585 : Travel & Places*/
{"🌥","sun behind large cloud" }, /*3586 : Travel & Places*/
{"🌦️","sun behind rain cloud" }, /*3587 : Travel & Places*/
{"🌦","sun behind rain cloud" }, /*3588 : Travel & Places*/
{"🌧️","cloud with rain" }, /*3589 : Travel & Places*/
{"🌧","cloud with rain" }, /*3590 : Travel & Places*/
{"🌨️","cloud with snow" }, /*3591 : Travel & Places*/
{"🌨","cloud with snow" }, /*3592 : Travel & Places*/
{"🌩️","cloud with lightning" }, /*3593 : Travel & Places*/
{"🌩","cloud with lightning" }, /*3594 : Travel & Places*/
{"🌪️","tornado" }, /*3595 : Travel & Places*/
{"🌪","tornado" }, /*3596 : Travel & Places*/
{"🌫️","fog" }, /*3597 : Travel & Places*/
{"🌫","fog" }, /*3598 : Travel & Places*/
{"🌬️","wind face" }, /*3599 : Travel & Places*/
{"🌬","wind face" }, /*3600 : Travel & Places*/
{"🌀","cyclone" }, /*3601 : Travel & Places*/
{"🌈","rainbow" }, /*3602 : Travel & Places*/
{"🌂","closed umbrella" }, /*3603 : Travel & Places*/
{"☂️","umbrella" }, /*3604 : Travel & Places*/
{"☂","umbrella" }, /*3605 : Travel & Places*/
{"☔","umbrella with rain drops" }, /*3606 : Travel & Places*/
{"⛱️","umbrella on ground" }, /*3607 : Travel & Places*/
{"⛱","umbrella on ground" }, /*3608 : Travel & Places*/
{"⚡","high voltage" }, /*3609 : Travel & Places*/
{"❄️","snowflake" }, /*3610 : Travel & Places*/
{"❄","snowflake" }, /*3611 : Travel & Places*/
{"☃️","snowman" }, /*3612 : Travel & Places*/
{"☃","snowman" }, /*3613 : Travel & Places*/
{"⛄","snowman without snow" }, /*3614 : Travel & Places*/
{"☄️","comet" }, /*3615 : Travel & Places*/
{"☄","comet" }, /*3616 : Travel & Places*/
{"🔥","fire" }, /*3617 : Travel & Places*/
{"💧","droplet" }, /*3618 : Travel & Places*/
{"🌊","water wave" }, /*3619 : Travel & Places*/
{"🎃","jack-o-lantern" }, /*3620 : Activities*/
{"🎄","Christmas tree" }, /*3621 : Activities*/
{"🎆","fireworks" }, /*3622 : Activities*/
{"🎇","sparkler" }, /*3623 : Activities*/
{"🧨","firecracker" }, /*3624 : Activities*/
{"✨","sparkles" }, /*3625 : Activities*/
{"🎈","balloon" }, /*3626 : Activities*/
{"🎉","party popper" }, /*3627 : Activities*/
{"🎊","confetti ball" }, /*3628 : Activities*/
{"🎋","tanabata tree" }, /*3629 : Activities*/
{"🎍","pine decoration" }, /*3630 : Activities*/
{"🎎","Japanese dolls" }, /*3631 : Activities*/
{"🎏","carp streamer" }, /*3632 : Activities*/
{"🎐","wind chime" }, /*3633 : Activities*/
{"🎑","moon viewing ceremony" }, /*3634 : Activities*/
{"🧧","red envelope" }, /*3635 : Activities*/
{"🎀","ribbon" }, /*3636 : Activities*/
{"🎁","wrapped gift" }, /*3637 : Activities*/
{"🎗️","reminder ribbon" }, /*3638 : Activities*/
{"🎗","reminder ribbon" }, /*3639 : Activities*/
{"🎟️","admission tickets" }, /*3640 : Activities*/
{"🎟","admission tickets" }, /*3641 : Activities*/
{"🎫","ticket" }, /*3642 : Activities*/
{"🎖️","military medal" }, /*3643 : Activities*/
{"🎖","military medal" }, /*3644 : Activities*/
{"🏆","trophy" }, /*3645 : Activities*/
{"🏅","sports medal" }, /*3646 : Activities*/
{"🥇","1st place medal" }, /*3647 : Activities*/
{"🥈","2nd place medal" }, /*3648 : Activities*/
{"🥉","3rd place medal" }, /*3649 : Activities*/
{"⚽","soccer ball" }, /*3650 : Activities*/
{"⚾","baseball" }, /*3651 : Activities*/
{"🥎","softball" }, /*3652 : Activities*/
{"🏀","basketball" }, /*3653 : Activities*/
{"🏐","volleyball" }, /*3654 : Activities*/
{"🏈","american football" }, /*3655 : Activities*/
{"🏉","rugby football" }, /*3656 : Activities*/
{"🎾","tennis" }, /*3657 : Activities*/
{"🥏","flying disc" }, /*3658 : Activities*/
{"🎳","bowling" }, /*3659 : Activities*/
{"🏏","cricket game" }, /*3660 : Activities*/
{"🏑","field hockey" }, /*3661 : Activities*/
{"🏒","ice hockey" }, /*3662 : Activities*/
{"🥍","lacrosse" }, /*3663 : Activities*/
{"🏓","ping pong" }, /*3664 : Activities*/
{"🏸","badminton" }, /*3665 : Activities*/
{"🥊","boxing glove" }, /*3666 : Activities*/
{"🥋","martial arts uniform" }, /*3667 : Activities*/
{"🥅","goal net" }, /*3668 : Activities*/
{"⛳","flag in hole" }, /*3669 : Activities*/
{"⛸️","ice skate" }, /*3670 : Activities*/
{"⛸","ice skate" }, /*3671 : Activities*/
{"🎣","fishing pole" }, /*3672 : Activities*/
{"🤿","diving mask" }, /*3673 : Activities*/
{"🎽","running shirt" }, /*3674 : Activities*/
{"🎿","skis" }, /*3675 : Activities*/
{"🛷","sled" }, /*3676 : Activities*/
{"🥌","curling stone" }, /*3677 : Activities*/
{"🎯","bullseye" }, /*3678 : Activities*/
{"🪀","yo-yo" }, /*3679 : Activities*/
{"🪁","kite" }, /*3680 : Activities*/
{"🎱","pool 8 ball" }, /*3681 : Activities*/
{"🔮","crystal ball" }, /*3682 : Activities*/
{"🪄","magic wand" }, /*3683 : Activities*/
{"🧿","nazar amulet" }, /*3684 : Activities*/
{"🎮","video game" }, /*3685 : Activities*/
{"🕹️","joystick" }, /*3686 : Activities*/
{"🕹","joystick" }, /*3687 : Activities*/
{"🎰","slot machine" }, /*3688 : Activities*/
{"🎲","game die" }, /*3689 : Activities*/
{"🧩","puzzle piece" }, /*3690 : Activities*/
{"🧸","teddy bear" }, /*3691 : Activities*/
{"🪅","piñata" }, /*3692 : Activities*/
{"🪆","nesting dolls" }, /*3693 : Activities*/
{"♠️","spade suit" }, /*3694 : Activities*/
{"♠","spade suit" }, /*3695 : Activities*/
{"♥️","heart suit" }, /*3696 : Activities*/
{"♥","heart suit" }, /*3697 : Activities*/
{"♦️","diamond suit" }, /*3698 : Activities*/
{"♦","diamond suit" }, /*3699 : Activities*/
{"♣️","club suit" }, /*3700 : Activities*/
{"♣","club suit" }, /*3701 : Activities*/
{"♟️","chess pawn" }, /*3702 : Activities*/
{"♟","chess pawn" }, /*3703 : Activities*/
{"🃏","joker" }, /*3704 : Activities*/
{"🀄","mahjong red dragon" }, /*3705 : Activities*/
{"🎴","flower playing cards" }, /*3706 : Activities*/
{"🎭","performing arts" }, /*3707 : Activities*/
{"🖼️","framed picture" }, /*3708 : Activities*/
{"🖼","framed picture" }, /*3709 : Activities*/
{"🎨","artist palette" }, /*3710 : Activities*/
{"🧵","thread" }, /*3711 : Activities*/
{"🪡","sewing needle" }, /*3712 : Activities*/
{"🧶","yarn" }, /*3713 : Activities*/
{"🪢","knot" }, /*3714 : Activities*/
{"👓","glasses" }, /*3715 : Objects*/
{"🕶️","sunglasses" }, /*3716 : Objects*/
{"🕶","sunglasses" }, /*3717 : Objects*/
{"🥽","goggles" }, /*3718 : Objects*/
{"🥼","lab coat" }, /*3719 : Objects*/
{"🦺","safety vest" }, /*3720 : Objects*/
{"👔","necktie" }, /*3721 : Objects*/
{"👕","t-shirt" }, /*3722 : Objects*/
{"👖","jeans" }, /*3723 : Objects*/
{"🧣","scarf" }, /*3724 : Objects*/
{"🧤","gloves" }, /*3725 : Objects*/
{"🧥","coat" }, /*3726 : Objects*/
{"🧦","socks" }, /*3727 : Objects*/
{"👗","dress" }, /*3728 : Objects*/
{"👘","kimono" }, /*3729 : Objects*/
{"🥻","sari" }, /*3730 : Objects*/
{"🩱","one-piece swimsuit" }, /*3731 : Objects*/
{"🩲","briefs" }, /*3732 : Objects*/
{"🩳","shorts" }, /*3733 : Objects*/
{"👙","bikini" }, /*3734 : Objects*/
{"👚","woman’s clothes" }, /*3735 : Objects*/
{"👛","purse" }, /*3736 : Objects*/
{"👜","handbag" }, /*3737 : Objects*/
{"👝","clutch bag" }, /*3738 : Objects*/
{"🛍️","shopping bags" }, /*3739 : Objects*/
{"🛍","shopping bags" }, /*3740 : Objects*/
{"🎒","backpack" }, /*3741 : Objects*/
{"🩴","thong sandal" }, /*3742 : Objects*/
{"👞","man’s shoe" }, /*3743 : Objects*/
{"👟","running shoe" }, /*3744 : Objects*/
{"🥾","hiking boot" }, /*3745 : Objects*/
{"🥿","flat shoe" }, /*3746 : Objects*/
{"👠","high-heeled shoe" }, /*3747 : Objects*/
{"👡","woman’s sandal" }, /*3748 : Objects*/
{"🩰","ballet shoes" }, /*3749 : Objects*/
{"👢","woman’s boot" }, /*3750 : Objects*/
{"👑","crown" }, /*3751 : Objects*/
{"👒","woman’s hat" }, /*3752 : Objects*/
{"🎩","top hat" }, /*3753 : Objects*/
{"🎓","graduation cap" }, /*3754 : Objects*/
{"🧢","billed cap" }, /*3755 : Objects*/
{"🪖","military helmet" }, /*3756 : Objects*/
{"⛑️","rescue worker’s helmet" }, /*3757 : Objects*/
{"⛑","rescue worker’s helmet" }, /*3758 : Objects*/
{"📿","prayer beads" }, /*3759 : Objects*/
{"💄","lipstick" }, /*3760 : Objects*/
{"💍","ring" }, /*3761 : Objects*/
{"💎","gem stone" }, /*3762 : Objects*/
{"🔇","muted speaker" }, /*3763 : Objects*/
{"🔈","speaker low volume" }, /*3764 : Objects*/
{"🔉","speaker medium volume" }, /*3765 : Objects*/
{"🔊","speaker high volume" }, /*3766 : Objects*/
{"📢","loudspeaker" }, /*3767 : Objects*/
{"📣","megaphone" }, /*3768 : Objects*/
{"📯","postal horn" }, /*3769 : Objects*/
{"🔔","bell" }, /*3770 : Objects*/
{"🔕","bell with slash" }, /*3771 : Objects*/
{"🎼","musical score" }, /*3772 : Objects*/
{"🎵","musical note" }, /*3773 : Objects*/
{"🎶","musical notes" }, /*3774 : Objects*/
{"🎙️","studio microphone" }, /*3775 : Objects*/
{"🎙","studio microphone" }, /*3776 : Objects*/
{"🎚️","level slider" }, /*3777 : Objects*/
{"🎚","level slider" }, /*3778 : Objects*/
{"🎛️","control knobs" }, /*3779 : Objects*/
{"🎛","control knobs" }, /*3780 : Objects*/
{"🎤","microphone" }, /*3781 : Objects*/
{"🎧","headphone" }, /*3782 : Objects*/
{"📻","radio" }, /*3783 : Objects*/
{"🎷","saxophone" }, /*3784 : Objects*/
{"🪗","accordion" }, /*3785 : Objects*/
{"🎸","guitar" }, /*3786 : Objects*/
{"🎹","musical keyboard" }, /*3787 : Objects*/
{"🎺","trumpet" }, /*3788 : Objects*/
{"🎻","violin" }, /*3789 : Objects*/
{"🪕","banjo" }, /*3790 : Objects*/
{"🥁","drum" }, /*3791 : Objects*/
{"🪘","long drum" }, /*3792 : Objects*/
{"📱","mobile phone" }, /*3793 : Objects*/
{"📲","mobile phone with arrow" }, /*3794 : Objects*/
{"☎️","telephone" }, /*3795 : Objects*/
{"☎","telephone" }, /*3796 : Objects*/
{"📞","telephone receiver" }, /*3797 : Objects*/
{"📟","pager" }, /*3798 : Objects*/
{"📠","fax machine" }, /*3799 : Objects*/
{"🔋","battery" }, /*3800 : Objects*/
{"🔌","electric plug" }, /*3801 : Objects*/
{"💻","laptop" }, /*3802 : Objects*/
{"🖥️","desktop computer" }, /*3803 : Objects*/
{"🖥","desktop computer" }, /*3804 : Objects*/
{"🖨️","printer" }, /*3805 : Objects*/
{"🖨","printer" }, /*3806 : Objects*/
{"⌨️","keyboard" }, /*3807 : Objects*/
{"⌨","keyboard" }, /*3808 : Objects*/
{"🖱️","computer mouse" }, /*3809 : Objects*/
{"🖱","computer mouse" }, /*3810 : Objects*/
{"🖲️","trackball" }, /*3811 : Objects*/
{"🖲","trackball" }, /*3812 : Objects*/
{"💽","computer disk" }, /*3813 : Objects*/
{"💾","floppy disk" }, /*3814 : Objects*/
{"💿","optical disk" }, /*3815 : Objects*/
{"📀","dvd" }, /*3816 : Objects*/
{"🧮","abacus" }, /*3817 : Objects*/
{"🎥","movie camera" }, /*3818 : Objects*/
{"🎞️","film frames" }, /*3819 : Objects*/
{"🎞","film frames" }, /*3820 : Objects*/
{"📽️","film projector" }, /*3821 : Objects*/
{"📽","film projector" }, /*3822 : Objects*/
{"🎬","clapper board" }, /*3823 : Objects*/
{"📺","television" }, /*3824 : Objects*/
{"📷","camera" }, /*3825 : Objects*/
{"📸","camera with flash" }, /*3826 : Objects*/
{"📹","video camera" }, /*3827 : Objects*/
{"📼","videocassette" }, /*3828 : Objects*/
{"🔍","magnifying glass tilted left" }, /*3829 : Objects*/
{"🔎","magnifying glass tilted right" }, /*3830 : Objects*/
{"🕯️","candle" }, /*3831 : Objects*/
{"🕯","candle" }, /*3832 : Objects*/
{"💡","light bulb" }, /*3833 : Objects*/
{"🔦","flashlight" }, /*3834 : Objects*/
{"🏮","red paper lantern" }, /*3835 : Objects*/
{"🪔","diya lamp" }, /*3836 : Objects*/
{"📔","notebook with decorative cover" }, /*3837 : Objects*/
{"📕","closed book" }, /*3838 : Objects*/
{"📖","open book" }, /*3839 : Objects*/
{"📗","green book" }, /*3840 : Objects*/
{"📘","blue book" }, /*3841 : Objects*/
{"📙","orange book" }, /*3842 : Objects*/
{"📚","books" }, /*3843 : Objects*/
{"📓","notebook" }, /*3844 : Objects*/
{"📒","ledger" }, /*3845 : Objects*/
{"📃","page with curl" }, /*3846 : Objects*/
{"📜","scroll" }, /*3847 : Objects*/
{"📄","page facing up" }, /*3848 : Objects*/
{"📰","newspaper" }, /*3849 : Objects*/
{"🗞️","rolled-up newspaper" }, /*3850 : Objects*/
{"🗞","rolled-up newspaper" }, /*3851 : Objects*/
{"📑","bookmark tabs" }, /*3852 : Objects*/
{"🔖","bookmark" }, /*3853 : Objects*/
{"🏷️","label" }, /*3854 : Objects*/
{"🏷","label" }, /*3855 : Objects*/
{"💰","money bag" }, /*3856 : Objects*/
{"🪙","coin" }, /*3857 : Objects*/
{"💴","yen banknote" }, /*3858 : Objects*/
{"💵","dollar banknote" }, /*3859 : Objects*/
{"💶","euro banknote" }, /*3860 : Objects*/
{"💷","pound banknote" }, /*3861 : Objects*/
{"💸","money with wings" }, /*3862 : Objects*/
{"💳","credit card" }, /*3863 : Objects*/
{"🧾","receipt" }, /*3864 : Objects*/
{"💹","chart increasing with yen" }, /*3865 : Objects*/
{"✉️","envelope" }, /*3866 : Objects*/
{"✉","envelope" }, /*3867 : Objects*/
{"📧","e-mail" }, /*3868 : Objects*/
{"📨","incoming envelope" }, /*3869 : Objects*/
{"📩","envelope with arrow" }, /*3870 : Objects*/
{"📤","outbox tray" }, /*3871 : Objects*/
{"📥","inbox tray" }, /*3872 : Objects*/
{"📦","package" }, /*3873 : Objects*/
{"📫","closed mailbox with raised flag" }, /*3874 : Objects*/
{"📪","closed mailbox with lowered flag" }, /*3875 : Objects*/
{"📬","open mailbox with raised flag" }, /*3876 : Objects*/
{"📭","open mailbox with lowered flag" }, /*3877 : Objects*/
{"📮","postbox" }, /*3878 : Objects*/
{"🗳️","ballot box with ballot" }, /*3879 : Objects*/
{"🗳","ballot box with ballot" }, /*3880 : Objects*/
{"✏️","pencil" }, /*3881 : Objects*/
{"✏","pencil" }, /*3882 : Objects*/
{"✒️","black nib" }, /*3883 : Objects*/
{"✒","black nib" }, /*3884 : Objects*/
{"🖋️","fountain pen" }, /*3885 : Objects*/
{"🖋","fountain pen" }, /*3886 : Objects*/
{"🖊️","pen" }, /*3887 : Objects*/
{"🖊","pen" }, /*3888 : Objects*/
{"🖌️","paintbrush" }, /*3889 : Objects*/
{"🖌","paintbrush" }, /*3890 : Objects*/
{"🖍️","crayon" }, /*3891 : Objects*/
{"🖍","crayon" }, /*3892 : Objects*/
{"📝","memo" }, /*3893 : Objects*/
{"💼","briefcase" }, /*3894 : Objects*/
{"📁","file folder" }, /*3895 : Objects*/
{"📂","open file folder" }, /*3896 : Objects*/
{"🗂️","card index dividers" }, /*3897 : Objects*/
{"🗂","card index dividers" }, /*3898 : Objects*/
{"📅","calendar" }, /*3899 : Objects*/
{"📆","tear-off calendar" }, /*3900 : Objects*/
{"🗒️","spiral notepad" }, /*3901 : Objects*/
{"🗒","spiral notepad" }, /*3902 : Objects*/
{"🗓️","spiral calendar" }, /*3903 : Objects*/
{"🗓","spiral calendar" }, /*3904 : Objects*/
{"📇","card index" }, /*3905 : Objects*/
{"📈","chart increasing" }, /*3906 : Objects*/
{"📉","chart decreasing" }, /*3907 : Objects*/
{"📊","bar chart" }, /*3908 : Objects*/
{"📋","clipboard" }, /*3909 : Objects*/
{"📌","pushpin" }, /*3910 : Objects*/
{"📍","round pushpin" }, /*3911 : Objects*/
{"📎","paperclip" }, /*3912 : Objects*/
{"🖇️","linked paperclips" }, /*3913 : Objects*/
{"🖇","linked paperclips" }, /*3914 : Objects*/
{"📏","straight ruler" }, /*3915 : Objects*/
{"📐","triangular ruler" }, /*3916 : Objects*/
{"✂️","scissors" }, /*3917 : Objects*/
{"✂","scissors" }, /*3918 : Objects*/
{"🗃️","card file box" }, /*3919 : Objects*/
{"🗃","card file box" }, /*3920 : Objects*/
{"🗄️","file cabinet" }, /*3921 : Objects*/
{"🗄","file cabinet" }, /*3922 : Objects*/
{"🗑️","wastebasket" }, /*3923 : Objects*/
{"🗑","wastebasket" }, /*3924 : Objects*/
{"🔒","locked" }, /*3925 : Objects*/
{"🔓","unlocked" }, /*3926 : Objects*/
{"🔏","locked with pen" }, /*3927 : Objects*/
{"🔐","locked with key" }, /*3928 : Objects*/
{"🔑","key" }, /*3929 : Objects*/
{"🗝️","old key" }, /*3930 : Objects*/
{"🗝","old key" }, /*3931 : Objects*/
{"🔨","hammer" }, /*3932 : Objects*/
{"🪓","axe" }, /*3933 : Objects*/
{"⛏️","pick" }, /*3934 : Objects*/
{"⛏","pick" }, /*3935 : Objects*/
{"⚒️","hammer and pick" }, /*3936 : Objects*/
{"⚒","hammer and pick" }, /*3937 : Objects*/
{"🛠️","hammer and wrench" }, /*3938 : Objects*/
{"🛠","hammer and wrench" }, /*3939 : Objects*/
{"🗡️","dagger" }, /*3940 : Objects*/
{"🗡","dagger" }, /*3941 : Objects*/
{"⚔️","crossed swords" }, /*3942 : Objects*/
{"⚔","crossed swords" }, /*3943 : Objects*/
{"🔫","water pistol" }, /*3944 : Objects*/
{"🪃","boomerang" }, /*3945 : Objects*/
{"🏹","bow and arrow" }, /*3946 : Objects*/
{"🛡️","shield" }, /*3947 : Objects*/
{"🛡","shield" }, /*3948 : Objects*/
{"🪚","carpentry saw" }, /*3949 : Objects*/
{"🔧","wrench" }, /*3950 : Objects*/
{"🪛","screwdriver" }, /*3951 : Objects*/
{"🔩","nut and bolt" }, /*3952 : Objects*/
{"⚙️","gear" }, /*3953 : Objects*/
{"⚙","gear" }, /*3954 : Objects*/
{"🗜️","clamp" }, /*3955 : Objects*/
{"🗜","clamp" }, /*3956 : Objects*/
{"⚖️","balance scale" }, /*3957 : Objects*/
{"⚖","balance scale" }, /*3958 : Objects*/
{"🦯","white cane" }, /*3959 : Objects*/
{"🔗","link" }, /*3960 : Objects*/
{"⛓️","chains" }, /*3961 : Objects*/
{"⛓","chains" }, /*3962 : Objects*/
{"🪝","hook" }, /*3963 : Objects*/
{"🧰","toolbox" }, /*3964 : Objects*/
{"🧲","magnet" }, /*3965 : Objects*/
{"🪜","ladder" }, /*3966 : Objects*/
{"⚗️","alembic" }, /*3967 : Objects*/
{"⚗","alembic" }, /*3968 : Objects*/
{"🧪","test tube" }, /*3969 : Objects*/
{"🧫","petri dish" }, /*3970 : Objects*/
{"🧬","dna" }, /*3971 : Objects*/
{"🔬","microscope" }, /*3972 : Objects*/
{"🔭","telescope" }, /*3973 : Objects*/
{"📡","satellite antenna" }, /*3974 : Objects*/
{"💉","syringe" }, /*3975 : Objects*/
{"🩸","drop of blood" }, /*3976 : Objects*/
{"💊","pill" }, /*3977 : Objects*/
{"🩹","adhesive bandage" }, /*3978 : Objects*/
{"🩺","stethoscope" }, /*3979 : Objects*/
{"🚪","door" }, /*3980 : Objects*/
{"🛗","elevator" }, /*3981 : Objects*/
{"🪞","mirror" }, /*3982 : Objects*/
{"🪟","window" }, /*3983 : Objects*/
{"🛏️","bed" }, /*3984 : Objects*/
{"🛏","bed" }, /*3985 : Objects*/
{"🛋️","couch and lamp" }, /*3986 : Objects*/
{"🛋","couch and lamp" }, /*3987 : Objects*/
{"🪑","chair" }, /*3988 : Objects*/
{"🚽","toilet" }, /*3989 : Objects*/
{"🪠","plunger" }, /*3990 : Objects*/
{"🚿","shower" }, /*3991 : Objects*/
{"🛁","bathtub" }, /*3992 : Objects*/
{"🪤","mouse trap" }, /*3993 : Objects*/
{"🪒","razor" }, /*3994 : Objects*/
{"🧴","lotion bottle" }, /*3995 : Objects*/
{"🧷","safety pin" }, /*3996 : Objects*/
{"🧹","broom" }, /*3997 : Objects*/
{"🧺","basket" }, /*3998 : Objects*/
{"🧻","roll of paper" }, /*3999 : Objects*/
{"🪣","bucket" }, /*4000 : Objects*/
{"🧼","soap" }, /*4001 : Objects*/
{"🪥","toothbrush" }, /*4002 : Objects*/
{"🧽","sponge" }, /*4003 : Objects*/
{"🧯","fire extinguisher" }, /*4004 : Objects*/
{"🛒","shopping cart" }, /*4005 : Objects*/
{"🚬","cigarette" }, /*4006 : Objects*/
{"⚰️","coffin" }, /*4007 : Objects*/
{"⚰","coffin" }, /*4008 : Objects*/
{"🪦","headstone" }, /*4009 : Objects*/
{"⚱️","funeral urn" }, /*4010 : Objects*/
{"⚱","funeral urn" }, /*4011 : Objects*/
{"🗿","moai" }, /*4012 : Objects*/
{"🪧","placard" }, /*4013 : Objects*/
{"🏧","ATM sign" }, /*4014 : Symbols*/
{"🚮","litter in bin sign" }, /*4015 : Symbols*/
{"🚰","potable water" }, /*4016 : Symbols*/
{"♿","wheelchair symbol" }, /*4017 : Symbols*/
{"🚹","men’s room" }, /*4018 : Symbols*/
{"🚺","women’s room" }, /*4019 : Symbols*/
{"🚻","restroom" }, /*4020 : Symbols*/
{"🚼","baby symbol" }, /*4021 : Symbols*/
{"🚾","water closet" }, /*4022 : Symbols*/
{"🛂","passport control" }, /*4023 : Symbols*/
{"🛃","customs" }, /*4024 : Symbols*/
{"🛄","baggage claim" }, /*4025 : Symbols*/
{"🛅","left luggage" }, /*4026 : Symbols*/
{"⚠️","warning" }, /*4027 : Symbols*/
{"⚠","warning" }, /*4028 : Symbols*/
{"🚸","children crossing" }, /*4029 : Symbols*/
{"⛔","no entry" }, /*4030 : Symbols*/
{"🚫","prohibited" }, /*4031 : Symbols*/
{"🚳","no bicycles" }, /*4032 : Symbols*/
{"🚭","no smoking" }, /*4033 : Symbols*/
{"🚯","no littering" }, /*4034 : Symbols*/
{"🚱","non-potable water" }, /*4035 : Symbols*/
{"🚷","no pedestrians" }, /*4036 : Symbols*/
{"📵","no mobile phones" }, /*4037 : Symbols*/
{"🔞","no one under eighteen" }, /*4038 : Symbols*/
{"☢️","radioactive" }, /*4039 : Symbols*/
{"☢","radioactive" }, /*4040 : Symbols*/
{"☣️","biohazard" }, /*4041 : Symbols*/
{"☣","biohazard" }, /*4042 : Symbols*/
{"⬆️","up arrow" }, /*4043 : Symbols*/
{"⬆","up arrow" }, /*4044 : Symbols*/
{"↗️","up-right arrow" }, /*4045 : Symbols*/
{"↗","up-right arrow" }, /*4046 : Symbols*/
{"➡️","right arrow" }, /*4047 : Symbols*/
{"➡","right arrow" }, /*4048 : Symbols*/
{"↘️","down-right arrow" }, /*4049 : Symbols*/
{"↘","down-right arrow" }, /*4050 : Symbols*/
{"⬇️","down arrow" }, /*4051 : Symbols*/
{"⬇","down arrow" }, /*4052 : Symbols*/
{"↙️","down-left arrow" }, /*4053 : Symbols*/
{"↙","down-left arrow" }, /*4054 : Symbols*/
{"⬅️","left arrow" }, /*4055 : Symbols*/
{"⬅","left arrow" }, /*4056 : Symbols*/
{"↖️","up-left arrow" }, /*4057 : Symbols*/
{"↖","up-left arrow" }, /*4058 : Symbols*/
{"↕️","up-down arrow" }, /*4059 : Symbols*/
{"↕","up-down arrow" }, /*4060 : Symbols*/
{"↔️","left-right arrow" }, /*4061 : Symbols*/
{"↔","left-right arrow" }, /*4062 : Symbols*/
{"↩️","right arrow curving left" }, /*4063 : Symbols*/
{"↩","right arrow curving left" }, /*4064 : Symbols*/
{"↪️","left arrow curving right" }, /*4065 : Symbols*/
{"↪","left arrow curving right" }, /*4066 : Symbols*/
{"⤴️","right arrow curving up" }, /*4067 : Symbols*/
{"⤴","right arrow curving up" }, /*4068 : Symbols*/
{"⤵️","right arrow curving down" }, /*4069 : Symbols*/
{"⤵","right arrow curving down" }, /*4070 : Symbols*/
{"🔃","clockwise vertical arrows" }, /*4071 : Symbols*/
{"🔄","counterclockwise arrows button" }, /*4072 : Symbols*/
{"🔙","BACK arrow" }, /*4073 : Symbols*/
{"🔚","END arrow" }, /*4074 : Symbols*/
{"🔛","ON! arrow" }, /*4075 : Symbols*/
{"🔜","SOON arrow" }, /*4076 : Symbols*/
{"🔝","TOP arrow" }, /*4077 : Symbols*/
{"🛐","place of worship" }, /*4078 : Symbols*/
{"⚛️","atom symbol" }, /*4079 : Symbols*/
{"⚛","atom symbol" }, /*4080 : Symbols*/
{"🕉️","om" }, /*4081 : Symbols*/
{"🕉","om" }, /*4082 : Symbols*/
{"✡️","star of David" }, /*4083 : Symbols*/
{"✡","star of David" }, /*4084 : Symbols*/
{"☸️","wheel of dharma" }, /*4085 : Symbols*/
{"☸","wheel of dharma" }, /*4086 : Symbols*/
{"☯️","yin yang" }, /*4087 : Symbols*/
{"☯","yin yang" }, /*4088 : Symbols*/
{"✝️","latin cross" }, /*4089 : Symbols*/
{"✝","latin cross" }, /*4090 : Symbols*/
{"☦️","orthodox cross" }, /*4091 : Symbols*/
{"☦","orthodox cross" }, /*4092 : Symbols*/
{"☪️","star and crescent" }, /*4093 : Symbols*/
{"☪","star and crescent" }, /*4094 : Symbols*/
{"☮️","peace symbol" }, /*4095 : Symbols*/
{"☮","peace symbol" }, /*4096 : Symbols*/
{"🕎","menorah" }, /*4097 : Symbols*/
{"🔯","dotted six-pointed star" }, /*4098 : Symbols*/
{"♈","Aries" }, /*4099 : Symbols*/
{"♉","Taurus" }, /*4100 : Symbols*/
{"♊","Gemini" }, /*4101 : Symbols*/
{"♋","Cancer" }, /*4102 : Symbols*/
{"♌","Leo" }, /*4103 : Symbols*/
{"♍","Virgo" }, /*4104 : Symbols*/
{"♎","Libra" }, /*4105 : Symbols*/
{"♏","Scorpio" }, /*4106 : Symbols*/
{"♐","Sagittarius" }, /*4107 : Symbols*/
{"♑","Capricorn" }, /*4108 : Symbols*/
{"♒","Aquarius" }, /*4109 : Symbols*/
{"♓","Pisces" }, /*4110 : Symbols*/
{"⛎","Ophiuchus" }, /*4111 : Symbols*/
{"🔀","shuffle tracks button" }, /*4112 : Symbols*/
{"🔁","repeat button" }, /*4113 : Symbols*/
{"🔂","repeat single button" }, /*4114 : Symbols*/
{"▶️","play button" }, /*4115 : Symbols*/
{"▶","play button" }, /*4116 : Symbols*/
{"⏩","fast-forward button" }, /*4117 : Symbols*/
{"⏭️","next track button" }, /*4118 : Symbols*/
{"⏭","next track button" }, /*4119 : Symbols*/
{"⏯️","play or pause button" }, /*4120 : Symbols*/
{"⏯","play or pause button" }, /*4121 : Symbols*/
{"◀️","reverse button" }, /*4122 : Symbols*/
{"◀","reverse button" }, /*4123 : Symbols*/
{"⏪","fast reverse button" }, /*4124 : Symbols*/
{"⏮️","last track button" }, /*4125 : Symbols*/
{"⏮","last track button" }, /*4126 : Symbols*/
{"🔼","upwards button" }, /*4127 : Symbols*/
{"⏫","fast up button" }, /*4128 : Symbols*/
{"🔽","downwards button" }, /*4129 : Symbols*/
{"⏬","fast down button" }, /*4130 : Symbols*/
{"⏸️","pause button" }, /*4131 : Symbols*/
{"⏸","pause button" }, /*4132 : Symbols*/
{"⏹️","stop button" }, /*4133 : Symbols*/
{"⏹","stop button" }, /*4134 : Symbols*/
{"⏺️","record button" }, /*4135 : Symbols*/
{"⏺","record button" }, /*4136 : Symbols*/
{"⏏️","eject button" }, /*4137 : Symbols*/
{"⏏","eject button" }, /*4138 : Symbols*/
{"🎦","cinema" }, /*4139 : Symbols*/
{"🔅","dim button" }, /*4140 : Symbols*/
{"🔆","bright button" }, /*4141 : Symbols*/
{"📶","antenna bars" }, /*4142 : Symbols*/
{"📳","vibration mode" }, /*4143 : Symbols*/
{"📴","mobile phone off" }, /*4144 : Symbols*/
{"♀️","female sign" }, /*4145 : Symbols*/
{"♀","female sign" }, /*4146 : Symbols*/
{"♂️","male sign" }, /*4147 : Symbols*/
{"♂","male sign" }, /*4148 : Symbols*/
{"⚧️","transgender symbol" }, /*4149 : Symbols*/
{"⚧","transgender symbol" }, /*4150 : Symbols*/
{"✖️","multiply" }, /*4151 : Symbols*/
{"✖","multiply" }, /*4152 : Symbols*/
{"➕","plus" }, /*4153 : Symbols*/
{"➖","minus" }, /*4154 : Symbols*/
{"➗","divide" }, /*4155 : Symbols*/
{"♾️","infinity" }, /*4156 : Symbols*/
{"♾","infinity" }, /*4157 : Symbols*/
{"‼️","double exclamation mark" }, /*4158 : Symbols*/
{"‼","double exclamation mark" }, /*4159 : Symbols*/
{"⁉️","exclamation question mark" }, /*4160 : Symbols*/
{"⁉","exclamation question mark" }, /*4161 : Symbols*/
{"❓","red question mark" }, /*4162 : Symbols*/
{"❔","white question mark" }, /*4163 : Symbols*/
{"❕","white exclamation mark" }, /*4164 : Symbols*/
{"❗","red exclamation mark" }, /*4165 : Symbols*/
{"〰️","wavy dash" }, /*4166 : Symbols*/
{"〰","wavy dash" }, /*4167 : Symbols*/
{"💱","currency exchange" }, /*4168 : Symbols*/
{"💲","heavy dollar sign" }, /*4169 : Symbols*/
{"⚕️","medical symbol" }, /*4170 : Symbols*/
{"⚕","medical symbol" }, /*4171 : Symbols*/
{"♻️","recycling symbol" }, /*4172 : Symbols*/
{"♻","recycling symbol" }, /*4173 : Symbols*/
{"⚜️","fleur-de-lis" }, /*4174 : Symbols*/
{"⚜","fleur-de-lis" }, /*4175 : Symbols*/
{"🔱","trident emblem" }, /*4176 : Symbols*/
{"📛","name badge" }, /*4177 : Symbols*/
{"🔰","Japanese symbol for beginner" }, /*4178 : Symbols*/
{"⭕","hollow red circle" }, /*4179 : Symbols*/
{"✅","check mark button" }, /*4180 : Symbols*/
{"☑️","check box with check" }, /*4181 : Symbols*/
{"☑","check box with check" }, /*4182 : Symbols*/
{"✔️","check mark" }, /*4183 : Symbols*/
{"✔","check mark" }, /*4184 : Symbols*/
{"❌","cross mark" }, /*4185 : Symbols*/
{"❎","cross mark button" }, /*4186 : Symbols*/
{"➰","curly loop" }, /*4187 : Symbols*/
{"➿","double curly loop" }, /*4188 : Symbols*/
{"〽️","part alternation mark" }, /*4189 : Symbols*/
{"〽","part alternation mark" }, /*4190 : Symbols*/
{"✳️","eight-spoked asterisk" }, /*4191 : Symbols*/
{"✳","eight-spoked asterisk" }, /*4192 : Symbols*/
{"✴️","eight-pointed star" }, /*4193 : Symbols*/
{"✴","eight-pointed star" }, /*4194 : Symbols*/
{"❇️","sparkle" }, /*4195 : Symbols*/
{"❇","sparkle" }, /*4196 : Symbols*/
{"©️","copyright" }, /*4197 : Symbols*/
{"©","copyright" }, /*4198 : Symbols*/
{"®️","registered" }, /*4199 : Symbols*/
{"®","registered" }, /*4200 : Symbols*/
{"™️","trade mark" }, /*4201 : Symbols*/
{"™","trade mark" }, /*4202 : Symbols*/
{"#️⃣","keycap: #" }, /*4203 : Symbols*/
{"#⃣","keycap: #" }, /*4204 : Symbols*/
{"*️⃣","keycap: *" }, /*4205 : Symbols*/
{"*⃣","keycap: *" }, /*4206 : Symbols*/
{"0️⃣","keycap: 0" }, /*4207 : Symbols*/
{"0⃣","keycap: 0" }, /*4208 : Symbols*/
{"1️⃣","keycap: 1" }, /*4209 : Symbols*/
{"1⃣","keycap: 1" }, /*4210 : Symbols*/
{"2️⃣","keycap: 2" }, /*4211 : Symbols*/
{"2⃣","keycap: 2" }, /*4212 : Symbols*/
{"3️⃣","keycap: 3" }, /*4213 : Symbols*/
{"3⃣","keycap: 3" }, /*4214 : Symbols*/
{"4️⃣","keycap: 4" }, /*4215 : Symbols*/
{"4⃣","keycap: 4" }, /*4216 : Symbols*/
{"5️⃣","keycap: 5" }, /*4217 : Symbols*/
{"5⃣","keycap: 5" }, /*4218 : Symbols*/
{"6️⃣","keycap: 6" }, /*4219 : Symbols*/
{"6⃣","keycap: 6" }, /*4220 : Symbols*/
{"7️⃣","keycap: 7" }, /*4221 : Symbols*/
{"7⃣","keycap: 7" }, /*4222 : Symbols*/
{"8️⃣","keycap: 8" }, /*4223 : Symbols*/
{"8⃣","keycap: 8" }, /*4224 : Symbols*/
{"9️⃣","keycap: 9" }, /*4225 : Symbols*/
{"9⃣","keycap: 9" }, /*4226 : Symbols*/
{"🔟","keycap: 10" }, /*4227 : Symbols*/
{"🔠","input latin uppercase" }, /*4228 : Symbols*/
{"🔡","input latin lowercase" }, /*4229 : Symbols*/
{"🔢","input numbers" }, /*4230 : Symbols*/
{"🔣","input symbols" }, /*4231 : Symbols*/
{"🔤","input latin letters" }, /*4232 : Symbols*/
{"🅰️","A button (blood type)" }, /*4233 : Symbols*/
{"🅰","A button (blood type)" }, /*4234 : Symbols*/
{"🆎","AB button (blood type)" }, /*4235 : Symbols*/
{"🅱️","B button (blood type)" }, /*4236 : Symbols*/
{"🅱","B button (blood type)" }, /*4237 : Symbols*/
{"🆑","CL button" }, /*4238 : Symbols*/
{"🆒","COOL button" }, /*4239 : Symbols*/
{"🆓","FREE button" }, /*4240 : Symbols*/
{"ℹ️","information" }, /*4241 : Symbols*/
{"ℹ","information" }, /*4242 : Symbols*/
{"🆔","ID button" }, /*4243 : Symbols*/
{"Ⓜ️","circled M" }, /*4244 : Symbols*/
{"Ⓜ","circled M" }, /*4245 : Symbols*/
{"🆕","NEW button" }, /*4246 : Symbols*/
{"🆖","NG button" }, /*4247 : Symbols*/
{"🅾️","O button (blood type)" }, /*4248 : Symbols*/
{"🅾","O button (blood type)" }, /*4249 : Symbols*/
{"🆗","OK button" }, /*4250 : Symbols*/
{"🅿️","P button" }, /*4251 : Symbols*/
{"🅿","P button" }, /*4252 : Symbols*/
{"🆘","SOS button" }, /*4253 : Symbols*/
{"🆙","UP! button" }, /*4254 : Symbols*/
{"🆚","VS button" }, /*4255 : Symbols*/
{"🈁","Japanese “here” button" }, /*4256 : Symbols*/
{"🈂️","Japanese “service charge” button" }, /*4257 : Symbols*/
{"🈂","Japanese “service charge” button" }, /*4258 : Symbols*/
{"🈷️","Japanese “monthly amount” button" }, /*4259 : Symbols*/
{"🈷","Japanese “monthly amount” button" }, /*4260 : Symbols*/
{"🈶","Japanese “not free of charge” button" }, /*4261 : Symbols*/
{"🈯","Japanese “reserved” button" }, /*4262 : Symbols*/
{"🉐","Japanese “bargain” button" }, /*4263 : Symbols*/
{"🈹","Japanese “discount” button" }, /*4264 : Symbols*/
{"🈚","Japanese “free of charge” button" }, /*4265 : Symbols*/
{"🈲","Japanese “prohibited” button" }, /*4266 : Symbols*/
{"🉑","Japanese “acceptable” button" }, /*4267 : Symbols*/
{"🈸","Japanese “application” button" }, /*4268 : Symbols*/
{"🈴","Japanese “passing grade” button" }, /*4269 : Symbols*/
{"🈳","Japanese “vacancy” button" }, /*4270 : Symbols*/
{"㊗️","Japanese “congratulations” button" }, /*4271 : Symbols*/
{"㊗","Japanese “congratulations” button" }, /*4272 : Symbols*/
{"㊙️","Japanese “secret” button" }, /*4273 : Symbols*/
{"㊙","Japanese “secret” button" }, /*4274 : Symbols*/
{"🈺","Japanese “open for business” button" }, /*4275 : Symbols*/
{"🈵","Japanese “no vacancy” button" }, /*4276 : Symbols*/
{"🔴","red circle" }, /*4277 : Symbols*/
{"🟠","orange circle" }, /*4278 : Symbols*/
{"🟡","yellow circle" }, /*4279 : Symbols*/
{"🟢","green circle" }, /*4280 : Symbols*/
{"🔵","blue circle" }, /*4281 : Symbols*/
{"🟣","purple circle" }, /*4282 : Symbols*/
{"🟤","brown circle" }, /*4283 : Symbols*/
{"⚫","black circle" }, /*4284 : Symbols*/
{"⚪","white circle" }, /*4285 : Symbols*/
{"🟥","red square" }, /*4286 : Symbols*/
{"🟧","orange square" }, /*4287 : Symbols*/
{"🟨","yellow square" }, /*4288 : Symbols*/
{"🟩","green square" }, /*4289 : Symbols*/
{"🟦","blue square" }, /*4290 : Symbols*/
{"🟪","purple square" }, /*4291 : Symbols*/
{"🟫","brown square" }, /*4292 : Symbols*/
{"⬛","black large square" }, /*4293 : Symbols*/
{"⬜","white large square" }, /*4294 : Symbols*/
{"◼️","black medium square" }, /*4295 : Symbols*/
{"◼","black medium square" }, /*4296 : Symbols*/
{"◻️","white medium square" }, /*4297 : Symbols*/
{"◻","white medium square" }, /*4298 : Symbols*/
{"◾","black medium-small square" }, /*4299 : Symbols*/
{"◽","white medium-small square" }, /*4300 : Symbols*/
{"▪️","black small square" }, /*4301 : Symbols*/
{"▪","black small square" }, /*4302 : Symbols*/
{"▫️","white small square" }, /*4303 : Symbols*/
{"▫","white small square" }, /*4304 : Symbols*/
{"🔶","large orange diamond" }, /*4305 : Symbols*/
{"🔷","large blue diamond" }, /*4306 : Symbols*/
{"🔸","small orange diamond" }, /*4307 : Symbols*/
{"🔹","small blue diamond" }, /*4308 : Symbols*/
{"🔺","red triangle pointed up" }, /*4309 : Symbols*/
{"🔻","red triangle pointed down" }, /*4310 : Symbols*/
{"💠","diamond with a dot" }, /*4311 : Symbols*/
{"🔘","radio button" }, /*4312 : Symbols*/
{"🔳","white square button" }, /*4313 : Symbols*/
{"🔲","black square button" }, /*4314 : Symbols*/
{"🏁","chequered flag" }, /*4315 : Flags*/
{"🚩","triangular flag" }, /*4316 : Flags*/
{"🎌","crossed flags" }, /*4317 : Flags*/
{"🏴","black flag" }, /*4318 : Flags*/
{"🏳️","white flag" }, /*4319 : Flags*/
{"🏳","white flag" }, /*4320 : Flags*/
{"🏳️‍🌈","rainbow flag" }, /*4321 : Flags*/
{"🏳‍🌈","rainbow flag" }, /*4322 : Flags*/
{"🏳️‍⚧️","transgender flag" }, /*4323 : Flags*/
{"🏳‍⚧️","transgender flag" }, /*4324 : Flags*/
{"🏳️‍⚧","transgender flag" }, /*4325 : Flags*/
{"🏳‍⚧","transgender flag" }, /*4326 : Flags*/
{"🏴‍☠️","pirate flag" }, /*4327 : Flags*/
{"🏴‍☠","pirate flag" }, /*4328 : Flags*/
{"🇦🇨","flag: Ascension Island" }, /*4329 : Flags*/
{"🇦🇩","flag: Andorra" }, /*4330 : Flags*/
{"🇦🇪","flag: United Arab Emirates" }, /*4331 : Flags*/
{"🇦🇫","flag: Afghanistan" }, /*4332 : Flags*/
{"🇦🇬","flag: Antigua & Barbuda" }, /*4333 : Flags*/
{"🇦🇮","flag: Anguilla" }, /*4334 : Flags*/
{"🇦🇱","flag: Albania" }, /*4335 : Flags*/
{"🇦🇲","flag: Armenia" }, /*4336 : Flags*/
{"🇦🇴","flag: Angola" }, /*4337 : Flags*/
{"🇦🇶","flag: Antarctica" }, /*4338 : Flags*/
{"🇦🇷","flag: Argentina" }, /*4339 : Flags*/
{"🇦🇸","flag: American Samoa" }, /*4340 : Flags*/
{"🇦🇹","flag: Austria" }, /*4341 : Flags*/
{"🇦🇺","flag: Australia" }, /*4342 : Flags*/
{"🇦🇼","flag: Aruba" }, /*4343 : Flags*/
{"🇦🇽","flag: Åland Islands" }, /*4344 : Flags*/
{"🇦🇿","flag: Azerbaijan" }, /*4345 : Flags*/
{"🇧🇦","flag: Bosnia & Herzegovina" }, /*4346 : Flags*/
{"🇧🇧","flag: Barbados" }, /*4347 : Flags*/
{"🇧🇩","flag: Bangladesh" }, /*4348 : Flags*/
{"🇧🇪","flag: Belgium" }, /*4349 : Flags*/
{"🇧🇫","flag: Burkina Faso" }, /*4350 : Flags*/
{"🇧🇬","flag: Bulgaria" }, /*4351 : Flags*/
{"🇧🇭","flag: Bahrain" }, /*4352 : Flags*/
{"🇧🇮","flag: Burundi" }, /*4353 : Flags*/
{"🇧🇯","flag: Benin" }, /*4354 : Flags*/
{"🇧🇱","flag: St. Barthélemy" }, /*4355 : Flags*/
{"🇧🇲","flag: Bermuda" }, /*4356 : Flags*/
{"🇧🇳","flag: Brunei" }, /*4357 : Flags*/
{"🇧🇴","flag: Bolivia" }, /*4358 : Flags*/
{"🇧🇶","flag: Caribbean Netherlands" }, /*4359 : Flags*/
{"🇧🇷","flag: Brazil" }, /*4360 : Flags*/
{"🇧🇸","flag: Bahamas" }, /*4361 : Flags*/
{"🇧🇹","flag: Bhutan" }, /*4362 : Flags*/
{"🇧🇻","flag: Bouvet Island" }, /*4363 : Flags*/
{"🇧🇼","flag: Botswana" }, /*4364 : Flags*/
{"🇧🇾","flag: Belarus" }, /*4365 : Flags*/
{"🇧🇿","flag: Belize" }, /*4366 : Flags*/
{"🇨🇦","flag: Canada" }, /*4367 : Flags*/
{"🇨🇨","flag: Cocos (Keeling) Islands" }, /*4368 : Flags*/
{"🇨🇩","flag: Congo - Kinshasa" }, /*4369 : Flags*/
{"🇨🇫","flag: Central African Republic" }, /*4370 : Flags*/
{"🇨🇬","flag: Congo - Brazzaville" }, /*4371 : Flags*/
{"🇨🇭","flag: Switzerland" }, /*4372 : Flags*/
{"🇨🇮","flag: Côte d’Ivoire" }, /*4373 : Flags*/
{"🇨🇰","flag: Cook Islands" }, /*4374 : Flags*/
{"🇨🇱","flag: Chile" }, /*4375 : Flags*/
{"🇨🇲","flag: Cameroon" }, /*4376 : Flags*/
{"🇨🇳","flag: China" }, /*4377 : Flags*/
{"🇨🇴","flag: Colombia" }, /*4378 : Flags*/
{"🇨🇵","flag: Clipperton Island" }, /*4379 : Flags*/
{"🇨🇷","flag: Costa Rica" }, /*4380 : Flags*/
{"🇨🇺","flag: Cuba" }, /*4381 : Flags*/
{"🇨🇻","flag: Cape Verde" }, /*4382 : Flags*/
{"🇨🇼","flag: Curaçao" }, /*4383 : Flags*/
{"🇨🇽","flag: Christmas Island" }, /*4384 : Flags*/
{"🇨🇾","flag: Cyprus" }, /*4385 : Flags*/
{"🇨🇿","flag: Czechia" }, /*4386 : Flags*/
{"🇩🇪","flag: Germany" }, /*4387 : Flags*/
{"🇩🇬","flag: Diego Garcia" }, /*4388 : Flags*/
{"🇩🇯","flag: Djibouti" }, /*4389 : Flags*/
{"🇩🇰","flag: Denmark" }, /*4390 : Flags*/
{"🇩🇲","flag: Dominica" }, /*4391 : Flags*/
{"🇩🇴","flag: Dominican Republic" }, /*4392 : Flags*/
{"🇩🇿","flag: Algeria" }, /*4393 : Flags*/
{"🇪🇦","flag: Ceuta & Melilla" }, /*4394 : Flags*/
{"🇪🇨","flag: Ecuador" }, /*4395 : Flags*/
{"🇪🇪","flag: Estonia" }, /*4396 : Flags*/
{"🇪🇬","flag: Egypt" }, /*4397 : Flags*/
{"🇪🇭","flag: Western Sahara" }, /*4398 : Flags*/
{"🇪🇷","flag: Eritrea" }, /*4399 : Flags*/
{"🇪🇸","flag: Spain" }, /*4400 : Flags*/
{"🇪🇹","flag: Ethiopia" }, /*4401 : Flags*/
{"🇪🇺","flag: European Union" }, /*4402 : Flags*/
{"🇫🇮","flag: Finland" }, /*4403 : Flags*/
{"🇫🇯","flag: Fiji" }, /*4404 : Flags*/
{"🇫🇰","flag: Falkland Islands" }, /*4405 : Flags*/
{"🇫🇲","flag: Micronesia" }, /*4406 : Flags*/
{"🇫🇴","flag: Faroe Islands" }, /*4407 : Flags*/
{"🇫🇷","flag: France" }, /*4408 : Flags*/
{"🇬🇦","flag: Gabon" }, /*4409 : Flags*/
{"🇬🇧","flag: United Kingdom" }, /*4410 : Flags*/
{"🇬🇩","flag: Grenada" }, /*4411 : Flags*/
{"🇬🇪","flag: Georgia" }, /*4412 : Flags*/
{"🇬🇫","flag: French Guiana" }, /*4413 : Flags*/
{"🇬🇬","flag: Guernsey" }, /*4414 : Flags*/
{"🇬🇭","flag: Ghana" }, /*4415 : Flags*/
{"🇬🇮","flag: Gibraltar" }, /*4416 : Flags*/
{"🇬🇱","flag: Greenland" }, /*4417 : Flags*/
{"🇬🇲","flag: Gambia" }, /*4418 : Flags*/
{"🇬🇳","flag: Guinea" }, /*4419 : Flags*/
{"🇬🇵","flag: Guadeloupe" }, /*4420 : Flags*/
{"🇬🇶","flag: Equatorial Guinea" }, /*4421 : Flags*/
{"🇬🇷","flag: Greece" }, /*4422 : Flags*/
{"🇬🇸","flag: South Georgia & South Sandwich Islands" }, /*4423 : Flags*/
{"🇬🇹","flag: Guatemala" }, /*4424 : Flags*/
{"🇬🇺","flag: Guam" }, /*4425 : Flags*/
{"🇬🇼","flag: Guinea-Bissau" }, /*4426 : Flags*/
{"🇬🇾","flag: Guyana" }, /*4427 : Flags*/
{"🇭🇰","flag: Hong Kong SAR China" }, /*4428 : Flags*/
{"🇭🇲","flag: Heard & McDonald Islands" }, /*4429 : Flags*/
{"🇭🇳","flag: Honduras" }, /*4430 : Flags*/
{"🇭🇷","flag: Croatia" }, /*4431 : Flags*/
{"🇭🇹","flag: Haiti" }, /*4432 : Flags*/
{"🇭🇺","flag: Hungary" }, /*4433 : Flags*/
{"🇮🇨","flag: Canary Islands" }, /*4434 : Flags*/
{"🇮🇩","flag: Indonesia" }, /*4435 : Flags*/
{"🇮🇪","flag: Ireland" }, /*4436 : Flags*/
{"🇮🇱","flag: Israel" }, /*4437 : Flags*/
{"🇮🇲","flag: Isle of Man" }, /*4438 : Flags*/
{"🇮🇳","flag: India" }, /*4439 : Flags*/
{"🇮🇴","flag: British Indian Ocean Territory" }, /*4440 : Flags*/
{"🇮🇶","flag: Iraq" }, /*4441 : Flags*/
{"🇮🇷","flag: Iran" }, /*4442 : Flags*/
{"🇮🇸","flag: Iceland" }, /*4443 : Flags*/
{"🇮🇹","flag: Italy" }, /*4444 : Flags*/
{"🇯🇪","flag: Jersey" }, /*4445 : Flags*/
{"🇯🇲","flag: Jamaica" }, /*4446 : Flags*/
{"🇯🇴","flag: Jordan" }, /*4447 : Flags*/
{"🇯🇵","flag: Japan" }, /*4448 : Flags*/
{"🇰🇪","flag: Kenya" }, /*4449 : Flags*/
{"🇰🇬","flag: Kyrgyzstan" }, /*4450 : Flags*/
{"🇰🇭","flag: Cambodia" }, /*4451 : Flags*/
{"🇰🇮","flag: Kiribati" }, /*4452 : Flags*/
{"🇰🇲","flag: Comoros" }, /*4453 : Flags*/
{"🇰🇳","flag: St. Kitts & Nevis" }, /*4454 : Flags*/
{"🇰🇵","flag: North Korea" }, /*4455 : Flags*/
{"🇰🇷","flag: South Korea" }, /*4456 : Flags*/
{"🇰🇼","flag: Kuwait" }, /*4457 : Flags*/
{"🇰🇾","flag: Cayman Islands" }, /*4458 : Flags*/
{"🇰🇿","flag: Kazakhstan" }, /*4459 : Flags*/
{"🇱🇦","flag: Laos" }, /*4460 : Flags*/
{"🇱🇧","flag: Lebanon" }, /*4461 : Flags*/
{"🇱🇨","flag: St. Lucia" }, /*4462 : Flags*/
{"🇱🇮","flag: Liechtenstein" }, /*4463 : Flags*/
{"🇱🇰","flag: Sri Lanka" }, /*4464 : Flags*/
{"🇱🇷","flag: Liberia" }, /*4465 : Flags*/
{"🇱🇸","flag: Lesotho" }, /*4466 : Flags*/
{"🇱🇹","flag: Lithuania" }, /*4467 : Flags*/
{"🇱🇺","flag: Luxembourg" }, /*4468 : Flags*/
{"🇱🇻","flag: Latvia" }, /*4469 : Flags*/
{"🇱🇾","flag: Libya" }, /*4470 : Flags*/
{"🇲🇦","flag: Morocco" }, /*4471 : Flags*/
{"🇲🇨","flag: Monaco" }, /*4472 : Flags*/
{"🇲🇩","flag: Moldova" }, /*4473 : Flags*/
{"🇲🇪","flag: Montenegro" }, /*4474 : Flags*/
{"🇲🇫","flag: St. Martin" }, /*4475 : Flags*/
{"🇲🇬","flag: Madagascar" }, /*4476 : Flags*/
{"🇲🇭","flag: Marshall Islands" }, /*4477 : Flags*/
{"🇲🇰","flag: North Macedonia" }, /*4478 : Flags*/
{"🇲🇱","flag: Mali" }, /*4479 : Flags*/
{"🇲🇲","flag: Myanmar (Burma)" }, /*4480 : Flags*/
{"🇲🇳","flag: Mongolia" }, /*4481 : Flags*/
{"🇲🇴","flag: Macao SAR China" }, /*4482 : Flags*/
{"🇲🇵","flag: Northern Mariana Islands" }, /*4483 : Flags*/
{"🇲🇶","flag: Martinique" }, /*4484 : Flags*/
{"🇲🇷","flag: Mauritania" }, /*4485 : Flags*/
{"🇲🇸","flag: Montserrat" }, /*4486 : Flags*/
{"🇲🇹","flag: Malta" }, /*4487 : Flags*/
{"🇲🇺","flag: Mauritius" }, /*4488 : Flags*/
{"🇲🇻","flag: Maldives" }, /*4489 : Flags*/
{"🇲🇼","flag: Malawi" }, /*4490 : Flags*/
{"🇲🇽","flag: Mexico" }, /*4491 : Flags*/
{"🇲🇾","flag: Malaysia" }, /*4492 : Flags*/
{"🇲🇿","flag: Mozambique" }, /*4493 : Flags*/
{"🇳🇦","flag: Namibia" }, /*4494 : Flags*/
{"🇳🇨","flag: New Caledonia" }, /*4495 : Flags*/
{"🇳🇪","flag: Niger" }, /*4496 : Flags*/
{"🇳🇫","flag: Norfolk Island" }, /*4497 : Flags*/
{"🇳🇬","flag: Nigeria" }, /*4498 : Flags*/
{"🇳🇮","flag: Nicaragua" }, /*4499 : Flags*/
{"🇳🇱","flag: Netherlands" }, /*4500 : Flags*/
{"🇳🇴","flag: Norway" }, /*4501 : Flags*/
{"🇳🇵","flag: Nepal" }, /*4502 : Flags*/
{"🇳🇷","flag: Nauru" }, /*4503 : Flags*/
{"🇳🇺","flag: Niue" }, /*4504 : Flags*/
{"🇳🇿","flag: New Zealand" }, /*4505 : Flags*/
{"🇴🇲","flag: Oman" }, /*4506 : Flags*/
{"🇵🇦","flag: Panama" }, /*4507 : Flags*/
{"🇵🇪","flag: Peru" }, /*4508 : Flags*/
{"🇵🇫","flag: French Polynesia" }, /*4509 : Flags*/
{"🇵🇬","flag: Papua New Guinea" }, /*4510 : Flags*/
{"🇵🇭","flag: Philippines" }, /*4511 : Flags*/
{"🇵🇰","flag: Pakistan" }, /*4512 : Flags*/
{"🇵🇱","flag: Poland" }, /*4513 : Flags*/
{"🇵🇲","flag: St. Pierre & Miquelon" }, /*4514 : Flags*/
{"🇵🇳","flag: Pitcairn Islands" }, /*4515 : Flags*/
{"🇵🇷","flag: Puerto Rico" }, /*4516 : Flags*/
{"🇵🇸","flag: Palestinian Territories" }, /*4517 : Flags*/
{"🇵🇹","flag: Portugal" }, /*4518 : Flags*/
{"🇵🇼","flag: Palau" }, /*4519 : Flags*/
{"🇵🇾","flag: Paraguay" }, /*4520 : Flags*/
{"🇶🇦","flag: Qatar" }, /*4521 : Flags*/
{"🇷🇪","flag: Réunion" }, /*4522 : Flags*/
{"🇷🇴","flag: Romania" }, /*4523 : Flags*/
{"🇷🇸","flag: Serbia" }, /*4524 : Flags*/
{"🇷🇺","flag: Russia" }, /*4525 : Flags*/
{"🇷🇼","flag: Rwanda" }, /*4526 : Flags*/
{"🇸🇦","flag: Saudi Arabia" }, /*4527 : Flags*/
{"🇸🇧","flag: Solomon Islands" }, /*4528 : Flags*/
{"🇸🇨","flag: Seychelles" }, /*4529 : Flags*/
{"🇸🇩","flag: Sudan" }, /*4530 : Flags*/
{"🇸🇪","flag: Sweden" }, /*4531 : Flags*/
{"🇸🇬","flag: Singapore" }, /*4532 : Flags*/
{"🇸🇭","flag: St. Helena" }, /*4533 : Flags*/
{"🇸🇮","flag: Slovenia" }, /*4534 : Flags*/
{"🇸🇯","flag: Svalbard & Jan Mayen" }, /*4535 : Flags*/
{"🇸🇰","flag: Slovakia" }, /*4536 : Flags*/
{"🇸🇱","flag: Sierra Leone" }, /*4537 : Flags*/
{"🇸🇲","flag: San Marino" }, /*4538 : Flags*/
{"🇸🇳","flag: Senegal" }, /*4539 : Flags*/
{"🇸🇴","flag: Somalia" }, /*4540 : Flags*/
{"🇸🇷","flag: Suriname" }, /*4541 : Flags*/
{"🇸🇸","flag: South Sudan" }, /*4542 : Flags*/
{"🇸🇹","flag: São Tomé & Príncipe" }, /*4543 : Flags*/
{"🇸🇻","flag: El Salvador" }, /*4544 : Flags*/
{"🇸🇽","flag: Sint Maarten" }, /*4545 : Flags*/
{"🇸🇾","flag: Syria" }, /*4546 : Flags*/
{"🇸🇿","flag: Eswatini" }, /*4547 : Flags*/
{"🇹🇦","flag: Tristan da Cunha" }, /*4548 : Flags*/
{"🇹🇨","flag: Turks & Caicos Islands" }, /*4549 : Flags*/
{"🇹🇩","flag: Chad" }, /*4550 : Flags*/
{"🇹🇫","flag: French Southern Territories" }, /*4551 : Flags*/
{"🇹🇬","flag: Togo" }, /*4552 : Flags*/
{"🇹🇭","flag: Thailand" }, /*4553 : Flags*/
{"🇹🇯","flag: Tajikistan" }, /*4554 : Flags*/
{"🇹🇰","flag: Tokelau" }, /*4555 : Flags*/
{"🇹🇱","flag: Timor-Leste" }, /*4556 : Flags*/
{"🇹🇲","flag: Turkmenistan" }, /*4557 : Flags*/
{"🇹🇳","flag: Tunisia" }, /*4558 : Flags*/
{"🇹🇴","flag: Tonga" }, /*4559 : Flags*/
{"🇹🇷","flag: Turkey" }, /*4560 : Flags*/
{"🇹🇹","flag: Trinidad & Tobago" }, /*4561 : Flags*/
{"🇹🇻","flag: Tuvalu" }, /*4562 : Flags*/
{"🇹🇼","flag: Taiwan" }, /*4563 : Flags*/
{"🇹🇿","flag: Tanzania" }, /*4564 : Flags*/
{"🇺🇦","flag: Ukraine" }, /*4565 : Flags*/
{"🇺🇬","flag: Uganda" }, /*4566 : Flags*/
{"🇺🇲","flag: U.S. Outlying Islands" }, /*4567 : Flags*/
{"🇺🇳","flag: United Nations" }, /*4568 : Flags*/
{"🇺🇸","flag: United States" }, /*4569 : Flags*/
{"🇺🇾","flag: Uruguay" }, /*4570 : Flags*/
{"🇺🇿","flag: Uzbekistan" }, /*4571 : Flags*/
{"🇻🇦","flag: Vatican City" }, /*4572 : Flags*/
{"🇻🇨","flag: St. Vincent & Grenadines" }, /*4573 : Flags*/
{"🇻🇪","flag: Venezuela" }, /*4574 : Flags*/
{"🇻🇬","flag: British Virgin Islands" }, /*4575 : Flags*/
{"🇻🇮","flag: U.S. Virgin Islands" }, /*4576 : Flags*/
{"🇻🇳","flag: Vietnam" }, /*4577 : Flags*/
{"🇻🇺","flag: Vanuatu" }, /*4578 : Flags*/
{"🇼🇫","flag: Wallis & Futuna" }, /*4579 : Flags*/
{"🇼🇸","flag: Samoa" }, /*4580 : Flags*/
{"🇽🇰","flag: Kosovo" }, /*4581 : Flags*/
{"🇾🇪","flag: Yemen" }, /*4582 : Flags*/
{"🇾🇹","flag: Mayotte" }, /*4583 : Flags*/
{"🇿🇦","flag: South Africa" }, /*4584 : Flags*/
{"🇿🇲","flag: Zambia" }, /*4585 : Flags*/
{"🇿🇼","flag: Zimbabwe" }, /*4586 : Flags*/
{"🏴󠁧󠁢󠁥󠁮󠁧󠁿","flag: England" }, /*4587 : Flags*/
{"🏴󠁧󠁢󠁳󠁣󠁴󠁿","flag: Scotland" }, /*4588 : Flags*/
{"🏴󠁧󠁢󠁷󠁬󠁳󠁿","flag: Wales" }, /*4589 : Flags*/
};
#endif
