/*
 * Treebird - Lightweight frontend for Pleroma
 * Copyright (C) 2022 Nekobit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ERROR_H
#define ERROR_H
#include <mastodont.h>
#include <stddef.h>
#include "session.h"
#include "path.h"

enum error_type
{
    E_ERROR,
    E_WARNING,
    E_NOTICE
};

char* construct_error(const char* error, enum error_type type, unsigned pad, size_t* size);
void content_not_found(FCGX_Request* req, struct session* ssn, mastodont_t* api, char* path);

#endif // ERROR_H
