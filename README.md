This repository contains Treebird FE files along with CI/CD configuration for
automatic testing and dockerization.

To read more about Treebird you should check out original
[README](treebird/README.md)

Cloned from fossil repository on [nekobit's server](https://fossil.nekobit.net/treebird/index)
